﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHDeviceClass
{
    public static class NHSecurity
    {
        static ASCIIEncoding enc = new ASCIIEncoding();

        // encryption constants
        public const byte LOWEST_VALID_CHARACTER = 0x20;
        public const byte HIGHEST_VALID_CHARACTER = 0x7A;
        public const byte NUMBER_OF_CHARACTERS_EXCLUDED_FROM_RANGE = 14;
        public const byte EXCLUDE_FROM_RANGE = 0xFF;
        public const byte VALID_CHARACTER_RANGE_SIZE = (((HIGHEST_VALID_CHARACTER - LOWEST_VALID_CHARACTER) + 1) - NUMBER_OF_CHARACTERS_EXCLUDED_FROM_RANGE);

        static public char[] CodecWorkingRangeToAsciiConversionTable = new char[VALID_CHARACTER_RANGE_SIZE]  //+ NUMBER_OF_CHARACTERS_EXCLUDED_FROM_RANGE - 1 ] =
            {
              '!',
              '#',
              '%',
              '(',
              ')',
              '*',
              '+',
              ',',
              '-',
              '.',
              '/',
              '0',
              '1',
              '2',
              '3',
              '4',
              '5',
              '6',
              '7',
              '8',
              '9',
              ':',
              ';',
              '=',
              '?',
              'A',
              'B',
              'C',
              'D',
              'E',
              'F',
              'G',
              'H',
              'I',
              'J',
              'K',
              'L',
              'M',
              'N',
              'O',
              'P',
              'Q',
              'R',
              'S',
              'T',
              'U',
              'V',
              'W',
              'X',
              'Y',
              'Z',    // 4 characters removed from here
              'a',
              'b',
              'c',
              'd',
              'e',
              'f',
              'g',
              'h',
              'i',
              'j',
              'k',
              'l',
              'm',
              'n',
              'o',
              'p',
              'q',
              'r',
              's',
              't',
              'u',
              'v',
              'w',
              'x',
              'y',
              'z'
            };

        static public byte[] AsciiToCodecWorkingRangeConversionTable = new byte[VALID_CHARACTER_RANGE_SIZE + NUMBER_OF_CHARACTERS_EXCLUDED_FROM_RANGE]
            {
              // table converts ascii character to range used in EncryptCharacter
              EXCLUDE_FROM_RANGE,   // space
              ' ' - ' ',            // !
              EXCLUDE_FROM_RANGE,   // " not permitted
              '!' - ' ',            // #
              EXCLUDE_FROM_RANGE,   // $
              '"' - ' ',            // %
              EXCLUDE_FROM_RANGE,   // & not permitted
              EXCLUDE_FROM_RANGE,   // ' not permitted
              '#' - ' ',            // (
              '$' - ' ',            // )
              '%' - ' ',            // *
              '&' - ' ',            // +
              '\'' - ' ',           // ,
              '(' - ' ',            // -
              ')' - ' ',            // .
              '*' - ' ',            // /
              '+' - ' ',            // 0
              ',' - ' ',            // 1
              '-' - ' ',            // 2
              '.' - ' ',            // 3
              '/' - ' ',            // 4
              '0' - ' ',            // 5
              '1' - ' ',            // 6
              '2' - ' ',            // 7
              '3' - ' ',            // 8
              '4' - ' ',            // 9
              '5' - ' ',            // :
              '6' - ' ',            // ;
              EXCLUDE_FROM_RANGE,   // < not permitted
              '7' - ' ',            // =
              EXCLUDE_FROM_RANGE,   // > not permitted
              '8' - ' ',            // ?
              EXCLUDE_FROM_RANGE,   // @
              '9' - ' ',            // A
              ':' - ' ',            // B
              ';' - ' ',            // C
              '<' - ' ',            // D
              '=' - ' ',            // E
              '>' - ' ',            // F
              '?' - ' ',            // G
              '@' - ' ',            // H
              'A' - ' ',            // I
              'B' - ' ',            // J
              'C' - ' ',            // K
              'D' - ' ',            // L
              'E' - ' ',            // M
              'F' - ' ',            // N
              'G' - ' ',            // O
              'H' - ' ',            // P
              'I' - ' ',            // Q
              'J' - ' ',            // R
              'K' - ' ',            // S
              'L' - ' ',            // T
              'M' - ' ',            // U
              'N' - ' ',            // V
              'O' - ' ',            // W
              'P' - ' ',            // X
              'Q' - ' ',            // Y
              'R' - ' ',            // Z
              EXCLUDE_FROM_RANGE,   // [ not permitted GSM
              EXCLUDE_FROM_RANGE,   // \ not permitted GSM 
              EXCLUDE_FROM_RANGE,   // ] not permitted GSM
              EXCLUDE_FROM_RANGE,   // ^ not permitted GSM
              EXCLUDE_FROM_RANGE,   // _        // 'V' etc different here on down from original
              EXCLUDE_FROM_RANGE,   // `
              'S' - ' ',            // a
              'T' - ' ',            // b
              'U' - ' ',            // c
              'V' - ' ',            // d
              'W' - ' ',            // e
              'X' - ' ',            // f
              'Y' - ' ',            // g
              'Z' - ' ',            // h
              '[' - ' ',           // i
              '\\' - ' ',            // j
              ']' - ' ',            // k
              '^' - ' ',            // l
              '_' - ' ',            // m
              '`' - ' ',            // n
              'a' - ' ',            // o
              'b' - ' ',            // p
              'c' - ' ',            // q
              'd' - ' ',            // r
              'e' - ' ',            // s
              'f' - ' ',            // t
              'g' - ' ',            // u
              'h' - ' ',            // v
              'i' - ' ',            // w
              'j' - ' ',            // x
              'k' - ' ',            // y
              'l' - ' ',            // z
              //EXCLUDE_FROM_RANGE,   // { not permitted GSM
              //EXCLUDE_FROM_RANGE,   // | not permitted GSM
              //EXCLUDE_FROM_RANGE,   // } not permitted GSM
              //EXCLUDE_FROM_RANGE,   // ~ not permitted GSM
            };
        //
        // EncryptMessage
        //
        // on entry the message is located in CommandMessage
        // and the Encrypted message is put in Buffer
        // and the encryption seed is passed in 'Seed'.
        //
        public static string EncryptMessage(char Seed, string CommandMessage)
        {
            int i;
            string Buffer = "";



            Buffer += Seed; // Make sure start of buffer has the unencrypted seed to use for decoding

            // encrypt message into temporary buffer
            for (i = 0; i < CommandMessage.Length; i++)
            {
                Seed = EncryptCharacter(Seed, CommandMessage[i]);
                Buffer += Seed;
            };

            return (Buffer);
        }


        public static char EncryptCharacter(char es, char ch)
        {
            ch = (char)AsciiToCodecWorkingRangeConversionTable[ch - LOWEST_VALID_CHARACTER];  // move character from range 0x20 - 0x7E to 0x00 - ( 0x5E - 5 )
            es = (char)AsciiToCodecWorkingRangeConversionTable[es - LOWEST_VALID_CHARACTER];  // move seed from range 0x20 - 0x7E to 0x00 - ( 0x5E - 5 )
            ch += es;
            ch %= (char)VALID_CHARACTER_RANGE_SIZE;
            return CodecWorkingRangeToAsciiConversionTable[(byte)ch];                         // move character from range 0x00 - ( 0x5E - 5 ) to permitted ascii range
        }

        //
        // DecryptMessage
        //
        // on entry the encoded message is located in Buffer
        // and the Decoded message is put in local DecodedMessage buffer
        // and the encryption seed is passed in 'Seed'.
        //
        public static string DecryptMessage(string Buffer)
        {
            int i;
            string DecodedMessage = "";
            // decrypt message into temporary buffer
            i = 0;
            char Seed;

            try
            {
                Seed = Buffer[i++];
                Seed = DecryptCharacter(Seed, Buffer[i++]);
                if (Seed.CompareTo('~') == 0)
                    return ("ERROR DecryptCharacter");

                DecodedMessage += Seed;
            }
            catch (Exception ex)
            {
                return ("ERROR DecryptMessage[" + ex.Message.ToString());
            }
            do
            {
                i--;
                Seed = Buffer[i++];
                Seed = DecryptCharacter(Seed, Buffer[i++]);
                if (Seed.CompareTo('~') == 0)
                    return ("ERROR DecryptCharacter");
                DecodedMessage += Seed;
            } while (i < Buffer.Length);

            return (DecodedMessage);
        }

        public static char DecryptCharacter(char ds, char ch)
        {
            try
            {
                ch = (char)AsciiToCodecWorkingRangeConversionTable[ch - LOWEST_VALID_CHARACTER];  // move character from range 0x20 - 0x7E to 0x00 - ( 0x5E - 5 )
                ds = (char)AsciiToCodecWorkingRangeConversionTable[ds - LOWEST_VALID_CHARACTER];  // move seed from range 0x20 - 0x7E to 0x00 - ( 0x5E - 5 )
                if (ds > ch)
                {
                    ch += (char)VALID_CHARACTER_RANGE_SIZE;
                }
                ch -= ds;
                return CodecWorkingRangeToAsciiConversionTable[ch];                         // move character from range 0x00 - ( 0x5E - 5 ) to permitted ascii range
            }
            catch (Exception)
            {
                return ('~');
            }
        }


        public static byte CheckSum(string MessageString)
        {
            byte ChecksumByte = 0;
            byte[] pString;
            byte ch;

            pString = enc.GetBytes(MessageString);

            for (int indx = 0; indx < MessageString.Length; indx++)
            {
                ch = pString[indx];
                ChecksumByte ^= ch;
            }
            return (ChecksumByte);

        }




    }
}
