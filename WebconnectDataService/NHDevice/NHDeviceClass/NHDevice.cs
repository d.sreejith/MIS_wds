﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Messaging;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Globalization;
using NHUtilsClass;
using System.Data.SqlTypes;
using System.Text;

namespace NHDeviceClass
{
    /// <summary>
    ///     NHDeviceClass is used to manage nighthawk devices (utility/non utility)
    ///     
    ///     The following functionalities are supported
    ///     send command to the device
    ///     receive command from the device
    ///     log activity for the device
    ///     WebConnect, NH API, Gateway, Microsoft Message Queue uses this 
    ///     
    ///     Version: 1.0.0.0
    ///     Created: 12/13/2010
    ///     Author : Moshiul Arefin     
    /// </summary>
    /// 

    public class NHDevice
    {
        public int DeviceID;
        public string ClientID;
        public string Phone;
        public int AccessCode;
        public string Esn;
        public string ItronMeterNumber;
        public int Network;
        public int DeviceType;
        public bool Active;
        public string CapCode;
        public string Frequency;
        public string Iccid;
        public string ErtID;
        public bool IsPrePay;
        public string Description;
        public string OPCodes;
        public string MsgPrefix;
        public bool TwoWay;
        public bool PrePay;
        public bool UtilityDevice;
        public string Protocol;
        public string Command;
        public DateTime CommandSent;
        public string CommandStatus;
        public string Status;
        public string CurrentLimit;
        public bool Armed;
        public bool CycleMode;
        public string GatewayMessage;
        public double PhaseACurrent;
        public double PhaseBCurrent;
        public int GpioStatus;
        public int XState;
        public int House_Voltage_Present;
        public string ErrCount;
        public double MeterReading;
        public double MeterReadingReceived;
        public double MeterReadingDelivered;
        public double MeterReadingDeferred;
        public DateTime TimeMeterRead;
        public double Voltage;
        public string Temperature;
        public string TouEnabled;
        public string HubAddress;

        public string ReportTrigger;
        public string ReportTriggerSent;
        public DateTime ReportTriggerTime;

        //added Kilowatt Trigger - Girija - 09/09/2014 - for Sensor devices
        public string KilowattTrigger;
        public string KilowattTriggerSent;
        public DateTime KilowattTriggerTime;

        public string VAHigh;
        public string VAHighSent;
        public DateTime VAHighReportTime;

        public string VALow;
        public string VALowSent;
        public DateTime VALowReportTime;

        // public int HouseVoltagePresent;

        public string PrePayProvider;
        public string PrePayProviderID;
        public string PrePayProviderLogin;
        public string PrePayProviderPassword;
        public string PrePayWebReferenceURL;
        public string PrePayWebReferenceMethod;

        public double Amps;// used to update insert meterreading //no value is read        
        public int ErrorCode;
        public string ErrorMsg;

        //MArefin 5/16/2012
        public string FirmwareVersion;
        public string HardwareVersion;
        public string ModemFirmwareVersion;

        //MArefin 6/4/2012
        public string FirmwareDownLoadStatus;
        public DateTime FirmwareDownLoadStart;
        public DateTime FirmwareDownLoadStop;

        //MArefin 6/7/2012
        public string ItronMeterIDSent;
        public string ErtIDSent;
        public string ErtBubbleUpSecondsSent;
        public string ErtTransRepeatsSent;
        public string ErtPowerSent;
        public string ErtReadEnableSent;
        public string rErt1Sent;
        public string rErt2Sent;
        public string rErt3Sent;
        public string rErt4Sent;

        public string ItronMeterID;
        public string ErtIDReceived;
        public string ErtBubbleUpSeconds;
        public string ErtTransRepeats;
        public string ErtPower;
        public string ErtReadEnable;
        public string rErt1;
        public string rErt2;
        public string rErt3;
        public string rErt4;

        public double Demand;
        public double kVar;

        //Added by Venkatesh 16/Apr/2015
        public string UserID;
        
        //MArefin 7/16/15       
        public double MinPF;

        /// <summary>
        /// Initializes a new instance of a NHDevice
        /// Version: 1.0.0.0
        /// Created: 12/13/2010
        /// Author : Moshiul Arefin 
        /// Modified: 8/19/2011
        /// Reason: To add Error Coding and Error message
        /// </summary>
        public NHDevice()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010199;
                this.ErrorMsg = err.Message;
            }
        }



        /// <summary>
        /// Creates a new NHDevice object with the specified paramerter
        /// Version: 1.0.0.0
        /// Created: 12/13/2010
        /// Author : Moshiul Arefin 
        /// Modified: 8/19/2011
        /// Reason: To add Error Coding and Error message
        /// </summary>
        /// <param name="esn"></param>
        public NHDevice(string esn)
        {
            try
            {
                this.Esn = esn;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Esn", SqlDbType.VarChar).Value = esn;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            this.DeviceID = (int)myReader["DeviceID"];
                            this.ClientID = myReader["ClientID"].ToString();
                            this.Phone = myReader["Phone"].ToString();
                            this.AccessCode = myReader["AccessCode"] == DBNull.Value ? 0 : (int)myReader["AccessCode"];
                            this.Esn = myReader["Esn"].ToString();
                            this.ItronMeterNumber = myReader["ItronMeterNumber"] == DBNull.Value ? "" : myReader["ItronMeterNumber"].ToString();
                            this.Network = (int)myReader["Network"];
                            this.DeviceType = (int)myReader["DeviceType"];
                            this.Active = (bool)myReader["Active"];
                            this.CapCode = myReader["CapCode"] == DBNull.Value ? "" : myReader["CapCode"].ToString();
                            this.Frequency = myReader["Frequency"] == DBNull.Value ? "" : myReader["Frequency"].ToString();
                            this.Iccid = myReader["Iccid"] == DBNull.Value ? "" : myReader["Iccid"].ToString();
                            this.ErtID = myReader["ErtID"] == DBNull.Value ? "" : myReader["ErtID"].ToString();
                            this.IsPrePay = myReader["IsPrePay"] == DBNull.Value ? false : (bool)myReader["IsPrePay"];  //is this device prepay                            
                            this.Description = myReader["Description"] == DBNull.Value ? "" : myReader["Description"].ToString();
                            this.OPCodes = myReader["Commands"] == DBNull.Value ? "" : myReader["Commands"].ToString();
                            this.MsgPrefix = myReader["MsgPrefix"] == DBNull.Value ? "" : myReader["MsgPrefix"].ToString();
                            this.TwoWay = myReader["TwoWay"] == DBNull.Value ? false : (bool)myReader["TwoWay"];
                            this.PrePay = myReader["PrePay"] == DBNull.Value ? false : (bool)myReader["PrePay"]; //is this a prepay devietype
                            this.UtilityDevice = myReader["UtilityDevice"] == DBNull.Value ? false : (bool)myReader["UtilityDevice"];
                            this.Protocol = myReader["Protocol"] == DBNull.Value ? "" : myReader["Protocol"].ToString();
                            this.Command = myReader["Command"] == DBNull.Value ? "" : myReader["Command"].ToString();
                            this.CommandSent = myReader["CommandSent"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["CommandSent"];
                            this.CommandStatus = myReader["CommandStatus"] == DBNull.Value ? "" : myReader["CommandStatus"].ToString();
                            this.Status = myReader["Status"] == DBNull.Value ? "" : myReader["Status"].ToString();
                            this.CurrentLimit = myReader["CurrentLimit"] == DBNull.Value ? "0" : myReader["CurrentLimit"].ToString();
                            this.Armed = myReader["Armed"] == DBNull.Value ? false : (bool)myReader["Armed"];
                            this.CycleMode = myReader["CycleMode"] == DBNull.Value ? false : (bool)myReader["CycleMode"];
                            this.GatewayMessage = myReader["GatewayMessage"] == DBNull.Value ? "" : myReader["GatewayMessage"].ToString();
                            this.PhaseACurrent = myReader["PhaseACurrent"] == DBNull.Value ? 0 : (double)myReader["PhaseACurrent"];
                            this.PhaseBCurrent = myReader["PhaseBCurrent"] == DBNull.Value ? 0 : (double)myReader["PhaseBCurrent"];
                            this.GpioStatus = myReader["GpioStatus"] == DBNull.Value ? 0 : (int)myReader["GpioStatus"];
                            this.XState = myReader["xState"] == DBNull.Value ? 1 : (int)myReader["xState"];
                            this.ErrCount = myReader["ErrCount"] == DBNull.Value ? "" : myReader["ErrCount"].ToString();
                            this.MeterReading = myReader["MeterReading"] == DBNull.Value ? -1 : (double)myReader["MeterReading"];
                            this.MeterReadingDeferred = myReader["MeterReadingDeferred"] == DBNull.Value ? -1 : (double)myReader["MeterReadingDeferred"];
                            this.TimeMeterRead = myReader["TimeMeterRead"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["TimeMeterRead"];
                            this.Voltage = myReader["voltage"] == DBNull.Value ? -1 : (double)myReader["voltage"];
                            this.TouEnabled = myReader["TouEnabled"] == DBNull.Value ? "0000" : myReader["TouEnabled"].ToString();
                            this.HubAddress = myReader["HubAddress"] == DBNull.Value ? "" : myReader["HubAddress"].ToString();
                            this.ReportTrigger = myReader["ReportTrigger"] == DBNull.Value ? "" : myReader["ReportTrigger"].ToString();
                            this.ReportTriggerSent = myReader["ReportTriggerSent"] == DBNull.Value ? "" : myReader["ReportTriggerSent"].ToString();
                            this.ReportTriggerTime = myReader["ReportTriggerTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ReportTriggerTime"];

                            //added by Girija to get Kilowatt trigger for devices
                            this.KilowattTrigger = myReader["KilowattTrigger"] == DBNull.Value ? "" : myReader["KilowattTrigger"].ToString();
                            this.KilowattTriggerSent = myReader["KilowattTriggerSent"] == DBNull.Value ? "" : myReader["KilowattTriggerSent"].ToString();
                            this.KilowattTriggerTime = myReader["KilowattTriggerTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["KilowattTriggerTime"];

                            this.VAHigh = myReader["VAHigh"] == DBNull.Value ? "0" : myReader["VAHigh"].ToString();
                            this.VAHighSent = myReader["VAHighSent"] == DBNull.Value ? "0" : myReader["VAHighSent"].ToString();
                            this.VAHighReportTime = myReader["VAHighReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["VAHighReportTime"];
                            this.VALow = myReader["VALow"] == DBNull.Value ? "0" : myReader["VALow"].ToString();
                            this.VALowSent = myReader["VALowSent"] == DBNull.Value ? "0" : myReader["VALowSent"].ToString();
                            this.VALowReportTime = myReader["VALowReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["VALowReportTime"];

                            this.House_Voltage_Present = myReader["House_Voltage_Present"] == DBNull.Value ? 0 : (int)myReader["House_Voltage_Present"];

                            this.PrePayProvider = myReader["ProviderName"] == DBNull.Value ? "" : myReader["ProviderName"].ToString();
                            this.PrePayProviderID = myReader["ProviderID"] == DBNull.Value ? "" : myReader["ProviderID"].ToString();
                            this.PrePayProviderLogin = myReader["Login"] == DBNull.Value ? "" : myReader["Login"].ToString();
                            this.PrePayProviderPassword = myReader["Password"] == DBNull.Value ? "" : myReader["Password"].ToString();
                            this.PrePayWebReferenceURL = myReader["WebReferenceURL"] == DBNull.Value ? "" : myReader["WebReferenceURL"].ToString();
                            this.PrePayWebReferenceMethod = myReader["WebReferenceMethod"] == DBNull.Value ? "" : myReader["WebReferenceMethod"].ToString();

                            this.FirmwareVersion = myReader["FirmwareVersion"] == DBNull.Value ? "" : myReader["FirmwareVersion"].ToString().Trim();
                            this.FirmwareDownLoadStatus = myReader["FirmwareDownLoadStatus"] == DBNull.Value ? "" : myReader["FirmwareDownLoadStatus"].ToString();
                            this.FirmwareDownLoadStart = myReader["FirmwareDownLoadStart"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["FirmwareDownLoadStart"];
                            this.FirmwareDownLoadStop = myReader["FirmwareDownLoadStop"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["FirmwareDownLoadStop"];

                            this.ItronMeterIDSent = myReader["ItronMeterIDSent"] == DBNull.Value ? null : myReader["ItronMeterIDSent"].ToString();
                            this.ErtIDSent = myReader["ErtIDSent"] == DBNull.Value ? null : myReader["ErtIDSent"].ToString();
                            this.ErtBubbleUpSecondsSent = myReader["ErtBubbleUpSecondsSent"] == DBNull.Value ? null : myReader["ErtBubbleUpSecondsSent"].ToString();
                            this.ErtTransRepeatsSent = myReader["ErtTransRepeatsSent"] == DBNull.Value ? null : myReader["ErtTransRepeatsSent"].ToString();
                            this.ErtPowerSent = myReader["ErtPowerSent"] == DBNull.Value ? null : myReader["ErtPowerSent"].ToString();
                            this.ErtReadEnableSent = myReader["ErtReadEnableSent"] == DBNull.Value ? null : myReader["ErtReadEnableSent"].ToString();
                            this.rErt1Sent = myReader["rErt1Sent"] == DBNull.Value ? null : myReader["rErt1Sent"].ToString();
                            this.rErt2Sent = myReader["rErt2Sent"] == DBNull.Value ? null : myReader["rErt2Sent"].ToString();
                            this.rErt3Sent = myReader["rErt3Sent"] == DBNull.Value ? null : myReader["rErt3Sent"].ToString();
                            this.rErt4Sent = myReader["rErt4Sent"] == DBNull.Value ? null : myReader["rErt4Sent"].ToString();

                            this.ItronMeterID = myReader["ItronMeterID"] == DBNull.Value ? null : myReader["ItronMeterID"].ToString();
                            this.ErtIDReceived = myReader["ErtIDReceived"] == DBNull.Value ? null : myReader["ErtIDReceived"].ToString();
                            this.ErtBubbleUpSeconds = myReader["ErtBubbleUpSecondsSent"] == DBNull.Value ? null : myReader["ErtBubbleUpSeconds"].ToString();
                            this.ErtTransRepeats = myReader["ErtTransRepeats"] == DBNull.Value ? null : myReader["ErtTransRepeats"].ToString();
                            this.ErtPower = myReader["ErtPower"] == DBNull.Value ? null : myReader["ErtPower"].ToString();
                            this.ErtReadEnable = myReader["ErtReadEnable"] == DBNull.Value ? null : myReader["ErtReadEnable"].ToString();
                            this.rErt1 = myReader["rErt1"] == DBNull.Value ? null : myReader["rErt1"].ToString();
                            this.rErt2 = myReader["rErt2"] == DBNull.Value ? null : myReader["rErt2"].ToString();
                            this.rErt3 = myReader["rErt3"] == DBNull.Value ? null : myReader["rErt3"].ToString();
                            this.rErt4 = myReader["rErt4"] == DBNull.Value ? null : myReader["rErt4"].ToString();

                            this.Demand = myReader["Demand"] == DBNull.Value ? -1 : (double)myReader["Demand"];
                            this.kVar = myReader["kVar"] == DBNull.Value ? -1 : (double)myReader["kVar"];

                            //MARefin 7/16/15
                            this.MinPF = myReader["MinPF"] == DBNull.Value ? -1 : (double)myReader["MinPF"];
                        }

                        this.ErrorCode = 0;
                        this.ErrorMsg = "";
                    }
                    else
                    {
                        this.ErrorCode = 04010201;
                        this.ErrorMsg = "Unable to find any device with Serial Number[" + esn + "]";
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010299;
                this.ErrorMsg = err.Message;
            }
        }



        /// <summary>
        /// Creates a new NHDevice object with the specified paramerter
        /// Version: 1.0.0.0
        /// Created: 12/13/2010
        /// Author : Moshiul Arefin 
        /// Modified: 8/17/2011
        /// Reason: To add Error Coding and Error message
        /// </summary>
        /// <param name="deviceID"></param>
        public NHDevice(int deviceID)
        {
            try
            {
                //this.DeviceID = deviceID;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            this.DeviceID = (int)myReader["DeviceID"];
                            this.ClientID = myReader["ClientID"].ToString();
                            this.Phone = myReader["Phone"].ToString();
                            this.AccessCode = myReader["AccessCode"] == DBNull.Value ? 0 : (int)myReader["AccessCode"];
                            this.Esn = myReader["Esn"].ToString();
                            this.ItronMeterNumber = myReader["ItronMeterNumber"] == DBNull.Value ? "" : myReader["ItronMeterNumber"].ToString();
                            this.Network = (int)myReader["Network"];
                            this.DeviceType = (int)myReader["DeviceType"];
                            this.Active = (bool)myReader["Active"];
                            this.CapCode = myReader["CapCode"] == DBNull.Value ? "" : myReader["CapCode"].ToString();
                            this.Frequency = myReader["Frequency"] == DBNull.Value ? "" : myReader["Frequency"].ToString();
                            this.Iccid = myReader["Iccid"] == DBNull.Value ? "" : myReader["Iccid"].ToString();
                            this.ErtID = myReader["ErtID"] == DBNull.Value ? "" : myReader["ErtID"].ToString();
                            this.IsPrePay = myReader["IsPrePay"] == DBNull.Value ? false : (bool)myReader["IsPrePay"];  //is this device prepay
                            this.Description = myReader["Description"] == DBNull.Value ? "" : myReader["Description"].ToString();
                            this.OPCodes = myReader["Commands"] == DBNull.Value ? "" : myReader["Commands"].ToString();
                            this.MsgPrefix = myReader["MsgPrefix"] == DBNull.Value ? "" : myReader["MsgPrefix"].ToString();
                            this.TwoWay = myReader["TwoWay"] == DBNull.Value ? false : (bool)myReader["TwoWay"];
                            this.PrePay = myReader["PrePay"] == DBNull.Value ? false : (bool)myReader["PrePay"]; //is this a prepay devietype
                            this.UtilityDevice = myReader["UtilityDevice"] == DBNull.Value ? false : (bool)myReader["UtilityDevice"];
                            this.Protocol = myReader["Protocol"] == DBNull.Value ? "" : myReader["Protocol"].ToString();
                            this.Command = myReader["Command"] == DBNull.Value ? "" : myReader["Command"].ToString();
                            this.CommandSent = myReader["CommandSent"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["CommandSent"];
                            this.CommandStatus = myReader["CommandStatus"] == DBNull.Value ? "" : myReader["CommandStatus"].ToString();
                            this.Status = myReader["Status"] == DBNull.Value ? "" : myReader["Status"].ToString();
                            this.CurrentLimit = myReader["CurrentLimit"] == DBNull.Value ? "0" : myReader["CurrentLimit"].ToString();
                            this.Armed = myReader["Armed"] == DBNull.Value ? false : (bool)myReader["Armed"];
                            this.CycleMode = myReader["CycleMode"] == DBNull.Value ? false : (bool)myReader["CycleMode"];
                            this.GatewayMessage = myReader["GatewayMessage"] == DBNull.Value ? "" : myReader["GatewayMessage"].ToString();
                            this.PhaseACurrent = myReader["PhaseACurrent"] == DBNull.Value ? 0 : (double)myReader["PhaseACurrent"];
                            this.PhaseBCurrent = myReader["PhaseBCurrent"] == DBNull.Value ? 0 : (double)myReader["PhaseBCurrent"];
                            this.GpioStatus = myReader["GpioStatus"] == DBNull.Value ? 0 : (int)myReader["GpioStatus"];
                            this.XState = myReader["xState"] == DBNull.Value ? 1 : (int)myReader["xState"];
                            this.ErrCount = myReader["ErrCount"] == DBNull.Value ? "" : myReader["ErrCount"].ToString();
                            this.MeterReading = myReader["MeterReading"] == DBNull.Value ? -1 : (double)myReader["MeterReading"];
                            this.MeterReadingDeferred = myReader["MeterReadingDeferred"] == DBNull.Value ? -1 : (double)myReader["MeterReadingDeferred"];
                            this.TimeMeterRead = myReader["TimeMeterRead"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["TimeMeterRead"];
                            this.Voltage = myReader["voltage"] == DBNull.Value ? -1 : (double)myReader["voltage"];
                            this.TouEnabled = myReader["TouEnabled"] == DBNull.Value ? "0000" : myReader["TouEnabled"].ToString();
                            this.HubAddress = myReader["HubAddress"] == DBNull.Value ? "" : myReader["HubAddress"].ToString();

                            this.ReportTrigger = myReader["ReportTrigger"] == DBNull.Value ? "" : myReader["ReportTrigger"].ToString();
                            this.ReportTriggerSent = myReader["ReportTriggerSent"] == DBNull.Value ? "" : myReader["ReportTriggerSent"].ToString();
                            this.ReportTriggerTime = myReader["ReportTriggerTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ReportTriggerTime"];

                            //added by Girija to get Kilowatt trigger for devices
                            this.KilowattTrigger = myReader["KilowattTrigger"] == DBNull.Value ? "" : myReader["KilowattTrigger"].ToString();
                            this.KilowattTriggerSent = myReader["KilowattTriggerSent"] == DBNull.Value ? "" : myReader["KilowattTriggerSent"].ToString();
                            this.KilowattTriggerTime = myReader["KilowattTriggerTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["KilowattTriggerTime"];

                            this.VAHigh = myReader["VAHigh"] == DBNull.Value ? "0" : myReader["VAHigh"].ToString();
                            this.VAHighSent = myReader["VAHighSent"] == DBNull.Value ? "0" : myReader["VAHighSent"].ToString();
                            this.VAHighReportTime = myReader["VAHighReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["VAHighReportTime"];
                            this.VALow = myReader["VALow"] == DBNull.Value ? "0" : myReader["VALow"].ToString();
                            this.VALowSent = myReader["VALowSent"] == DBNull.Value ? "0" : myReader["VALowSent"].ToString();
                            this.VALowReportTime = myReader["VALowReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["VALowReportTime"];

                            this.House_Voltage_Present = myReader["House_Voltage_Present"] == DBNull.Value ? 0 : (int)myReader["House_Voltage_Present"];

                            this.PrePayProvider = myReader["ProviderName"] == DBNull.Value ? "" : myReader["ProviderName"].ToString();
                            this.PrePayProviderID = myReader["ProviderID"] == DBNull.Value ? "" : myReader["ProviderID"].ToString();
                            this.PrePayProviderLogin = myReader["Login"] == DBNull.Value ? "" : myReader["Login"].ToString();
                            this.PrePayProviderPassword = myReader["Password"] == DBNull.Value ? "" : myReader["Password"].ToString();
                            this.PrePayWebReferenceURL = myReader["WebReferenceURL"] == DBNull.Value ? "" : myReader["WebReferenceURL"].ToString();
                            this.PrePayWebReferenceMethod = myReader["WebReferenceMethod"] == DBNull.Value ? "" : myReader["WebReferenceMethod"].ToString();

                            this.FirmwareVersion = myReader["FirmwareVersion"] == DBNull.Value ? "" : myReader["FirmwareVersion"].ToString().Trim();
                            this.FirmwareDownLoadStatus = myReader["FirmwareDownLoadStatus"] == DBNull.Value ? "" : myReader["FirmwareDownLoadStatus"].ToString();
                            this.FirmwareDownLoadStart = myReader["FirmwareDownLoadStart"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["FirmwareDownLoadStart"];
                            this.FirmwareDownLoadStop = myReader["FirmwareDownLoadStop"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["FirmwareDownLoadStop"];

                            this.ItronMeterIDSent = myReader["ItronMeterIDSent"] == DBNull.Value ? null : myReader["ItronMeterIDSent"].ToString();
                            this.ErtIDSent = myReader["ErtIDSent"] == DBNull.Value ? null : myReader["ErtIDSent"].ToString();
                            this.ErtBubbleUpSecondsSent = myReader["ErtBubbleUpSecondsSent"] == DBNull.Value ? null : myReader["ErtBubbleUpSecondsSent"].ToString();
                            this.ErtTransRepeatsSent = myReader["ErtTransRepeatsSent"] == DBNull.Value ? null : myReader["ErtTransRepeatsSent"].ToString();
                            this.ErtPowerSent = myReader["ErtPowerSent"] == DBNull.Value ? null : myReader["ErtPowerSent"].ToString();
                            this.ErtReadEnableSent = myReader["ErtReadEnableSent"] == DBNull.Value ? null : myReader["ErtReadEnableSent"].ToString();
                            this.rErt1Sent = myReader["rErt1Sent"] == DBNull.Value ? null : myReader["rErt1Sent"].ToString();
                            this.rErt2Sent = myReader["rErt2Sent"] == DBNull.Value ? null : myReader["rErt2Sent"].ToString();
                            this.rErt3Sent = myReader["rErt3Sent"] == DBNull.Value ? null : myReader["rErt3Sent"].ToString();
                            this.rErt4Sent = myReader["rErt4Sent"] == DBNull.Value ? null : myReader["rErt4Sent"].ToString();

                            this.ItronMeterID = myReader["ItronMeterID"] == DBNull.Value ? null : myReader["ItronMeterID"].ToString();
                            this.ErtIDReceived = myReader["ErtIDReceived"] == DBNull.Value ? null : myReader["ErtIDReceived"].ToString();
                            this.ErtBubbleUpSeconds = myReader["ErtBubbleUpSecondsSent"] == DBNull.Value ? null : myReader["ErtBubbleUpSeconds"].ToString();
                            this.ErtTransRepeats = myReader["ErtTransRepeats"] == DBNull.Value ? null : myReader["ErtTransRepeats"].ToString();
                            this.ErtPower = myReader["ErtPower"] == DBNull.Value ? null : myReader["ErtPower"].ToString();
                            this.ErtReadEnable = myReader["ErtReadEnable"] == DBNull.Value ? null : myReader["ErtReadEnable"].ToString();
                            this.rErt1 = myReader["rErt1"] == DBNull.Value ? null : myReader["rErt1"].ToString();
                            this.rErt2 = myReader["rErt2"] == DBNull.Value ? null : myReader["rErt2"].ToString();
                            this.rErt3 = myReader["rErt3"] == DBNull.Value ? null : myReader["rErt3"].ToString();
                            this.rErt4 = myReader["rErt4"] == DBNull.Value ? null : myReader["rErt4"].ToString();

                            this.Demand = myReader["Demand"] == DBNull.Value ? -1 : (double)myReader["Demand"];
                            this.kVar = myReader["kVar"] == DBNull.Value ? -1 : (double)myReader["kVar"];

                            //MARefin 7/16/15
                            this.MinPF = myReader["MinPF"] == DBNull.Value ? -1 : (double)myReader["MinPF"];

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";

                        }
                    }
                    else
                    {
                        this.ErrorCode = 04010301;
                        this.ErrorMsg = "Unable to find any device with DeviceID[" + deviceID + "]";
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010399;
                this.ErrorMsg = err.Message;
            }
        }




        /// <summary>
        /// Creates a new NHDevice object with the specified paramerter
        /// Version: 1.0.0.0
        /// Created: 05/17/2011
        /// Author : Moshiul Arefin 
        /// Modified: 6/30/2011
        /// Reason: We'll have a single numerex production gateway, during the coversion process the gateway needs to be able to support
        ///         the new and the old structure of the database tables
        /// 
        /// Modified: 8/19/2011
        /// Reason: To change the error coding 
        /// </summary>
        /// <param name="parmName"></param>
        /// <param name="parmValue"></param>
        public NHDevice(string parmName, string parmValue)
        {
            this.DeviceID = 0; //return DeviceID = 0 if no record is found in the database
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    // SqlCommand command = new SqlCommand("sp_NH_Device_GetDeviceBy" + parmName, conn);
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    //parmName Supported
                    /*
                    @DeviceID int = null,
	                @Esn varchar(50) = null,
	                @MeterNumber varchar(50) = null,
	                @Phone varchar(50) = null,
	                @SIM varchar(20) = null,
	                @SenderID varchar(50) = null
                    */
                    parmName = parmName.Replace("SIMM", "SIM");

                    command.Parameters.Add("@" + parmName, SqlDbType.VarChar).Value = parmValue;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();
                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            this.DeviceID = (int)myReader["DeviceID"];
                            this.ClientID = myReader["ClientID"].ToString();
                            this.Phone = myReader["Phone"].ToString();
                            this.AccessCode = myReader["AccessCode"] == DBNull.Value ? 0 : (int)myReader["AccessCode"];
                            this.Esn = myReader["Esn"].ToString();
                            this.ItronMeterNumber = myReader["ItronMeterNumber"] == DBNull.Value ? "" : myReader["ItronMeterNumber"].ToString();
                            this.Network = (int)myReader["Network"];
                            this.DeviceType = (int)myReader["DeviceType"];
                            this.Active = (bool)myReader["Active"];
                            this.CapCode = myReader["CapCode"] == DBNull.Value ? "" : myReader["CapCode"].ToString();
                            this.Frequency = myReader["Frequency"] == DBNull.Value ? "" : myReader["Frequency"].ToString();
                            this.Iccid = myReader["Iccid"] == DBNull.Value ? "" : myReader["Iccid"].ToString();
                            this.ErtID = myReader["ErtID"] == DBNull.Value ? "" : myReader["ErtID"].ToString();
                            this.IsPrePay = myReader["IsPrePay"] == DBNull.Value ? false : (bool)myReader["IsPrePay"];  //is this device prepay
                            this.Description = myReader["Description"] == DBNull.Value ? "" : myReader["Description"].ToString();
                            this.OPCodes = myReader["Commands"] == DBNull.Value ? "" : myReader["Commands"].ToString();
                            this.MsgPrefix = myReader["MsgPrefix"] == DBNull.Value ? "" : myReader["MsgPrefix"].ToString();
                            this.TwoWay = myReader["TwoWay"] == DBNull.Value ? false : (bool)myReader["TwoWay"];
                            this.PrePay = myReader["PrePay"] == DBNull.Value ? false : (bool)myReader["PrePay"]; //is this a prepay devietype
                            this.UtilityDevice = myReader["UtilityDevice"] == DBNull.Value ? false : (bool)myReader["UtilityDevice"];
                            this.Protocol = myReader["Protocol"] == DBNull.Value ? "" : myReader["Protocol"].ToString();
                            this.Command = myReader["Command"] == DBNull.Value ? "" : myReader["Command"].ToString();
                            this.CommandSent = myReader["CommandSent"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["CommandSent"];
                            this.CommandStatus = myReader["CommandStatus"] == DBNull.Value ? "" : myReader["CommandStatus"].ToString();
                            this.Status = myReader["Status"] == DBNull.Value ? "" : myReader["Status"].ToString();
                            this.CurrentLimit = myReader["CurrentLimit"] == DBNull.Value ? "0" : myReader["CurrentLimit"].ToString();
                            this.Armed = myReader["Armed"] == DBNull.Value ? false : (bool)myReader["Armed"];
                            this.CycleMode = myReader["CycleMode"] == DBNull.Value ? false : (bool)myReader["CycleMode"];
                            this.GatewayMessage = myReader["GatewayMessage"] == DBNull.Value ? "" : myReader["GatewayMessage"].ToString();
                            this.PhaseACurrent = myReader["PhaseACurrent"] == DBNull.Value ? 0 : (double)myReader["PhaseACurrent"];
                            this.PhaseBCurrent = myReader["PhaseBCurrent"] == DBNull.Value ? 0 : (double)myReader["PhaseBCurrent"];
                            this.GpioStatus = myReader["GpioStatus"] == DBNull.Value ? 0 : (int)myReader["GpioStatus"];
                            this.XState = myReader["xState"] == DBNull.Value ? 1 : (int)myReader["xState"];
                            this.ErrCount = myReader["ErrCount"] == DBNull.Value ? "" : myReader["ErrCount"].ToString();
                            this.MeterReading = myReader["MeterReading"] == DBNull.Value ? -1 : (double)myReader["MeterReading"];
                            this.MeterReadingDeferred = myReader["MeterReadingDeferred"] == DBNull.Value ? -1 : (double)myReader["MeterReadingDeferred"];
                            this.TimeMeterRead = myReader["TimeMeterRead"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["TimeMeterRead"];
                            this.Voltage = myReader["voltage"] == DBNull.Value ? -1 : (double)myReader["voltage"];
                            this.TouEnabled = myReader["TouEnabled"] == DBNull.Value ? "0000" : myReader["TouEnabled"].ToString();
                            this.HubAddress = myReader["HubAddress"] == DBNull.Value ? "" : myReader["HubAddress"].ToString();

                            this.ReportTrigger = myReader["ReportTrigger"] == DBNull.Value ? "" : myReader["ReportTrigger"].ToString();
                            this.ReportTriggerSent = myReader["ReportTriggerSent"] == DBNull.Value ? "" : myReader["ReportTriggerSent"].ToString();
                            this.ReportTriggerTime = myReader["ReportTriggerTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ReportTriggerTime"];

                            //added by Girija to get Kilowatt trigger for devices
                            this.KilowattTrigger = myReader["KilowattTrigger"] == DBNull.Value ? "" : myReader["KilowattTrigger"].ToString();
                            this.KilowattTriggerSent = myReader["KilowattTriggerSent"] == DBNull.Value ? "" : myReader["KilowattTriggerSent"].ToString();
                            this.KilowattTriggerTime = myReader["KilowattTriggerTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["KilowattTriggerTime"];

                            this.VAHigh = myReader["VAHigh"] == DBNull.Value ? "0" : myReader["VAHigh"].ToString();
                            this.VAHighSent = myReader["VAHighSent"] == DBNull.Value ? "0" : myReader["VAHighSent"].ToString();
                            this.VAHighReportTime = myReader["VAHighReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["VAHighReportTime"];
                            this.VALow = myReader["VALow"] == DBNull.Value ? "0" : myReader["VALow"].ToString();
                            this.VALowSent = myReader["VALowSent"] == DBNull.Value ? "0" : myReader["VALowSent"].ToString();
                            this.VALowReportTime = myReader["VALowReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["VALowReportTime"];

                            this.House_Voltage_Present = myReader["House_Voltage_Present"] == DBNull.Value ? 0 : (int)myReader["House_Voltage_Present"];

                            this.PrePayProvider = myReader["ProviderName"] == DBNull.Value ? "" : myReader["ProviderName"].ToString();
                            this.PrePayProviderID = myReader["ProviderID"] == DBNull.Value ? "" : myReader["ProviderID"].ToString();
                            this.PrePayProviderLogin = myReader["Login"] == DBNull.Value ? "" : myReader["Login"].ToString();
                            this.PrePayProviderPassword = myReader["Password"] == DBNull.Value ? "" : myReader["Password"].ToString();
                            this.PrePayWebReferenceURL = myReader["WebReferenceURL"] == DBNull.Value ? "" : myReader["WebReferenceURL"].ToString();
                            this.PrePayWebReferenceMethod = myReader["WebReferenceMethod"] == DBNull.Value ? "" : myReader["WebReferenceMethod"].ToString();

                            this.FirmwareVersion = myReader["FirmwareVersion"] == DBNull.Value ? "" : myReader["FirmwareVersion"].ToString().Trim();
                            this.FirmwareDownLoadStatus = myReader["FirmwareDownLoadStatus"] == DBNull.Value ? "" : myReader["FirmwareDownLoadStatus"].ToString();
                            this.FirmwareDownLoadStart = myReader["FirmwareDownLoadStart"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["FirmwareDownLoadStart"];
                            this.FirmwareDownLoadStop = myReader["FirmwareDownLoadStop"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["FirmwareDownLoadStop"];

                            this.ItronMeterIDSent = myReader["ItronMeterIDSent"] == DBNull.Value ? null : myReader["ItronMeterIDSent"].ToString();
                            this.ErtIDSent = myReader["ErtIDSent"] == DBNull.Value ? null : myReader["ErtIDSent"].ToString();
                            this.ErtBubbleUpSecondsSent = myReader["ErtBubbleUpSecondsSent"] == DBNull.Value ? null : myReader["ErtBubbleUpSecondsSent"].ToString();
                            this.ErtTransRepeatsSent = myReader["ErtTransRepeatsSent"] == DBNull.Value ? null : myReader["ErtTransRepeatsSent"].ToString();
                            this.ErtPowerSent = myReader["ErtPowerSent"] == DBNull.Value ? null : myReader["ErtPowerSent"].ToString();
                            this.ErtReadEnableSent = myReader["ErtReadEnableSent"] == DBNull.Value ? null : myReader["ErtReadEnableSent"].ToString();
                            this.rErt1Sent = myReader["rErt1Sent"] == DBNull.Value ? null : myReader["rErt1Sent"].ToString();
                            this.rErt2Sent = myReader["rErt2Sent"] == DBNull.Value ? null : myReader["rErt2Sent"].ToString();
                            this.rErt3Sent = myReader["rErt3Sent"] == DBNull.Value ? null : myReader["rErt3Sent"].ToString();
                            this.rErt4Sent = myReader["rErt4Sent"] == DBNull.Value ? null : myReader["rErt4Sent"].ToString();

                            this.ItronMeterID = myReader["ItronMeterID"] == DBNull.Value ? null : myReader["ItronMeterID"].ToString();
                            this.ErtIDReceived = myReader["ErtIDReceived"] == DBNull.Value ? null : myReader["ErtIDReceived"].ToString();
                            this.ErtBubbleUpSeconds = myReader["ErtBubbleUpSecondsSent"] == DBNull.Value ? null : myReader["ErtBubbleUpSeconds"].ToString();
                            this.ErtTransRepeats = myReader["ErtTransRepeats"] == DBNull.Value ? null : myReader["ErtTransRepeats"].ToString();
                            this.ErtPower = myReader["ErtPower"] == DBNull.Value ? null : myReader["ErtPower"].ToString();
                            this.ErtReadEnable = myReader["ErtReadEnable"] == DBNull.Value ? null : myReader["ErtReadEnable"].ToString();
                            this.rErt1 = myReader["rErt1"] == DBNull.Value ? null : myReader["rErt1"].ToString();
                            this.rErt2 = myReader["rErt2"] == DBNull.Value ? null : myReader["rErt2"].ToString();
                            this.rErt3 = myReader["rErt3"] == DBNull.Value ? null : myReader["rErt3"].ToString();
                            this.rErt4 = myReader["rErt4"] == DBNull.Value ? null : myReader["rErt4"].ToString();

                            this.Demand = myReader["Demand"] == DBNull.Value ? -1 : (double)myReader["Demand"];
                            this.kVar = myReader["kVar"] == DBNull.Value ? -1 : (double)myReader["kVar"];

                            //MARefin 7/16/15
                            this.MinPF = myReader["MinPF"] == DBNull.Value ? -1 : (double)myReader["MinPF"];

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        this.ErrorCode = 04010401;
                        this.ErrorMsg = "Unable to find any device with " + parmName + "[" + parmValue + "]";
                    }
                    conn.Close();
                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 04010499;
                this.ErrorMsg = err.Message;
            }

        }










        /// <summary>
        /// Creates a new NHDevice object with the specified paramerter
        /// Version: 1.0.0.0
        /// Created: 05/17/2011
        /// Author : Moshiul Arefin
        /// to lookup paging devices
        /// </summary>
        /// <param name="AccessCode"></param>
        /// <param name="CapCode"></param>
        public NHDevice(int AccessCode, string CapCode)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@AccessCode", SqlDbType.Int).Value = AccessCode;
                    command.Parameters.Add("@SIM", SqlDbType.VarChar).Value = CapCode;

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();
                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            this.DeviceID = (int)myReader["DeviceID"];
                            this.ClientID = myReader["ClientID"].ToString();
                            this.Phone = myReader["Phone"].ToString();
                            this.AccessCode = myReader["AccessCode"] == DBNull.Value ? 0 : (int)myReader["AccessCode"];
                            this.Esn = myReader["Esn"].ToString();
                            this.ItronMeterNumber = myReader["ItronMeterNumber"] == DBNull.Value ? "" : myReader["ItronMeterNumber"].ToString();
                            this.Network = (int)myReader["Network"];
                            this.DeviceType = (int)myReader["DeviceType"];
                            this.Active = (bool)myReader["Active"];
                            this.CapCode = myReader["CapCode"] == DBNull.Value ? "" : myReader["CapCode"].ToString();
                            this.Frequency = myReader["Frequency"] == DBNull.Value ? "" : myReader["Frequency"].ToString();
                            this.Iccid = myReader["Iccid"] == DBNull.Value ? "" : myReader["Iccid"].ToString();
                            this.ErtID = myReader["ErtID"] == DBNull.Value ? "" : myReader["ErtID"].ToString();
                            this.IsPrePay = myReader["IsPrePay"] == DBNull.Value ? false : (bool)myReader["IsPrePay"];  //is this device prepay
                            this.Description = myReader["Description"] == DBNull.Value ? "" : myReader["Description"].ToString();
                            this.OPCodes = myReader["Commands"] == DBNull.Value ? "" : myReader["Commands"].ToString();
                            this.MsgPrefix = myReader["MsgPrefix"] == DBNull.Value ? "" : myReader["MsgPrefix"].ToString();
                            this.TwoWay = myReader["TwoWay"] == DBNull.Value ? false : (bool)myReader["TwoWay"];
                            this.PrePay = myReader["PrePay"] == DBNull.Value ? false : (bool)myReader["PrePay"]; //is this a prepay devietype
                            this.UtilityDevice = myReader["UtilityDevice"] == DBNull.Value ? false : (bool)myReader["UtilityDevice"];
                            this.Protocol = myReader["Protocol"] == DBNull.Value ? "" : myReader["Protocol"].ToString();
                            this.Command = myReader["Command"] == DBNull.Value ? "" : myReader["Command"].ToString();
                            this.CommandSent = myReader["CommandSent"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["CommandSent"];
                            this.CommandStatus = myReader["CommandStatus"] == DBNull.Value ? "" : myReader["CommandStatus"].ToString();
                            this.Status = myReader["Status"] == DBNull.Value ? "" : myReader["Status"].ToString();
                            this.CurrentLimit = myReader["CurrentLimit"] == DBNull.Value ? "0" : myReader["CurrentLimit"].ToString();
                            this.Armed = myReader["Armed"] == DBNull.Value ? false : (bool)myReader["Armed"];
                            this.CycleMode = myReader["CycleMode"] == DBNull.Value ? false : (bool)myReader["CycleMode"];
                            this.GatewayMessage = myReader["GatewayMessage"] == DBNull.Value ? "" : myReader["GatewayMessage"].ToString();
                            this.PhaseACurrent = myReader["PhaseACurrent"] == DBNull.Value ? 0 : (double)myReader["PhaseACurrent"];
                            this.PhaseBCurrent = myReader["PhaseBCurrent"] == DBNull.Value ? 0 : (double)myReader["PhaseBCurrent"];
                            this.GpioStatus = myReader["GpioStatus"] == DBNull.Value ? 0 : (int)myReader["GpioStatus"];
                            this.XState = myReader["xState"] == DBNull.Value ? 1 : (int)myReader["xState"];
                            this.ErrCount = myReader["ErrCount"] == DBNull.Value ? "" : myReader["ErrCount"].ToString();
                            this.MeterReading = myReader["MeterReading"] == DBNull.Value ? -1 : (double)myReader["MeterReading"];
                            this.MeterReadingDeferred = myReader["MeterReadingDeferred"] == DBNull.Value ? -1 : (double)myReader["MeterReadingDeferred"];
                            this.TimeMeterRead = myReader["TimeMeterRead"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["TimeMeterRead"];
                            this.Voltage = myReader["voltage"] == DBNull.Value ? -1 : (double)myReader["voltage"];
                            this.TouEnabled = myReader["TouEnabled"] == DBNull.Value ? "0000" : myReader["TouEnabled"].ToString();
                            this.HubAddress = myReader["HubAddress"] == DBNull.Value ? "" : myReader["HubAddress"].ToString();

                            this.ReportTrigger = myReader["ReportTrigger"] == DBNull.Value ? "" : myReader["ReportTrigger"].ToString();
                            this.ReportTriggerSent = myReader["ReportTriggerSent"] == DBNull.Value ? "" : myReader["ReportTriggerSent"].ToString();
                            this.ReportTriggerTime = myReader["ReportTriggerTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ReportTriggerTime"];

                            //added by Girija to get Kilowatt trigger for devices
                            this.KilowattTrigger = myReader["KilowattTrigger"] == DBNull.Value ? "" : myReader["KilowattTrigger"].ToString();
                            this.KilowattTriggerSent = myReader["KilowattTriggerSent"] == DBNull.Value ? "" : myReader["KilowattTriggerSent"].ToString();
                            this.KilowattTriggerTime = myReader["KilowattTriggerTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["KilowattTriggerTime"];

                            this.VAHigh = myReader["VAHigh"] == DBNull.Value ? "0" : myReader["VAHigh"].ToString();
                            this.VAHighSent = myReader["VAHighSent"] == DBNull.Value ? "0" : myReader["VAHighSent"].ToString();
                            this.VAHighReportTime = myReader["VAHighReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["VAHighReportTime"];
                            this.VALow = myReader["VALow"] == DBNull.Value ? "0" : myReader["VALow"].ToString();
                            this.VALowSent = myReader["VALowSent"] == DBNull.Value ? "0" : myReader["VALowSent"].ToString();
                            this.VALowReportTime = myReader["VALowReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["VALowReportTime"];

                            this.House_Voltage_Present = myReader["House_Voltage_Present"] == DBNull.Value ? 0 : (int)myReader["House_Voltage_Present"];

                            this.PrePayProvider = myReader["ProviderName"] == DBNull.Value ? "" : myReader["ProviderName"].ToString();
                            this.PrePayProviderID = myReader["ProviderID"] == DBNull.Value ? "" : myReader["ProviderID"].ToString();
                            this.PrePayProviderLogin = myReader["Login"] == DBNull.Value ? "" : myReader["Login"].ToString();
                            this.PrePayProviderPassword = myReader["Password"] == DBNull.Value ? "" : myReader["Password"].ToString();
                            this.PrePayWebReferenceURL = myReader["WebReferenceURL"] == DBNull.Value ? "" : myReader["WebReferenceURL"].ToString();
                            this.PrePayWebReferenceMethod = myReader["WebReferenceMethod"] == DBNull.Value ? "" : myReader["WebReferenceMethod"].ToString();

                            this.FirmwareVersion = myReader["FirmwareVersion"] == DBNull.Value ? "" : myReader["FirmwareVersion"].ToString().Trim();
                            this.FirmwareDownLoadStatus = myReader["FirmwareDownLoadStatus"] == DBNull.Value ? "" : myReader["FirmwareDownLoadStatus"].ToString();
                            this.FirmwareDownLoadStart = myReader["FirmwareDownLoadStart"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["FirmwareDownLoadStart"];
                            this.FirmwareDownLoadStop = myReader["FirmwareDownLoadStop"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["FirmwareDownLoadStop"];

                            this.ItronMeterIDSent = myReader["ItronMeterIDSent"] == DBNull.Value ? null : myReader["ItronMeterIDSent"].ToString();
                            this.ErtIDSent = myReader["ErtIDSent"] == DBNull.Value ? null : myReader["ErtIDSent"].ToString();
                            this.ErtBubbleUpSecondsSent = myReader["ErtBubbleUpSecondsSent"] == DBNull.Value ? null : myReader["ErtBubbleUpSecondsSent"].ToString();
                            this.ErtTransRepeatsSent = myReader["ErtTransRepeatsSent"] == DBNull.Value ? null : myReader["ErtTransRepeatsSent"].ToString();
                            this.ErtPowerSent = myReader["ErtPowerSent"] == DBNull.Value ? null : myReader["ErtPowerSent"].ToString();
                            this.ErtReadEnableSent = myReader["ErtReadEnableSent"] == DBNull.Value ? null : myReader["ErtReadEnableSent"].ToString();
                            this.rErt1Sent = myReader["rErt1Sent"] == DBNull.Value ? null : myReader["rErt1Sent"].ToString();
                            this.rErt2Sent = myReader["rErt2Sent"] == DBNull.Value ? null : myReader["rErt2Sent"].ToString();
                            this.rErt3Sent = myReader["rErt3Sent"] == DBNull.Value ? null : myReader["rErt3Sent"].ToString();
                            this.rErt4Sent = myReader["rErt4Sent"] == DBNull.Value ? null : myReader["rErt4Sent"].ToString();

                            this.ItronMeterID = myReader["ItronMeterID"] == DBNull.Value ? null : myReader["ItronMeterID"].ToString();
                            this.ErtIDReceived = myReader["ErtIDReceived"] == DBNull.Value ? null : myReader["ErtIDReceived"].ToString();
                            this.ErtBubbleUpSeconds = myReader["ErtBubbleUpSecondsSent"] == DBNull.Value ? null : myReader["ErtBubbleUpSeconds"].ToString();
                            this.ErtTransRepeats = myReader["ErtTransRepeats"] == DBNull.Value ? null : myReader["ErtTransRepeats"].ToString();
                            this.ErtPower = myReader["ErtPower"] == DBNull.Value ? null : myReader["ErtPower"].ToString();
                            this.ErtReadEnable = myReader["ErtReadEnable"] == DBNull.Value ? null : myReader["ErtReadEnable"].ToString();
                            this.rErt1 = myReader["rErt1"] == DBNull.Value ? null : myReader["rErt1"].ToString();
                            this.rErt2 = myReader["rErt2"] == DBNull.Value ? null : myReader["rErt2"].ToString();
                            this.rErt3 = myReader["rErt3"] == DBNull.Value ? null : myReader["rErt3"].ToString();
                            this.rErt4 = myReader["rErt4"] == DBNull.Value ? null : myReader["rErt4"].ToString();

                            this.Demand = myReader["Demand"] == DBNull.Value ? -1 : (double)myReader["Demand"];
                            this.kVar = myReader["kVar"] == DBNull.Value ? -1 : (double)myReader["kVar"];

                            //MARefin 7/16/15
                            this.MinPF = myReader["MinPF"] == DBNull.Value ? -1 : (double)myReader["MinPF"];

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        this.ErrorCode = 01;
                        this.ErrorMsg = "Unable to find any device with AccessCode[" + AccessCode + "] and CapCode[" + CapCode + "]";
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }

        }





        /// <summary>
        /// To Get the Device id from the ERTId passed
        /// Version: 1.0.0.0
        /// Created: 4/30/2015
        /// Author : Sivakumar Kandasamy
        /// </summary>
        public DataTable DeviceDetailsForERTID(string ERTID)
        {

            DataTable dt = new DataTable("Device");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDeviceIDFromERTID", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ErtID", SqlDbType.VarChar).Value = ERTID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        this.ErrorCode = 04021401;
                        this.ErrorMsg = "Stored Procedure [sp_NH_Device_GetDeviceIDFromERTID] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 04021499;
                this.ErrorMsg = err.Message;
            }

            return dt;
        }

        /// <summary>
        /// To Get the Hub id's from the clientid passed
        /// Version: 1.0.0.0
        /// Created: 07/13/2015
        /// Author : Nanda
        /// </summary>
        public DataTable GetHUBIdByClientID(string ClientID)
        {

            DataTable dt = new DataTable("HUBID");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_GetHUBIdByClientId", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        this.ErrorCode = 04021401;
                        this.ErrorMsg = "Stored Procedure [sp_NH_Device_GetHUBIdByClientId] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 04021499;
                this.ErrorMsg = err.Message;
            }

            return dt;
        }

        public static NHDevice GetDetails(int deviceID, NHError Error)
        {
            NHDevice device = new NHDevice();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDetails", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            device.DeviceID = (int)myReader["DeviceID"];
                            device.ClientID = myReader["ClientID"].ToString();
                            device.UserID = myReader["UserID"].ToString();
                            device.Esn = myReader["ESN"].ToString();
                            device.ItronMeterNumber = myReader["ItronMeterNumber"].ToString();
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04010399;
                Error.ErrorMsg = err.Message;
            }
            return device;
        }

        /// <summary>
        /// Creates a new NHDevice object with the specified paramerter
        /// Version: 1.0.0.0
        /// Created: 12/17/2015
        /// Author : Venkatesh
        /// </summary>
        /// <param name="deviceID"></param>
        public static NHDevice GetDeviceReadings(int deviceID)
        {
            NHDevice device = new NHDevice();
            try
            {               
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDeviceReadingByDeviceID", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            device.DeviceID = (int)myReader["DeviceID"];
                            device.TwoWay = myReader["TwoWay"] == DBNull.Value ? false : (bool)myReader["TwoWay"];  
                            device.Esn = myReader["Esn"].ToString();
                            device.ItronMeterNumber = myReader["ItronMeterNumber"] == DBNull.Value ? "" : myReader["ItronMeterNumber"].ToString();
                            device.TimeMeterRead = myReader["TimeMeterRead"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["TimeMeterRead"];                                                          
                            device.MeterReading = myReader["MeterReading"] == DBNull.Value ? -1 : (double)myReader["MeterReading"];                                                               
                            device.Demand = myReader["Demand"] == DBNull.Value ? -1 : (double)myReader["Demand"];
                            device.kVar = myReader["kVar"] == DBNull.Value ? -1 : (double)myReader["kVar"];                  

                            device.ErrorCode = 0;
                            device.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        device.ErrorCode = 04010301;
                        device.ErrorMsg = "Unable to find any device with DeviceID[" + deviceID + "]";
                    }
                    conn.Close();
                }
            }
            catch (Exception err)
            {
                device.ErrorCode = 04010399;
                device.ErrorMsg = err.Message;
            }
            return device;
        }
    }




    public class NHDeviceNH800
    {
        public int DeviceID;
        public string Esn;
        public string Command;
        public DateTime CommandSent;
        public string CommandStatus;
        public string Status;
        public string GatewayMessage;
        public DateTime ReportTime;
        public DateTime StatusReportTime;
        public string Output0;
        public string Output1;
        public string Output2;
        public string Output3;
        public string Output4;
        public string Output5;
        public string Output6;
        public string Output7;
        public string Output8;
        public DateTime RebootTime0;
        public DateTime RebootTime1;
        public DateTime RebootTime2;
        public DateTime RebootTime3;
        public DateTime RebootTime4;
        public DateTime RebootTime5;
        public DateTime RebootTime6;
        public DateTime RebootTime7;
        public DateTime RebootTime8;
        public DateTime LockedTime;
        public string LockedBy;
        public int ErrorCode;
        public string ErrorMsg;

        public NHDeviceNH800()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }

        }


        public NHDeviceNH800(int DeviceID)
        {
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDeviceNH800", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = DeviceID;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            this.DeviceID = Convert.ToInt32(myReader["DeviceID"]);
                            this.Esn = myReader["Esn"].ToString();
                            this.Command = myReader["Command"] == DBNull.Value ? "" : myReader["Command"].ToString();
                            this.CommandSent = myReader["CommandSent"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["CommandSent"];
                            this.CommandStatus = myReader["CommandStatus"] == DBNull.Value ? "" : myReader["CommandStatus"].ToString();
                            this.Status = myReader["Status"] == DBNull.Value ? "" : myReader["Status"].ToString();
                            this.GatewayMessage = myReader["GatewayMessage"] == DBNull.Value ? "" : myReader["GatewayMessage"].ToString();
                            this.ReportTime = myReader["ReportTime"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["ReportTime"];
                            this.StatusReportTime = myReader["StatusReportTime"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["StatusReportTime"];
                            this.Output0 = myReader["Output0"] == DBNull.Value ? "" : myReader["Output0"].ToString();
                            this.Output1 = myReader["Output1"] == DBNull.Value ? "" : myReader["Output1"].ToString();
                            this.Output2 = myReader["Output2"] == DBNull.Value ? "" : myReader["Output2"].ToString();
                            this.Output3 = myReader["Output3"] == DBNull.Value ? "" : myReader["Output3"].ToString();
                            this.Output4 = myReader["Output4"] == DBNull.Value ? "" : myReader["Output4"].ToString();
                            this.Output5 = myReader["Output5"] == DBNull.Value ? "" : myReader["Output5"].ToString();
                            this.Output6 = myReader["Output6"] == DBNull.Value ? "" : myReader["Output6"].ToString();
                            this.Output7 = myReader["Output7"] == DBNull.Value ? "" : myReader["Output7"].ToString();
                            this.Output8 = myReader["Output8"] == DBNull.Value ? "" : myReader["Output8"].ToString();
                            this.RebootTime0 = myReader["RebootTime0"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime0"];
                            this.RebootTime1 = myReader["RebootTime1"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime1"];
                            this.RebootTime2 = myReader["RebootTime2"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime2"];
                            this.RebootTime3 = myReader["RebootTime3"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime3"];
                            this.RebootTime4 = myReader["RebootTime4"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime4"];
                            this.RebootTime5 = myReader["RebootTime5"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime5"];
                            this.RebootTime6 = myReader["RebootTime6"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime6"];
                            this.RebootTime7 = myReader["RebootTime7"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime7"];
                            this.RebootTime8 = myReader["RebootTime8"] == DBNull.Value ? System.DateTime.MinValue : (DateTime)myReader["RebootTime8"];
                            this.LockedTime = myReader["LockedTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["LockedTime"];
                            this.LockedBy = myReader["LockedBy"] == DBNull.Value ? "" : myReader["LockedBy"].ToString();

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";

                        }
                    }
                    else
                    {
                        this.ErrorCode = 91;
                        this.ErrorMsg = "Stored Procedure[sp_NH_Device_GetDeviceNH800]  was unable to find any device with DeviceID[" + DeviceID + "]";
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }


        }


    }

    /// <summary>
    ///     NHErtReading object for holding data retrieved from the ErtReader enabled meters
    ///       
    ///     Version: 1.0.0.0
    ///     Created: 1/14/2014
    ///     Author : Moshiul Arefin   
    /// </summary>
    public class NHErtReading
    {
        public int ErtHubID;
        public ulong ErtID;
        public double ErtReading;
        public byte ErtTamperInversion;
        public byte ErtType;
        public byte ErtTamperRemove;
        public short ErtRSSI;
        public DateTime ErtTimeStamp;

        public int ErrorCode;
        public string ErrorMsg;

        public NHErtReading()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 04050199;
                this.ErrorMsg = err.Message;
            }
        }

        /*
        public NHErtReading(int DeviceID)
        {
            try
            {
                this.DeviceID = DeviceID;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetErtReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ErtHubID", SqlDbType.Int).Value = ErtHubID;
                    command.Parameters.Add("@Row", SqlDbType.Int).Value = 1;
                    command.Parameters.Add("@Count", SqlDbType.Int).Value = 1;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {                           
                            this.ErtID = myReader["ErtID"] == DBNull.Value ? 0 : (ulong)myReader["ErtID"];
                            this.ErtReading = myReader["ErtReading"] == DBNull.Value ? 0 : (double)myReader["ErtReading"];
                            this.ErtTamperInversion = myReader["ErtTamperInversion"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtTamperInversion"];
                            this.ErtType = myReader["ErtType"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtType"];
                            this.ErtTamperRemove = myReader["ErtTamperRemove"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtTamperRemove"];
                            this.ErtRSSI = myReader["ErtRSSI"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtRSSI"];
                            this.ErtTimeStamp = myReader["ErtTimeStamp"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ErtTimeStamp"];

                        }

                        this.ErrorCode = 0;
                        this.ErrorMsg = "";
                    }
                    else
                    {
                        this.ErrorCode = 04050201;
                        this.ErrorMsg = "Unable to find any device with DeviceID[" + DeviceID + "]"; ;
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 04050299;
                this.ErrorMsg = err.Message;
            }
        }
        */

        /// <summary>
        ///     To get the recent three hub id from ert readings table for the ertid passed
        ///   
        ///       
        ///     Version: 1.0.0.0
        ///     Created: 7/4/2015
        ///     Author : Sivakumar Kandasamy  
        /// </summary>
        public List<int> NHErtReadingPreviousHubID(ulong ErtID)
        {
            List<int> lstHubId = new List<int>();

            try
            {
                this.ErtID = ErtID;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetHubIDFromErtReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ErtID", SqlDbType.BigInt).Value = ErtID;
                    //command.Parameters.Add("@Row", SqlDbType.Int).Value = 1;
                    //command.Parameters.Add("@Count", SqlDbType.Int).Value = 1;

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            ErtHubID = myReader["ErtHubID"] == DBNull.Value ? 0 : (int)myReader["ErtHubID"];
                            lstHubId.Add(ErtHubID);
                            //this.ErtReading = myReader["ErtReading"] == DBNull.Value ? 0 : (double)myReader["ErtReading"];
                            //this.ErtTamperInversion = myReader["ErtTamperInversion"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtTamperInversion"];
                            //this.ErtType = myReader["ErtType"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtType"];
                            //this.ErtTamperRemove = myReader["ErtTamperRemove"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtTamperRemove"];
                            //this.ErtRSSI = myReader["ErtRSSI"] == DBNull.Value ? (short)0 : (short)myReader["ErtRSSI"];
                            //this.ErtTimeStamp = myReader["ErtTimeStamp"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ErtTimeStamp"];

                        }

                        this.ErrorCode = 0;
                        this.ErrorMsg = "";
                    }
                    else
                    {
                        this.ErrorCode = 04050302;
                        this.ErrorMsg = "Unable to find any device with ErtID[" + ErtID + "]"; ;
                    }
                    conn.Close();

                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 04050399;
                this.ErrorMsg = err.Message;
            }
            return lstHubId;
        }

        public NHErtReading(ulong ErtID)
        {
            try
            {
                this.ErtID = ErtID;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetErtReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ErtID", SqlDbType.BigInt).Value = ErtID;
                    command.Parameters.Add("@Row", SqlDbType.Int).Value = 1;
                    command.Parameters.Add("@Count", SqlDbType.Int).Value = 1;

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            this.ErtHubID = myReader["ErtHubID"] == DBNull.Value ? 0 : (int)myReader["ErtHubID"];
                            this.ErtReading = myReader["ErtReading"] == DBNull.Value ? 0 : (double)myReader["ErtReading"];
                            this.ErtTamperInversion = myReader["ErtTamperInversion"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtTamperInversion"];
                            this.ErtType = myReader["ErtType"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtType"];
                            this.ErtTamperRemove = myReader["ErtTamperRemove"] == DBNull.Value ? (byte)0 : (byte)myReader["ErtTamperRemove"];
                            this.ErtRSSI = myReader["ErtRSSI"] == DBNull.Value ? (short)0 : (short)myReader["ErtRSSI"];
                            this.ErtTimeStamp = myReader["ErtTimeStamp"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ErtTimeStamp"];

                        }

                        this.ErrorCode = 0;
                        this.ErrorMsg = "";
                    }
                    else
                    {
                        this.ErrorCode = 04050301;
                        this.ErrorMsg = "Unable to find any device with ErtID[" + ErtID + "]"; ;
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 04050399;
                this.ErrorMsg = err.Message;
            }
        }


    }



    public class NHDeviceActivity
    {
        //Guid
        public string Guid;
        public int DeviceID;
        public int? iTranID = null;
        public string GatewayMessage;
        public string Protocol;
        public int? NetworkID = null;

        public DateTime ReportTime;
        //public int MsgID;
        public string Command;
        public string CommandStatus;
        public DateTime? StatusReportTime = null;
        public string UserID;

        public double? phaseACurrent = null;
        public double? phaseBCurrent = null;
        public double? Voltage = null;
        public string temperature;
        public int? gpioStatus = null;
        public int? xState = null;
        public string errCount;
        public double? MeterReading = null;
        public double? MeterReadingDeferred = null;
        public DateTime? TimeMeterRead = null;


        public int ErrorCode;
        public string ErrorMsg;

        public NHDeviceActivity()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }

        }

        public NHDeviceActivity(int deviceID)
        {

            this.DeviceID = 0; //return DeviceID = 0 if no record is found in the database
            //DataSet ds = new DataSet("DeviceData");
            DataTable dt = new DataTable("DeviceData");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDeviceActivity", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    //adapter.Fill(ds, "DeviceHistory");
                    adapter.Fill(dt);

                    //if there is any rows in the useractivity table return the data
                    //else the deviceID is zero
                    //if (ds.Tables["DeviceHistory"].Rows.Count > 0)
                    if (dt.Rows.Count > 0)
                    {
                        //DataRow row0 = ds.Tables["DeviceHistory"].Rows[0];
                        DataRow row0 = dt.Rows[0];

                        this.DeviceID = (int)row0["DeviceID"];
                        this.ReportTime = (DateTime)row0["ReportTime"];
                        this.Command = row0["Command"].ToString();
                        this.CommandStatus = row0["CommandStatus"].ToString();
                        this.StatusReportTime = (DateTime)row0["StatusReportTime"];
                        this.UserID = row0["UserID"].ToString();
                    }


                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 4010;
                //this.ErrorMsg = "\n\rError on Stored procedure sp_GetDeviceIDBySenderID : " + err.Message;
                this.ErrorMsg = "\n\rError on Stored procedure sp_NH_Device_GetDeviceActivity: " + err.Message;
            }




        }

    }

    public class NHDevice3Phase
    {
        public string Guid;
        public int DeviceID;
        public double? kWhDelivered = null;
        public double? kWhReceived = null;
        public double? kVARhLagging = null;
        public double? kVARhLeading = null;
        public double? kVARhNetDelivered = null;
        public double? kVARhNetReceived = null;
        public double? kVAhDelivered = null;
        public double? kVAhReceived = null;
        //public double? PowerFactor = null;
        public double? kWDelivered = null;
        public double? kWReceived = null;
        public double? kVARLagging = null;
        public double? kVARLeading = null;
        public double? kVADelivered = null;
        public double? kVAReceived = null;
        public double? MinPowerFactor = null;
        public double? PhaseAvoltage = null;
        public double? phaseACurrent = null;
        public double? PhaseBvoltage = null;
        public double? PhaseBvoltageAngle = null;
        public double? PhaseBcurrent = null;
        public double? PhaseBcurrentAngle = null;
        public double? PhaseCvoltage = null;
        public double? PhaseCvoltageAngle = null;
        public double? PhaseCcurrent = null;
        public double? PhaseCcurrentAngle = null;
        public int ErrorCode;
        public string ErrorMsg;

        public NHDevice3Phase()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }

        }

        public static DataTable Get3PhaseDeviceStatus(int DeviceID)
        {
            DataTable dt = new DataTable("DeviceStatus");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_Get3PhaseDeviceStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = DeviceID;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);                    
                }
            }
            catch (Exception err)
            {
               
            }

            return dt;

        }


        //will create this at a later phase
        /*
         * 
        public NHDeviceHistory(int deviceID)
        {

            this.DeviceID = 0; //return DeviceID = 0 if no record is found in the database
            //DataSet ds = new DataSet("DeviceData");
            DataTable dt = new DataTable("DeviceData");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDeviceActivity", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    //adapter.Fill(ds, "DeviceHistory");
                    adapter.Fill(dt);

                    //if there is any rows in the useractivity table return the data
                    //else the deviceID is zero
                    //if (ds.Tables["DeviceHistory"].Rows.Count > 0)
                    if (dt.Rows.Count > 0)
                    {
                        //DataRow row0 = ds.Tables["DeviceHistory"].Rows[0];
                        DataRow row0 = dt.Rows[0];

                        this.DeviceID = (int)row0["DeviceID"];
                        this.ReportTime = (DateTime)row0["ReportTime"];
                        this.Command = row0["Command"].ToString();
                        this.CommandStatus = row0["CommandStatus"].ToString();
                        this.StatusReportTime = (DateTime)row0["StatusReportTime"];
                        this.UserID = row0["UserID"].ToString();
                    }


                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 4010;
                //this.ErrorMsg = "\n\rError on Stored procedure sp_GetDeviceIDBySenderID : " + err.Message;
                this.ErrorMsg = "\n\rError on Stored procedure sp_NH_Device_GetDeviceActivity: " + err.Message;
            }     


        }*/
    }


    public class NHDeviceManagement
    {




        public struct OutBoundMessage
        {
            public Guid MessageID;
            public String PhoneNumber;
            public String ICCID;
            public String SecurityCode;
            public int MessageNumber;
            public String PayLoad;
            public String UnEncrypted;
            public String Status;
            public int DeviceID;
            public int Network;
            public DateTime Created;
            public String UserName;
            public String ClientID;
            public String Protocol;
            public int iTranID;
            public int DB_Enum; // 0=test,1=Demo.2=Prod,3=Dev - SQL Databases
        }

        public struct OutBoundWaterMessage
        {
            public string ClientID;
            public DateTime Created;
            public int DB_Enum;
            public int DeviceID;
            public string ICCID;
            public int iTranID;
            public Guid MessageID;
            public int MessageNumber;
            public int Network;
            public string Opcode;
            public string OpcodeSuffix;
            public string PayLoad;
            public string PhoneNumber;
            public string Protocol;
            public string SecurityCode;
            public string Status;
            public string UnEncrypted;
            public string UserName;
        }
        static CultureInfo ci = new CultureInfo("en-us");
        const string DevQueueName = @"FormatName:Direct=TCP:192.168.1.172\private$\DevTxNumerexMSMQ";
        const string TxNumerexQueueName = @"FormatName:Direct=TCP:192.168.1.172\private$\TxNumerexMSMQ";
        const string TxVerizonQueueName = @"FormatName:Direct=TCP:192.168.1.172\private$\TxVerizonMSMQ";
        const string TxVerizonTestQueueName = @"FormatName:Direct=TCP:192.168.1.172\private$\TxVerizonTestMSMQ";
        const string TxVerizonSensorQueueName = @"FormatName:Direct=TCP:192.168.1.175\private$\TxVerizonSensorMSMQ";
        //const string TxVerizonSensorQueueName = @"FormatName:Direct=TCP:74.205.92.175\private$\TxVerizonSensorMSMQ";

        const string TxWCTPQueueNameDEMO = @".\private$\TxWCTPMSMQDEMO";
        const string TxWCTPQueueNamePROD = @"FormatName:Direct=TCP:192.168.1.170\private$\TxWCTPMSMQPROD";
        const string TxSNPPQueueNameDEMO = @".\private$\TxSNPPMSMQDEMO";
        const string TxSNPPQueueNamePROD = @".\private$\TxSNPPMSMQPROD";
        const string TxSaipanQueueName = @"FormatName:Direct=TCP:192.168.1.175\private$\TxSaipanMSMQ";
        const string TxTrinidadQueueName = @"FormatName:Direct=TCP:192.168.1.172\private$\TxTrinidadMSMQ";
        const string TxAttQueueName = @"FormatName:Direct=TCP:192.168.1.172\private$\TxAttMSMQ";
        const string TxBahamasQueueName = @"FormatName:Direct=TCP:192.168.1.175\private$\TxBahamasMSMQ";

        const string _TxEventServiceMQ = @"FormatName:Direct=TCP:74.205.92.174\private$\TxEventServiceMQ";
        const string TxEventServiceMQ = @".\private$\EventServiceWaterCommandMSMQ";

        //MArefin 10/17/2012 txTrilliantmsmqdemo
        //MARefin 10/17/2014 rafactored trilliantMQ              //FormatName:Direct=TCP:192.168.1.173
        public static string TxTrilliantQueueName = @".\private$\" + NHSiteGlobal.TrilliantMSMQ; //txTrilliantmsmqdemo";

        public int ErrorCode;
        public string ErrorMsg;

        /*
         * this will be changed to support non numerex devices i.e oneway paging devices
         * 
        public static int SendCommand(int deviceID, string userID, string opCode, string opCodeSuffix, string utcString)
        {
            int msgID = 0;
            NHDevice device = new NHDevice(deviceID);

            Dictionary<string, string> payLoad = new Dictionary<string, string>();

            payLoad.Add("Cl", String.Format("{0}2", device.AccessCode)); //connect
            payLoad.Add("Op", String.Format("{0}1", device.AccessCode)); //disconnect
            payLoad.Add("AR", String.Format("{0}0", device.AccessCode)); //arm
            payLoad.Add("Cy", String.Format("{0}3", device.AccessCode)); //30/30 mode
            payLoad.Add("PO", "Check Status"); //
            payLoad.Add("SM", "Send Message"); //
            payLoad.Add("SC", "Current Limit"); //
            payLoad.Add("CM", "Current Mode"); //
            payLoad.Add("SK", "Set Credit"); //
            payLoad.Add("SU", "Set Url"); //
            payLoad.Add("SO", "Set Output"); //                
            payLoad.Add("EC", "Set Ert Config"); // 
            payLoad.Add("CE", "Get Ert Config"); // 
            payLoad.Add("RS", "Reset Meter"); // 
            payLoad.Add("KT", "Kilowatt Trigger"); // 
            payLoad.Add("RT", "Report Trigger"); // 
            payLoad.Add("TO", "Time Of Use SMS"); // 
            payLoad.Add("TI", "Time Of Use IP"); // 
            payLoad.Add("TY", "Enable Time Of Use"); // 
            payLoad.Add("TN", "Disable Time Of Use"); // 
            payLoad.Add("MQ", "Get Meter Factor"); // 
            payLoad.Add("MF", "Set Meter Factor"); // 
            payLoad.Add("FU", "Firmware Update"); // 
            payLoad.Add("GV", "Get Version"); // 
            payLoad.Add("SR", "Software Reset"); // 

            string strproc = "sp_NH_Device_SendMsgToOutBound";
            if (device.TwoWay)
            {
                strproc = "sp_NH_SendMsgToOutBound2Way";
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand(strproc, conn);
                command.CommandType = CommandType.StoredProcedure;

                // devicetype 3, 5, 8(NH800)
                command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                command.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = device.Phone;
                command.Parameters.Add("@SecurityCode", SqlDbType.VarChar).Value = device.Esn;
                command.Parameters.Add("@PayLoad", SqlDbType.VarChar).Value = payLoad[opCode];
                command.Parameters.Add("@Status", SqlDbType.VarChar).Value = "SENDING";
                command.Parameters.Add("@MessageNumber", SqlDbType.Int).Value = 1;
                command.Parameters.Add("@Network", SqlDbType.Int).Value = device.Network;
                command.Parameters.Add("@Created", SqlDbType.DateTime).Value = utcString;
                command.Parameters.Add("@UserID", SqlDbType.VarChar).Value = userID;
                command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = device.ClientID;
                command.Parameters.Add("@Protocol", SqlDbType.VarChar).Value = device.Protocol;

                //device 1, 2, 9, 10, 11, 12, 13, 14, 15
                if (device.TwoWay)
                {
                    command.Parameters.Add("@OpCode", SqlDbType.VarChar).Value = opCode;
                    command.Parameters.Add("@MsgPrefix", SqlDbType.VarChar).Value = device.MsgPrefix;
                    command.Parameters.Add("@OpCodeSuffix", SqlDbType.VarChar).Value = opCodeSuffix;
                }


                SqlParameter MsgIDParameter = new SqlParameter("@MsgID", SqlDbType.Int);
                MsgIDParameter.Direction = ParameterDirection.Output;
                command.Parameters.Add(MsgIDParameter);

                conn.Open();
                int rows = command.ExecuteNonQuery();
                msgID = (int)MsgIDParameter.Value;
                conn.Close();
            }

            return msgID;

        }

        */

        /// <summary>
        /// AllDeviceActivityByDate will be used to retrieve activity for specified date range for all devices in a single database
        /// Version: 1.0.0.0
        /// Created: 11/29/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="TzOffset"></param>
        /// <param name="StartDateTime"></param>
        /// <param name="EndDateTime"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static DataTable AllDeviceActivityByDate(int TzOffset, DateTime EndDateTime, DateTime StartDateTime, NHError Error)
        {

            DataTable dt = new DataTable("DeviceActivity");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_AllDeviceActivityByDate", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@TzOffset", SqlDbType.Int).Value = TzOffset;
                    command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = StartDateTime;
                    command.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDateTime;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04020901;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Device_AllDeviceActivityByDate] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04020999;
                Error.ErrorMsg = err.Message;
            }

            return dt;


        }


        /// <summary>
        /// DeviceActivityByDate will return activity for a particular device from start to the end date
        /// </summary>
        /// <param name="DeviceID"></param>
        /// <param name="TzOffset"></param>
        /// <param name="EndDateTime"></param>
        /// <param name="StartDateTime"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static DataTable DeviceActivityByDate(int DeviceID, int TzOffset, DateTime EndDateTime, DateTime StartDateTime, NHError Error)
        {

            DataTable dt = new DataTable("DeviceActivity");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_DeviceActivityByDate", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = DeviceID;
                    command.Parameters.Add("@TzOffset", SqlDbType.Int).Value = TzOffset;
                    command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = StartDateTime;
                    command.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDateTime;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04020901;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Device_DeviceActivityByDate] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04020999;
                Error.ErrorMsg = err.Message;
            }

            return dt;


        }



        /// <summary>
        /// MTBadByDate will be used to retrieve activity for specified date range for all devices in a single database
        /// Version: 1.0.0.0
        /// Created: 9/4/2013
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="TzOffset"></param>
        /// <param name="StartDateTime"></param>
        /// <param name="EndDateTime"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static DataTable MTBadByDate(int TzOffset, DateTime EndDateTime, DateTime StartDateTime, NHError Error)
        {

            DataTable dt = new DataTable("DeviceActivity");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_MTBadByDate", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@TzOffset", SqlDbType.Int).Value = TzOffset;
                    command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = StartDateTime;
                    command.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDateTime;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021301;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Device_MTBadByDate] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021399;
                Error.ErrorMsg = err.Message;
            }

            return dt;


        }


        /// <summary>
        /// MTBadByClient will be used to retrieve activity for specified date range for all devices for a single client
        /// Version: 1.0.0.0
        /// Created: 9/30/2013
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="TzOffset"></param>
        /// <param name="StartDateTime"></param>
        /// <param name="EndDateTime"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static DataTable MTBadByClient(string ClientID, int TzOffset, DateTime EndDateTime, DateTime StartDateTime, NHError Error)
        {

            DataTable dt = new DataTable("DeviceActivity");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_MTBadByClient", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    command.Parameters.Add("@TzOffset", SqlDbType.Int).Value = TzOffset;
                    command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = StartDateTime;
                    command.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDateTime;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021401;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Device_MTBadByDate] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021499;
                Error.ErrorMsg = err.Message;
            }

            return dt;


        }

        /// <summary>
        /// UpdateDeviceStatusNH800 updates locktime and lockedby for the time being
        /// will have to modify this method, store proc and gateway to update tbl_NH_DeviceStatusNH800 table
        /// 
        /// Version: 1.0.0.0
        /// Created: 11/29/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="DeviceNH800"></param>
        public static void UpdateDeviceStatusNH800(NHDeviceNH800 DeviceNH800)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateDeviceStatusNH800", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = DeviceNH800.DeviceID;
                    command.Parameters.Add("@LockedTime", SqlDbType.DateTime).Value = DeviceNH800.LockedTime == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.LockedTime;
                    command.Parameters.Add("@LockedBy", SqlDbType.VarChar).Value = DeviceNH800.LockedBy;
                    command.Parameters.Add("@RebootTime0", SqlDbType.DateTime).Value = DeviceNH800.RebootTime0 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime0;
                    command.Parameters.Add("@RebootTime1", SqlDbType.DateTime).Value = DeviceNH800.RebootTime1 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime1;
                    command.Parameters.Add("@RebootTime2", SqlDbType.DateTime).Value = DeviceNH800.RebootTime2 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime2;
                    command.Parameters.Add("@RebootTime3", SqlDbType.DateTime).Value = DeviceNH800.RebootTime3 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime3;
                    command.Parameters.Add("@RebootTime4", SqlDbType.DateTime).Value = DeviceNH800.RebootTime4 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime4;
                    command.Parameters.Add("@RebootTime5", SqlDbType.DateTime).Value = DeviceNH800.RebootTime5 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime5;
                    command.Parameters.Add("@RebootTime6", SqlDbType.DateTime).Value = DeviceNH800.RebootTime6 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime6;
                    command.Parameters.Add("@RebootTime7", SqlDbType.DateTime).Value = DeviceNH800.RebootTime7 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime7;
                    command.Parameters.Add("@RebootTime8", SqlDbType.DateTime).Value = DeviceNH800.RebootTime8 == DateTime.MinValue ? SqlDateTime.Null : DeviceNH800.RebootTime8;
                    command.Parameters.Add("@Output0", SqlDbType.VarChar).Value = DeviceNH800.Output0;
                    command.Parameters.Add("@Output1", SqlDbType.VarChar).Value = DeviceNH800.Output1;
                    command.Parameters.Add("@Output2", SqlDbType.VarChar).Value = DeviceNH800.Output2;
                    command.Parameters.Add("@Output3", SqlDbType.VarChar).Value = DeviceNH800.Output3;
                    command.Parameters.Add("@Output4", SqlDbType.VarChar).Value = DeviceNH800.Output4;
                    command.Parameters.Add("@Output5", SqlDbType.VarChar).Value = DeviceNH800.Output5;
                    command.Parameters.Add("@Output6", SqlDbType.VarChar).Value = DeviceNH800.Output6;
                    command.Parameters.Add("@Output7", SqlDbType.VarChar).Value = DeviceNH800.Output7;
                    command.Parameters.Add("@Output8", SqlDbType.VarChar).Value = DeviceNH800.Output8;

                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                    DeviceNH800.ErrorCode = 0;

                    if (rows == -1)
                    {
                        DeviceNH800.ErrorCode = 91;
                        DeviceNH800.ErrorMsg = "Error Updating NH800 Device Status : DeviceID [" + DeviceNH800.DeviceID + "]";

                    }
                }

            }
            catch (Exception err)
            {
                DeviceNH800.ErrorCode = 99;
                DeviceNH800.ErrorMsg = "\n\rError Updating NH800 Device Status : DeviceID [" + DeviceNH800.DeviceID + "] " + err.Message;
            }
        }

        /// <summary>
        /// InsertMsgID will create a unique MsgID for the SNPP
        /// Version: 1.0.0.0
        /// Created: 05/17/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="deviceID"></param>
        /// <returns></returns>
        public static int InsertMsgID(int DeviceID)
        {
            int msgID = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertMsgID", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = DeviceID;
                    SqlParameter MsgIDParameter = new SqlParameter("@MsgID", SqlDbType.Int);
                    MsgIDParameter.Direction = ParameterDirection.Output;
                    command.Parameters.Add(MsgIDParameter);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    msgID = (int)MsgIDParameter.Value;
                    conn.Close();
                }
            }
            catch (Exception err)
            {
            }

            return msgID;

        }



        /// <summary>
        /// Version: 1.0.0.0
        /// Created: 05/17/2011
        /// Author : Moshiul Arefin 
        /// ErrorCode 0 for a successfull lookup
        /// </summary>
        /// <param name="Phone"></param>
        /// <param name="MsgID"></param>
        /// <returns></returns>
        public static NHSNPPData GetSNPPData(string Phone, int MsgID)
        {
            NHSNPPData SNPPDeliveryData = new NHSNPPData();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetSNPPDelivery", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Phone", SqlDbType.VarChar).Value = Phone;
                    command.Parameters.Add("@ZeroFilledMessageID", SqlDbType.VarChar).Value = String.Format("msgid:{0:d8}", MsgID);

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            SNPPDeliveryData.DeliveryID = myReader["DeliveryID"].ToString();
                            SNPPDeliveryData.Status = myReader["Status"].ToString();
                            SNPPDeliveryData.MsgText = myReader["MsgText"].ToString();
                            SNPPDeliveryData.ReportTime = (DateTime)myReader["ReportTime"];
                            SNPPDeliveryData.Queue = myReader["Queue"].ToString();

                        }

                        SNPPDeliveryData.ErrorCode = 0;
                        SNPPDeliveryData.ErrorMsg = "";

                    }
                    else
                    {
                        SNPPDeliveryData.ErrorCode = 0;
                        SNPPDeliveryData.ErrorMsg = "Unable to find any matching record";

                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                SNPPDeliveryData.ErrorCode = 91;//create the error code
                SNPPDeliveryData.ErrorMsg = err.Message;
            }

            return SNPPDeliveryData;

        }




        /// <summary>
        /// Version: 1.0.0.0
        /// Created: 12/13/2011
        /// Author : Moshiul Arefin 
        /// ErrorCode 0 for a successfull update
        /// </summary>
        /// <param name="SNPPDeliveryData"></param>
        /// <param name="QueueUpdate"></param>
        public static void UpdateSNPPData(NHSNPPData SNPPDeliveryData, string QueueUpdate)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateSNPPDelivery", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeliveryID", SqlDbType.VarChar).Value = SNPPDeliveryData.DeliveryID;
                    command.Parameters.Add("@Queue", SqlDbType.VarChar).Value = SNPPDeliveryData.Queue;
                    command.Parameters.Add("@ReportTime", SqlDbType.VarChar).Value = SNPPDeliveryData.ReportTime;
                    command.Parameters.Add("@MsgText", SqlDbType.VarChar).Value = SNPPDeliveryData.MsgText;
                    command.Parameters.Add("@QueueUpdate", SqlDbType.VarChar).Value = QueueUpdate;



                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();

                    if (rows > 0)
                    {
                        //rows is the number of rows got updated/affected
                        SNPPDeliveryData.ErrorCode = 0;
                        SNPPDeliveryData.ErrorMsg = "";
                    }
                    else
                    {
                        //rows 0 means no row affteced 
                        //rows -1 is for all others

                        SNPPDeliveryData.ErrorCode = 1;
                        SNPPDeliveryData.ErrorMsg = "Unable to update snppp deliverydata";

                    }
                    /*
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            SNPPDeliveryData.DeliveryID = myReader["DeliveryID"].ToString();
                            SNPPDeliveryData.Status = myReader["Status"].ToString();
                            SNPPDeliveryData.MsgText = myReader["MsgText"].ToString();
                            SNPPDeliveryData.ReportTime = (DateTime)myReader["ReportTime"];
                            SNPPDeliveryData.Queue = myReader["Queue"].ToString();

                        }

                        SNPPDeliveryData.ErrorCode = 0;
                        SNPPDeliveryData.ErrorMsg = "";

                    }
                    else
                    {
                        SNPPDeliveryData.ErrorCode = 0;
                        SNPPDeliveryData.ErrorMsg = "Unable to find any matching record";

                    }

                    conn.Close();
                    */
                }
            }
            catch (Exception err)
            {
                SNPPDeliveryData.ErrorCode = 91;//create the error code
                SNPPDeliveryData.ErrorMsg = err.Message;
            }


        }





        public static string SendWaterCommandMSMQ(int deviceID, string userID, string opCode, string opCodeSuffix, string utcString, int DBEnum)
        {
            try
            {
                NHDevice device = new NHDevice(deviceID);

                MessageQueue EventServiceMQ = new MessageQueue();

                NHDeviceManagement.OutBoundWaterMessage Message = new NHDeviceManagement.OutBoundWaterMessage();

                //cellular devices
                //string SendPayload = "";

                //SendPayload = device.MsgPrefix + "12345" + opCode + opCodeSuffix;

                //byte CheckSum = objNHSecurity.CheckSum(SendPayload);
                //SendPayload += CheckSum.ToString("X2", objManagementEntity.Ci);
                //string EncryptedMessage = objNHSecurity.EncryptMessage('T', SendPayload);
                //Message.PayLoad = EncryptedMessage;

                Message.ClientID = device.ClientID;
                Message.Created = System.DateTime.UtcNow;
                Message.DeviceID = device.DeviceID;
                Message.iTranID = 0;
                Message.ICCID = device.CapCode;
                Message.MessageID = Guid.NewGuid();
                Message.MessageNumber = 1;
                Message.Network = device.Network;
                //Message.UnEncrypted = SendPayload;
                Message.PhoneNumber = device.Phone;
                Message.Protocol = device.Protocol;
                Message.SecurityCode = device.Esn;
                Message.Status = "SENDING";
                Message.UserName = userID;
                Message.DB_Enum = DBEnum; // 0=test,1=Demo.2=Prod,3=Dev - SQL Databases
                Message.Opcode = opCode;
                Message.OpcodeSuffix = opCodeSuffix;

                string Label = Message.DeviceID.ToString() + "_" + Message.SecurityCode + "_" + Message.DB_Enum.ToString();

                EventServiceMQ = new MessageQueue(TxEventServiceMQ, true);
                EventServiceMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundWaterMessage) });
                MessageQueueTransaction EventServiceTran = new MessageQueueTransaction();

                try
                {
                    EventServiceTran.Begin();
                    EventServiceMQ.Send(Message, Label, EventServiceTran);
                    EventServiceTran.Commit();
                }
                catch (Exception ex)
                {
                    EventServiceTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    EventServiceMQ.Close();
                }
            }
            catch (Exception ex)
            {
                return ("Failed:[" + ex.Message + "]");
            }
            return ("Success");
        }

        /// <summary>
        /// Version: 1.0.0.0
        /// Created: /2011
        /// Author : Reggie 
        /// Modified on 10/20/2011 by Moshiul Arefin
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="userID"></param>
        /// <param name="opCode"></param>
        /// <param name="opCodeSuffix"></param>
        /// <param name="utcString"></param>
        /// <param name="DBEnum"></param>
        /// <returns></returns>
        public static string SendCommandMSMQ(int deviceID, string userID, string opCode, string opCodeSuffix, string utcString, int DBEnum)
        {
            NHDevice device = new NHDevice(deviceID);
            MessageQueue NumerexMQ = new MessageQueue();
            MessageQueue VerizonMQ = new MessageQueue();
            MessageQueue VerizonTestMQ = new MessageQueue();
            MessageQueue VerizonSensorMQ = new MessageQueue();
            MessageQueue TrilliantMQ = new MessageQueue();
            MessageQueue SaipanMQ = new MessageQueue();
            MessageQueue TrinidadMQ = new MessageQueue();
            MessageQueue AttMQ = new MessageQueue();
            MessageQueue BahamasMQ = new MessageQueue();

            /*Commenting the connection string an redirecting it to demo*/
            string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString();


            NHNetwork Network = new NHNetwork();
            Network.Command = "SELECT";
            Network.NetworkID = device.Network;
            DataTable dt = NHUtils.ManageNetworks(Network);

            switch (Network.Protocol.Trim())
            {
                case "WCTP":
                    if (database == "NHKWCConnectionStringDemo")
                        NumerexMQ = new MessageQueue(TxWCTPQueueNameDEMO, true);
                    else
                        NumerexMQ = new MessageQueue(TxWCTPQueueNamePROD, true);
                    break;
                case "SNPP":
                    if (database == "NHKWCConnectionStringDemo")
                        NumerexMQ = new MessageQueue(TxSNPPQueueNameDEMO, true);
                    else
                        NumerexMQ = new MessageQueue(TxSNPPQueueNamePROD, true);
                    break;
                case "NUMEREX":
                    NumerexMQ = new MessageQueue(TxNumerexQueueName, true);
                    break;
                case "NUMEREXTST":
                    NumerexMQ = new MessageQueue(DevQueueName, true);
                    break;
                case "NPHASE":
                    VerizonMQ = new MessageQueue(TxVerizonQueueName, true);
                    break;
                case "TRILLIANT":
                    TrilliantMQ = new MessageQueue(TxTrilliantQueueName, true);
                    break;
                case "SMSC":
                    SaipanMQ = new MessageQueue(TxSaipanQueueName, true);
                    break;
                case "SMSC_TRINI":
                    TrinidadMQ = new MessageQueue(TxTrinidadQueueName, true);
                    break;
                case "SMSC_ATT":
                    AttMQ = new MessageQueue(TxAttQueueName, true);
                    break;
                case "NPHASE_TST":
                    VerizonTestMQ = new MessageQueue(TxVerizonTestQueueName, true);
                    break;
                case "NPHASE_SEN":
                    VerizonSensorMQ = new MessageQueue(TxVerizonSensorQueueName, true);
                    break;
                case "SMSC_BAM":
                    BahamasMQ = new MessageQueue(TxBahamasQueueName,true);
                    break;
                default:
                    NumerexMQ = new MessageQueue(DevQueueName, true);
                    break;
            }

            OutBoundMessage Message = new OutBoundMessage();
            //cellular devices
            string SendPayload = "";
            if (device.DeviceType.CompareTo(8) > 0)
            {
                SendPayload = device.MsgPrefix + "12345" + opCode + opCodeSuffix;

                //Add Synapase Mesh wrapper
                //deviceType 39
                //or 
                //6 digit hex value
                //it will have a hub address                
                /*
                 * Moshiul Arefin 1/20/2015
                 * Commenting out the mesh wrapper part b/c it will be dealt with in the application layer
                if ((device.DeviceType.CompareTo(39) == 0 ||
                            (device.Phone.Length == 6 && NHUtils.IsHexString(device.Phone))
                         )
                    && device.HubAddress != "")
                {
                    //md + src + dest + len + opcode + parameter
                    //Payload[!!12345MD5E8E3F,5E8D98,004,GV44]
                    SendPayload = device.MsgPrefix + "12345MD" + new NHDevice("Phone", device.HubAddress).HubAddress + "," + device.Phone + ",004," + opCode + opCodeSuffix;

                }
                */

                byte CheckSum = NHSecurity.CheckSum(SendPayload);
                SendPayload += CheckSum.ToString("X2", ci);
                string EncryptedMessage = NHSecurity.EncryptMessage('T', SendPayload);
                Message.PayLoad = EncryptedMessage;
            }
            //otherwise it's legacy paging devices
            else
            {
                SendPayload = opCode + opCodeSuffix;
                Message.PayLoad = SendPayload;
            }

            Message.ClientID = device.ClientID;
            Message.Created = System.DateTime.UtcNow;
            Message.DeviceID = device.DeviceID;
            Message.iTranID = 0;
            Message.ICCID = device.CapCode;
            Message.MessageID = Guid.NewGuid();
            Message.MessageNumber = 1;
            Message.Network = device.Network;
            Message.UnEncrypted = SendPayload;
            Message.PhoneNumber = device.Phone;
            Message.Protocol = device.Protocol;
            Message.SecurityCode = device.Esn;
            Message.Status = "SENDING";
            Message.UserName = userID;
            Message.DB_Enum = DBEnum; // 0=test,1=Demo.2=Prod,3=Dev - SQL Databases

            string Label = Message.DeviceID.ToString() + "_" + Message.SecurityCode + "_" + Message.DB_Enum.ToString();

            if (Network.Protocol.Trim().CompareTo("NPHASE") == 0)
            {
                VerizonMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction VerizonTran = new MessageQueueTransaction();

                try
                {
                    VerizonTran.Begin();
                    VerizonMQ.Send(Message, Label, VerizonTran);
                    VerizonTran.Commit();
                }
                catch (Exception ex)
                {
                    VerizonTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    VerizonMQ.Close();
                }
            }
            else if (Network.Protocol.Trim().CompareTo("NPHASE_TST") == 0)
            {
                VerizonTestMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction VerizonTestTran = new MessageQueueTransaction();

                try
                {
                    VerizonTestTran.Begin();
                    VerizonTestMQ.Send(Message, Label, VerizonTestTran);
                    VerizonTestTran.Commit();
                }
                catch (Exception ex)
                {
                    VerizonTestTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    VerizonTestMQ.Close();
                }
            }
            else if (Network.Protocol.Trim().CompareTo("NPHASE_SEN") == 0)
            {
                VerizonSensorMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction VerizonSensorTran = new MessageQueueTransaction();

                try
                {
                    VerizonSensorTran.Begin();
                    VerizonSensorMQ.Send(Message, Label, VerizonSensorTran);
                    VerizonSensorTran.Commit();
                }
                catch (Exception ex)
                {
                    VerizonSensorTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    VerizonSensorMQ.Close();
                }
            }
            else if (Network.Protocol.Trim().CompareTo("SMSC") == 0)
            {
                SaipanMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction SaipanTran = new MessageQueueTransaction();

                try
                {
                    SaipanTran.Begin();
                    SaipanMQ.Send(Message, Label, SaipanTran);
                    SaipanTran.Commit();
                }
                catch (Exception ex)
                {
                    SaipanTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    SaipanMQ.Close();
                }
            }
            else if (Network.Protocol.Trim().CompareTo("SMSC_ATT") == 0)
            {
                AttMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction AttTran = new MessageQueueTransaction();

                try
                {
                    AttTran.Begin();
                    AttMQ.Send(Message, Label, AttTran);
                    AttTran.Commit();
                }
                catch (Exception ex)
                {
                    AttTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    AttMQ.Close();
                }
            }
            else if (Network.Protocol.Trim().CompareTo("SMSC_BAM") == 0)
            {
                BahamasMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction BahamasTran = new MessageQueueTransaction();

                try
                {
                    BahamasTran.Begin();
                    BahamasMQ.Send(Message, Label, BahamasTran);
                    BahamasTran.Commit();
                }
                catch (Exception ex)
                {
                    BahamasTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    BahamasMQ.Close();
                }
            }
            else if (Network.Protocol.Trim().CompareTo("SMSC_TRINI") == 0)
            {
                TrinidadMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction TrinidadTran = new MessageQueueTransaction();

                try
                {
                    TrinidadTran.Begin();
                    TrinidadMQ.Send(Message, Label, TrinidadTran);
                    TrinidadTran.Commit();
                }
                catch (Exception ex)
                {
                    TrinidadTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    TrinidadMQ.Close();
                }
            }
            else if (Network.Protocol.Trim().CompareTo("TRILLIANT") == 0)
            {
                TrilliantMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction TrilliantTran = new MessageQueueTransaction();

                try
                {
                    TrilliantTran.Begin();
                    TrilliantMQ.Send(Message, Label, TrilliantTran);
                    TrilliantTran.Commit();
                }
                catch (Exception ex)
                {
                    TrilliantTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    TrilliantMQ.Close();
                }
            }
            else
            {
                NumerexMQ.Formatter = new XmlMessageFormatter(new Type[] { typeof(OutBoundMessage) });
                MessageQueueTransaction NumerexTran = new MessageQueueTransaction();

                try
                {
                    NumerexTran.Begin();
                    NumerexMQ.Send(Message, Label, NumerexTran);
                    NumerexTran.Commit();
                }
                catch (Exception ex)
                {
                    NumerexTran.Abort();
                    return ("Failed:[" + ex.Message + "]");
                }
                finally
                {
                    NumerexMQ.Close();
                }
            }

            return ("Success");
        }


        //this method is used for updating timeouts

        public static DataSet GetDeviceTimeOut()
        {
            DataSet ds = new DataSet("TimeOuts");

            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_CheckForTimeOutsNumerex", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds, "TimeOut");
                }

            }
            catch (Exception err)
            {
                //ErrorMsg = 4009;

            }


            return ds;
        }


        //insert meater readings
        public static void InsertMeterReadings(NHDevice device, int touIndex, string tOd, double meterReading)
        {
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertMeterReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = device.ClientID;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@PacketID", SqlDbType.Int).Value = touIndex;
                    command.Parameters.Add("@MeterReportTime", SqlDbType.DateTime).Value = Convert.ToDateTime(tOd);
                    command.Parameters.Add("@MeterReading", SqlDbType.Float).Value = meterReading;
                    command.Parameters.Add("@Voltage", SqlDbType.Float).Value = 999.9;
                    command.Parameters.Add("@Amps", SqlDbType.Float).Value = 999.9;

                    SqlParameter SuccessParameter = new SqlParameter("@Success", SqlDbType.VarChar);
                    SuccessParameter.DbType = DbType.String;
                    SuccessParameter.Size = 255;
                    SuccessParameter.Direction = ParameterDirection.Output;

                    command.Parameters.Add(SuccessParameter);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();

                    string RESULT = (string)SuccessParameter.Value;
                    if (RESULT.CompareTo("Already in table.") == 0)
                    {
                        device.ErrorCode = 0;
                        device.ErrorMsg = "Device: " + device.DeviceID + " Already Inserted Meter Readings[" + meterReading + "]DateTime[" + tOd + "]PacketID[" + touIndex + "]";
                    }
                    else
                    {
                        device.ErrorCode = 0;
                        device.ErrorMsg = "Device: " + device.DeviceID + " Inserting Meter Readings[" + meterReading + "]DateTime[" + tOd + "]PacketID[" + touIndex + "]";
                    }

                    conn.Close();
                }

            }
            catch (Exception err)
            {
                device.ErrorCode = 4007;
                device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "\n\rError Inserting Meter Readings[" + meterReading + "]PacketID[" + touIndex + "]" + err.Message;
                //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }           

        }


        /// <summary>
        /// InsertErtReadingsIP inserts IP Ert data into the  tbl_NH_Devices_Ert_Readings table
        /// </summary>
        /// <param name="ertDevice"></param>
        public static void InsertErtReadingsIP(NHErtReading ertDevice)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertErtReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ErtHubID", SqlDbType.Int).Value = ertDevice.ErtHubID;
                    command.Parameters.Add("@ErtID", SqlDbType.BigInt).Value = ertDevice.ErtID;
                    command.Parameters.Add("@ErtReading", SqlDbType.Float).Value = ertDevice.ErtReading;
                    command.Parameters.Add("@ErtTamperInversion", SqlDbType.TinyInt).Value = ertDevice.ErtTamperInversion;
                    command.Parameters.Add("@ErtType", SqlDbType.TinyInt).Value = ertDevice.ErtType;
                    command.Parameters.Add("@ErtTamperRemove", SqlDbType.TinyInt).Value = ertDevice.ErtTamperRemove;
                    command.Parameters.Add("@ErtRSSI", SqlDbType.SmallInt).Value = ertDevice.ErtRSSI;
                    command.Parameters.Add("@ErtTimeStamp", SqlDbType.DateTime).Value = ertDevice.ErtTimeStamp;


                    SqlParameter SuccessParameter = new SqlParameter("@Success", SqlDbType.VarChar);
                    SuccessParameter.DbType = DbType.String;
                    SuccessParameter.Size = 255;
                    SuccessParameter.Direction = ParameterDirection.Output;

                    command.Parameters.Add(SuccessParameter);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();

                    string RESULT = (string)SuccessParameter.Value;
                    if (RESULT.CompareTo("Already in table.") == 0)
                    {
                        ertDevice.ErrorCode = 0;
                        ertDevice.ErrorMsg = "Device: " + ertDevice.ErtHubID + ", Ert: " + ertDevice.ErtID + " Already Inserted Ert Readings[" + ertDevice.ErtReading + "]DateTime[" + ertDevice.ErtTimeStamp + "]";
                    }
                    else
                    {
                        ertDevice.ErrorCode = 0;
                        ertDevice.ErrorMsg = "Device: " + ertDevice.ErtHubID + ", Ert: " + ertDevice.ErtID + " Inserting Ert Readings[" + ertDevice.ErtReading + "]DateTime[" + ertDevice.ErtTimeStamp + "]";
                    }

                    conn.Close();
                }

            }
            catch (Exception err)
            {
                ertDevice.ErrorCode = 99;
                ertDevice.ErrorMsg = "NH Error Code: " + ertDevice.ErrorCode + "\n\rError Inserting Ert Readings[" + ertDevice.ErtReading + "]" + err.Message;
                //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }


        }



        /// <summary>
        /// InsertMeterReadingsIP inserts IP TOU data into the meterreading table
        /// </summary>
        /// <param name="device"></param>
        /// <param name="touIndex"></param>
        /// <param name="tOd"></param>
        public static void InsertMeterReadingsIP(NHDevice device, int touIndex, string tOd)
        {
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertMeterReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = device.ClientID;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@PacketID", SqlDbType.Int).Value = touIndex;
                    command.Parameters.Add("@MeterReportTime", SqlDbType.DateTime).Value = Convert.ToDateTime(tOd);
                    command.Parameters.Add("@MeterReading", SqlDbType.Float).Value = device.MeterReading;
                    command.Parameters.Add("@Voltage", SqlDbType.Float).Value = device.Voltage;
                    command.Parameters.Add("@Amps", SqlDbType.Float).Value = device.Amps;

                    SqlParameter SuccessParameter = new SqlParameter("@Success", SqlDbType.VarChar);
                    SuccessParameter.DbType = DbType.String;
                    SuccessParameter.Size = 255;
                    SuccessParameter.Direction = ParameterDirection.Output;

                    command.Parameters.Add(SuccessParameter);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();

                    string RESULT = (string)SuccessParameter.Value;
                    if (RESULT.CompareTo("Already in table.") == 0)
                    {
                        device.ErrorCode = 0;
                        device.ErrorMsg = "Device: " + device.DeviceID + " Already Inserted Meter Readings[" + device.MeterReading + "]DateTime[" + tOd + "]PacketID[" + touIndex + "]";
                    }
                    else
                    {
                        device.ErrorCode = 0;
                        device.ErrorMsg = "Device: " + device.DeviceID + " Inserting Meter Readings[" + device.MeterReading + "]DateTime[" + tOd + "]PacketID[" + touIndex + "]";
                    }

                    conn.Close();
                }

            }
            catch (Exception err)
            {
                device.ErrorCode = 99;
                device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "\n\rError Inserting Meter Readings[" + device.MeterReading + "]PacketID[" + touIndex + "]" + err.Message;
                //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }           

        }

        /// <summary>
        /// InsertBadMeterReadingsIP inserts IP TOU data into the tbl_NH_BadMeterReadings table
        /// </summary>
        /// <param name="device"></param>
        /// <param name="touIndex"></param>
        /// <param name="tOd"></param>
        public static void InsertBadMeterReadingsIP(NHDevice device, int touIndex, string tOd)
        {
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertBadMeterReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = device.ClientID;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@PacketID", SqlDbType.Int).Value = touIndex;
                    command.Parameters.Add("@MeterReportTime", SqlDbType.VarChar).Value = tOd;
                    command.Parameters.Add("@MeterReading", SqlDbType.Float).Value = device.MeterReading;
                    command.Parameters.Add("@Voltage", SqlDbType.Float).Value = device.Voltage;
                    command.Parameters.Add("@Amps", SqlDbType.Float).Value = device.Amps;

                    SqlParameter SuccessParameter = new SqlParameter("@Success", SqlDbType.VarChar);
                    SuccessParameter.DbType = DbType.String;
                    SuccessParameter.Size = 255;
                    SuccessParameter.Direction = ParameterDirection.Output;

                    command.Parameters.Add(SuccessParameter);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();

                    string RESULT = (string)SuccessParameter.Value;
                    if (RESULT.CompareTo("Already in table.") == 0)
                    {
                        device.ErrorCode = 0;
                        device.ErrorMsg = "Device: " + device.DeviceID + " Already Inserted Bad Meter Readings[" + device.MeterReading + "]DateTime[" + tOd + "]PacketID[" + touIndex + "]";
                    }
                    else
                    {
                        device.ErrorCode = 0;
                        device.ErrorMsg = "Device: " + device.DeviceID + " Inserting Bad Meter Readings[" + device.MeterReading + "]DateTime[" + tOd + "]PacketID[" + touIndex + "]";
                    }

                    conn.Close();
                }

            }
            catch (Exception err)
            {
                device.ErrorCode = 99;
                device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "\n\rError Inserting Bad Meter Readings[" + device.MeterReading + "]PacketID[" + touIndex + "]" + err.Message;
                //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }
           

        }



        /// <summary>
        /// InsertTOUReadings inserts TOU data into the meterreading table
        /// </summary>
        /// <param name="device"></param>
        /// <param name="touIndex"></param>
        /// <param name="tOd"></param>
        public static void InsertTOUReadings(NHDevice device, int touIndex, string tOd)
        {
            string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertTOUReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = device.ClientID;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@PacketID", SqlDbType.Int).Value = touIndex;
                    command.Parameters.Add("@MeterReportTime", SqlDbType.DateTime).Value = Convert.ToDateTime(tOd);
                    command.Parameters.Add("@MeterReadingDelivered", SqlDbType.Float).Value = device.MeterReadingDelivered;
                    command.Parameters.Add("@Voltage", SqlDbType.Float).Value = device.Voltage;
                    command.Parameters.Add("@Amps", SqlDbType.Float).Value = device.Amps;
                    command.Parameters.Add("@MeterReadingReceived", SqlDbType.Float).Value = device.MeterReadingReceived;

                    SqlParameter SuccessParameter = new SqlParameter("@Success", SqlDbType.VarChar);
                    SuccessParameter.DbType = DbType.String;
                    SuccessParameter.Size = 255;
                    SuccessParameter.Direction = ParameterDirection.Output;

                    command.Parameters.Add(SuccessParameter);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();

                    string RESULT = (string)SuccessParameter.Value;
                    if (RESULT.CompareTo("Already in table.") == 0)
                    {
                        device.ErrorCode = 0;
                        device.ErrorMsg = "Device: " + device.DeviceID + " Already Inserted Meter Readings[" + device.MeterReading + "]DateTime[" + tOd + "]PacketID[" + touIndex + "]";
                    }
                    else
                    {
                        device.ErrorCode = 0;
                        device.ErrorMsg = "Device: " + device.DeviceID + " Inserting Meter Readings[" + device.MeterReading + "]DateTime[" + tOd + "]PacketID[" + touIndex + "]";
                    }

                    conn.Close();
                }
            }
            catch (Exception err)
            {
                device.ErrorCode = 99;
                device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "\n\rError Inserting Meter Readings[" + device.MeterReading + "]PacketID[" + touIndex + "]" + err.Message;
                //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }



        }
        public static DataTable GetVoltageMonitor(NHError error)
        {
            DataTable dt = new DataTable("VoltMonitorFreq");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetVoltMonitorFreq", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);
                    error.ErrorCode = 0;
                }
            }
            catch (Exception err)
            {
                error.ErrorCode = 4009;
                error.ErrorMsg = "NH Error Code: " + error.ErrorCode + "\n\rError Retreiving Voltage Monitor Frequency";
            }

            return dt;

        }


        public static void UpdateVoltageMonitor(NHDevice device, DateTime datetime)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateVoltMonitorFreq", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@TimeMonitored", SqlDbType.DateTime2).Value = datetime;

                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                    device.ErrorCode = 0;
                }
            }
            catch (Exception err)
            {
                device.ErrorCode = 4009;
                device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "DeviceID : " + device.DeviceID + "\n\rError Updating Voltage Monitor Time";
                //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }
        }

        //public static void UpdateVoltageMonitor(NHDevice device, DateTime datetime, string Freq)
        //{
        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
        //        {
        //            SqlCommand command = new SqlCommand("sp_NH_Device_UpdateVoltMonitorFreq", conn);
        //            command.CommandType = CommandType.StoredProcedure;
        //            command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
        //            //command.Parameters.Add("@TimeMonitored", SqlDbType.DateTime2).Value = datetime;
        //            command.Parameters.Add("@MonitorFreq", SqlDbType.Int).Value = Freq;

        //            conn.Open();
        //            int rows = command.ExecuteNonQuery();
        //            conn.Close();
        //            device.ErrorCode = 0;
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        device.ErrorCode = 4009;
        //        device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "DeviceID : " + device.DeviceID + "\n\rError Updating Voltage Monitor Time" + err.Message;
        //        //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
        //    }
        //}

        //public static int GetVoltageMonitor(NHDevice device)
        //{
        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
        //        {
        //            SqlCommand command = new SqlCommand("sp_NH_Device_GetVoltMonitor", conn);
        //            command.CommandType = CommandType.StoredProcedure;
        //            command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
        //            //command.Parameters.Add("@TimeMonitored", SqlDbType.DateTime2).Value = datetime;

        //            conn.Open();
        //            int rows = command.ExecuteNonQuery();
        //            conn.Close();
        //            device.ErrorCode = 0;
        //            return rows;
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        device.ErrorCode = 4009;
        //        device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "DeviceID : " + device.DeviceID + "\n\rError Retreiving Voltage Monitor Freq" + err.Message;
        //        return 0;                
        //    }
        //}




        public static int ManageVoltageMonitor(NHDevice device, string Freq, string cmd)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    int rows = '0';
                    SqlCommand command = new SqlCommand("sp_NH_Device_ManageVoltageMonitor", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    //command.Parameters.Add("@TimeMonitored", SqlDbType.DateTime2).Value = null;
                    command.Parameters.Add("@MonitorFreq", SqlDbType.Int).Value = Freq;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = cmd;
                    conn.Open();

                    if (cmd == "SELECT")
                    {
                        rows = (Int32)command.ExecuteScalar();
                    }
                    else
                    {
                        rows = command.ExecuteNonQuery();

                    }
                    conn.Close();
                    device.ErrorCode = 0;
                    return rows;
                }
            }
            catch (Exception err)
            {
                device.ErrorCode = 4010;
                device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "DeviceID : " + device.DeviceID + "\n\rError Updating Voltage Monitor Time" + err.Message;
                //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
                return 0;
            }
        }





        //get account balance
        public static DataSet GetAccountBalance(NHDevice device)
        {
            DataSet ds = new DataSet("AccountBalance");
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_AccountBalanceByDeviceID", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = device.DeviceID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(ds, "AccountBalance");
                    device.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                device.ErrorCode = 4008;
                device.ErrorMsg = "NH Error Code: " + device.ErrorCode + "Row Not Found in AccountBalance for[" + device.DeviceID + err.Message;
                //device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }

            return ds;            

        }





        //Numerex gateway is updating the device status
        public static void UpdateDeviceStatus(NHDevice device, string gatewayMessage, string gatewayUTCstring)
        {            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateDeviceStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@GatewayMessage", SqlDbType.VarChar).Value = gatewayMessage;
                    command.Parameters.Add("@ReportTime", SqlDbType.VarChar).Value = gatewayUTCstring;
                    //command.Parameters.Add("@Command", SqlDbType.VarChar).Value = "Status";// Need to show command as Connect
                    //command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = gatewayMessage;// CommandStatus]: "QUEUED"/"SENDING"/"SENT"/"COMPLETED"/"FAILED"
                    //command.Parameters.Add("@Status", SqlDbType.VarChar).Value = gatewayMessage;// Need to update status from ARM to Connect OK
                    //command.Parameters.Add("@StatusReportTime", SqlDbType.VarChar).Value = gatewayUTCstring;


                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                    device.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                device.ErrorCode = 4002;
                device.ErrorMsg = "\n\rError Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }
            
        }




        /// <summary>
        /// UpdateDeviceStatus is used to update device status (connected, disconnected, armed, 3030, current limit), to update status update time
        /// to update command Status (queued/sending/sent/completed/failed), update report time
        /// Version: 1.0.0.0
        /// Created: 12/02/2010
        /// Modified: 10/21/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="gatewayMessage"></param>
        /// <param name="gatewayUTCstring"></param>
        /// <param name="cmdStatus"></param>
        /// <param name="status"></param>
        /// <param name="statusUTCstring"></param>
        public static void UpdateDeviceStatus(NHDevice device, string gatewayMessage, string gatewayUTCstring, string cmdStatus, string status, string statusUTCstring)
        {

            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateDeviceStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@GatewayMessage", SqlDbType.VarChar).Value = gatewayMessage;
                    command.Parameters.Add("@ReportTime", SqlDbType.VarChar).Value = gatewayUTCstring;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = cmdStatus;// CommandStatus]: "QUEUED"/"SENDING"/"SENT"/"COMPLETED"/"FAILED"
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;// Need to update status from ARM to Connect OK
                    command.Parameters.Add("@StatusReportTime", SqlDbType.VarChar).Value = statusUTCstring;


                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                    device.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                device.ErrorCode = 4002;
                device.ErrorMsg = "\n\rError Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }
            



        }

        public static void UpdateDeviceStatus(string execStatement, string opCode, NHDevice device, string dbTable)
        {
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateDeviceData", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@DBTable", SqlDbType.VarChar).Value = dbTable;
                    command.Parameters.Add("@ExecStatement", SqlDbType.VarChar).Value = execStatement;



                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                    device.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                device.ErrorCode = 4006;
                device.ErrorMsg = "\n\rError " + opCode + " Updating Device Status : DeviceID [" + device.DeviceID + "] " + err.Message;
            }
           

        }

        //Added by Siva to update the devicestatus
        public static void UpdateDeviceStatusDisconnect(int deviceID,int xState)
        {
            NHDevice device = new NHDevice();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateDeviceStatusDisconnectCheck", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;
                    command.Parameters.Add("@XState", SqlDbType.VarChar).Value = xState;
                    conn.Open();

                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception err)
            {
            }
        }

        /// <summary>
        /// UpdateDeviceStatus create to change the utcstring datetime datatype
        /// Moshiul Arefin
        /// 3/12/2012
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="opCode"></param>
        /// <param name="opCodeSuffix"></param>
        /// <param name="CommandSent"></param>
        public static void UpdateDeviceStatus(int deviceID, string opCode, string opCodeSuffix, DateTime commandSent)
        {
            try
            {
                //For mesh opcodes 
                if (opCode.ToUpper().Contains("MD"))
                {
                    //Get rid of md 
                    opCode = opCode.ToUpper().Replace("MD", "");

                    //Get rid of synapse address
                    NHSynapse Synapse = new NHSynapse(new NHDevice(deviceID));

                    opCodeSuffix = opCodeSuffix.Replace(Synapse.LeafAddress, "");
                }

                NHDeviceControl DeviceControl = new NHDeviceControl(opCode);

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateCommandStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                    //command.Parameters.Add("@MsgID", SqlDbType.Int).Value = msgID;
                    //command.Parameters.Add("@Command", SqlDbType.VarChar).Value = opCode + opCodeSuffix;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = DeviceControl.ControlName;
                    command.Parameters.Add("@CommandSent", SqlDbType.DateTime2).Value = commandSent;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = "SENDING";
                    command.Parameters.Add("@OPCode", SqlDbType.VarChar).Value = opCode;
                    command.Parameters.Add("@OPCodeSuffix", SqlDbType.VarChar).Value = opCodeSuffix;
                    //command.Parameters.Add("@ResetOption", SqlDbType.VarChar).Value = utcString;

                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();


                }
            }
            catch (Exception err)
            {


            }





        }


        /// <summary>
        /// Version: 1.0.0.0
        /// Created: 23/02/2016
        /// Author : Sivakumar Kandasamy
        /// </summary>
        public static NHWaterMeter GetWaterStatus(int deviceID)
        {
            NHWaterMeter NHWaterStatus = new NHWaterMeter();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetWaterMeter", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            NHWaterStatus.Signal = myReader["Signal"] == DBNull.Value ? 0 : (int)myReader["Signal"];
                            NHWaterStatus.FormatCode = myReader["FormatCode"] == DBNull.Value ? "" : myReader["FormatCode"].ToString();
                            NHWaterStatus.Description = myReader["Description"] == DBNull.Value ? "" : myReader["Description"].ToString();
                            NHWaterStatus.Battery1Level = myReader["Battery1Level"] == DBNull.Value ? 0 : (double)myReader["Battery1Level"];
                            NHWaterStatus.Battery2Level = myReader["Battery2Level"] == DBNull.Value ? 0 : (double)myReader["Battery2Level"];
                            NHWaterStatus.Sensor1MaxT = myReader["Sensor1MaxT"] == DBNull.Value ? 0 : (double)myReader["Sensor1MaxT"];
                            NHWaterStatus.Sensor2MaxT = myReader["Sensor2MaxT"] == DBNull.Value ? 0 : (double)myReader["Sensor2MaxT"];
                            NHWaterStatus.Sensor1MinT = myReader["Sensor1MinT"] == DBNull.Value ? 0 : (double)myReader["Sensor1MinT"];
                            NHWaterStatus.Sensor2MinT = myReader["Sensor2MinT"] == DBNull.Value ? 0 : (double)myReader["Sensor2MinT"];
                            NHWaterStatus.rxPayload = myReader["rxPayload"] == DBNull.Value ? "" : myReader["rxPayload"].ToString();
                            NHWaterStatus.MeterReading = myReader["MeterReading"] == DBNull.Value ? 0 : (float)myReader["MeterReading"];
                            NHWaterStatus.Temperature = myReader["Temperature"] == DBNull.Value ? "" : myReader["Temperature"].ToString();
                            NHWaterStatus.SwitchState = myReader["SwitchState"] == DBNull.Value ? "" : myReader["SwitchState"].ToString();
                            NHWaterStatus.Alert = myReader["Alert"] == DBNull.Value ? "" : myReader["Alert"].ToString();
                            NHWaterStatus.AlertEnable = myReader["AlertEnable"] == DBNull.Value ? false : (bool)myReader["AlertEnable"];
                            NHWaterStatus.LoggingStatus = myReader["LoggingStatus"] == DBNull.Value ? "" : myReader["LoggingStatus"].ToString();
                            NHWaterStatus.LoggingInterval = myReader["LoggingInterval"] == DBNull.Value ? "" : myReader["LoggingInterval"].ToString();
                            NHWaterStatus.UnreadLogEntries = myReader["UnreadLogEntries"] == DBNull.Value ? "" : myReader["UnreadLogEntries"].ToString();
                            NHWaterStatus.FirmwareVersion = myReader["FirmwareVersion"] == DBNull.Value ? "" : myReader["FirmwareVersion"].ToString();
                            NHWaterStatus.HardwareVersion = myReader["HardwareVersion"] == DBNull.Value ? "" : myReader["HardwareVersion"].ToString();
                            NHWaterStatus.PythonVersion = myReader["PythonVersion"] == DBNull.Value ? "" : myReader["PythonVersion"].ToString();
                            NHWaterStatus.ConfigVersion = myReader["ConfigVersion"] == DBNull.Value ? "" : myReader["ConfigVersion"].ToString();
                            NHWaterStatus.AdditionalInfo = myReader["AdditionalInfo"] == DBNull.Value ? "" : myReader["AdditionalInfo"].ToString();
                            //NHWaterStatus.TimeStamp = myReader["TimeStamp"] == DBNull.Value ? "" : myReader["TimeStamp"].ToString();
                            NHWaterStatus.Checksum = myReader["Checksum"] == DBNull.Value ? "" : myReader["Checksum"].ToString();
                            NHWaterStatus.UpTimeInterval = myReader["WakeUpInterval"] == DBNull.Value ? 0 : (int)myReader["WakeUpInterval"];
                            NHWaterStatus.UpTimeInterval = myReader["UpTimeInterval"] == DBNull.Value ? 0 : (int)myReader["UpTimeInterval"];
                            NHWaterStatus.CommunicationTimeOut = myReader["CommunicationTimeOut"] == DBNull.Value ? "" : myReader["CommunicationTimeOut"].ToString();
                            NHWaterStatus.TemperatureConfig = myReader["temperatureConfig"] == DBNull.Value ? "" : myReader["temperatureConfig"].ToString();
                            NHWaterStatus.AdditionalInfoRequest = myReader["AdditionalInfoRequest"] == DBNull.Value ? "" : myReader["AdditionalInfoRequest"].ToString();
                            NHWaterStatus.BulkTransferRequest = myReader["BulkTransferRequest"] == DBNull.Value ? "" : myReader["BulkTransferRequest"].ToString();
                            NHWaterStatus.BehaviorConfig = myReader["BehaviorConfig"] == DBNull.Value ? "" : myReader["BehaviorConfig"].ToString();
                            NHWaterStatus.ErrorCode = 0;
                            NHWaterStatus.ErrorMsg = "";

                        }
                    }
                    else
                    {
                        NHWaterStatus.ErrorCode = 01;
                        NHWaterStatus.ErrorMsg = "Unable to find any Water Meter with DeviceID[" + deviceID + "]";
                    }
                    conn.Close();
                }
            }
            catch (Exception err)
            {
                NHWaterStatus.ErrorCode = 91;//create the error code
                NHWaterStatus.ErrorMsg = err.Message;
            }



            return NHWaterStatus;

        }
        /// <summary>
        /// AddUserActivity will be used to insert records into the useractivity table whenever a user excercise any device
        /// Version: 1.0.0.0
        /// Created: 12/02/2010
        /// Modified: 10/21/2011
        /// Modified: 3/12/2012 to change the timestamp to datetime2
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="userID"></param>
        /// <param name="opCode"></param>
        /// <param name="CommandStatus"></param>
        /// <param name="utcString"></param>
        /// <param name="iTranID"></param>
        /// <returns></returns>
        public static NHDeviceActivity AddUserActivity(int deviceID, string userID, string opCode, string CommandStatus, DateTime CommandSent, int iTranID)
        {
            NHDeviceActivity DeviceActivity = new NHDeviceActivity();

            try
            {
                NHDeviceControl DeviceControl = new NHDeviceControl(opCode);

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertUserActivity", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime2).Value = CommandSent;
                    command.Parameters.Add("@iTranID", SqlDbType.Int).Value = iTranID;
                    //command.Parameters.Add("@Command", SqlDbType.VarChar).Value = opCode;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = DeviceControl.ControlName;
                    command.Parameters.Add("@UserID", SqlDbType.VarChar).Value = userID;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = CommandStatus;
                    command.Parameters.Add("@StatusReportTime", SqlDbType.DateTime2).Value = CommandSent;
                    command.Parameters.Add("@NetworkID", SqlDbType.Int).Value = new NHDevice(deviceID).Network;

                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();

                    if (rows == -1)
                    {
                        DeviceActivity.ErrorCode = 91;//create the error code
                        DeviceActivity.ErrorMsg = "Failed to update activity table stroed proc[sp_NH_Device_InsertUserActivity] for deviceID[" + deviceID + "]";
                    }

                }
            }
            catch (Exception err)
            {
                DeviceActivity.ErrorCode = 91;//create the error code
                DeviceActivity.ErrorMsg = err.Message;
            }



            return DeviceActivity;

        }



        /// <summary>
        /// AddUserActivity will be used to insert records into the useractivity table whenever a user excercise any device
        /// Version: 1.0.0.0
        /// Created: 4/16/2012
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="Activity"></param>
        public static void AddUserActivity(NHDeviceActivity Activity)
        {
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertUserActivity", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = Activity.DeviceID;
                    command.Parameters.Add("@iTranID", SqlDbType.Int).Value = Activity.iTranID;
                    command.Parameters.Add("@GatewayMessage", SqlDbType.VarChar).Value = Activity.GatewayMessage;
                    command.Parameters.Add("@Protocol", SqlDbType.VarChar).Value = Activity.Protocol;
                    command.Parameters.Add("@NetworkID", SqlDbType.Int).Value = Activity.NetworkID;

                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime2).Value = Activity.ReportTime;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = Activity.Command;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = Activity.CommandStatus;
                    command.Parameters.Add("@StatusReportTime", SqlDbType.DateTime2).Value = Activity.StatusReportTime;
                    command.Parameters.Add("@UserID", SqlDbType.VarChar).Value = Activity.UserID;

                    command.Parameters.Add("@phaseACurrent", SqlDbType.Float).Value = Activity.phaseACurrent;
                    command.Parameters.Add("@phaseBCurrent", SqlDbType.Float).Value = Activity.phaseBCurrent;
                    command.Parameters.Add("@Voltage", SqlDbType.Float).Value = Activity.Voltage;
                    command.Parameters.Add("@Temperature", SqlDbType.VarChar).Value = Activity.temperature;
                    command.Parameters.Add("@gpioStatus", SqlDbType.Int).Value = Activity.gpioStatus;
                    command.Parameters.Add("@xState", SqlDbType.Int).Value = Activity.xState;
                    command.Parameters.Add("@errCount", SqlDbType.VarChar).Value = Activity.errCount;
                    command.Parameters.Add("@MeterReading", SqlDbType.Float).Value = Activity.MeterReading;
                    command.Parameters.Add("@MeterReadingDeferred", SqlDbType.Float).Value = Activity.MeterReadingDeferred;
                    command.Parameters.Add("@TimeMeterRead", SqlDbType.DateTime).Value = Activity.TimeMeterRead;

                    SqlParameter Guid = new SqlParameter("@Guid", SqlDbType.VarChar);
                    Guid.DbType = DbType.String;
                    Guid.Size = 255;
                    Guid.Direction = ParameterDirection.Output;

                    command.Parameters.Add(Guid);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();


                    Activity.Guid = (string)Guid.Value;

                    
                    conn.Close();

                    if (rows == -1)
                    {
                        Activity.ErrorCode = 04020391;
                        Activity.ErrorMsg = "Failed to update activity table stroed proc[sp_NH_Device_InsertUserActivity] for deviceID[" + Activity.DeviceID + "]";
                    }

                }
            }
            catch (Exception err)
            {
                Activity.ErrorCode = 04020399;
                Activity.ErrorMsg = err.Message;
            }


        }

        /// <summary>
        /// AddActivity3Phase will be used to insert records into the devicehistory table
        /// Version: 1.0.0.0
        /// Created: 5/13/2015
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="Activity"></param>
        public static void AddActivity3Phase(NHDevice3Phase Device3Phase)
        {
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertActivity3Phase", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = Device3Phase.DeviceID;
                    command.Parameters.Add("@Guid", SqlDbType.VarChar).Value = Device3Phase.Guid;
                    command.Parameters.Add("@kWhDelivered", SqlDbType.Float).Value = Device3Phase.kWhDelivered;
                    command.Parameters.Add("@kWhReceived", SqlDbType.Float).Value = Device3Phase.kWhReceived;
                    command.Parameters.Add("@kVARhLagging", SqlDbType.Float).Value = Device3Phase.kVARhLagging;
                    command.Parameters.Add("@kVARhLeading", SqlDbType.Float).Value = Device3Phase.kVARhLeading;
                    command.Parameters.Add("@kVARhNetDelivered", SqlDbType.Float).Value = Device3Phase.kVARhNetDelivered;
                    command.Parameters.Add("@kVARhNetReceived", SqlDbType.Float).Value = Device3Phase.kVARhNetReceived;
                    command.Parameters.Add("@kVAhDelivered", SqlDbType.Float).Value = Device3Phase.kVAhDelivered;
                    command.Parameters.Add("@kVAhReceived", SqlDbType.Float).Value = Device3Phase.kVAhReceived;
                    //command.Parameters.Add("@PowerFactor", SqlDbType.Float).Value = Device3Phase.PowerFactor;
                    command.Parameters.Add("@kWDelivered", SqlDbType.Float).Value = Device3Phase.kWDelivered;
                    command.Parameters.Add("@kWReceived", SqlDbType.Float).Value = Device3Phase.kWReceived;
                    command.Parameters.Add("@kVARLagging", SqlDbType.Float).Value = Device3Phase.kVARLagging;
                    command.Parameters.Add("@kVARLeading", SqlDbType.Float).Value = Device3Phase.kVARLeading;
                    command.Parameters.Add("@kVADelivered", SqlDbType.Float).Value = Device3Phase.kVADelivered;
                    command.Parameters.Add("@kVAReceived", SqlDbType.Float).Value = Device3Phase.kVAReceived;
                    command.Parameters.Add("@MinPowerFactor", SqlDbType.Float).Value = Device3Phase.MinPowerFactor;
                    command.Parameters.Add("@PhaseAvoltage", SqlDbType.Float).Value = Device3Phase.PhaseAvoltage;
                    command.Parameters.Add("@phaseACurrent", SqlDbType.Float).Value = Device3Phase.phaseACurrent;
                    command.Parameters.Add("@PhaseBvoltage", SqlDbType.Float).Value = Device3Phase.PhaseBvoltage;
                    command.Parameters.Add("@PhaseBvoltageAngle", SqlDbType.Float).Value = Device3Phase.PhaseBvoltageAngle;
                    command.Parameters.Add("@PhaseBcurrent", SqlDbType.Float).Value = Device3Phase.PhaseBcurrent;
                    command.Parameters.Add("@PhaseBcurrentAngle", SqlDbType.Float).Value = Device3Phase.PhaseBcurrentAngle;
                    command.Parameters.Add("@PhaseCvoltage", SqlDbType.Float).Value = Device3Phase.PhaseCvoltage;
                    command.Parameters.Add("@PhaseCvoltageAngle", SqlDbType.Float).Value = Device3Phase.PhaseCvoltageAngle;
                    command.Parameters.Add("@PhaseCcurrent", SqlDbType.Float).Value = Device3Phase.PhaseCcurrent;
                    command.Parameters.Add("@PhaseCcurrentAngle", SqlDbType.Float).Value = Device3Phase.PhaseCcurrentAngle;


                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();

                    if (rows == -1)
                    {
                        Device3Phase.ErrorCode = 04020391;
                        Device3Phase.ErrorMsg = "Failed to update DeviceHistory_3Phase table stroed proc[sp_NH_Device_InsertDeviceHistory] for deviceID[" + Device3Phase.DeviceID + "]";
                    }

                }
            }
            catch (Exception err)
            {
                Device3Phase.ErrorCode = 04020399;
                Device3Phase.ErrorMsg = err.Message;
            }


        }




        /// <summary>
        /// obsolete method, replace this method call with 
        /// UpdateDeviceStatus(int deviceID, string opCode, string opCodeSuffix, DateTime commandSent)
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="opCode"></param>
        /// <param name="opCodeSuffix"></param>
        /// <param name="utcString"></param>
        public static void UpdateDeviceStatus(int deviceID, string opCode, string opCodeSuffix, string utcString)
        {
            try
            {
                //For mesh opcodes 
                if (opCode.ToUpper().Contains("MD"))
                {
                    //Get rid of md 
                    opCode = opCode.ToUpper().Replace("MD", "");

                    //Get rid of synapse address
                    NHSynapse Synapse = new NHSynapse(new NHDevice(deviceID));

                    opCodeSuffix = opCodeSuffix.Replace(Synapse.LeafAddress, "");
                }

                NHDeviceControl DeviceControl = new NHDeviceControl(opCode);

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateCommandStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                    //command.Parameters.Add("@MsgID", SqlDbType.Int).Value = msgID;
                    //command.Parameters.Add("@Command", SqlDbType.VarChar).Value = opCode + opCodeSuffix;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = DeviceControl.ControlName;
                    command.Parameters.Add("@CommandSent", SqlDbType.DateTime).Value = utcString;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = "SENDING";
                    command.Parameters.Add("@OPCode", SqlDbType.VarChar).Value = opCode;
                    command.Parameters.Add("@OPCodeSuffix", SqlDbType.VarChar).Value = opCodeSuffix;
                    //command.Parameters.Add("@ResetOption", SqlDbType.VarChar).Value = utcString;

                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();


                }
            }
            catch (Exception err)
            {


            }





        }





        /// <summary>
        /// obsolete method, replace this method call with
        /// AddUserActivity(int deviceID, string userID, string opCode, string CommandStatus, DateTime CommandSent, int iTranID)
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="userID"></param>
        /// <param name="opCode"></param>
        /// <param name="CommandStatus"></param>
        /// <param name="utcString"></param>
        /// <param name="iTranID"></param>
        public static void UpdateUserActivity(int deviceID, string userID, string opCode, string CommandStatus, string utcString, int iTranID)
        {
            Dictionary<string, string> cmnd = new Dictionary<string, string>();

            cmnd.Add("Cl", "Connect"); //connect
            cmnd.Add("Op", "Disconnect"); //disconnect
            cmnd.Add("AR", "Arm"); //arm
            cmnd.Add("Cy", "30/30 Mode"); //30/30 mode
            cmnd.Add("PO", "Check Status"); //
            cmnd.Add("SM", "Send Message"); //
            cmnd.Add("SC", "Current Limit"); //
            cmnd.Add("CM", "Current Mode"); //
            cmnd.Add("SK", "Set Credit"); //
            cmnd.Add("SU", "Set Url"); //
            cmnd.Add("SO", "Set Output"); // 
            cmnd.Add("EC", "Set Ert Config"); // 
            cmnd.Add("CE", "Get Ert Config"); // 
            cmnd.Add("RS", "Reset Meter"); // 
            cmnd.Add("KT", "Kilowatt Trigger"); // 
            cmnd.Add("RT", "Report Trigger"); // 
            cmnd.Add("TO", "Time Of Use SMS"); // 
            cmnd.Add("TI", "Time Of Use IP"); // 
            cmnd.Add("TY", "Enable Time Of Use"); // 
            cmnd.Add("TN", "Disable Time Of Use"); // 
            cmnd.Add("MQ", "Get Meter Factor"); // 
            cmnd.Add("MF", "Set Meter Factor"); // 
            cmnd.Add("FU", "Firmware Update"); // 
            cmnd.Add("GV", "Get Version"); // 
            cmnd.Add("SR", "Software Reset"); // 
            cmnd.Add("PU", "Power Up"); // 
            cmnd.Add("VA", "High/Low Voltage"); //

            string OPCodeString = "";

            try
            {
                OPCodeString = cmnd[opCode];
            }
            catch
            {
                OPCodeString = opCode;

            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_InsertUserActivity", conn);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                command.Parameters.Add("@ReportTime", SqlDbType.DateTime).Value = utcString;
                command.Parameters.Add("@iTranID", SqlDbType.Int).Value = iTranID;
                command.Parameters.Add("@Command", SqlDbType.VarChar).Value = OPCodeString;
                command.Parameters.Add("@UserID", SqlDbType.VarChar).Value = userID;
                command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = CommandStatus;
                command.Parameters.Add("@StatusReportTime", SqlDbType.VarChar).Value = utcString;
                command.Parameters.Add("@NetworkID", SqlDbType.Int).Value = new NHDevice(deviceID).Network;

                conn.Open();
                int rows_updateHistory = command.ExecuteNonQuery();
                conn.Close();

            }


        }


        //Numerex gateway is updating the useractivity table
        public static void UpdateUserActivity(NHDevice device, int iTranID, string protocol, string gatewayMessage)
        {            
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateUserActivityByiTranID", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@iTranID", SqlDbType.Int).Value = iTranID;
                    command.Parameters.Add("@Protocol", SqlDbType.VarChar).Value = protocol;
                    command.Parameters.Add("@GatewayMessage", SqlDbType.VarChar).Value = gatewayMessage;

                    conn.Open();
                    int rows_updateHistory = command.ExecuteNonQuery();
                    conn.Close();

                    if (rows_updateHistory == 0)
                    {
                        device.ErrorCode = 4003;
                        device.ErrorMsg = "Row Not Found for iTranID: " + iTranID + " in User Activity table!";

                    }

                    else device.ErrorCode = 0;

                }
            }
            catch (Exception err)
            {
                device.ErrorCode = 4004;
                device.ErrorMsg = "FAILED Error Updating User Activity: : " + err.Message;
            }
            

        }


        //numerex gateway updating useractivity table if it's a PO or PU
        public static void UpdateUserActivity(NHDevice device, int iTranID)
        {
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateUserActivityReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@MeterReading", SqlDbType.Float).Value = device.MeterReading;
                    command.Parameters.Add("@MeterReadingDeferred", SqlDbType.Float).Value = device.MeterReadingDeferred;
                    command.Parameters.Add("@TimeMeterRead", SqlDbType.DateTime).Value = device.TimeMeterRead;
                    command.Parameters.Add("@PhaseACurrent", SqlDbType.Float).Value = device.PhaseACurrent;
                    command.Parameters.Add("@PhaseBCurrent", SqlDbType.Float).Value = device.PhaseBCurrent;
                    command.Parameters.Add("@Voltage", SqlDbType.Float).Value = device.Voltage;
                    command.Parameters.Add("@Temperature", SqlDbType.VarChar).Value = device.Temperature;
                    command.Parameters.Add("@GpioStatus", SqlDbType.Int).Value = device.GpioStatus;
                    command.Parameters.Add("@XState", SqlDbType.Int).Value = device.XState;
                    command.Parameters.Add("@ErrCount", SqlDbType.VarChar).Value = device.ErrCount;
                    command.Parameters.Add("@iTranID", SqlDbType.Int).Value = iTranID;




                    conn.Open();
                    int rows_updateHistory = command.ExecuteNonQuery();
                    conn.Close();

                    device.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                /*
                device.ErrorCode = 4005;
                device.ErrorMsg = "\n\rError Updating UserActivity: " + err.Message;
                    * */
            }            

        }



        //Numerex gateway 
        public static void UpdateUserActivity(NHDevice device, string stStatus, string stCommand, string networkID, string gatewayUTCstring, int MsgID, int iTranID)
        {
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertUserActivity", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = device.DeviceID;
                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime).Value = gatewayUTCstring;
                    command.Parameters.Add("@iTranID", SqlDbType.Int).Value = iTranID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = stCommand;
                    command.Parameters.Add("@UserID", SqlDbType.VarChar).Value = "Unsolicited";
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = stStatus;
                    command.Parameters.Add("@StatusReportTime", SqlDbType.VarChar).Value = gatewayUTCstring;
                    command.Parameters.Add("@NetworkID", SqlDbType.Int).Value = networkID;

                    conn.Open();
                    int rows_updateHistory = command.ExecuteNonQuery();
                    conn.Close();

                    device.ErrorCode = 0;
                }
            }
            catch (Exception err)
            {
                device.ErrorCode = 4005;
                device.ErrorMsg = "\n\rError Updating UserActivity: " + err.Message;
            }

        }




        public static void UpdateDeviceStatusNH800(int deviceID, string opCode, string opCodeSuffix, string utcString)
        {
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {

                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateCommandStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = opCode + opCodeSuffix;
                    command.Parameters.Add("@CommandSent", SqlDbType.DateTime).Value = utcString;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = "SENDING";
                    command.Parameters.Add("@OPCode", SqlDbType.VarChar).Value = opCode;
                    command.Parameters.Add("@OPCodeSuffix", SqlDbType.VarChar).Value = opCodeSuffix;


                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();


                }
            }
            catch (Exception err)
            {


            }





        }

        /// <summary>
        /// SendUpdateReportCommand Sends command to the msgqueue/outbound, updates device status table, inserts into the useractivity table
        /// Version: 1.0.0.0
        /// Created: 12/02/2010
        /// Modified: 10/21/2011
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="deviceID"></param>
        /// <param name="userID"></param>
        /// <param name="opCode"></param>
        /// <param name="opCodeSuffix"></param>
        /// <returns></returns>
        public static int SendUpdateReportCommand(int deviceID, string userID, string opCode, string opCodeSuffix)
        {
            //to get the dbenum
            string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString().Replace("NHKWCConnectionString", "");
            int DBEnum = 3; //default is development

            if (database == "Test") DBEnum = 0; //test

            else if (database == "Demo") DBEnum = 1; //demo

            else if (database == "Production") DBEnum = 2; //prod           

            else if (database == "WebSense") DBEnum = 1; //prod           


            DateTime utcNow = DateTime.UtcNow;
            string utcString = UTCString(utcNow);
            


            NHDevice device = new NHDevice(deviceID);

            //so it will work with already changed code
            if (!opCode.Contains("MD") || opCode.Length == 2)
            {
                NHSynapse Synapse = new NHSynapse(device);

                opCode = Synapse.MD + opCode;

                opCodeSuffix = opCodeSuffix == null ? Synapse.LeafAddress : opCodeSuffix + Synapse.LeafAddress;
            }

            string opCodeOrig = opCode;

            

            //check device type to decide to format OPCode for legacy products
            //legacy/Oneway 
            if (!device.TwoWay)
            {
                /*
      
                    case "Connect":
                        Cmd = String.Format("{0}2", ACode);
                        break;

                    case "Disconnect":
                        Cmd = String.Format("{0}1", ACode);
                        break;

                    case "Arm":
                        Cmd = String.Format("{0}0", ACode);
                        break;

                    case "30/30 Mode":
                        Cmd = String.Format("{0}3", ACode);
                        break;
     
                 NH800
                    1------1yzabb
                     ACode (1 + 6 digit serial number +1 )

                    last 3 digits(abb) are optional.

                    y - command 0(on),1(off)
                    z - output  (0 - 8), 0 is for all the outputs
                    a - increment (0 permanent, 1 seconds, 2 minutes, 3 hours, 4 days)
                    b - time value (01-99)
                

                case "On":
                    Cmd = ACode + "0" + Output; // Turn all outputs/a output on using serial number 
                    // 1 + 6 digit serial number + 1 + on/off digit + relay digit
                    break;

                case "Off":
                    Cmd = ACode + "1" + Output; // Turn all outputs/a output off using serial number 
                    break;

                case "Reboot":
                    
                    // Cmd = "3000000"; // simulate a reboot command
                    Cmd = ACode + "1" + Output + getTimeInterval(Output); 
                // Simulate reboot command for a output or all outputs
                    //Cmd = String.Format("{0}10110", ACode); // reboot for 10 seconds
                    break;

    */

                int ACode = device.AccessCode;

                //nh800
                opCode = opCode.Replace("ON", "0").Replace("OFF", "1").Replace("REBOOT", "1"); //this will give us on/off/reboot bit + output

                //opCodeSuffix will have the timeincrement bits
                //if (opCode.Contains("REBOOT"))
                // {
                //   OPOut += "110";
                // }

                //ceo700 
                opCode = opCode.Replace("AR", "0").Replace("Op", "1").Replace("Cl", "2").Replace("Cy", "3"); //this will give us AR(0)/Op(1)/Cl(2)/3030(3)

                opCode = ACode + opCode;
            }


            string result = SendCommandMSMQ(deviceID, userID, opCode, opCodeSuffix, utcString, DBEnum);

            if (result.CompareTo("Failed") == 0) { return 99; } //failed to send a message to the outbound queue
            else
            {
                if (device.DeviceType == 8)
                {
                    UpdateDeviceStatusNH800(deviceID, opCode, opCodeSuffix, utcString);
                }
                else
                {
                    UpdateDeviceStatus(deviceID, opCodeOrig, opCodeSuffix, utcNow);
                }

                //MArefin 5/3/2012
                //AddUserActivity(deviceID, userID, opCodeOrig, "SENDING", utcNow, 0);

                NHDeviceActivity InsertSending = new NHDeviceActivity();

                InsertSending.DeviceID = deviceID;
                InsertSending.GatewayMessage = "WebConnect to MessageQueue";
                InsertSending.Protocol = device.Protocol;
                InsertSending.NetworkID = device.Network;
                InsertSending.Command = new NHDeviceControl(opCodeOrig.ToUpper().Replace("MD", "")).ControlName; //for mesh md
                InsertSending.CommandStatus = "SENDING";
                InsertSending.ReportTime = DateTime.UtcNow;
                InsertSending.StatusReportTime = DateTime.UtcNow;
                InsertSending.UserID = userID;

                AddUserActivity(InsertSending);
            }

            return 0;

        }




        //method to support the old strurcture        
        public static DataTable CheckOutBound()
        {
            //store porc doesn't exist in new stucture
            DataTable dt = new DataTable("OutBoundMsgs");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    // Check for OutBound Message that need to be sent to Jasper using API
                    //
                    SqlCommand command = new SqlCommand("sp_NH_CheckNumerexOutBound", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);
                }
            }
            catch
            {


            }

            return dt;


        }

        //method to support the old strurcture
        public static string UpdateOutBound(string status, int msgID, int iTranID)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString());
            try
            {

                using (conn)
                {
                    string updateOutBoundTbl = "UPDATE OutBound SET Status = '" + status + "', iTranID = '" + iTranID + "' WHERE MessageID = '" + msgID + "'";

                    SqlCommand command = new SqlCommand(updateOutBoundTbl, conn);
                    command.CommandType = CommandType.Text;

                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();

                    if (rows == 0)
                    {
                        return "Row Not Found for " + msgID + " in OutBound Queue for Database[" + conn.Database + "]";
                    }
                    else
                    {
                        return "Success";
                    }
                }
            }
            catch (Exception err)
            {
                return "Error Updating OutBound Database [" + conn.Database + "] Status:" + err.Message;
            }

        }



        public static DataSet GetAllTOUDataByRow(int DeviceID, int row, int count)
        {
            DataSet ds = new DataSet("TOUDataSet");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_TimeOfUseDataByRow", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = DeviceID;
                command.Parameters.Add("@Row", SqlDbType.Int).Value = row;
                command.Parameters.Add("@Count", SqlDbType.Int).Value = count;
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(ds, "TOUDataTable");

            }

            return ds;
        }


        /// <summary>
        /// GetAllTOUDataByDateTime is to retrieve interval data from the StartDT to EndDT
        /// Author: Moshiul Arefin
        /// Date: 3/27/2012
        /// </summary>
        /// <param name="DeviceID"></param>
        /// <param name="StartDT"></param>
        /// <param name="EndDT"></param>
        /// <returns></returns>
        public static DataSet GetAllTOUDataByDateTime(int DeviceID, DateTime StartDT, DateTime EndDT)
        {
            DataSet ds = new DataSet("TOUDataSet");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_TimeOfUseDataByDateTime", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = DeviceID;
                command.Parameters.Add("@StartDT", SqlDbType.DateTime).Value = StartDT;
                command.Parameters.Add("@EndDT", SqlDbType.DateTime).Value = EndDT;
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(ds, "TOUDataTable");

            }

            return ds;
        }





        //created for NH API
        public static DataSet GetAllDeviceData(string clientID)
        {
            DataSet ds = new DataSet("DeviceData");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetAllDeviceData", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = clientID;
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(ds, "AllDevices");

            }

            return ds;
        }

        public static DataSet GetAllDeviceDataByRow(string clientID, int row, int count)
        {
            DataSet ds = new DataSet("CurrentDeviceData");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetAllDeviceDataByRow", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = clientID;
                command.Parameters.Add("@Row", SqlDbType.Int).Value = row;
                command.Parameters.Add("@Count", SqlDbType.Int).Value = count;
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(ds, "AllDevices");

            }

            return ds;
        }

        public static DataSet GetCurrentDeviceData(int deviceID)
        {
            DataSet ds = new DataSet("DeviceData");
            
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(ds, "Device");
            }

            return ds;            

        }


        /// <summary>
        /// Insert ERTID as in-active into devices_ert table.
        /// Version: 1.0.0.0
        /// Created: 10/12/2015
        /// Author : Nanda 
        /// </summary>
        public static void AddERTDeviceAsInactive(int ERTID, string ClientID, NHError error)
        {           
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Devices_AddERTDeviceAsInactive", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@ERTID", SqlDbType.VarChar).Value = ERTID;
                command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;

                conn.Open();

                int result = command.ExecuteNonQuery();

                conn.Close();

                if (result == 0)
                {

                    error.ErrorCode = 01020891;
                    error.ErrorMsg = "Stored procedure[sp_NH_Devices_AddERTDeviceAsInactive] failed to insert database tables for the ERTID[" + ERTID + "]";
                }
                else if (result > 0)
                {
                    error.ErrorCode = 0;
                    error.ErrorMsg = "";
                }
            }        

        }

        /// <summary>
        /// Used to get the client and ERTID's which are in database
        /// Version: 1.0.0.0
        /// Created: 5/22/2015
        /// Author : Sivakumar Kandasamy 
        /// </summary>
        public static DataSet GetAllERTIDByClientId(string clientID)
        {
            DataSet ds = new DataSet("CurrentDeviceData");

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetERTIDFromClientID", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = clientID;
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                adapter.Fill(ds, "AllDevices");

            }

            return ds;
        }

        //created for NH API
        //lookup status of a message in the outbound queue with msgNumber
        public static string CheckMessageStatus(int msgNumber)
        {
            string status = "";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetMessageStatus", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@MsgNumber", SqlDbType.VarChar).Value = msgNumber;
                SqlDataReader myReader = null;
                conn.Open();
                myReader = command.ExecuteReader();
                while (myReader.Read())
                {
                    status = myReader["Status"].ToString();

                }
                conn.Close();
            }

            return status;

        }

        //created for NH API
        //lookup status of a message in the outbound queue with msgNumber
        public static string CheckMessageStatusMSMQ(int DeviceID)
        {
            string status = "";
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = DeviceID;
                SqlDataReader myReader = null;
                conn.Open();
                myReader = command.ExecuteReader();
                while (myReader.Read())
                {
                    status = myReader["CommandStatus"].ToString();

                }
                conn.Close();
            }
            DataSet ds = new DataSet("OutBoundStatus");

            StringWriter wrkString = new StringWriter();

            XmlTextWriter xmlVq = new XmlTextWriter(wrkString);
            xmlVq.Formatting = Formatting.Indented;

            xmlVq.WriteStartDocument();
            xmlVq.WriteDocType("CommandStatus", null, null, null);
            xmlVq.WriteStartElement("OutBoundStatus");
            xmlVq.WriteAttributeString("OutBoundStatus", status);
            xmlVq.WriteEndElement();

            string tmp = wrkString.ToString();

            tmp = tmp.Replace("utf-16", "utf-8");
            xmlVq.Close();
            wrkString.Close();
            StringReader xmlSR = new System.IO.StringReader(tmp);
            ds.ReadXml(xmlSR, XmlReadMode.Auto);
            return status;

        }








        //not used 
        public static bool PrePayType(string esn)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Esn", SqlDbType.VarChar).Value = esn;
                SqlDataReader myReader = null;
                conn.Open();
                myReader = command.ExecuteReader();
                while (myReader.Read())
                {
                    if (myReader["PrePay"] == DBNull.Value)
                    {
                        conn.Close();
                        return false;
                    }
                    else if ((bool)myReader["PrePay"])
                    {
                        conn.Close();
                        return true;
                    }


                }
                conn.Close();
            }

            return false;
        }

        //not used
        //this function returns true if the device is utility and flase if the devie is nonUtility
        public static bool IsUtilityDevice(string esn)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Esn", SqlDbType.VarChar).Value = esn;
                SqlDataReader myReader = null;
                conn.Open();
                myReader = command.ExecuteReader();
                while (myReader.Read())
                {

                    if ((bool)myReader["UtilityDevice"])
                    {
                        conn.Close();
                        return true;
                    }

                    else
                    {
                        conn.Close();
                        return false;
                    }
                }
                conn.Close();
            }

            return false;
        }

        //
        public static bool IsNH800(string esn)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
            {
                SqlCommand command = new SqlCommand("sp_NH_Device_GetDevice", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@Esn", SqlDbType.VarChar).Value = esn;
                SqlDataReader myReader = null;
                conn.Open();
                myReader = command.ExecuteReader();
                while (myReader.Read())
                {

                    if ((int)myReader["DeviceType"] == 8)
                    {
                        conn.Close();
                        return true;
                    }

                    else
                    {
                        conn.Close();
                        return false;
                    }
                }
                conn.Close();
            }

            return false;
        }






        // this function return current UTC date time as a string
        public static string UTCString(DateTime d)
        {
            //DateTime d = DateTime.UtcNow;
            string utcString = (d.Month) + "/";
            utcString += d.Day + "/";
            utcString += d.Year + " ";

            utcString += d.Hour + ":";
            utcString += d.Minute + ":";
            utcString += d.Second;

            return utcString;

        }




        /// <summary>
        /// GetDeviceControls is used by the WebService 
        /// Version: 1.0.0.0
        /// Created: 03/15/2011
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="deviceID"></param>
        /// <returns></returns>
        public static DataSet GetDeviceControls(int deviceID)
        {
            string tmp = "";
            StringReader xmlSR;
            DataSet ds = new DataSet("Commands");
            StringWriter wrkString = new StringWriter();

            XmlTextWriter xmlVq = new XmlTextWriter(wrkString);
            xmlVq.Formatting = Formatting.Indented;

            //get all the opcodes available for this device
            NHDevice device = new NHDevice(deviceID);

            //load the options in a datatable to avoid multiple calls to the database
            NHDeviceControl control = new NHDeviceControl();
            control.Command = "SELECT"; //this will select all the control options
            DataTable dtOptions = ManageControls(control);

            string[] controls = device.OPCodes.Trim().Split(',');                //DeviceCommands(deviceID).Split(',');

            int count = controls.Length;

            //first look up number of commands available
            xmlVq.WriteStartDocument();
            xmlVq.WriteDocType("Commands", null, null, null);
            xmlVq.WriteStartElement("Commands");
            xmlVq.WriteAttributeString("Count", count.ToString());
            xmlVq.WriteAttributeString("DeviceType", device.DeviceType.ToString());

            int cmdNo = 0;//is zero indexed so increment after building xml for an OPCode
            //go to each row in the datatabe
            foreach (DataRow row in dtOptions.Rows)
            {
                //check if the current row or the control is one of the control supported by this device
                if (controls.Contains(row["OPCode"]))
                {
                    control.ControlName = row["ControlName"].ToString();
                    control.OPCode = row["OPCode"].ToString();
                    control.Options = row["Options"].ToString();
                    control.ControlOrder = Convert.ToInt32(row["ControlOrder"]);

                    string[] options = GetDeviceControlOptions(control, cmdNo, device.DeviceType.ToString(), deviceID);

                    xmlVq.WriteRaw(options[1]);

                    cmdNo += 1;
                }

            }

            xmlVq.WriteEndElement();

            tmp = wrkString.ToString();

            tmp = tmp.Replace("utf-16", "utf-8");
            xmlVq.Close();
            wrkString.Close();
            xmlSR = new System.IO.StringReader(tmp);
            ds.ReadXml(xmlSR, XmlReadMode.Auto);

            return ds;

        }




        /// <summary>
        /// GetDeviceControlOptions will parse control options and return readable string and a XML string
        /// Version: 1.0.0.0
        /// Created: 10/04/2011
        /// Author : Moshiul Arefin
        /// Modified: 11/14/2011 by MArefin to support NH800 and legacy product support
        /// </summary>
        /// <param name="Control"></param>
        /// <param name="CmdNo">if it gets called by a function that's parsing multiple command</param>
        /// <param name="DeviceType"></param>
        /// <param name="deviceID"></param>
        /// <returns></returns>
        public static string[] GetDeviceControlOptions(NHDeviceControl Control, int CmdNo, string DeviceType, int deviceID)
        {
            //devictype & deviceID will let us know if it's getting called by superadmin module or by actual devicetype
            //ALL & 0 for super admin
            string returnString = "";
            //string returnXML = "";

            int optionNo = 0;// we need to keep a count of the options that the devicetype supports


            StringWriter wrkString = new StringWriter();
            XmlTextWriter xmlVq = new XmlTextWriter(wrkString);
            xmlVq.Formatting = Formatting.Indented;
            xmlVq.WriteStartElement("Cmd" + CmdNo);
            xmlVq.WriteAttributeString("Label", Control.ControlName);

            //modified on 11/14/2011 by MArefin 
            if (DeviceType == "ALL" & deviceID == 0)
            {
                if (Control.ControlOrder >= 800)
                {
                    //nh800
                    xmlVq.WriteAttributeString("Value", "[8 digit AccessCode] + [1 digit on(0)/off(1)/reboot(1)] + [1 digit port 0(all), 1 to 8]");

                }
                else
                {
                    //not nh800 
                    //xmlVq.WriteAttributeString("Value", "[4 digit AccessCode] + [1 digit AR(0)/Op(1)/Cl(2)/3030(3)] + ");
                    //this also includes some paging devices
                    if (Control.OPCode == "Cl" || Control.OPCode == "Op" || Control.OPCode == "AR" || Control.OPCode == "Cy")
                    {
                        xmlVq.WriteAttributeString("Value", "Cellular Devices: [" + Control.OPCode + "], Paging devices: [4 digit AccessCode] + [1 digit AR(0)/Op(1)/Cl(2)/3030(3)]");
                    }
                    else
                    {
                        xmlVq.WriteAttributeString("Value", Control.OPCode);
                    }
                }

            }
            //called by API 
            else
            {
                //cellular devices
                if (Convert.ToInt32(DeviceType) > 8)
                {
                    xmlVq.WriteAttributeString("Value", Control.OPCode);
                }
                //otherwise it's legacy paging devices
                else
                {

                    NHDevice Device = new NHDevice(deviceID);
                    if (Control.ControlOrder >= 800)
                    {
                        //nh800
                        string OPOut = Control.OPCode.Replace("ON", "0").Replace("OFF", "1").Replace("REBOOT", "1"); //this will give us on/off/reboot bit + output
                        if (Control.OPCode.Contains("REBOOT"))
                        {
                            OPOut += "110";
                        }

                        xmlVq.WriteAttributeString("Value", Device.AccessCode + OPOut);

                    }
                    else
                    {
                        //not nh800 but old paging devices
                        string newOPCode = Control.OPCode.Replace("AR", "0").Replace("Op", "1").Replace("Cl", "2").Replace("Cy", "3"); //this will give us AR(0)/Op(1)/Cl(2)/3030(3)
                        xmlVq.WriteAttributeString("Value", Device.AccessCode + newOPCode);
                    }


                }
            }




            if (Control.Options == null || Control.Options == "")
            {
                xmlVq.WriteAttributeString("Options", "None");
                returnString += "None";
            }
            else
            {
                if (DeviceType == "ALL")
                {
                    xmlVq.WriteAttributeString("Options", "Yes");
                }
                //option1[lbl1:val1,lbl2:val2,lbl{10-20}{inc}:{10-20}{inc}](DeviceType:20,26),option2[lbl1:val1](DeviceType:all)
                //start here

                //separate char ),
                string[] optionArray = Regex.Split(Control.Options, @"\),");
                //option1[lbl1:val1,lbl2:val2,lbl{10-20}{inc}:{10-20}{inc}](DeviceType:20,26
                //option2[lbl1:val1](DeviceType:all)

                for (int i = 0; i < optionArray.Length; i++)
                {
                    //separate char [    
                    //remove ) from the end
                    //option1[lbl1:val1,lbl2:val2,lbl{10-20}{inc}:{10-20}{inc}](DeviceType:20,26
                    //option2[lbl1:val1](DeviceType:all)
                    string[] options = optionArray[i].Trim(')').Split('[');

                    //option1                 
                    //lbl1:val1,lbl2:val2,lbl{10-20}{inc}:{10-20}{inc}](DeviceType:20,26
                    //option2
                    //lbl1:val1](DeviceType:all

                    string optionName = options[0];
                    //option1
                    //option2
                    returnString += "Option" + i.ToString() + ": " + optionName + "<br>";
                    //xmlVq.WriteAttributeString("Option" + i.ToString(), options[0]);

                    //separate char ](                 
                    //lbl1:val1,lbl2:val2,lbl{10-20}{inc}:{10-20}{inc}](DeviceType:20,26
                    //lbl1:val1](DeviceType:all
                    string[] parameterArray = Regex.Split(options[1], @"]\(");
                    //lbl1:val1,lbl2:val2,lbl{10-20}{inc}:{10-20}{inc}
                    //DeviceType:20,26
                    //lbl1:val1
                    //DeviceType:all  



                    //remove DeviceType:
                    //split by ,
                    string[] deviceTypesArray = parameterArray[1].Replace("DeviceTypes:", "").Trim().Split(',');
                    //20
                    //26
                    //for option 2
                    //all 


                    //check if the current device type is in the list if not for all devices
                    if (DeviceType == "ALL")
                    {
                        xmlVq.WriteAttributeString("Option" + i.ToString(), optionName);
                    }
                    else
                    {
                        for (int x = 0; x < deviceTypesArray.Length; x++)
                        {
                            //if the devicetype sent matches with the devicetype that's in the parameter/option string
                            //then build xml
                            //if devicetype is in the list or all is in the list
                            if (deviceTypesArray[x] == DeviceType || deviceTypesArray[x] == "ALL")
                            {
                                if (optionNo == 0)
                                {
                                    xmlVq.WriteAttributeString("Options", "Yes");
                                }

                                xmlVq.WriteAttributeString("Option" + optionNo.ToString(), optionName);
                                optionNo += 1;

                                break;
                            }

                        }
                    }


                    string[] parameters = parameterArray[0].Split(',');
                    //lbl1:val1
                    //lbl2:val2
                    //lbl{10-20}{inc}:{10-20}{inc}

                    //for option2
                    //lbl1:val1

                    for (int j = 0; j < parameters.Length; j++)
                    {
                        //look for { 
                        if (parameters[j].Contains("}:{"))
                        {
                            //lbl{10-20}{inc}:{10-20}{inc}

                            //separate }:{
                            string[] parValue = Regex.Split(parameters[j], "}:{");
                            //lbl{10-20}{inc
                            //10-20}{inc}

                            //separate }{
                            string[] parNameInc = Regex.Split(parValue[0], "}{");
                            //lbl{10-20
                            //inc

                            string[] parName = parNameInc[0].Split('{');
                            //lbl
                            //10-20
                            string name = parName[0];

                            string[] nameInc = parName[1].Split('-');
                            //10
                            //20
                            int startNameCount = Convert.ToInt32(nameInc[0]);
                            int endNameCount = Convert.ToInt32(nameInc[1]);
                            int nameCountInc = Convert.ToInt32(parNameInc[1]);


                            string[] parValInc = Regex.Split(parValue[1], "}{");
                            //10-20
                            //inc}

                            string[] parVal = parValInc[0].Split('-');
                            //10
                            //20
                            int startValCount = Convert.ToInt32(parVal[0]);
                            int endValCount = Convert.ToInt32(parVal[1]);
                            int valCountInc = Convert.ToInt32(parValInc[1].Trim('}'));


                            returnString += "Parameter" + j.ToString() + " Name: " + name + startNameCount.ToString().PadLeft(2, '0') + " to " + name + endNameCount.ToString().PadLeft(2, '0') + " Incremetnted by " + nameCountInc + "<br>";
                            returnString += "Parameter" + j.ToString() + " Value: " + startValCount.ToString().PadLeft(2, '0') + " to " + endValCount.ToString().PadLeft(2, '0') + " Incremetnted by " + valCountInc + "<br>";

                            if (DeviceType == "ALL")
                            {
                                int y = startValCount;
                                for (int z = startNameCount; z <= endNameCount; z += nameCountInc)
                                {
                                    //xmlVq.WriteAttributeString(XmlConvert.EncodeLocalName(values[0] + z.ToString().PadLeft(2, '0')), z.ToString().PadLeft(2, '0'));

                                    xmlVq.WriteAttributeString(XmlConvert.EncodeLocalName(name + z.ToString().PadLeft(2, '0')), y.ToString().PadLeft(2, '0'));
                                    y += valCountInc;

                                }
                            }
                            else
                            {
                                for (int x = 0; x < deviceTypesArray.Length; x++)
                                {
                                    //if the devicetype sent matches with the devicetype that's in the parameter/option string
                                    //then build xml
                                    if (deviceTypesArray[x] == DeviceType || deviceTypesArray[x] == "ALL")
                                    {
                                        int y = startValCount;
                                        for (int z = startNameCount; z <= endNameCount; z += nameCountInc)
                                        {
                                            //xmlVq.WriteAttributeString(XmlConvert.EncodeLocalName(values[0] + z.ToString().PadLeft(2, '0')), z.ToString().PadLeft(2, '0'));

                                            xmlVq.WriteAttributeString(XmlConvert.EncodeLocalName(name + z.ToString().PadLeft(2, '0')), y.ToString().PadLeft(2, '0'));
                                            y += valCountInc;

                                        }

                                        break;
                                    }
                                }


                            }

                        }

                        else
                        {
                            //lbl1:val1
                            //lbl2:val2                        

                            //for option2
                            //lbl1:val1

                            //separate by : to get option and the value

                            string[] parValue = parameters[j].Split(':');
                            //lbl1
                            //val1
                            //lbl2
                            //val2                        

                            //for option2
                            //lbl1
                            //val1


                            string parName = parValue[0];
                            //lbl1                        
                            //lbl2

                            //for option2
                            //lbl1

                            string parVal = parValue[1];
                            //val1
                            //val2                        

                            //for option2
                            //val1


                            returnString += "Parameter" + j.ToString() + " Name: " + parName + "<br>";
                            returnString += "Parameter" + j.ToString() + " Value: " + parVal + "<br>";

                            if (DeviceType == "ALL")
                            {
                                xmlVq.WriteAttributeString(XmlConvert.EncodeLocalName(parName), parVal);
                            }
                            else
                            {
                                for (int x = 0; x < deviceTypesArray.Length; x++)
                                {
                                    //if the devicetype sent matches with the devicetype that's in the parameter/option string
                                    //then build xml
                                    if (deviceTypesArray[x] == DeviceType || deviceTypesArray[x] == "ALL")
                                    {
                                        xmlVq.WriteAttributeString(XmlConvert.EncodeLocalName(parName), parVal);
                                        break;
                                    }
                                }
                            }
                        }


                    }


                    string deviceTypes = parameterArray[1];

                    returnString += "Option available to " + deviceTypes.Replace(":", ": ").Replace(",", ", ") + "<br>";

                }

                if (DeviceType == "ALL")
                {
                    xmlVq.WriteAttributeString("OptionEnd", "End");
                }
                else
                {
                    if (optionNo > 0)
                    {
                        xmlVq.WriteAttributeString("OptionEnd", "End");
                    }
                    else
                    {
                        xmlVq.WriteAttributeString("Options", "None");

                    }

                }

                /*
                
                if (DeviceType == "ALL" || optionNo > 0)
                {
                    xmlVq.WriteAttributeString("OptionEnd", "End");
                }

                if (optionNo == 0)
                {
                    xmlVq.WriteAttributeString("Options", "None");
                }
                 * */
            }

            xmlVq.WriteEndElement();
            xmlVq.Close();
            wrkString.Close();

            string[] returnArray = { returnString, wrkString.ToString() };

            return returnArray;
        }





        /// <summary>
        /// GetOptionCount will be used in editcontrol page in wc
        /// Version: 1.0.0.0
        /// Created: 10/04/2011
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="OptionString"></param>
        /// <returns></returns>
        public static int GetOptionCount(string OptionString)
        {
            ///option1[lbl1:val1,lbl2:val2,lbl{10-20}{inc}:{10-20}{inc}](DeviceType:20,26),option2[lbl1:val1](DeviceType:all)
            //start here

            //separate char ),
            string[] optionArray = Regex.Split(OptionString, @"\),");
            //option1[lbl1:val1,lbl2:val2,lbl{10-20}{inc}:{10-20}{inc}](DeviceType:20,26
            //option2[lbl1:val1](DeviceType:all)

            return optionArray.Length;
        }


        /// <summary>
        /// ManageControl allows nighthawk super user to add/edit/delete/select available controls
        /// Version: 1.0.0.0
        /// Created: 10/04/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="Control"></param>
        /// <param name="OpCode"></param>
        /// <param name="Options"></param>
        /// <param name="Description"></param>
        /// <param name="Command">tsql select, insert, update, delete</param>
        public static DataTable ManageControls(NHDeviceControl Control)
        {
            //add OpCode, Options and description to the database

            DataTable dt = new DataTable("Controls");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_ManageControls", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@OpCode", SqlDbType.VarChar).Value = Control.OPCode;
                    command.Parameters.Add("@ControlOrder", SqlDbType.VarChar).Value = Control.ControlOrder;
                    command.Parameters.Add("@ControlName", SqlDbType.VarChar).Value = Control.ControlName;
                    command.Parameters.Add("@Options", SqlDbType.VarChar).Value = Control.Options;
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = Control.Description;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = Control.Command;

                    if (Control.Command == "SELECT")
                    {
                        if (Control.OPCode == null)
                        {
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(dt);

                            if (dt.Rows.Count <= 0)
                            {
                                Control.ErrorCode = 04020191;
                                Control.ErrorMsg = "Stored Procedure [sp_NH_Device_Manage_Controls] returned zero record";
                            }
                        }
                        //to select one OPCode
                        else
                        {
                            SqlDataReader myReader = null;
                            conn.Open();
                            myReader = command.ExecuteReader();

                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    Control.OPCode = myReader["OPCode"].ToString();
                                    Control.ControlOrder = Convert.ToInt32(myReader["ControlOrder"]);
                                    Control.ControlName = myReader["ControlName"].ToString();
                                    Control.Options = myReader["Options"].ToString();
                                    Control.Description = myReader["Description"].ToString();
                                }
                            }
                            else
                            {
                                Control.ErrorCode = 04020192;
                                Control.ErrorMsg = "Stored Procedure [sp_NH_Device_ManageControls] returned zero record";

                            }
                            conn.Close();

                        }

                    }

                    else //insert, update, delete
                    {
                        conn.Open();

                        int rows = command.ExecuteNonQuery();

                        conn.Close();

                        if (rows == -1)
                        {
                            Control.ErrorCode = 04020193;
                            Control.ErrorMsg = "Stored Procedure [sp_NH_Device_ManageControls] failed to update database tables for the OPCode[" + Control.OPCode + "]";
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception err)
            {
                Control.ErrorCode = 04020199;
                Control.ErrorMsg = err.Message;
            }

            return dt;
        }


        /// <summary>
        /// AddOption is used by WebConnect SuperAdmin GUI to conviently add options when a new command gets added to the device
        /// Version: 1.0.0.0
        /// Created: 09/30/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="OptionName">Name of new the option to be added</param>
        /// <param name="OptionString">Database OptionsString that is currently getting build and OptionName needs to get added to it</param>
        /// <returns></returns>
        public static string AddOption(string OptionName, string OptionString)
        {
            if (OptionString == "")
            {
                OptionString = OptionName + "[]";
            }
            else
            {
                OptionString += "," + OptionName + "[]";
            }

            return OptionString;

        }



        /// <summary>
        /// AddDeviceType is used to add devicetypes to the working string
        /// Version: 1.0.0.0
        /// Created: 09/30/2011
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="DeviceType">DeviceTypes that this option is going to be available to</param>
        /// <param name="OptionString">Database OptionsString that is currently getting build and parameters needs to get added to this string</param>
        /// <returns></returns>
        public static string AddDeviceType(string DeviceType, string OptionString)
        {
            //devicetype will always get added to the end of the working option string
            //first decide if it's the first time devicetype is getting added to this string
            //if the last ] is the last char of the string then this is the first time
            int i = OptionString.LastIndexOf("]");

            if ((OptionString.Length - 1) == i)
            {
                //first time
                OptionString += "(DeviceTypes:" + DeviceType + ")";
            }
            else
            {
                //remove the last ) from the string
                OptionString = OptionString.Remove(OptionString.LastIndexOf(")"));

                //add the another devicetype to this working string
                OptionString += "," + DeviceType + ")";

            }

            return OptionString;
        }





        /// <summary>
        /// AddParamater is used by WebConnect SuperAdmin GUI to conviently add parameters to a options when a new command gets added to the device
        /// Version: 1.0.0.0
        /// Created: 09/30/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="Parameter"></param>
        /// <param name="OptionString">Database OptionsString that is currently getting build and parameters needs to get added to this string</param>
        /// <returns></returns>
        public static string AddParameter(NHControlParamater Parameter, string OptionString)
        {
            //paramter can be fixed or variable so it can be convinently created stored or parsed 
            //fixed for example: parm11:val11
            //variable for example: parm1{0-n}{inc}:val1{0-n}{inc} during execution it will be parsed as 
            //parm10:val10, parm1(0+inc):val1(0+inc),..., parm1n:val1n
            string parameterString = "";

            //create paramater string
            if (Parameter.Type.ToUpper() == "VARIABLE")
            {
                parameterString = Parameter.Name + "{" + Parameter.NameMin + "-" + Parameter.NameMax + "}{" + Parameter.NameIncrement + "}:{"
                                + Parameter.ValMin + "-" + Parameter.ValMax + "}{" + Parameter.ValIncrement + "}";

                //parname{namemin-namemax}{nameinc}:{valmin-valmax}{valinc}

            }
            else
            {
                parameterString = Parameter.Name + ":" + Parameter.Value;

                //parname:value
            }

            //now add this paramater string to the OptionString            

            //if this is the first parameter then replace "[]" with "[" + paramater string + "]"
            if (OptionString.Contains("[]"))
            {
                OptionString = OptionString.Replace("[]", "[" + parameterString + "]");
            }
            //replace last "]" with "," + parameter string + "]"
            else
            {
                int i = OptionString.LastIndexOf("]");

                OptionString = OptionString.Remove(i);

                OptionString = OptionString + "," + parameterString + "]";
            }

            return OptionString;

        }




        /// <summary>
        /// ManageDeviceTypes is used by WebConnect SuperAdmin GUI to conviently add/edit/delete device types
        /// Version: 1.0.0.0
        /// Created: 09/30/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="DeviceType"></param>
        /// <returns></returns>
        public static DataTable ManageDeviceTypes(NHDeviceType DeviceType)
        {
            DataTable dt = new DataTable("DeviceTypes");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_ManageDeviceTypes", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceType", SqlDbType.Int).Value = DeviceType.DeviceType;
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = DeviceType.Description;
                    command.Parameters.Add("@Controls", SqlDbType.VarChar).Value = DeviceType.Controls;
                    command.Parameters.Add("@MsgPrefix", SqlDbType.VarChar).Value = DeviceType.MsgPrefix;
                    command.Parameters.Add("@TwoWay", SqlDbType.Bit).Value = DeviceType.TwoWay;
                    command.Parameters.Add("@Features", SqlDbType.VarChar).Value = DeviceType.Features;
                    command.Parameters.Add("@PrePay", SqlDbType.Bit).Value = DeviceType.PrePay;
                    command.Parameters.Add("@UtilityDevice", SqlDbType.Bit).Value = DeviceType.UtilityDevice;
                    command.Parameters.Add("@Comment", SqlDbType.VarChar).Value = DeviceType.Comment;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = DeviceType.Command;

                    if (DeviceType.Command == "SELECT")
                    {
                        //to select one DeviceType
                        if (DeviceType.DeviceType >= 1)
                        {
                            SqlDataReader myReader = null;
                            conn.Open();
                            myReader = command.ExecuteReader();

                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {

                                    // DeviceType.DeviceType =  myReader["DeviceType"].ToString();
                                    DeviceType.Description = myReader["Description"].ToString();
                                    DeviceType.Controls = myReader["Commands"].ToString();
                                    DeviceType.MsgPrefix = myReader["MsgPrefix"].ToString();
                                    DeviceType.TwoWay = (bool)myReader["TwoWay"];
                                    DeviceType.Features = myReader["Features"].ToString();
                                    DeviceType.PrePay = (bool)myReader["PrePay"];
                                    DeviceType.UtilityDevice = (bool)myReader["UtilityDevice"];
                                    DeviceType.Comment = myReader["Comment"].ToString();
                                }
                            }
                            else
                            {
                                DeviceType.ErrorCode = 04020291;
                                DeviceType.ErrorMsg = "Stored Procedure [sp_NH_Device_ManageDeviceTypes] returned zero record";

                            }
                            conn.Close();

                        }
                        else
                        {


                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(dt);

                            if (dt.Rows.Count <= 0)
                            {
                                DeviceType.ErrorCode = 04020292;
                                DeviceType.ErrorMsg = "Stored Procedure [sp_NH_Device_ManageDeviceTypes] returned zero record";
                            }
                        }

                    }

                    else //insert, update, delete
                    {
                        conn.Open();

                        int rows = command.ExecuteNonQuery();

                        conn.Close();

                        if (rows == 0)
                        {
                            DeviceType.ErrorCode = 04020293;
                            DeviceType.ErrorMsg = "Stored Procedure [sp_NH_Device_ManageDeviceTypes] failed to update database tables for the DeviceType[" + DeviceType.DeviceType + "]";
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception err)
            {
                DeviceType.ErrorCode = 04020299;
                DeviceType.ErrorMsg = err.Message;
            }

            return dt;

        }







        public static DataTable GetDeviceActivity(NHDevice Device)
        {
            DataTable dt = new DataTable("DeviceActivity");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDeviceActivity", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = Device.DeviceID;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Device.ErrorCode = 04021091;
                        Device.ErrorMsg = "Stored Procedure [sp_NH_Device_GetDeviceActivity] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04021099;
                Device.ErrorMsg = err.Message;
            }

            return dt;

        }





        public static DataTable GetRecentDevices(string ClientID, int topNum, NHError Error)
        {
            DataTable dt = new DataTable("RecentDevices");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetRecentDevices", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    command.Parameters.Add("@topNum", SqlDbType.VarChar).Value = topNum;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 92;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Device_GetRecentDevices] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 99;
                Error.ErrorMsg = err.Message;
            }

            return dt;

        }





        public static DataTable GetDeviceSearchResult(NHDevice Device)
        {
            DataTable dt = new DataTable("Devices");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_SearchInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = Device.ClientID;
                    command.Parameters.Add("@SerialNumber", SqlDbType.VarChar).Value = Device.Esn;
                    command.Parameters.Add("@MeterNumber", SqlDbType.VarChar).Value = Device.ItronMeterNumber;
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = Device.Description;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Device.ErrorCode = 92;
                        Device.ErrorMsg = "Stored Procedure [[sp_NH_Device_SearchInventory]] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 99;
                Device.ErrorMsg = err.Message;
            }

            return dt;

        }

        public static DataTable GetERTDeviceSearchResult(NHDevice Device)
        {
            DataTable dt = new DataTable("Devices");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_SearchERTInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = Device.ClientID;
                    command.Parameters.Add("@ErtId", SqlDbType.VarChar).Value = Device.ErtID;                   
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Device.ErrorCode = 92;
                        Device.ErrorMsg = "Stored Procedure [[sp_NH_Device_SearchInventory]] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 99;
                Device.ErrorMsg = err.Message;
            }

            return dt;

        }

        /// <summary>
        /// SearchDevice will be used to search devices either by esn/itronnumber/simnumber
        /// created for support purpose by nighthawk staff
        /// Created: 3/9/2012
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="SerialNumber"></param>
        /// <param name="MeterNumber"></param>
        /// <param name="SIMNumber"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static DataTable SearchDevice(string SerialNumber, string MeterNumber, string SIMNumber, string ErtID, NHError Error)
        {
            DataTable dt = new DataTable("Devices");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_SearchDevice_Admin", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@SerialNumber", SqlDbType.VarChar).Value = SerialNumber;
                    command.Parameters.Add("@MeterNumber", SqlDbType.VarChar).Value = MeterNumber;
                    command.Parameters.Add("@SIMNumber", SqlDbType.VarChar).Value = SIMNumber;
                    command.Parameters.Add("@ErtID", SqlDbType.VarChar).Value = ErtID;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 92;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Device_SearchDevice] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 99;
                Error.ErrorMsg = err.Message;
            }

            return dt;

        }

        /// <summary>
        /// SearchDevice will be used to search devices either by esn/itronnumber/simnumber
        /// created for support purpose by nighthawk staff
        /// Created: 3/9/2012
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="SerialNumber"></param>
        /// <param name="MeterNumber"></param>
        /// <param name="SIMNumber"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static DataTable SearchDevice(string SerialNumber, string MeterNumber, string SIMNumber, NHError Error)
        {
            DataTable dt = new DataTable("Devices");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_SearchDevice_Admin", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@SerialNumber", SqlDbType.VarChar).Value = SerialNumber;
                    command.Parameters.Add("@MeterNumber", SqlDbType.VarChar).Value = MeterNumber;
                    command.Parameters.Add("@SIMNumber", SqlDbType.VarChar).Value = SIMNumber;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 92;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Device_SearchDevice] returned zero record";
                    }

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 99;
                Error.ErrorMsg = err.Message;
            }

            return dt;

        }


        /// <summary>
        /// To import ERT devices
        /// Version: 1.0.0.0
        /// Created: 3/27/2015
        /// Author : Sivakumar
        /// </summary>
        /// <param ></param>
        public static void ImportERTDevice(String strXML, NHError Error)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_ImportERTDevice", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@xmlString", strXML);

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    if (rows == -1)
                    {
                        Error.ErrorCode = 04020601;
                        Error.ErrorMsg = "Unable to insert ERT devices into the system";
                    }

                    conn.Close();
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04020699;
                Error.ErrorMsg = err.Message;
            }


        }

        /// <summary>
        /// To import MVRS
        /// Version: 1.0.0.0
        /// Created: 07/09/2015
        /// Author : Venkatesh
        /// </summary>
        /// <param ></param>
        public static void ImportMVRS(String strXML, NHError Error)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_ImportMVRS", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@xmlString", strXML);

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    if (rows == -1)
                    {
                        Error.ErrorCode = 04020601;
                        Error.ErrorMsg = "Unable to insert MVRS details into the system";
                    }

                    conn.Close();
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04020699;
                Error.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// To import MVRS
        /// Version: 1.0.0.0
        /// Created: 07/09/2015
        /// Author : Venkatesh
        /// </summary>
        /// <param ></param>
        public static void ImportMVRSNonERTMeters(String strXML, NHError Error)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_ImportMVRSNonERTMeters", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@xmlString", strXML);

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    if (rows == -1)
                    {
                        Error.ErrorCode = 04020601;
                        Error.ErrorMsg = "Unable to insert MVRS details into the system";
                    }

                    conn.Close();
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04020699;
                Error.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// Get Device ID and Device Type based on ESN - FCS-MVRS Implementation
        /// Version: 1.0.0.0
        /// Created: 11/13/2015
        /// Author : Venkatesh
        /// </summary>
        /// <param ></param>
        public static DataTable GetDeviceIDByEsn(String strXML, NHError Error)
        {
            DataTable dtDevice = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetDeviceIDByEsn", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@xmlString", strXML);

                    conn.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dtDevice);

                    if (dtDevice.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 01;
                        Error.ErrorMsg = "Unable to find the device's";
                    }

                    conn.Close();
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04020699;
                Error.ErrorMsg = err.Message;
            }

            return dtDevice;
        }

        /// <summary>
        /// To import FCS-MVRS Readings
        /// Version: 1.0.0.0
        /// Created: 11/12/2015
        /// Author : Venkatesh
        /// </summary>
        /// <param ></param>
        public static void ImportFCSMVRSReadings(String strXML, NHError Error)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_ImportFCSMVRSReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@xmlString", strXML);
                    
                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    if (rows == -1)
                    {
                        Error.ErrorCode = 04020601;
                        Error.ErrorMsg = "Unable to insert readings into the system";
                    }

                    conn.Close();
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04020699;
                Error.ErrorMsg = err.Message;
            }
        }        

        /// <summary>
        /// Version: 1.0.0.0
        /// Created: 11/18/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="Device"></param>
        public static void ImportDevice(NHDevice Device)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_ImportDevice", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = Device.ClientID;
                    command.Parameters.Add("@Phone", SqlDbType.VarChar).Value = Device.Phone;
                    command.Parameters.Add("@AccessCode", SqlDbType.Int).Value = Device.AccessCode;
                    command.Parameters.Add("@Esn", SqlDbType.VarChar).Value = Device.Esn;
                    command.Parameters.Add("@ItronMeterNumber", SqlDbType.VarChar).Value = Device.ItronMeterNumber;
                    command.Parameters.Add("@Network", SqlDbType.Int).Value = Device.Network;
                    command.Parameters.Add("@DeviceType", SqlDbType.Int).Value = Device.DeviceType;
                    command.Parameters.Add("@CapCode", SqlDbType.VarChar).Value = Device.CapCode;
                    command.Parameters.Add("@Frequency", SqlDbType.VarChar).Value = Device.Frequency;
                    command.Parameters.Add("@Iccid", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@ErtID", SqlDbType.VarChar).Value = Device.ErtID;
                    //MARefin 5/16/2012
                    command.Parameters.Add("@FirmwareVersion", SqlDbType.VarChar).Value = Device.FirmwareVersion;
                    command.Parameters.Add("@HardwareVersion", SqlDbType.VarChar).Value = Device.HardwareVersion;
                    command.Parameters.Add("@ModemFirmwareVersion", SqlDbType.VarChar).Value = Device.ModemFirmwareVersion;

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    conn.Close();

                    if (rows == -1)
                    {
                        Device.ErrorCode = 04020601;
                        Device.ErrorMsg = "Unable to insert device with Esn:[" + Device.Esn + "] into the system";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020699;
                Device.ErrorMsg = err.Message;
            }


        }


        /// <summary>
        /// FindSaipanInventory os will be used to lookup devices in the Saipan inventory database
        /// Version: 1.0.0.0
        /// Created: 1/08/2013
        /// Author : Reggie Rippetoe
        /// </summary>
        /// <param name="Device"></param>
        public static void FindSaipanInventory(NHDevice Device)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NHKWCConnectionStringInventory"].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_FindSaipanInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@Sim", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@ESN", SqlDbType.VarChar).Value = Device.Esn;
                    command.Parameters.Add("@MeterNumber", SqlDbType.VarChar).Value = Device.ItronMeterNumber;

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            Device.Phone = myReader["MSISDN"].ToString();
                            Device.AccessCode = 0;
                            Device.Esn = myReader["ESN"].ToString();
                            Device.ItronMeterNumber = myReader["MeterNumber"].ToString();
                            Device.Network = Convert.ToInt32(myReader["Network"]);
                            Device.DeviceType = Convert.ToInt32(myReader["DeviceType"]);
                            Device.CapCode = myReader["Sim"].ToString();
                            Device.Frequency = "850.1900";
                            Device.Iccid = myReader["Sim"].ToString();
                            Device.ErtID = myReader["ErtID"].ToString();
                            Device.Status = myReader["Status"].ToString();
                            //MARefin 5/16/2012
                            Device.FirmwareVersion = myReader["FirmwareVersion"].ToString();
                            Device.HardwareVersion = myReader["HardwareVersion"].ToString();
                            Device.ModemFirmwareVersion = myReader["ModemFirmwareVersion"].ToString();

                            Device.ErrorCode = 0;
                            Device.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        Device.ErrorCode = 04020501;
                        Device.ErrorMsg = "Unable to find any device with Sim[" + Device.CapCode + "]";
                    }
                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020599;
                Device.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// FindTrinidadInventory os will be used to lookup devices in the Trinidad inventory database
        /// Version: 1.0.0.0
        /// Created: 3/11/2014
        /// Author : Reggie Rippetoe
        /// </summary>
        /// <param name="Device"></param>
        public static void FindTrinidadInventory(NHDevice Device)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NHKWCConnectionStringInventory"].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_FindTrinidadInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@Sim", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@ESN", SqlDbType.VarChar).Value = Device.Esn;
                    command.Parameters.Add("@MeterNumber", SqlDbType.VarChar).Value = Device.ItronMeterNumber;

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            Device.Phone = myReader["MSISDN"].ToString();
                            Device.AccessCode = 0;
                            Device.Esn = myReader["ESN"].ToString();
                            Device.ItronMeterNumber = myReader["MeterNumber"].ToString();
                            Device.Network = Convert.ToInt32(myReader["Network"]);
                            Device.DeviceType = Convert.ToInt32(myReader["DeviceType"]);
                            Device.CapCode = myReader["Sim"].ToString();
                            Device.Frequency = "850.1900";
                            Device.Iccid = myReader["Sim"].ToString();
                            Device.ErtID = myReader["ErtID"].ToString();
                            Device.Status = myReader["Status"].ToString();
                            //MARefin 5/16/2012
                            Device.FirmwareVersion = myReader["FirmwareVersion"].ToString();
                            Device.HardwareVersion = myReader["HardwareVersion"].ToString();
                            Device.ModemFirmwareVersion = myReader["ModemFirmwareVersion"].ToString();

                            Device.ErrorCode = 0;
                            Device.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        Device.ErrorCode = 04020501;
                        Device.ErrorMsg = "Unable to find any device with Sim[" + Device.CapCode + "]";
                    }
                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020599;
                Device.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// FindSimInventory os will be used to lookup devices in the inventory database
        /// Version: 1.0.0.0
        /// Created: 4/18/2012
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="Device"></param>
        public static void FindSimInventory(NHDevice Device)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NHKWCConnectionStringInventory"].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_FindSimInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@Sim", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@ESN", SqlDbType.VarChar).Value = Device.Esn;
                    command.Parameters.Add("@MeterNumber", SqlDbType.VarChar).Value = Device.ItronMeterNumber;

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            Device.Phone = myReader["IMSI"].ToString();
                            Device.AccessCode = 0;
                            Device.Esn = myReader["ESN"].ToString();
                            Device.ItronMeterNumber = myReader["MeterNumber"].ToString();
                            Device.Network = Convert.ToInt32(myReader["Network"]);
                            Device.DeviceType = Convert.ToInt32(myReader["DeviceType"]);
                            Device.CapCode = myReader["Sim"].ToString();
                            Device.Frequency = "850.1900";
                            Device.Iccid = myReader["Sim"].ToString();
                            Device.ErtID = myReader["ErtID"].ToString();
                            Device.Status = myReader["Status"].ToString();
                            //MARefin 5/16/2012
                            Device.FirmwareVersion = myReader["FirmwareVersion"].ToString();
                            Device.HardwareVersion = myReader["HardwareVersion"].ToString();
                            Device.ModemFirmwareVersion = myReader["ModemFirmwareVersion"].ToString();

                            Device.ErrorCode = 0;
                            Device.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        Device.ErrorCode = 04020501;
                        Device.ErrorMsg = "Unable to find any device with Sim[" + Device.CapCode + "]";
                    }
                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020599;
                Device.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// UpdateSimInventory os will be used to update devices in the inventory database
        /// Version: 1.0.0.0
        /// Created: 4/18/2012
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="Device"></param>
        public static void UpdateSimInventory(NHDevice Device)
        {
            try
            {
                string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString().Replace("NHKWCConnectionString", "");


                if (database == "" || database == "Development") database = "Dev";

                else if (database == "Production") database = "Prod"; //demo/prod


                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NHKWCConnectionStringInventory"].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateSimInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@Sim", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = "TRANSFERED";
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = database.ToUpper() + "[" + Device.ClientID + "]";

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    conn.Close();

                    if (rows < 1)
                    {
                        Device.ErrorCode = 04020701;
                        Device.ErrorMsg = "Unable to update device with Sim:[" + Device.Iccid + "] into the system";
                    }
                    else
                    {
                        Device.ErrorCode = 0;
                        Device.ErrorMsg = "";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020799;
                Device.ErrorMsg = err.Message;
            }
        }
        /// <summary>
        /// UpdateSaipanInventoryStatus os will be used to update devices in the Saipan inventory database
        /// Version: 1.0.0.0
        /// Created: 1/08/2013
        /// Author : Reggie Rippetoe
        /// </summary>
        /// <param name="Device"></param>
        public static void UpdateSaipanInventoryStatus(NHDevice Device)
        {
            try
            {
                string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString().Replace("NHKWCConnectionString", "");


                if (database == "" || database == "Development") database = "Dev";

                else if (database == "Production") database = "Prod"; //demo/prod

                SqlConnectionStringBuilder SaipanSqlstrConnection = new SqlConnectionStringBuilder();
                SaipanSqlstrConnection.ConnectionString = "Password=test01;Persist Security Info=True;User ID=test;Initial Catalog=Staging;Data Source=74.205.92.172";

                using (SqlConnection conn = new SqlConnection(SaipanSqlstrConnection.ConnectionString.ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_UpdateSaipanInventoryStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@Sim", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = "TRANSFERED";
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = database.ToUpper() + "[" + Device.ClientID + "]";

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    conn.Close();

                    if (rows < 1)
                    {
                        Device.ErrorCode = 04020701;
                        Device.ErrorMsg = "Unable to update device with Sim:[" + Device.Iccid + "] into the system";
                    }
                    else
                    {
                        Device.ErrorCode = 0;
                        Device.ErrorMsg = "";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020799;
                Device.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// UpdateTrinidadInventoryStatus os will be used to update devices in the Saipan inventory database
        /// Version: 1.0.0.0
        /// Created: 3/11/2014
        /// Author : Reggie Rippetoe
        /// </summary>
        /// <param name="Device"></param>
        public static void UpdateTrinidadInventoryStatus(NHDevice Device)
        {
            try
            {
                string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString().Replace("NHKWCConnectionString", "");


                if (database == "" || database == "Development") database = "Dev";

                else if (database == "Production") database = "Prod"; //demo/prod

                SqlConnectionStringBuilder SaipanSqlstrConnection = new SqlConnectionStringBuilder();
                SaipanSqlstrConnection.ConnectionString = "Password=test01;Persist Security Info=True;User ID=test;Initial Catalog=Staging;Data Source=74.205.92.172";

                using (SqlConnection conn = new SqlConnection(SaipanSqlstrConnection.ConnectionString.ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_UpdateTrinidadInventoryStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@Sim", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = "TRANSFERED";
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = database.ToUpper() + "[" + Device.ClientID + "]";

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    conn.Close();

                    if (rows < 1)
                    {
                        Device.ErrorCode = 04020701;
                        Device.ErrorMsg = "Unable to update device with Sim:[" + Device.Iccid + "] into the system";
                    }
                    else
                    {
                        Device.ErrorCode = 0;
                        Device.ErrorMsg = "";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020799;
                Device.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// FindCDMAInventory os will be used to lookup devices in the CDMA inventory database
        /// Version: 1.0.0.0
        /// Created: 12/14/2012
        /// Author : Reggie Rippetoe
        /// Updated: 12/17/2012
        /// Reason: Updated the ErrorCode number and error message to MEID instead of SIM
        /// </summary>
        /// <param name="Device"></param>
        public static void FindCDMAInventory(NHDevice Device)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NHKWCConnectionStringInventory"].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_FindCDMAInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@MEID", SqlDbType.VarChar).Value = Device.Iccid;                    
                    command.Parameters.Add("@ESN", SqlDbType.VarChar).Value = Device.Esn;
                    command.Parameters.Add("@MeterNumber", SqlDbType.VarChar).Value = Device.ItronMeterNumber;

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            Device.Phone = myReader["CNUM"].ToString();
                            Device.AccessCode = 0;
                            Device.Esn = myReader["ESN"].ToString();
                            Device.ItronMeterNumber = myReader["MeterNumber"].ToString();
                            Device.Network = Convert.ToInt32(myReader["Network"]);
                            Device.DeviceType = Convert.ToInt32(myReader["DeviceType"]);
                            Device.CapCode = myReader["MEID"].ToString();
                            Device.Frequency = "850.1900";
                            Device.Iccid = myReader["MEID"].ToString();
                            Device.ErtID = myReader["ErtID"].ToString();
                            Device.Status = myReader["Status"].ToString();
                            //MARefin 5/16/2012
                            Device.FirmwareVersion = myReader["FirmwareVersion"].ToString();
                            Device.HardwareVersion = myReader["HardwareVersion"].ToString();
                            Device.ModemFirmwareVersion = myReader["ModemFirmwareVersion"].ToString();

                            Device.ErrorCode = 0;
                            Device.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        Device.ErrorCode = 04021101;
                        Device.ErrorMsg = "Unable to find any device with MEID[" + Device.CapCode + "]";
                    }
                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04021199;
                Device.ErrorMsg = err.Message;
            }
        }


        /// <summary>
        /// UpdateCDMAInventory os will be used to update devices in the inventory database
        /// Version: 1.0.0.0
        /// Created: 4/18/2012
        /// Author : Reggie Rippetoe
        /// Updated: 12/17/2012
        /// Reason: Updated the ErrorCode number and error message to MEID instead of SIM        
        /// </summary>
        /// <param name="Device"></param>
        public static void UpdateCDMAInventory(NHDevice Device)
        {
            try
            {
                string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString().Replace("NHKWCConnectionString", "");


                if (database == "" || database == "Development") database = "Dev";

                else if (database == "Production") database = "Prod"; //demo/prod


                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NHKWCConnectionStringInventory"].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_UpdateCDMAInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@MEID", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = "TRANSFERED";
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = database.ToUpper() + "[" + Device.ClientID + "]";

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    conn.Close();

                    if (rows < 1)
                    {
                        Device.ErrorCode = 04021201;
                        Device.ErrorMsg = "Unable to update device with MEID:[" + Device.Iccid + "] into the system";
                    }
                    else
                    {
                        Device.ErrorCode = 0;
                        Device.ErrorMsg = "";
                    }


                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04021299;
                Device.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// FindSNAPInventory  will be used to lookup devices in the synapse inventory database
        /// Version: 1.0.0.0
        /// Created: 12/30/2015
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="Device"></param>
        public static void FindSNAPInventory(NHDevice Device)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NHKWCConnectionStringInventory"].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_FindLeafInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@MacAddress", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@ESN", SqlDbType.VarChar).Value = Device.Esn;
                    command.Parameters.Add("@MeterNumber", SqlDbType.VarChar).Value = Device.ItronMeterNumber;

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            Device.Phone = myReader["ShortNetworkID"].ToString();
                            Device.AccessCode = 0;
                            Device.Esn = myReader["ESN"].ToString();
                            Device.ItronMeterNumber = myReader["MeterNumber"].ToString();
                            Device.Network = Convert.ToInt32(myReader["Network"]);
                            Device.DeviceType = Convert.ToInt32(myReader["DeviceType"]);
                            Device.CapCode = myReader["MacAddress"].ToString();
                            Device.Frequency = "850.1900";
                            Device.Iccid = myReader["MacAddress"].ToString();
                            Device.ErtID = myReader["ErtID"].ToString();
                            Device.Status = myReader["Status"].ToString();
                            //MARefin 5/16/2012
                            Device.FirmwareVersion = myReader["FirmwareVersion"].ToString();
                            Device.HardwareVersion = myReader["HardwareVersion"].ToString();
                            Device.ModemFirmwareVersion = myReader["ModemFirmwareVersion"].ToString();

                            Device.ErrorCode = 0;
                            Device.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        Device.ErrorCode = 04021101;
                        Device.ErrorMsg = "Unable to find any device with MEID[" + Device.CapCode + "]";
                    }
                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04021199;
                Device.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// UpdateSNAPInventory will be used to update devices in the inventory database
        /// Version: 1.0.0.0
        /// Created: 12/30/2015
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="Device"></param>
        public static void UpdateSNAPInventory(NHDevice Device)
        {
            try
            {
                string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString().Replace("NHKWCConnectionString", "");


                if (database == "" || database == "Development") database = "Dev";

                else if (database == "Production") database = "Prod"; //demo/prod


                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["NHKWCConnectionStringInventory"].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_UpdateLEAFInventory", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@MacAddress", SqlDbType.VarChar).Value = Device.Iccid;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = "TRANSFERED";
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = database.ToUpper() + "[" + Device.ClientID + "]";

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    conn.Close();

                    if (rows < 1)
                    {
                        Device.ErrorCode = 04021201;
                        Device.ErrorMsg = "Unable to update device with MacAddress:[" + Device.Iccid + "] into the system";
                    }
                    else
                    {
                        Device.ErrorCode = 0;
                        Device.ErrorMsg = "";
                    }


                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04021299;
                Device.ErrorMsg = err.Message;
            }
        }

        /// <summary>
        /// DeleteDevice will be used to remove devices from  database
        /// Version: 1.0.0.0
        /// Created: 4/18/2012
        /// Author : Moshiul Arefin
        /// </summary>
        /// <param name="Device"></param>
        public static void DeleteDevice(NHDevice Device)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_DeleteDevice", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = Device.DeviceID;

                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    conn.Close();

                    if (rows == -1)
                    {
                        Device.ErrorCode = 04020801;
                        Device.ErrorMsg = "Unable to delete device from test with Sim:[" + Device.Iccid + "] from the system";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020899;
                Device.ErrorMsg = err.Message;
            }
        }


        /// <summary>
        /// UpdateDeviceCredentials will be used to update device credentials, for now it's used to move devices from the reseller to the customer
        /// Version: 1.0.0.0
        /// Created: 4/18/2012
        /// Modified: 6/5/2012, added devicetype, changed the store procedure
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="Device"></param>
        public static void UpdateDeviceCredentials(NHDevice Device)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_Manage_DeviceCredentials", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = Device.ClientID;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = Device.DeviceID;
                    command.Parameters.Add("@DeviceType", SqlDbType.Int).Value = Device.DeviceType;
                    command.Parameters.Add("@ItronMeterNumber", SqlDbType.VarChar).Value = Device.ItronMeterNumber;
                    command.Parameters.Add("@ErtID", SqlDbType.VarChar).Value = Device.ErtID;
                    command.Parameters.Add("@Active", SqlDbType.Bit).Value = Device.Active;
                    command.Parameters.Add("@Phone", SqlDbType.VarChar).Value = Device.Phone;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = "UPDATE";


                    conn.Open();

                    int rows = command.ExecuteNonQuery();

                    conn.Close();

                    if (rows == -1)
                    {
                        Device.ErrorCode = 04020401;
                        Device.ErrorMsg = "Unable to update device with Esn:[" + Device.Esn + "] into the system";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Device.ErrorCode = 04020499;
                Device.ErrorMsg = err.Message;
            }

        }




        /// <summary>
        /// GetProvisionedUnit will search units from test by part of the value for the specified parName 
        /// </summary>
        /// <param name="parName"></param>
        /// <param name="parVal"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static DataTable GetProvisionedUnit(string parName, string parVal, NHError Error)
        {
            DataTable dt = new DataTable("Devices");
            try
            {
                /*
                @Esn varchar(50) = null,
	            @MeterNumber varchar(50) = null,
	            @Phone varchar(50) = null,
	            @SIM varchar(20) = null,
	            @SenderID varchar(50) = null                
                */

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_SearchDevice", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@" + parName, SqlDbType.VarChar).Value = parVal;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 01;
                        Error.ErrorMsg = "Unable to find any device matching " + parName + "[" + parVal + "]";
                    }
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 99;
                Error.ErrorMsg = err.Message;
            }

            return dt;
        }




        /// <summary>
        /// GetProvisionedUnitActivity will get all the units that was provistioned on a specified date
        /// </summary>
        /// <param name="parName"></param>
        /// <param name="parVal"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static DataTable GetProvisionedUnitActivity(string Esn, DateTime ReportTime, NHError Error)
        {
            DataTable dt = new DataTable("DeviceActivity");
            try
            {
                /*
                @Esn varchar(50) = null,
	                    
                */

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetProvisionedDeviceActivity", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Esn", SqlDbType.VarChar).Value = Esn;
                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime).Value = ReportTime;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 01;
                        Error.ErrorMsg = "Unable to find any device matching ESN[" + Esn + "] and ReportTime[" + ReportTime + "]";
                    }
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 99;
                Error.ErrorMsg = err.Message;
            }

            return dt;

        }

        //public static IEnumerable<IDataRecord> ExecSqlDataReader( int queryType )
        public static DataTable GetMeterReadingsData(int deviceID, DateTime StartDateTime, DateTime EndDateTime, int TzOffset)
        {
            DataTable dt = new DataTable("MeterReadings");
            SqlDataReader reader = null;
            NHError Error = new NHError();

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetMeterReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;
                    command.Parameters.Add("@TzOffset", SqlDbType.Int).Value = TzOffset;
                    command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = StartDateTime;
                    command.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDateTime;
                    conn.Open();
                    reader = command.ExecuteReader();
                    dt.Load(reader);
                }

            }
            catch (Exception ex)
            {
                Error.ErrorCode = 04021699;
                Error.ErrorMsg = ex.Message;
            }
            return dt;
        }

        public static DataTable CheckForActiveDevices()
        {
            DataTable dt = new DataTable("ActiveDevices");

            NHError Error = new NHError();

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_DBActiveDevicesCheck", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    //command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021301;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_DBActiveDevicesCheck] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021399;
                Error.ErrorMsg = err.Message;
            }
            return dt;
        }

        public static DataTable CheckForInActiveDevices()
        {
            DataTable dt = new DataTable("InActiveDevices");

            NHError Error = new NHError();

            try
            {
                //this.ClientID = ClientID;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_DBInActiveDevicesCheck", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    // command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021401;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_DBInActiveDevicesCheck] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021499;
                Error.ErrorMsg = err.Message;
            }
            return dt;


        }
        public static DataTable CheckForActiveDevicesMeterReadings()
        {
            DataTable dt = new DataTable("ActiveDevicesMeterReadings");

            NHError Error = new NHError();

            try
            {
                //this.ClientID = ClientID;

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_GetDevicesUnusualMeterReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    // command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021501;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_GetDevicesUnusualMeterReadings] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021599;
                Error.ErrorMsg = err.Message;
            }
            return dt;


        }


        /*Function To get duplicate ERTID Values*/
        public static DataTable CheckForDUplicateERTIDDevices()
        {
            DataTable dt = new DataTable("DuplicateERTIDDevices");

            NHError Error = new NHError();

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_DBDuplicateERTIDCheck", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    //command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021301;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_DBDuplicateERTIDCheck] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021399;
                Error.ErrorMsg = err.Message;
            }
            return dt;
        }

        /*Function To get Wrong ERTID Values*/
        public static DataTable CheckForWrongERTIDDevices()
        {
            DataTable dt = new DataTable("WrongERTIDDevices");

            NHError Error = new NHError();

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_DBWrongERTIDCheck", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    //command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021301;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_DBWrongERTIDCheck] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021399;
                Error.ErrorMsg = err.Message;
            }
            return dt;
        }


        /*Function To get devices which are disconnected state and load is beign pulled*/
        public static DataTable CheckForDisconnectedMeterLoadCheck()
        {
            DataTable dt = new DataTable("DisconnectedMeterLoadCheck");

            NHError Error = new NHError();

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_DBDisconnectPowerCheck", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    //command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021301;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_DBDisconnectPowerCheck] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021399;
                Error.ErrorMsg = err.Message;
            }
            return dt;
        }

        /*Function To get devices whose reading are fluctuating to zero and large difference with previous reading*/
        public static DataTable CheckForFluctuatingReading()
        {
            DataTable dt = new DataTable("FluctuatingReading");

            NHError Error = new NHError();

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_DBFlactuatingReading", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    //command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021301;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_DBFlactuatingReading] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021399;
                Error.ErrorMsg = err.Message;
            }
            return dt;
        }

        /*Function To get Runaway devices report*/
        public static DataTable RunawayDevicesReport(int TzOffset, DateTime EndDateTime, DateTime StartDateTime,int iFirstRun, NHError Error)
        {
            DataTable dt = new DataTable("SMSRunAwayReading");

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_DBRunawayDevicesReport", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    //command.Parameters.Add("@RunawayDevice", SqlDbType.VarChar).Value = iRunawayDevicesCheck;
                    command.Parameters.Add("@TzOffset", SqlDbType.Int).Value = TzOffset;
                    command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = StartDateTime;
                    command.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDateTime;
                    command.Parameters.Add("@FirstRunawayDevice", SqlDbType.Int).Value = iFirstRun;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021301;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_DBSMSSentToDeviceReport] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021399;
                Error.ErrorMsg = err.Message;
            }
            return dt;
        }

        public static DataTable SensorTemperature()
        {
            DataTable dt = new DataTable("SensorTemperature");

            NHError Error = new NHError();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_GetSensorTemperature", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    // command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021501;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_GetSensorTemperature] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021599;
                Error.ErrorMsg = err.Message;
            }
            return dt;


        }



        public static DataTable SensorVibration()
        {
            DataTable dt = new DataTable("SensorVibration");

            NHError Error = new NHError();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_GetSensorVibration", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    // command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = ClientID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021501;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_GetSensorVibration] returned zero record";
                    }

                    conn.Close();

                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 04021599;
                Error.ErrorMsg = err.Message;
            }
            return dt;


        }


        /// <summary>
        /// UpdateSensorStatus is used to update sensor status, to update status update time
        /// to update command Status (queued/sending/sent/completed/failed), update report time
        /// Version: 1.0.0.0
        /// Created: 11/21/2014        
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="sensor"></param>
        /// 
        public static void UpdateSensorStatus(NHSensor sensor)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateSensorStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = sensor.DeviceID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = sensor.Command;
                    command.Parameters.Add("@CommandSent", SqlDbType.DateTime).Value = sensor.CommandSent == DateTime.MinValue ? SqlDateTime.Null : sensor.CommandSent;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = sensor.CommandStatus;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = sensor.Status;
                    command.Parameters.Add("@GatewayMessage", SqlDbType.VarChar).Value = sensor.GatewayMessage;
                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime).Value = sensor.ReportTime == DateTime.MinValue ? SqlDateTime.Null : sensor.ReportTime;
                    command.Parameters.Add("@StatusReportTime", SqlDbType.DateTime).Value = sensor.StatusReportTime == DateTime.MinValue ? SqlDateTime.Null : sensor.StatusReportTime;
                    command.Parameters.Add("@Sensor1", SqlDbType.Int).Value = sensor.Sensor1;
                    command.Parameters.Add("@Sensor2", SqlDbType.Int).Value = sensor.Sensor2;
                    command.Parameters.Add("@Sensor3", SqlDbType.Int).Value = sensor.Sensor3;
                    command.Parameters.Add("@Sensor4", SqlDbType.Int).Value = sensor.Sensor4;
                    command.Parameters.Add("@Sensor5", SqlDbType.Int).Value = sensor.Sensor5;
                    command.Parameters.Add("@Sensor6", SqlDbType.Int).Value = sensor.Sensor6;
                    command.Parameters.Add("@Sensor1Low", SqlDbType.Int).Value = sensor.Sensor1Low;
                    command.Parameters.Add("@Sensor1LowSent", SqlDbType.Int).Value = sensor.Sensor1LowSent;
                    command.Parameters.Add("@Sensor1High", SqlDbType.Int).Value = sensor.Sensor1High;
                    command.Parameters.Add("@Sensor1HighSent", SqlDbType.Int).Value = sensor.Sensor1HighSent;
                    command.Parameters.Add("@Sensor2Low", SqlDbType.Int).Value = sensor.Sensor2Low;
                    command.Parameters.Add("@Sensor2LowSent", SqlDbType.Int).Value = sensor.Sensor2LowSent;
                    command.Parameters.Add("@Sensor2High", SqlDbType.Int).Value = sensor.Sensor2High;
                    command.Parameters.Add("@Sensor2HighSent", SqlDbType.Int).Value = sensor.Sensor2HighSent;
                    command.Parameters.Add("@Sensor3Low", SqlDbType.Int).Value = sensor.Sensor3Low;
                    command.Parameters.Add("@Sensor3LowSent", SqlDbType.Int).Value = sensor.Sensor3LowSent;
                    command.Parameters.Add("@Sensor3High", SqlDbType.Int).Value = sensor.Sensor3High;
                    command.Parameters.Add("@Sensor3HighSent", SqlDbType.Int).Value = sensor.Sensor3HighSent;
                    command.Parameters.Add("@Sensor4Low", SqlDbType.Int).Value = sensor.Sensor4Low;
                    command.Parameters.Add("@Sensor4LowSent", SqlDbType.Int).Value = sensor.Sensor4LowSent;
                    command.Parameters.Add("@Sensor4High", SqlDbType.Int).Value = sensor.Sensor4High;
                    command.Parameters.Add("@Sensor4HighSent", SqlDbType.Int).Value = sensor.Sensor4HighSent;
                    command.Parameters.Add("@Sensor5Low", SqlDbType.Int).Value = sensor.Sensor5Low;
                    command.Parameters.Add("@Sensor5LowSent", SqlDbType.Int).Value = sensor.Sensor5LowSent;
                    command.Parameters.Add("@Sensor5High", SqlDbType.Int).Value = sensor.Sensor5High;
                    command.Parameters.Add("@Sensor5HighSent", SqlDbType.Int).Value = sensor.Sensor5HighSent;
                    command.Parameters.Add("@Sensor6Low", SqlDbType.Int).Value = sensor.Sensor6Low;
                    command.Parameters.Add("@Sensor6LowSent", SqlDbType.Int).Value = sensor.Sensor6LowSent;
                    command.Parameters.Add("@Sensor6High", SqlDbType.Int).Value = sensor.Sensor6High;
                    command.Parameters.Add("@Sensor6HighSent", SqlDbType.Int).Value = sensor.Sensor6HighSent;
                    command.Parameters.Add("@SignalStrength", SqlDbType.Int).Value = sensor.SignalStrength;
                    command.Parameters.Add("@LogEnabled", SqlDbType.Bit).Value = sensor.LogEnabled;
                    command.Parameters.Add("@LogInterval", SqlDbType.Int).Value = sensor.LogInterval;
                    command.Parameters.Add("@LogIntervalSent", SqlDbType.Int).Value = sensor.LogIntervalSent;
                    command.Parameters.Add("@RelayStatus", SqlDbType.VarChar).Value = sensor.RelayStatus;


                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                    sensor.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                sensor.ErrorCode = 4002;
                sensor.ErrorMsg = "\n\rError Updating Sensor Status : DeviceID [" + sensor.DeviceID + "] " + err.Message;
            }




        }


        /// <summary>
        /// UpdateCBCStatus is used to update CBC status, to update status update time
        /// to update command Status (queued/sending/sent/completed/failed), update report time
        /// Version: 1.0.0.0
        /// Created: 6/11/2014        
        /// Author : Moshiul Arefin 
        /// </summary>
        /// <param name="sensor"></param>
        /// 
        public static void UpdateCBCStatus(NHCBC cbc)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateCBCStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = cbc.DeviceID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = cbc.Command;
                    command.Parameters.Add("@CommandSent", SqlDbType.DateTime).Value = cbc.CommandSent;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = cbc.CommandStatus;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = cbc.Status;
                    command.Parameters.Add("@GatewayMessage", SqlDbType.VarChar).Value = cbc.GatewayMessage;
                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime).Value = cbc.ReportTime;
                    command.Parameters.Add("@StatusReportTime", SqlDbType.DateTime).Value = cbc.StatusReportTime;
                    command.Parameters.Add("@Signal", SqlDbType.Int).Value = cbc.Signal;
                    command.Parameters.Add("@Control_Mode", SqlDbType.VarChar).Value = cbc.Control_Mode;
                    command.Parameters.Add("@LastCmdSwitchState", SqlDbType.VarChar).Value = cbc.LastCmdSwitchState;
                    command.Parameters.Add("@ReqSwitchState", SqlDbType.VarChar).Value = cbc.ReqSwitchState;
                    command.Parameters.Add("@Voltage", SqlDbType.Float).Value = cbc.Voltage;
                    command.Parameters.Add("@TempReadingSigned", SqlDbType.Int).Value = cbc.TempReadingSigned;
                    command.Parameters.Add("@HighVoltageAlertFlag", SqlDbType.Bit).Value = cbc.HighVoltageAlertFlag;
                    command.Parameters.Add("@LowVoltageAlertFlag", SqlDbType.Bit).Value = cbc.LowVoltageAlertFlag;
                    command.Parameters.Add("@HighTempAlertFlag", SqlDbType.Bit).Value = cbc.HighTempAlertFlag;
                    command.Parameters.Add("@LowTempAlertFlag", SqlDbType.Bit).Value = cbc.LowTempAlertFlag;
                    command.Parameters.Add("@NeutralCurrFaultFlag", SqlDbType.Bit).Value = cbc.NeutralCurrFaultFlag;
                    command.Parameters.Add("@Spare", SqlDbType.Bit).Value = cbc.Spare;
                    command.Parameters.Add("@EnableConfig", SqlDbType.Bit).Value = cbc.EnableConfig;
                    command.Parameters.Add("@EnableAlertMsg", SqlDbType.VarChar).Value = cbc.EnableAlertMsg;
                    command.Parameters.Add("@EnableSwitchCtl", SqlDbType.VarChar).Value = cbc.EnableSwitchCtl;
                    command.Parameters.Add("@AltTempVoltageCntrlOp", SqlDbType.VarChar).Value = cbc.AltTempVoltageCntrlOp;	
                    command.Parameters.Add("@SafetyDelayRemoteClose", SqlDbType.Int).Value = cbc.SafetyDelayRemoteClose;
                    command.Parameters.Add("@SafetyDelayRemoteOpen", SqlDbType.Int).Value = cbc.SafetyDelayRemoteOpen;
                    command.Parameters.Add("@CurrentLow", SqlDbType.Int).Value = cbc.CurrentLow;
                    command.Parameters.Add("@CurrentLowSent", SqlDbType.Int).Value = cbc.CurrentLowSent;
                    command.Parameters.Add("@CurrentHigh", SqlDbType.Int).Value = cbc.CurrentHigh;
                    command.Parameters.Add("@CurrentHighSent", SqlDbType.Int).Value = cbc.CurrentHighSent;
                    command.Parameters.Add("@VoltageLow", SqlDbType.Int).Value = cbc.VoltageLow;
                    command.Parameters.Add("@VoltageLowSent", SqlDbType.Int).Value = cbc.VoltageLowSent;
                    command.Parameters.Add("@VoltageHigh", SqlDbType.Int).Value = cbc.VoltageHigh;
                    command.Parameters.Add("@VoltageHighSent", SqlDbType.Int).Value = cbc.VoltageHighSent;
                    command.Parameters.Add("@TemperatureLow", SqlDbType.Int).Value = cbc.TemperatureLow;
                    command.Parameters.Add("@TemperatureLowSent", SqlDbType.Int).Value = cbc.TemperatureLowSent;
                    command.Parameters.Add("@TemperatureHigh", SqlDbType.Int).Value = cbc.TemperatureHigh;
                    command.Parameters.Add("@TemperatureHighSent", SqlDbType.Int).Value = cbc.TemperatureHighSent;
                    command.Parameters.Add("@ChangeSourceCode", SqlDbType.VarChar).Value = cbc.ChangeSourceCode;
                    command.Parameters.Add("@EventSourceCode", SqlDbType.VarChar).Value = cbc.EventSourceCode;
        
                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                    cbc.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                cbc.ErrorCode = 4002;
                cbc.ErrorMsg = "\n\rError Updating CBC Status : DeviceID [" + cbc.DeviceID + "] " + err.Message;
            }




        }

        /// <summary>
        /// Version: 1.0.0.0
        /// Created: 6/11/2014        
        /// Author : Sivakumar
        /// </summary>
        /// <param name="sensor"></param>
        /// 
        public static void UpdateWaterStatus(NHWaterMeter Water)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_UpdateWaterStatus", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = Water.DeviceID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = Water.Command;
                    command.Parameters.Add("@CommandSent", SqlDbType.DateTime).Value = Water.CommandSent == DateTime.MinValue ? SqlDateTime.Null : Water.CommandSent;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = Water.CommandStatus;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = Water.Status;
                    command.Parameters.Add("@GatewayMessage", SqlDbType.VarChar).Value = Water.GatewayMessage;
                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime).Value = Water.ReportTime == DateTime.MinValue ? SqlDateTime.Null : Water.ReportTime;
                    command.Parameters.Add("@StatusReportTime", SqlDbType.DateTime).Value = Water.StatusReportTime == DateTime.MinValue ? SqlDateTime.Null : Water.StatusReportTime;
                    command.Parameters.Add("@Signal", SqlDbType.Int).Value = Water.Signal;
                    command.Parameters.Add("@FormatCode", SqlDbType.VarChar).Value = Water.FormatCode;
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = Water.Description;
                    command.Parameters.Add("@Battery1Level", SqlDbType.Float).Value = Water.Battery1Level;
                    command.Parameters.Add("@Battery2Level", SqlDbType.Float).Value = Water.Battery2Level;
                    command.Parameters.Add("@Sensor1MaxT", SqlDbType.Int).Value = Water.Sensor1MaxT;
                    command.Parameters.Add("@Sensor2MaxT", SqlDbType.Int).Value = Water.Sensor2MaxT;
                    command.Parameters.Add("@Sensor1MinT", SqlDbType.Int).Value = Water.Sensor1MinT;
                    command.Parameters.Add("@Sensor2MinT", SqlDbType.Int).Value = Water.Sensor2MinT;
                    command.Parameters.Add("@rxPayload ", SqlDbType.VarChar).Value = Water.rxPayload;
                    command.Parameters.Add("@MeterReading", SqlDbType.Float).Value = Water.MeterReading;
                    command.Parameters.Add("@Temperature", SqlDbType.VarChar).Value = Water.Temperature;
                    command.Parameters.Add("@SwitchState ", SqlDbType.VarChar).Value = Water.SwitchState;
                    command.Parameters.Add("@AlertBits", SqlDbType.VarChar).Value = Water.Alert;
                    //command.Parameters.Add("@AlertEnable", SqlDbType.Bit).Value = Water.AlertEnable;
                    command.Parameters.Add("@LoggingStatus", SqlDbType.Bit).Value = Water.LoggingStatus;
                    command.Parameters.Add("@LoggingInterval", SqlDbType.Int).Value = Water.LoggingInterval;
                    //command.Parameters.Add("@UnreadLogEntries", SqlDbType.VarChar).Value = Water.UnreadLogEntries;
                    command.Parameters.Add("@FirmwareVersion", SqlDbType.VarChar).Value = Water.FirmwareVersion;
                    command.Parameters.Add("@HardwareVersion", SqlDbType.VarChar).Value = Water.HardwareVersion;
                    command.Parameters.Add("@PythonVersion", SqlDbType.VarChar).Value = Water.PythonVersion;
                    command.Parameters.Add("@ConfigVersion", SqlDbType.VarChar).Value = Water.ConfigVersion;
                    command.Parameters.Add("@AdditionalInfo", SqlDbType.VarChar).Value = Water.AdditionalInfo;
                    command.Parameters.Add("@mktimestamp", SqlDbType.VarChar).Value = Water.mktimestamp;
                    command.Parameters.Add("@Checksum", SqlDbType.VarChar).Value = Water.Checksum;

                    command.Parameters.Add("@WakeUpInterval", SqlDbType.Int).Value = Water.WakeUpInterval;
                    command.Parameters.Add("@UpTimeInterval", SqlDbType.Int).Value = Water.UpTimeInterval;
                    command.Parameters.Add("@CommunicationTimeOut", SqlDbType.Char).Value = Water.CommunicationTimeOut;
                    command.Parameters.Add("@temperatureConfig", SqlDbType.Char).Value = Water.TemperatureConfig;
                    command.Parameters.Add("@AdditionalInfoRequest", SqlDbType.Char).Value = Water.AdditionalInfoRequest;
                    command.Parameters.Add("@BulkTransferRequest", SqlDbType.Char).Value = Water.BulkTransferRequest;
                    command.Parameters.Add("@BehaviorConfig", SqlDbType.Char).Value = Water.BehaviorConfig;

                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    conn.Close();
                    Water.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                Water.ErrorCode = 4002;
                Water.ErrorMsg = "\n\rError Updating Water Meter Status : DeviceID [" + Water.DeviceID + "] " + err.Message;
            }




        }

        /// <summary>
        /// Version: 1.0.0.0
        /// Created: 2/22/2016        
        /// Author : Sivakumar
        public static DataTable GetWaterStatusCommand(int iHubID,NHError Error)
        {
            DataTable dt = new DataTable();
            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetWaterCommands", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@HubID", SqlDbType.Int).Value = iHubID;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 04021401;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Device_GetWaterCommands] returned zero record";
                    }
                }

            }
            catch (Exception err)
            {
                Error.ErrorCode = 4002;
                Error.ErrorMsg = "\n\rError While fetching the water commands : DeviceID [" + iHubID + "] " + err.Message;
            }
            return dt;
        }

        /*

        public static void AddUserActivity(NHSensor sensor)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertUserActivitySensor", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = sensor.DeviceID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = sensor.Command;
                    command.Parameters.Add("@CommandSent", SqlDbType.DateTime).Value = sensor.CommandSent == DateTime.MinValue ? SqlDateTime.Null : sensor.CommandSent;
                    command.Parameters.Add("@CommandStatus", SqlDbType.VarChar).Value = sensor.CommandStatus;
                    command.Parameters.Add("@Status", SqlDbType.VarChar).Value = sensor.Status;
                    command.Parameters.Add("@GatewayMessage", SqlDbType.VarChar).Value = sensor.GatewayMessage;
                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime).Value = sensor.ReportTime == DateTime.MinValue ? SqlDateTime.Null : sensor.ReportTime;
                    command.Parameters.Add("@StatusReportTime", SqlDbType.DateTime).Value = sensor.StatusReportTime == DateTime.MinValue ? SqlDateTime.Null : sensor.StatusReportTime;
                    command.Parameters.Add("@Sensor1", SqlDbType.Int).Value = sensor.Sensor1;
                    command.Parameters.Add("@Sensor2", SqlDbType.Int).Value = sensor.Sensor2;
                    command.Parameters.Add("@Sensor3", SqlDbType.Int).Value = sensor.Sensor3;
                    command.Parameters.Add("@Sensor4", SqlDbType.Int).Value = sensor.Sensor4;
                    command.Parameters.Add("@Sensor5", SqlDbType.Int).Value = sensor.Sensor5;
                    command.Parameters.Add("@Sensor6", SqlDbType.Int).Value = sensor.Sensor6;
                    command.Parameters.Add("@Sensor1Low", SqlDbType.Int).Value = sensor.Sensor1Low;
                    command.Parameters.Add("@Sensor1LowSent", SqlDbType.Int).Value = sensor.Sensor1LowSent;
                    command.Parameters.Add("@Sensor1High", SqlDbType.Int).Value = sensor.Sensor1High;
                    command.Parameters.Add("@Sensor1HighSent", SqlDbType.Int).Value = sensor.Sensor1HighSent;
                    command.Parameters.Add("@Sensor2Low", SqlDbType.Int).Value = sensor.Sensor2Low;
                    command.Parameters.Add("@Sensor2LowSent", SqlDbType.Int).Value = sensor.Sensor2LowSent;
                    command.Parameters.Add("@Sensor2High", SqlDbType.Int).Value = sensor.Sensor2High;
                    command.Parameters.Add("@Sensor2HighSent", SqlDbType.Int).Value = sensor.Sensor2HighSent;
                    command.Parameters.Add("@Sensor3Low", SqlDbType.Int).Value = sensor.Sensor3Low;
                    command.Parameters.Add("@Sensor3LowSent", SqlDbType.Int).Value = sensor.Sensor3LowSent;
                    command.Parameters.Add("@Sensor3High", SqlDbType.Int).Value = sensor.Sensor3High;
                    command.Parameters.Add("@Sensor3HighSent", SqlDbType.Int).Value = sensor.Sensor3HighSent;
                    command.Parameters.Add("@Sensor4Low", SqlDbType.Int).Value = sensor.Sensor4Low;
                    command.Parameters.Add("@Sensor4LowSent", SqlDbType.Int).Value = sensor.Sensor4LowSent;
                    command.Parameters.Add("@Sensor4High", SqlDbType.Int).Value = sensor.Sensor4High;
                    command.Parameters.Add("@Sensor4HighSent", SqlDbType.Int).Value = sensor.Sensor4HighSent;
                    command.Parameters.Add("@Sensor5Low", SqlDbType.Int).Value = sensor.Sensor5Low;
                    command.Parameters.Add("@Sensor5LowSent", SqlDbType.Int).Value = sensor.Sensor5LowSent;
                    command.Parameters.Add("@Sensor5High", SqlDbType.Int).Value = sensor.Sensor5High;
                    command.Parameters.Add("@Sensor5HighSent", SqlDbType.Int).Value = sensor.Sensor5HighSent;
                    command.Parameters.Add("@Sensor6Low", SqlDbType.Int).Value = sensor.Sensor6Low;
                    command.Parameters.Add("@Sensor6LowSent", SqlDbType.Int).Value = sensor.Sensor6LowSent;
                    command.Parameters.Add("@Sensor6High", SqlDbType.Int).Value = sensor.Sensor6High;
                    command.Parameters.Add("@Sensor6HighSent", SqlDbType.Int).Value = sensor.Sensor6HighSent;
                    command.Parameters.Add("@SignalStrength", SqlDbType.Int).Value = sensor.SignalStrength;
                    command.Parameters.Add("@LogEnabled", SqlDbType.Bit).Value = sensor.LogEnabled;
                    command.Parameters.Add("@LogInterval", SqlDbType.Int).Value = sensor.LogInterval;
                    command.Parameters.Add("@LogIntervalSent", SqlDbType.Int).Value = sensor.LogIntervalSent;


                    conn.Open();
                    int rows = command.ExecuteNonQuery();
                    if (rows == -1)
                    {
                        sensor.ErrorCode = 04020391;
                        sensor.ErrorMsg = "Failed to update activity table stroed proc[sp_NH_Device_InsertUserActivitySensor] for deviceID[" + sensor.DeviceID + "]";
                    }
                    conn.Close();
                    sensor.ErrorCode = 0;
                }

            }
            catch (Exception err)
            {
                sensor.ErrorCode = 99;
                sensor.ErrorMsg = "\n\rError Updating Sensor Status : DeviceID [" + sensor.DeviceID + "] " + err.Message;
            }




        }
*/

        public static int SendUpdateReportCommand(NHSensor sensor, string userID, string opCode, string opCodeSuffix)
        {
            DateTime utcNow = DateTime.UtcNow;
            string utcString = UTCString(utcNow);
            string opCodeOrig = opCode;

            opCodeSuffix = opCodeSuffix == null ? "" : opCodeSuffix;

            //to get the dbenum
            string database = ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString().Replace("NHKWCConnectionString", "");
            int DBEnum = 3; //default is development

            if (database == "Test") DBEnum = 0; //test

            else if (database == "Demo") DBEnum = 1; //demo

            else if (database == "Production") DBEnum = 2; //prod           

            else if (database == "WebSense") DBEnum = 1; //Demo WebSense           


            string result = SendCommandMSMQ(sensor.DeviceID, userID, opCode, opCodeSuffix, utcString, DBEnum);

            if (result.CompareTo("Failed") == 0) { } //failed to send a message to the outbound queue
            else
            {

                UpdateDeviceStatus(sensor.DeviceID, opCodeOrig, opCodeSuffix, utcNow);

                UpdateSensorStatus(sensor);
                NHDevice device = new NHDevice(sensor.DeviceID);


                NHDeviceActivity InsertSending = new NHDeviceActivity();

                InsertSending.DeviceID = sensor.DeviceID;
                InsertSending.GatewayMessage = "WebConnect to MessageQueue";
                InsertSending.Protocol = device.Protocol;
                InsertSending.NetworkID = device.Network;
                InsertSending.Command = new NHDeviceControl(opCodeOrig).ControlName;
                InsertSending.CommandStatus = "SENDING";
                InsertSending.ReportTime = DateTime.UtcNow;
                InsertSending.StatusReportTime = DateTime.UtcNow;
                InsertSending.UserID = userID;

                AddUserActivity(InsertSending);
            }

            return 0;

        }

        //insert Water readings
        public static void InsertWaterReadings(NHWaterMeter reading)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertWaterReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = reading.DeviceID;
                    command.Parameters.Add("@PacketID", SqlDbType.Int).Value = reading.PacketID;
                    command.Parameters.Add("@ReportTime", SqlDbType.DateTime).Value = reading.ReportTime == DateTime.MinValue ? SqlDateTime.Null : reading.ReportTime;
                    command.Parameters.Add("@Battery1Level", SqlDbType.Int).Value = reading.Battery1Level;
                    command.Parameters.Add("@Battery2Level", SqlDbType.Int).Value = reading.Battery2Level;
                    command.Parameters.Add("@Sensor1MaxT", SqlDbType.Int).Value = reading.Sensor1MaxT;
                    command.Parameters.Add("@Sensor2MaxT", SqlDbType.Int).Value = reading.Sensor2MaxT;
                    command.Parameters.Add("@Sensor1MinT", SqlDbType.Int).Value = reading.Sensor1MinT;
                    command.Parameters.Add("@Sensor2MinT", SqlDbType.Int).Value = reading.Sensor2MinT;
                    command.Parameters.Add("@HubID", SqlDbType.Int).Value = reading.HubID;
                    

                    SqlParameter SuccessParameter = new SqlParameter("@Success", SqlDbType.VarChar);
                    SuccessParameter.DbType = DbType.String;
                    SuccessParameter.Size = 255;
                    SuccessParameter.Direction = ParameterDirection.Output;

                    command.Parameters.Add(SuccessParameter);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();

                    string RESULT = (string)SuccessParameter.Value;
                    if (RESULT.CompareTo("Already in table.") == 0)
                    {
                        reading.ErrorCode = 0;
                        reading.ErrorMsg = "Device: " + reading.DeviceID + " Already Inserted Water Readings[" + reading.Battery1Level + ", "
                                                                                                                + reading.Battery2Level + ", "
                                                                                                                + reading.Sensor1MaxT + ", "
                                                                                                                + reading.Sensor2MaxT + ", "
                                                                                                                + reading.Sensor1MinT + ", "
                                                                                                                + reading.Sensor2MinT + "], DateTime[" + reading.ReportTime + "]PacketID[" + reading.PacketID + "]";
                    }
                    else
                    {
                        reading.ErrorCode = 0;
                        reading.ErrorMsg = "Device: " + reading.DeviceID + " Inserting Water Readings[" + reading.Battery1Level + ", "
                                                                                                                + reading.Battery2Level + ", "
                                                                                                                + reading.Sensor1MaxT + ", "
                                                                                                                + reading.Sensor2MaxT + ", "
                                                                                                                + reading.Sensor1MinT + ", "
                                                                                                                + reading.Sensor2MinT + "], DateTime[" + reading.ReportTime + "]PacketID[" + reading.PacketID + "]";
                    }

                    conn.Close();
                }

            }
            catch (Exception err)
            {
                reading.ErrorCode = 99;
                reading.ErrorMsg = "NH Error Code: " + reading.ErrorCode + "\n\rError Inserting Water Readings[" + reading.Battery1Level + ", "
                                                                                                                + reading.Battery2Level + ", "
                                                                                                                + reading.Sensor1MaxT + ", "
                                                                                                                + reading.Sensor2MaxT + ", "
                                                                                                                + reading.Sensor1MinT + ", "
                                                                                                                + reading.Sensor2MinT + "], DateTime[" + reading.ReportTime + "]PacketID[" + reading.PacketID + "]" + err.Message;


            }

        }


        //insert water commands
        public static void InsertWaterCommands(int DeviceID,string Commands,int LeafID,int HubID,NHError Error)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertWaterCommands", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@HubID", SqlDbType.Int).Value = DeviceID;
                    command.Parameters.Add("@Commands", SqlDbType.DateTime).Value = Commands;
                    command.Parameters.Add("@TimeStamp", SqlDbType.DateTime).Value = DateTime.UtcNow;

                    conn.Open();
                    int rows = command.ExecuteNonQuery();

                    if (rows <= 0)
	                {
                        Error.ErrorCode = 04010500;
                        Error.ErrorMsg = "No record is Inserted.";
		 
	                }

                    conn.Close();
                }

            }
            catch (Exception err)
            {
                Error.ErrorCode = 99;
                Error.ErrorMsg = "NH Error Code: " + err.Message.ToString();


            }

        }

        //insert sensor readings
        public static void InsertSensorReadings(NHSensorReading reading)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_InsertSensorReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = reading.DeviceID;
                    command.Parameters.Add("@PacketID", SqlDbType.Int).Value = reading.PacketID;
                    command.Parameters.Add("@SensorReportTime", SqlDbType.DateTime).Value = reading.SensorReportTime == DateTime.MinValue ? SqlDateTime.Null : reading.SensorReportTime;
                    command.Parameters.Add("@Sensor1", SqlDbType.Int).Value = reading.Sensor1;
                    command.Parameters.Add("@Sensor2", SqlDbType.Int).Value = reading.Sensor2;
                    command.Parameters.Add("@Sensor3", SqlDbType.Int).Value = reading.Sensor3;
                    command.Parameters.Add("@Sensor4", SqlDbType.Int).Value = reading.Sensor4;
                    command.Parameters.Add("@Sensor5", SqlDbType.Int).Value = reading.Sensor5;
                    command.Parameters.Add("@Sensor6", SqlDbType.Int).Value = reading.Sensor6;


                    SqlParameter SuccessParameter = new SqlParameter("@Success", SqlDbType.VarChar);
                    SuccessParameter.DbType = DbType.String;
                    SuccessParameter.Size = 255;
                    SuccessParameter.Direction = ParameterDirection.Output;

                    command.Parameters.Add(SuccessParameter);

                    conn.Open();
                    int rows = command.ExecuteNonQuery();

                    string RESULT = (string)SuccessParameter.Value;
                    if (RESULT.CompareTo("Already in table.") == 0)
                    {
                        reading.ErrorCode = 0;
                        reading.ErrorMsg = "Device: " + reading.DeviceID + " Already Inserted Sensor Readings[" + reading.Sensor1 + ", "
                                                                                                                + reading.Sensor2 + ", "
                                                                                                                + reading.Sensor3 + ", "
                                                                                                                + reading.Sensor4 + ", "
                                                                                                                + reading.Sensor5 + ", "
                                                                                                                + reading.Sensor6 + "], DateTime[" + reading.SensorReportTime + "]PacketID[" + reading.PacketID + "]";
                    }
                    else
                    {
                        reading.ErrorCode = 0;
                        reading.ErrorMsg = "Device: " + reading.DeviceID + " Inserting Sensor Readings[" + reading.Sensor1 + ", "
                                                                                                                + reading.Sensor2 + ", "
                                                                                                                + reading.Sensor3 + ", "
                                                                                                                + reading.Sensor4 + ", "
                                                                                                                + reading.Sensor5 + ", "
                                                                                                                + reading.Sensor6 + "], DateTime[" + reading.SensorReportTime + "]PacketID[" + reading.PacketID + "]";
                    }

                    conn.Close();
                }

            }
            catch (Exception err)
            {
                reading.ErrorCode = 99;
                reading.ErrorMsg = "NH Error Code: " + reading.ErrorCode + "\n\rError Inserting Sensor Readings[" + reading.Sensor1 + ", "
                                                                                                                + reading.Sensor2 + ", "
                                                                                                                + reading.Sensor3 + ", "
                                                                                                                + reading.Sensor4 + ", "
                                                                                                                + reading.Sensor5 + ", "
                                                                                                                + reading.Sensor6 + "], DateTime[" + reading.SensorReportTime + "]PacketID[" + reading.PacketID + "]" + err.Message;


            }

        }


        //public static IEnumerable<IDataRecord> ExecSqlDataReader( int queryType )
        public static DataTable GetSensorReadingsData(int deviceID, DateTime StartDateTime, DateTime EndDateTime, double TzOffset)
        {
            DataTable dt = new DataTable("SensorReadings");
            SqlDataReader reader = null;
            NHError Error = new NHError();

            try
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetSensorReadings", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.VarChar).Value = deviceID;
                    command.Parameters.Add("@TzOffset", SqlDbType.Float).Value = TzOffset;
                    command.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = StartDateTime;
                    command.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = EndDateTime;
                    conn.Open();
                    reader = command.ExecuteReader();
                    dt.Load(reader);
                }

            }
            catch (Exception ex)
            {
                Error.ErrorCode = 04021699;
                Error.ErrorMsg = ex.Message;
            }
            return dt;
        }

    }



    public class NHDeviceType
    {
        public int DeviceType;
        public string Description;
        public string Controls;
        public string MsgPrefix;
        public bool TwoWay;
        public string Features;
        public bool PrePay;
        public bool UtilityDevice;
        public string Comment;
        public string Command; //select, insert, update, delete smtp information form sp_NH_Manage_DeviceTypes
        public int ErrorCode;
        public string ErrorMsg;

        public NHDeviceType()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 04040199;
                this.ErrorMsg = err.Message;
            }

        }
    }


    public class NHDeviceControl
    {

        /// <summary>        
        /// Created: 10/04/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        public string OPCode;
        public int ControlOrder;
        public string ControlName;
        public string Options;
        public string Description;
        public string Command; //select, insert, update, delete smtp information form tbl_NH_DeviceControl
        public int ErrorCode;
        public string ErrorMsg;


        /// <summary>
        /// Initializes a new instance of NHDeviceControl
        /// Created: 10/04/2011
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        public NHDeviceControl()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 04030199;
                this.ErrorMsg = err.Message;
            }

        }



        /// <summary>
        /// Initializes a new instance of NHDeviceControl
        /// Created: 11/18/2011
        /// Author: Moshiul Arefin 
        /// Modified: 5/15/2015 by Moshiul Arefin
        /// Version: 1.0.0.0
        /// </summary>
        /// <param name="OPCode"></param>
        public NHDeviceControl(string OPCode)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_ManageControls", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@OpCode", SqlDbType.VarChar).Value = OPCode;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = "SELECT";

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            this.OPCode = myReader["OPCode"].ToString();
                            this.ControlOrder = Convert.ToInt32(myReader["ControlOrder"]);
                            this.ControlName = myReader["ControlName"].ToString();
                            this.Options = myReader["Options"].ToString();
                            this.Description = myReader["Description"].ToString();
                        }
                    }
                    else
                    {
                        this.ErrorCode = 02;
                        this.ErrorMsg = "OPCode not found in the system";

                    }
                    conn.Close();


                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }

        }


    }



    public class NHControlParamater
    {
        public string Name;
        public string Value;
        public string Type;
        public string NameMin;
        public string NameMax;
        public string NameIncrement;
        public string ValMin;
        public string ValMax;
        public string ValIncrement;
        public int ErrorCode;
        public string ErrorMsg;

        public NHControlParamater()
        {

        }

    }


    public class NHSNPPData
    {
        public string DeliveryID;
        public string Status;
        public string MsgText;
        public DateTime ReportTime;
        public string Queue;
        public string MsgID;
        public string DeviceID;
        public int ErrorCode;
        public string ErrorMsg;


        public NHSNPPData()
        {

        }



    }

    public class NHCBC
    {
        public int DeviceID;	
	    public string Command;
	    public DateTime CommandSent;
	    public string CommandStatus;
	    public string Status;
	    public string GatewayMessage;
	    public DateTime ReportTime;
	    public DateTime StatusReportTime;
        public int Signal;
        public string Control_Mode;
	    public string LastCmdSwitchState;
	    public string ReqSwitchState;
	    public double Voltage;
	    public int TempReadingSigned;
	    public bool HighVoltageAlertFlag;
	    public bool LowVoltageAlertFlag;
	    public bool HighTempAlertFlag;
	    public bool LowTempAlertFlag;
	    public bool NeutralCurrFaultFlag;
	    public bool Spare;
        public bool EnableConfig;
        public string EnableAlertMsg;
        public string EnableSwitchCtl;
        public string AltTempVoltageCntrlOp;	    
	    public int SafetyDelayRemoteClose;
	    public int SafetyDelayRemoteOpen;
	    public int CurrentLow;
        public int CurrentLowSent;
        public int CurrentHigh;
        public int CurrentHighSent;
        public int VoltageLow;
        public int VoltageLowSent;
        public int VoltageHigh;
        public int VoltageHighSent;
        public int TemperatureLow;
        public int TemperatureLowSent;
        public int TemperatureHigh;
        public int TemperatureHighSent;
        public string ChangeSourceCode;
	    public string EventSourceCode;
        public int ErrorCode;
        public string ErrorMsg;

        public NHCBC()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010199;
                this.ErrorMsg = err.Message;
            }
        }

        public NHCBC(int deviceID)
        {
            try
            {
                this.DeviceID = deviceID;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetCBC", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {

                            //this.Command = myReader["Command"] == DBNull.Value ? "" : myReader["Command"].ToString();
                            //this.CommandSent = myReader["CommandSent"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["CommandSent"];
                            //this.CommandStatus = myReader["CommandStatus"] == DBNull.Value ? "" : myReader["CommandStatus"].ToString();
                            //this.Status = myReader["Status"] == DBNull.Value ? "" : myReader["Status"].ToString();
                            //this.GatewayMessage = myReader["GatewayMessage"] == DBNull.Value ? "" : myReader["GatewayMessage"].ToString();
                            //this.ReportTime = myReader["ReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ReportTime"];
                            //this.StatusReportTime = myReader["StatusReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["StatusReportTime"];                            
	                        this.Signal = myReader["Signal"] == DBNull.Value ? 0 : (int)myReader["Signal"];
	                        this.Control_Mode = myReader["Control_Mode"] == DBNull.Value ? "" : myReader["Control_Mode"].ToString();
	                        this.LastCmdSwitchState = myReader["LastCmdSwitchState"] == DBNull.Value ? "" : myReader["LastCmdSwitchState"].ToString();
	                        this.ReqSwitchState = myReader["ReqSwitchState"] == DBNull.Value ? "" : myReader["ReqSwitchState"].ToString();
                            this.Voltage = myReader["Voltage"] == DBNull.Value ? 0 : (double)myReader["Voltage"];
	                        this.TempReadingSigned = myReader["TempReadingSigned"] == DBNull.Value ? 0 : (int)myReader["TempReadingSigned"];
	                        this.HighVoltageAlertFlag = myReader["HighVoltageAlertFlag"] == DBNull.Value ? false : (bool)myReader["HighVoltageAlertFlag"];
	                        this.LowVoltageAlertFlag = myReader["LowVoltageAlertFlag"] == DBNull.Value ? false : (bool)myReader["LowVoltageAlertFlag"];
	                        this.HighTempAlertFlag = myReader["HighTempAlertFlag"] == DBNull.Value ? false : (bool)myReader["HighTempAlertFlag"];
	                        this.LowTempAlertFlag = myReader["LowTempAlertFlag"] == DBNull.Value ? false : (bool)myReader["LowTempAlertFlag"];
	                        this.NeutralCurrFaultFlag = myReader["NeutralCurrFaultFlag"] == DBNull.Value ? false : (bool)myReader["NeutralCurrFaultFlag"];
	                        this.Spare = myReader["Spare"] == DBNull.Value ? false : (bool)myReader["Spare"];
	                        this.EnableConfig = myReader["EnableConfig"] == DBNull.Value ? false : (bool)myReader["EnableConfig"];
                            this.EnableAlertMsg = myReader["EnableAlertMsg"] == DBNull.Value ? "" : myReader["EnableAlertMsg"].ToString();
                            this.EnableSwitchCtl = myReader["EnableSwitchCtl"] == DBNull.Value ? "" : myReader["EnableSwitchCtl"].ToString();
                            this.AltTempVoltageCntrlOp = myReader["AltTempVoltageCntrlOp"] == DBNull.Value ? "" : myReader["AltTempVoltageCntrlOp"].ToString();
	                        this.SafetyDelayRemoteClose = myReader["SafetyDelayRemoteClose"] == DBNull.Value ? 0 : (int)myReader["SafetyDelayRemoteClose"];
	                        this.SafetyDelayRemoteOpen = myReader["SafetyDelayRemoteOpen"] == DBNull.Value ? 0 : (int)myReader["SafetyDelayRemoteOpen"];
	                        this.CurrentLow = myReader["CurrentLow"] == DBNull.Value ? 0 : (int)myReader["CurrentLow"];
                            this.CurrentLowSent = myReader["CurrentLowSent"] == DBNull.Value ? 0 : (int)myReader["CurrentLowSent"];
                            this.CurrentHigh = myReader["CurrentHigh"] == DBNull.Value ? 0 : (int)myReader["CurrentHigh"];
                            this.CurrentHighSent = myReader["CurrentHighSent"] == DBNull.Value ? 0 : (int)myReader["CurrentHighSent"];
                            this.VoltageLow = myReader["VoltageLow"] == DBNull.Value ? 0 : (int)myReader["VoltageLow"];
                            this.VoltageLowSent = myReader["VoltageLowSent"] == DBNull.Value ? 0 : (int)myReader["VoltageLowSent"];
                            this.VoltageHigh = myReader["VoltageHigh"] == DBNull.Value ? 0 : (int)myReader["VoltageHigh"];
                            this.VoltageHighSent = myReader["VoltageHighSent"] == DBNull.Value ? 0 : (int)myReader["VoltageHighSent"];
                            this.TemperatureLow = myReader["TemperatureLow"] == DBNull.Value ? 0 : (int)myReader["TemperatureLow"];
                            this.TemperatureLowSent = myReader["TemperatureLowSent"] == DBNull.Value ? 0 : (int)myReader["TemperatureLowSent"];
                            this.TemperatureHigh = myReader["TemperatureHigh"] == DBNull.Value ? 0 : (int)myReader["TemperatureHigh"];
                            this.TemperatureHighSent = myReader["TemperatureHighSent"] == DBNull.Value ? 0 : (int)myReader["TemperatureHighSent"];
                            this.ChangeSourceCode = myReader["ChangeSourceCode"] == DBNull.Value ? "" : myReader["ChangeSourceCode"].ToString();
	                        this.EventSourceCode = myReader["EventSourceCode"] == DBNull.Value ? "" : myReader["EventSourceCode"].ToString();                                                      


                            this.ErrorCode = 0;
                            this.ErrorMsg = "";

                        }
                    }
                    else
                    {
                        this.ErrorCode = 01;
                        this.ErrorMsg = "Unable to find any CBC with DeviceID[" + deviceID + "]";
                    }
                    conn.Close();
                }
                    
             
            }

            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }
        }
    }


    public class NHWaterMeter
    {
        public int DeviceID;
        public string Command;
        public DateTime CommandSent;
        public string CommandStatus;
        public string Status;
        public string GatewayMessage;
        public DateTime ReportTime;
        public DateTime StatusReportTime;
        public string FormatCode;
        public int Signal;
        public string Description;
        public double Battery1Level;
        public double Battery2Level;
        public double Sensor1MaxT;
        public double Sensor2MaxT;
        public double Sensor1MinT;
        public double Sensor2MinT;
        public string rxPayload;
        public double MeterReading;
        public string Temperature;
        public string SwitchState;
        public string Alert;
        public bool AlertEnable;
        public string LoggingStatus;
        public string LoggingInterval;
        public string UnreadLogEntries;
        public string FirmwareVersion;
        public string HardwareVersion;
        public string PythonVersion;
        public string ConfigVersion;
        public string AdditionalInfo;
        public string mktimestamp;
        public string Checksum;  
        public int ErrorCode;
        public string ErrorMsg;
        public int PacketID;
        public int HubID;
       	public int WakeUpInterval;
	    public int UpTimeInterval;
	    public string CommunicationTimeOut;
        public string TemperatureConfig;
        public string AdditionalInfoRequest;
        public string BulkTransferRequest;
        public string BehaviorConfig;

        public NHWaterMeter()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010199;
                this.ErrorMsg = err.Message;
            }
        }

        //public NHWater(int deviceID)
        //{
        //    try
        //    {
        //        this.DeviceID = deviceID;
        //        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
        //        {
        //            SqlCommand command = new SqlCommand("sp_NH_Device_GetWaterMeter", conn);
        //            command.CommandType = CommandType.StoredProcedure;
        //            command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
        //            SqlDataReader myReader = null;
        //            conn.Open();
        //            myReader = command.ExecuteReader();

        //            if (myReader.HasRows)
        //            {
        //                while (myReader.Read())
        //                {
        //                    this.Signal = myReader["Signal"] == DBNull.Value ? 0 : (int)myReader["Signal"];
        //                    this.FormatCode = myReader["FormatCode"] == DBNull.Value ? "" : myReader["FormatCode"].ToString();
        //                    this.Description = myReader["Description"] == DBNull.Value ? "" : myReader["Description"].ToString();
        //                    this.Battery1Level = myReader["Battery1Level"] == DBNull.Value ? 0 : (float)myReader["Battery1Level"];
        //                    this.Battery2Level = myReader["Battery2Level"] == DBNull.Value ? 0 : (float)myReader["Battery2Level"];
        //                    this.Sensor1MaxT = myReader["Sensor1MaxT"] == DBNull.Value ? 0 : (int)myReader["Sensor1MaxT"];
        //                    this.Sensor2MaxT = myReader["Sensor2MaxT"] == DBNull.Value ? 0 : (int)myReader["Sensor2MaxT"];
        //                    this.Sensor1MinT = myReader["Sensor1MinT"] == DBNull.Value ? 0 : (int)myReader["Sensor1MinT"];
        //                    this.Sensor2MinT = myReader["Sensor2MinT"] == DBNull.Value ? 0 : (int)myReader["Sensor2MinT"];
        //                    this.rxPayload = myReader["rxPayload"] == DBNull.Value ? "" : myReader["rxPayload"].ToString();
        //                    this.MeterReading = myReader["MeterReading"] == DBNull.Value ? 0 : (float)myReader["MeterReading"];
        //                    this.Temperature = myReader["Temperature"] == DBNull.Value ? "" : myReader["Temperature"].ToString();
        //                    this.SwitchState = myReader["SwitchState"] == DBNull.Value ? "" : myReader["SwitchState"].ToString();
        //                    this.Alert = myReader["Alert"] == DBNull.Value ? "" : myReader["Alert"].ToString();
        //                    this.AlertEnable = myReader["AlertEnable"] == DBNull.Value ? false : (bool)myReader["AlertEnable"];
        //                    this.LoggingStatus = myReader["LoggingStatus"] == DBNull.Value ? false : (bool)myReader["LoggingStatus"];
        //                    this.LoggingInterval = myReader["LoggingInterval"] == DBNull.Value ? 0 : (int)myReader["LoggingInterval"];
        //                    this.UnreadLogEntries = myReader["UnreadLogEntries"] == DBNull.Value ? "" : myReader["UnreadLogEntries"].ToString();
        //                    this.FirmwareVersion = myReader["FirmwareVersion"] == DBNull.Value ? "" : myReader["FirmwareVersion"].ToString();
        //                    this.HardwareVersion = myReader["HardwareVersion"] == DBNull.Value ? "" : myReader["HardwareVersion"].ToString();
        //                    this.PythonVersion = myReader["PythonVersion"] == DBNull.Value ? "" : myReader["PythonVersion"].ToString();
        //                    this.ConfigVersion = myReader["ConfigVersion"] == DBNull.Value ? "" : myReader["ConfigVersion"].ToString();
        //                    this.AdditionalInfo = myReader["AdditionalInfo"] == DBNull.Value ? "" : myReader["AdditionalInfo"].ToString();
        //                    this.TimeStamp = myReader["TimeStamp"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["TimeStamp"];
        //                    this.Checksum = myReader["Checksum"] == DBNull.Value ? "" : myReader["Checksum"].ToString();
        //                    this.ErrorCode = 0;
        //                    this.ErrorMsg = "";

        //                }
        //            }
        //            else
        //            {
        //                this.ErrorCode = 01;
        //                this.ErrorMsg = "Unable to find any Water Meter with DeviceID[" + deviceID + "]";
        //            }
        //            conn.Close();
        //        }


        //    }

        //    catch (Exception err)
        //    {
        //        this.ErrorCode = 99;
        //        this.ErrorMsg = err.Message;
        //    }
        //}
    }

    public class NHSensor
    {
        public int DeviceID;
        public string Command;
        public DateTime CommandSent;
        public string CommandStatus;
        public string Status;
        public string GatewayMessage;
        public DateTime ReportTime;
        public DateTime StatusReportTime;
        public int? Sensor1;
        public int? Sensor2;
        public int? Sensor3;
        public int? Sensor4;
        public int? Sensor5;
        public int? Sensor6;
        public int? Sensor1Low;
        public int? Sensor1LowSent;
        public int? Sensor1High;
        public int? Sensor1HighSent;
        public int? Sensor2Low;
        public int? Sensor2LowSent;
        public int? Sensor2High;
        public int? Sensor2HighSent;
        public int? Sensor3Low;
        public int? Sensor3LowSent;
        public int? Sensor3High;
        public int? Sensor3HighSent;
        public int? Sensor4Low;
        public int? Sensor4LowSent;
        public int? Sensor4High;
        public int? Sensor4HighSent;
        public int? Sensor5Low;
        public int? Sensor5LowSent;
        public int? Sensor5High;
        public int? Sensor5HighSent;
        public int? Sensor6Low;
        public int? Sensor6LowSent;
        public int? Sensor6High;
        public int? Sensor6HighSent;
        public int? SignalStrength;
        public bool? LogEnabled;
        public int? LogInterval;
        public int? LogIntervalSent;
        public string RelayStatus;

        public int ErrorCode;
        public string ErrorMsg;

        public NHSensor()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010199;
                this.ErrorMsg = err.Message;
            }
        }

        public NHSensor(int deviceID)
        {
            try
            {
                this.DeviceID = deviceID;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetSensor", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {

                            this.Command = myReader["Command"] == DBNull.Value ? "" : myReader["Command"].ToString();
                            this.CommandSent = myReader["CommandSent"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["CommandSent"];
                            this.CommandStatus = myReader["CommandStatus"] == DBNull.Value ? "" : myReader["CommandStatus"].ToString();
                            this.Status = myReader["Status"] == DBNull.Value ? "" : myReader["Status"].ToString();
                            this.GatewayMessage = myReader["GatewayMessage"] == DBNull.Value ? "" : myReader["GatewayMessage"].ToString();
                            this.ReportTime = myReader["ReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["ReportTime"];
                            this.StatusReportTime = myReader["StatusReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["StatusReportTime"];
                            this.Sensor1 = myReader["Sensor1"] == DBNull.Value ? null : (int?)myReader["Sensor1"];
                            this.Sensor2 = myReader["Sensor2"] == DBNull.Value ? null : (int?)myReader["Sensor2"];
                            this.Sensor3 = myReader["Sensor3"] == DBNull.Value ? null : (int?)myReader["Sensor3"];
                            this.Sensor4 = myReader["Sensor4"] == DBNull.Value ? null : (int?)myReader["Sensor4"];
                            this.Sensor5 = myReader["Sensor5"] == DBNull.Value ? null : (int?)myReader["Sensor5"];
                            this.Sensor6 = myReader["Sensor6"] == DBNull.Value ? null : (int?)myReader["Sensor6"];
                            this.Sensor1Low = myReader["Sensor1Low"] == DBNull.Value ? null : (int?)myReader["Sensor1Low"];
                            this.Sensor1LowSent = myReader["Sensor1LowSent"] == DBNull.Value ? null : (int?)myReader["Sensor1LowSent"];
                            this.Sensor1High = myReader["Sensor1High"] == DBNull.Value ? null : (int?)myReader["Sensor1High"];
                            this.Sensor1HighSent = myReader["Sensor1HighSent"] == DBNull.Value ? null : (int?)myReader["Sensor1HighSent"];
                            this.Sensor2Low = myReader["Sensor2Low"] == DBNull.Value ? null : (int?)myReader["Sensor2Low"];
                            this.Sensor2LowSent = myReader["Sensor2LowSent"] == DBNull.Value ? null : (int?)myReader["Sensor2LowSent"];
                            this.Sensor2High = myReader["Sensor2High"] == DBNull.Value ? null : (int?)myReader["Sensor2High"];
                            this.Sensor2HighSent = myReader["Sensor2HighSent"] == DBNull.Value ? null : (int?)myReader["Sensor2HighSent"];
                            this.Sensor3Low = myReader["Sensor3Low"] == DBNull.Value ? null : (int?)myReader["Sensor3Low"];
                            this.Sensor3LowSent = myReader["Sensor3LowSent"] == DBNull.Value ? null : (int?)myReader["Sensor3LowSent"];
                            this.Sensor3High = myReader["Sensor3High"] == DBNull.Value ? null : (int?)myReader["Sensor3High"];
                            this.Sensor3HighSent = myReader["Sensor3HighSent"] == DBNull.Value ? null : (int?)myReader["Sensor3HighSent"];
                            this.Sensor4Low = myReader["Sensor4Low"] == DBNull.Value ? null : (int?)myReader["Sensor4Low"];
                            this.Sensor4LowSent = myReader["Sensor4LowSent"] == DBNull.Value ? null : (int?)myReader["Sensor4LowSent"];
                            this.Sensor4High = myReader["Sensor4High"] == DBNull.Value ? null : (int?)myReader["Sensor4High"];
                            this.Sensor4HighSent = myReader["Sensor4HighSent"] == DBNull.Value ? null : (int?)myReader["Sensor4HighSent"];
                            this.Sensor5Low = myReader["Sensor5Low"] == DBNull.Value ? null : (int?)myReader["Sensor5Low"];
                            this.Sensor5LowSent = myReader["Sensor5LowSent"] == DBNull.Value ? null : (int?)myReader["Sensor5LowSent"];
                            this.Sensor5High = myReader["Sensor5High"] == DBNull.Value ? null : (int?)myReader["Sensor5High"];
                            this.Sensor5HighSent = myReader["Sensor5HighSent"] == DBNull.Value ? null : (int?)myReader["Sensor5HighSent"];
                            this.Sensor6Low = myReader["Sensor6Low"] == DBNull.Value ? null : (int?)myReader["Sensor6Low"];
                            this.Sensor6LowSent = myReader["Sensor6LowSent"] == DBNull.Value ? null : (int?)myReader["Sensor6LowSent"];
                            this.Sensor6High = myReader["Sensor6High"] == DBNull.Value ? null : (int?)myReader["Sensor6High"];
                            this.Sensor6HighSent = myReader["Sensor6HighSent"] == DBNull.Value ? null : (int?)myReader["Sensor6HighSent"];
                            this.SignalStrength = myReader["SignalStrength"] == DBNull.Value ? null : (int?)myReader["SignalStrength"];
                            this.LogEnabled = myReader["LogEnabled"] == DBNull.Value ? null : (bool?)myReader["LogEnabled"];
                            this.LogInterval = myReader["LogInterval"] == DBNull.Value ? null : (int?)myReader["LogInterval"];
                            this.LogIntervalSent = myReader["LogIntervalSent"] == DBNull.Value ? null : (int?)myReader["LogIntervalSent"];
                            this.RelayStatus = myReader["RelayStatus"] == DBNull.Value ? "" : myReader["RelayStatus"].ToString();

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";

                        }
                    }
                    else
                    {
                        this.ErrorCode = 01;
                        this.ErrorMsg = "Unable to find any sensor with DeviceID[" + deviceID + "]";
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }
        }
    }

    public class RelaySensor
    {
        public int ErrorCode;
        public string ErrorMsg;
        public string dev;

        public RelaySensor()
        {
            try
            {
                this.dev = "";
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010199;
                this.ErrorMsg = err.Message;
            }
        }

        public RelaySensor(NHDevice Device)
        {
            try
            {
                this.dev = "";

                if ((Device.Esn.StartsWith("S")) && (Device.Description.StartsWith("GSR")) && (Device.Description.Substring(5, 1) == "L"))
                {
                    this.dev = "Sensor";
                }
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }
        }

     }

    public class NHSensorReading
    {
        public int DeviceID;
        public int PacketID;
        public DateTime SensorReportTime;
        public int Sensor1;
        public int Sensor2;
        public int Sensor3;
        public int Sensor4;
        public int Sensor5;
        public int Sensor6;

        public int ErrorCode;
        public string ErrorMsg;

        public NHSensorReading()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010199;
                this.ErrorMsg = err.Message;
            }
        }

        public NHSensorReading(int deviceID)
        {
            try
            {
                this.DeviceID = deviceID;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetSensorReading", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@DeviceID", SqlDbType.Int).Value = deviceID;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            //this.DeviceID = (int)myReader["DeviceID"];
                            this.PacketID = (int)myReader["PacketID"];
                            this.SensorReportTime = myReader["SensorReportTime"] == DBNull.Value ? DateTime.MinValue : (DateTime)myReader["SensorReportTime"];
                            this.Sensor1 = myReader["Sensor1"] == DBNull.Value ? 0 : (int)myReader["Sensor1"];
                            this.Sensor2 = myReader["Sensor2"] == DBNull.Value ? 0 : (int)myReader["Sensor2"];
                            this.Sensor3 = myReader["Sensor3"] == DBNull.Value ? 0 : (int)myReader["Sensor3"];
                            this.Sensor4 = myReader["Sensor4"] == DBNull.Value ? 0 : (int)myReader["Sensor4"];
                            this.Sensor5 = myReader["Sensor5"] == DBNull.Value ? 0 : (int)myReader["Sensor5"];
                            this.Sensor6 = myReader["Sensor6"] == DBNull.Value ? 0 : (int)myReader["Sensor6"];

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";

                        }
                    }
                    else
                    {
                        this.ErrorCode = 01;
                        this.ErrorMsg = "Unable to find any sensor with DeviceID[" + deviceID + "]";
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }
        }

    }


    public class NHANSIC1219
    {
        public string ClientID;
        public int DeviceType;
        public byte PSEMIdentity;   //1 byte
        public byte[] PacketSize;     //2 bytes
        public byte NbrPackets;     //1 byte
        public byte BaudRate;       //1 byte
        public byte[] UserId;       //2 bytes
        public byte[] User;         //10 bytes 
        public byte[] Password;     //20 bytes
        public byte Time;         //1 byte
        public int ErrorCode;
        public string ErrorMsg;


        public NHANSIC1219()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010199;
                this.ErrorMsg = err.Message;
            }
        }


        public NHANSIC1219(string ClientID, int DeviceType)
        {
            try
            {
                this.ClientID = ClientID;
                this.DeviceType = DeviceType;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Device_GetANSIC1219", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@ClientID", SqlDbType.VarChar).Value = this.ClientID;
                    command.Parameters.Add("@DeviceType", SqlDbType.Int).Value = this.DeviceType;
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {

                            this.PSEMIdentity = NHUtils.ToByte(myReader["PSEMIdentity"].ToString());  //1 byte                                
                            this.PacketSize = NHUtils.ToByteArray(myReader["PacketSize"].ToString());     //2 bytes                            
                            this.NbrPackets = NHUtils.ToByte(myReader["NbrPackets"].ToString());     //1 byte
                            this.BaudRate = NHUtils.ToByte(myReader["BaudRate"].ToString());       //1 byte
                            this.UserId = NHUtils.ToByteArray(myReader["UserId"].ToString());       //2 bytes
                            this.User = NHUtils.ToByteArray(myReader["User"].ToString());         //10 bytes 
                            this.Password = NHUtils.ToByteArray(myReader["Password"].ToString());     //20 bytes
                            this.Time = NHUtils.ToByte(myReader["Time"].ToString());         //1 byte

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";

                        }
                    }
                    else
                    {
                        this.ErrorCode = 01;
                        this.ErrorMsg = "Unable to find permission for for devicetype[" + DeviceType + "] and cleintid[" + ClientID + "]";
                    }
                    conn.Close();
                }
            }

            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }
        }
    }


    public class NHSynapse
    {
        public string LeafAddress;
        public string MD;
        public int ErrorCode;
        public string ErrorMsg;

        public NHSynapse()
        {
            try
            {
                this.MD = "";
                this.LeafAddress = "";
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }

            catch (Exception err)
            {
                this.ErrorCode = 04010199;
                this.ErrorMsg = err.Message;
            }

        }

        public NHSynapse(NHDevice Device)
        {
            try
            {
                //Add Synapase Mesh wrapper
                //deviceType 39
                //or 
                //6 digit hex value
                //it will have a hub address                
                /*
                 * Moshiul Arefin 1/20/2015
                 * Commenting out the mesh wrapper part b/c it will be dealt with in the application layer
                if ((device.DeviceType.CompareTo(39) == 0 ||
                            (device.Phone.Length == 6 && NHUtils.IsHexString(device.Phone))
                         )
                    && device.HubAddress != "")
                {
                    //md + src + dest + len + opcode + parameter
                    //Payload[!!12345MD5E8E3F,5E8D98,004,GV44]
                    SendPayload = device.MsgPrefix + "12345MD" + new NHDevice("Phone", device.HubAddress).HubAddress + "," + device.Phone + ",004," + opCode + opCodeSuffix;

                }
                */

                this.MD = "";
                this.LeafAddress = "";

                if ((Device.DeviceType.CompareTo(39) == 0 ||
                            (Device.Phone.Length == 6 && NHUtils.IsHexString(Device.Phone))
                         )
                    && Device.HubAddress != "")
                {
                    this.MD = "MD";
                    this.LeafAddress = "," + Device.Phone;
                }


                this.ErrorCode = 0;
                this.ErrorMsg = "";


            }

            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }

        }
    }
    
}





