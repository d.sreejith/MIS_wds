﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Services;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Data;
using System.Data.OleDb;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using NHDeviceClass;
using System.Diagnostics;

using McWane.Industrial.Contracts;
using McWane.Industrial.DAL;
using McWane.Industrial.Utility;
using NHUtilsClass;

namespace McWane.Industrial.Business
{
    public class SensorBusiness
    {
        SensorDAL obj_SensorDAL = new SensorDAL();
        SecurityDAL obj_SecurityDAL = new SecurityDAL();
        SensorFormula obj_SensorFormula = new SensorFormula();

        public int CreateFormula(string Clientid, FormulaConfiguration data)
        {
            try
            {
                int result = obj_SensorDAL.CreateForumula(Clientid, data);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteForumula(string Clientid, int Formulaid)
        {
            try
            {
                int result = obj_SensorDAL.DeleteForumula(Clientid, Formulaid);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BulkUpdatePortConfiguration(int Deviceid, Port data)
        {
            try
            {
                obj_SensorDAL.BulkUpdatePortConfiguration(Deviceid, data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteAlert(string Clientid, int Alertid)
        {
            try
            {
                int result = obj_SensorDAL.DeleteAlert(Clientid, Alertid);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CreateAlert(string Clientid, AlertConfiguration data)
        {
            try
            {
                int result = obj_SensorDAL.CreateAlert(Clientid, data);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateAlert(string Clientid, AlertConfiguration data)
        {
            try
            {
                int result = obj_SensorDAL.UpdateAlert(Clientid, data);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetPortConfiguration(int Deviceid)
        {
            try
            {
                DataTable dt_PortConfig = obj_SensorDAL.GetPortConfiguration(Deviceid);
                return dt_PortConfig;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ValidateSecretKey(string SecretKey)
        {
            try
            {
                bool validateStatus = obj_SecurityDAL.ValidateSecretKey(SecretKey);
                return validateStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CreateSecretKey(string Clientid)
        {
            try
            {
                string key = obj_SecurityDAL.CreateSecretKey(Clientid);
                return key;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MHDevice GetMHDeviceConfig(int Deviceid)
        {
            MHDevice obj_DeviceConfig = new MHDevice();
            NHDevice obj_Device = new NHDevice(Deviceid);
            NHSensor obj_Sensor = new NHSensor(Deviceid);
            DataTable dt_PortConfig = GetPortConfiguration(Deviceid);



            if (obj_Device.DeviceID != 0)
            {
                obj_DeviceConfig.DeviceID = obj_Device.DeviceID;
                obj_DeviceConfig.DeviceType = obj_Device.DeviceType;

                for (int i = 0; i < dt_PortConfig.Rows.Count; i++)
                {
                    SensorPort obj_SensorPort = new SensorPort();

                    obj_SensorPort.Port = dt_PortConfig.Rows[i]["PortNumber"].ToString();
                    obj_SensorPort.FormulaID = dt_PortConfig.Rows[i]["FormulaID"].ToString();
                    obj_SensorPort.SensorLabel = dt_PortConfig.Rows[i]["SensorLabel"].ToString();
                    obj_SensorPort.LowRange = dt_PortConfig.Rows[i]["LowRange"].ToString();
                    obj_SensorPort.HighRange = dt_PortConfig.Rows[i]["HighRange"].ToString();
                    obj_SensorPort.SensorType = dt_PortConfig.Rows[i]["SensorType"].ToString();
                    obj_SensorPort.Formula = dt_PortConfig.Rows[i]["Formula"].ToString();
                    obj_SensorPort.WCDefaultFormula = dt_PortConfig.Rows[i]["WCDefaultFormula"].ToString();
                    obj_SensorPort.Unit = dt_PortConfig.Rows[i]["Unit"].ToString();
                    obj_SensorPort.Name = dt_PortConfig.Rows[i]["Name"].ToString();

                    //For Port 1 set low & high threshold value
                    if (obj_SensorPort.Port == "1")
                    {
                        if (obj_Sensor.Sensor1Low == 0)
                        {
                            obj_SensorPort.LTE = "false";
                            obj_SensorPort.LTV = Double.Parse(obj_Sensor.Sensor1Low.ToString());
                        }
                        else
                        {
                            obj_SensorPort.LTE = "true";
                            obj_SensorPort.LTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor1LowSent.GetValueOrDefault(), obj_Device.DeviceType, 1);
                        }
                        if (obj_Sensor.Sensor1High == 0)
                        {
                            obj_SensorPort.HTE = "false";
                            obj_SensorPort.HTV = Double.Parse(obj_Sensor.Sensor1High.ToString());
                        }
                        else
                        {
                            obj_SensorPort.HTE = "true";
                            obj_SensorPort.HTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor1HighSent.GetValueOrDefault(), obj_Device.DeviceType, 1);
                        }
                    }
                    //For Port 2 set low & high threshold value
                    if (obj_SensorPort.Port == "2")
                    {
                        if (obj_Sensor.Sensor2Low == 0)
                        {
                            obj_SensorPort.LTE = "false";
                            obj_SensorPort.LTV = Double.Parse(obj_Sensor.Sensor2Low.ToString());
                        }
                        else
                        {
                            obj_SensorPort.LTE = "true";
                            obj_SensorPort.LTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor2LowSent.GetValueOrDefault(), obj_Device.DeviceType, 2);
                        }
                        if (obj_Sensor.Sensor2High == 0)
                        {
                            obj_SensorPort.HTE = "false";
                            obj_SensorPort.HTV = Double.Parse(obj_Sensor.Sensor2High.ToString());
                        }
                        else
                        {
                            obj_SensorPort.HTE = "true";
                            obj_SensorPort.HTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor2HighSent.GetValueOrDefault(), obj_Device.DeviceType, 2);
                        }
                    }
                    //For Port 3 set low & high threshold value
                    if (obj_SensorPort.Port == "3")
                    {
                        if (obj_Sensor.Sensor3Low == 0)
                        {
                            obj_SensorPort.LTE = "false";
                            obj_SensorPort.LTV = Double.Parse(obj_Sensor.Sensor3Low.ToString());
                        }
                        else
                        {
                            obj_SensorPort.LTE = "true";
                            obj_SensorPort.LTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor3LowSent.GetValueOrDefault(), obj_Device.DeviceType, 3);
                        }
                        if (obj_Sensor.Sensor3High == 0)
                        {
                            obj_SensorPort.HTE = "false";
                            obj_SensorPort.HTV = Double.Parse(obj_Sensor.Sensor3High.ToString());
                        }
                        else
                        {
                            obj_SensorPort.HTE = "true";
                            obj_SensorPort.HTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor3HighSent.GetValueOrDefault(), obj_Device.DeviceType, 3);
                        }
                    }
                    //For Port 4 set low & high threshold value
                    if (obj_SensorPort.Port == "4")
                    {
                        if (obj_Sensor.Sensor4Low == 0)
                        {
                            obj_SensorPort.LTE = "false";
                            obj_SensorPort.LTV = Double.Parse(obj_Sensor.Sensor4Low.ToString());
                        }
                        else
                        {
                            obj_SensorPort.LTE = "true";
                            obj_SensorPort.LTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor4LowSent.GetValueOrDefault(), obj_Device.DeviceType, 4);
                        }
                        if (obj_Sensor.Sensor4High == 0)
                        {
                            obj_SensorPort.HTE = "false";
                            obj_SensorPort.HTV = Double.Parse(obj_Sensor.Sensor4High.ToString());
                        }
                        else
                        {
                            obj_SensorPort.HTE = "true";
                            obj_SensorPort.HTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor4HighSent.GetValueOrDefault(), obj_Device.DeviceType, 4);
                        }
                    }
                    //For Port 5 set low & high threshold value
                    if (obj_SensorPort.Port == "5")
                    {
                        if (obj_Sensor.Sensor5Low == 0)
                        {
                            obj_SensorPort.LTE = "false";
                            obj_SensorPort.LTV = Double.Parse(obj_Sensor.Sensor5Low.ToString());
                        }
                        else
                        {
                            obj_SensorPort.LTE = "true";
                            obj_SensorPort.LTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor5LowSent.GetValueOrDefault(), obj_Device.DeviceType, 5);
                        }
                        if (obj_Sensor.Sensor5High == 0)
                        {
                            obj_SensorPort.HTE = "false";
                            obj_SensorPort.HTV = Double.Parse(obj_Sensor.Sensor5High.ToString());
                        }
                        else
                        {
                            obj_SensorPort.HTE = "true";
                            obj_SensorPort.HTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor5HighSent.GetValueOrDefault(), obj_Device.DeviceType, 5);
                        }
                    }
                    //For Port 6 set low & high threshold value
                    if (obj_SensorPort.Port == "6")
                    {
                        if (obj_Sensor.Sensor6Low == 0)
                        {
                            obj_SensorPort.LTE = "false";
                            obj_SensorPort.LTV = Double.Parse(obj_Sensor.Sensor6Low.ToString());
                        }
                        else
                        {
                            obj_SensorPort.LTE = "true";
                            obj_SensorPort.LTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor6LowSent.GetValueOrDefault(), obj_Device.DeviceType, 6);
                        }
                        if (obj_Sensor.Sensor6High == 0)
                        {
                            obj_SensorPort.HTE = "false";
                            obj_SensorPort.HTV = Double.Parse(obj_Sensor.Sensor6High.ToString());
                        }
                        else
                        {
                            obj_SensorPort.HTE = "true";
                            obj_SensorPort.HTV = SensorFormula.ADCToSensorValueByDeviceType(obj_Sensor.Sensor6HighSent.GetValueOrDefault(), obj_Device.DeviceType, 6);
                        }
                    }
                    obj_DeviceConfig.SensorPort.Add(obj_SensorPort);
                }
                return obj_DeviceConfig;
            }
            else
            {
                throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
            }
        }

        public void SendReportCommand(string reverseFormula, AlertConfiguration data)
        {
            string parameter = obj_SensorFormula.GETParameter(reverseFormula);

            string reverseFormulaForLowThreshold = reverseFormula.Replace(parameter.Trim(), data.LowThreshold.ToString());
            double rawValueOfLowThreshold = Convert.ToDouble(new NCalc.Expression(reverseFormulaForLowThreshold).Evaluate());

            string reverseFormulaForHighThreshold = reverseFormula.Replace(parameter.Trim(), data.HighThreshold.ToString());
            double rawValueOfHighThreshold = Convert.ToDouble(new NCalc.Expression(reverseFormulaForHighThreshold).Evaluate());

            int vl1 = (int)rawValueOfLowThreshold;
            int vh1 = (int)rawValueOfHighThreshold;

            /* <TODO> implement CurrentSession.UserID */
            //string CurrentSession.UserID = "";
            string CurrentSession = "WDS User";
            //string opCodeSuffix = "2," + vl1.ToString().PadLeft(4, '0') + "," + vh1.ToString().PadLeft(4, '0') + "," + vl2.ToString().PadLeft(4, '0') + "," + vh2.ToString().PadLeft(4, '0') + ",";
            string opCodeSuffix = "1," + vl1.ToString().PadLeft(4, '0') + "," + vh1.ToString().PadLeft(4, '0') + ",";

            NHSensor sensor = new NHSensor(data.DeviceID);
            sensor.Command = new NHDeviceControl("ET").ControlName;
            sensor.CommandSent = DateTime.Now;
            sensor.CommandStatus = "SENDING";

            if (data.PortNumber == "1")
            {
                data.PortNumber = "0"; /*Device port number mapping*/

                sensor.Sensor1LowSent = vl1;
                sensor.Sensor1HighSent = vh1;
            }
            if (data.PortNumber == "2")
            {
                data.PortNumber = "1"; /*Device port number mapping*/

                sensor.Sensor2LowSent = vl1;
                sensor.Sensor2HighSent = vh1;
            }
            if (data.PortNumber == "3")
            {
                data.PortNumber = "2"; /*Device port number mapping*/

                sensor.Sensor3LowSent = vl1;
                sensor.Sensor3HighSent = vh1;
            }
            if (data.PortNumber == "4")
            {
                sensor.Sensor4LowSent = vl1;
                sensor.Sensor4HighSent = vh1;
            }
            if (data.PortNumber == "5")
            {
                sensor.Sensor5LowSent = vl1;
                sensor.Sensor5HighSent = vh1;
            }
            if (data.PortNumber == "6")
            {
                sensor.Sensor6LowSent = vl1;
                sensor.Sensor6HighSent = vh1;
            }
            //NHDeviceManagement.SendUpdateReportCommand(sensor, CurrentSession.UserID, "ET", opCodeSuffix);
            //Send sensor configured data send to MHDevice
            NHDeviceManagement.SendUpdateReportCommand(sensor, CurrentSession, "ET", opCodeSuffix);
        }

        public LatestMeasurements GetMHDeviceLatestValue(int deviceid)
        {
            try
            {
                LatestMeasurements obj_SensorMeasurement = new LatestMeasurements();
                SensorDAL obj_SensorLatestValue = new SensorDAL();
                DataSet ds_SensorLatestMeasurement = obj_SensorLatestValue.GetMHDeviceLatestValue(deviceid);
                if (ds_SensorLatestMeasurement.Tables[0].Rows.Count > 0)
                {
                    obj_SensorMeasurement.DeviceID = Convert.ToInt32(ds_SensorLatestMeasurement.Tables[0].Rows[0]["DeviceID"].ToString());
                    obj_SensorMeasurement.DeviceType = Convert.ToInt32(ds_SensorLatestMeasurement.Tables[0].Rows[0]["DeviceType"].ToString());
                    DataTable dtSensorConfiguration = new DataTable();
                    dtSensorConfiguration = obj_SensorDAL.GetPortConfiguration(deviceid);

                    obj_SensorMeasurement.LatestRecord = (from _GetMHDeviceSensors in ds_SensorLatestMeasurement.Tables[0].AsEnumerable()
                                                          select new Records()
                                                          {
                                                              SensorReportTime = SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetMHDeviceSensors["SensorReportTime"])),
                                                              SensorReadings = new double[] { 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor1ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor1ProcessedValue"]) ,2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor2ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor2ProcessedValue"]),2),
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor3ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor3ProcessedValue"]),2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor4ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor4ProcessedValue"]),2),
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor5ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor5ProcessedValue"]),2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor6ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor6ProcessedValue"]),2),
                            }
                                                          }).ToList<Records>();

                    foreach (var obj in obj_SensorMeasurement.LatestRecord)
                    {
                        double[] SelectedColumns = new double[dtSensorConfiguration.Rows.Count];
                        int j = 0;
                        List<int> lstPorts = new List<int>();
                        foreach (DataRow dr in dtSensorConfiguration.Rows)
                        {
                            lstPorts.Add(int.Parse(dr["PortNumber"].ToString()));
                            string strProcessedColumnName = "Sensor" + int.Parse(dr["PortNumber"].ToString()) + "ProcessedValue";
                            SelectedColumns[j] = obj.SensorReadings[int.Parse(dr["PortNumber"].ToString()) - 1];
                            j++;
                        }
                        obj.SensorReadings = SelectedColumns;
                    }

                    return obj_SensorMeasurement;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NoOfMeasurements GetMHDeviceNoOfReadings(int deviceid, int count)
        {
            try
            {
                NoOfMeasurements obj_SensorMeasurements = new NoOfMeasurements();
                SensorDAL obj_SensorReadings = new SensorDAL();
                DataSet ds_SensorNoOfMeasurements = obj_SensorReadings.GetMHDeviceNoOfReadings(deviceid, count);

                if (ds_SensorNoOfMeasurements.Tables[0].Rows.Count > 0)
                {

                    obj_SensorMeasurements.DeviceID = Convert.ToInt32(ds_SensorNoOfMeasurements.Tables[0].Rows[0]["DeviceID"].ToString());
                    obj_SensorMeasurements.Count = count;
                    obj_SensorMeasurements.DeviceType = Convert.ToInt32(ds_SensorNoOfMeasurements.Tables[0].Rows[0]["DeviceType"].ToString());


                    DataTable dtSensorConfiguration = dtSensorConfiguration = obj_SensorDAL.GetPortConfiguration(deviceid);

                    dtSensorConfiguration = obj_SensorDAL.GetPortConfiguration(deviceid);


                    obj_SensorMeasurements.Records = (from _GetMHDeviceSensors in ds_SensorNoOfMeasurements.Tables[0].AsEnumerable()
                                                      select new Records()
                                                      {
                                                          SensorReportTime = SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetMHDeviceSensors["SensorReportTime"])),
                                                          SensorReadings = new double[] {  

                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor1ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor1ProcessedValue"]) ,2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor2ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor2ProcessedValue"]),2),
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor3ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor3ProcessedValue"]),2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor4ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor4ProcessedValue"]),2),
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor5ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor5ProcessedValue"]),2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor6ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor6ProcessedValue"]),2),
                                                         

                            }
                                                      }).ToList<Records>();

                    foreach (var obj in obj_SensorMeasurements.Records)
                    {
                        double[] SelectedColumns = new double[dtSensorConfiguration.Rows.Count];
                        int j = 0;
                        List<int> lstPorts = new List<int>();
                        foreach (DataRow dr in dtSensorConfiguration.Rows)
                        {
                            lstPorts.Add(int.Parse(dr["PortNumber"].ToString()));
                            string strProcessedColumnName = "Sensor" + int.Parse(dr["PortNumber"].ToString()) + "ProcessedValue";
                            SelectedColumns[j] = obj.SensorReadings[int.Parse(dr["PortNumber"].ToString()) - 1];
                            j++;
                        }
                        obj.SensorReadings = SelectedColumns;
                    }



                    return obj_SensorMeasurements;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HistoricMeasurements GetMHDeviceSensorReadings(int deviceid, string sensortype, string sdatetime, string edatetime)
        {
            try
            {
                HistoricMeasurements obj_SensorReadings = new HistoricMeasurements();
                SensorDAL obj_SensorReadingsBetweenDates = new SensorDAL();
                DataTable dt_SensorMeasurementsBetweenTime = obj_SensorReadingsBetweenDates.GetMHDeviceSensorReadings(deviceid, sensortype, sdatetime, edatetime);
                if (dt_SensorMeasurementsBetweenTime.Rows.Count > 0)
                {
                    obj_SensorReadings.DeviceID = Convert.ToInt32(dt_SensorMeasurementsBetweenTime.Rows[0]["DeviceID"].ToString());
                    obj_SensorReadings.DeviceType = Convert.ToInt32(dt_SensorMeasurementsBetweenTime.Rows[0]["DeviceType"].ToString());
                    string[] strSensorReportTime = dt_SensorMeasurementsBetweenTime.Rows.OfType<DataRow>().Select(k => k["SensorReportTime"].ToString()).ToArray();
                    string[] strSensor = dt_SensorMeasurementsBetweenTime.Rows.OfType<DataRow>().Select(k => k["Sensor"].ToString()).ToArray();
                    double[] dblSensorReportTime = new double[strSensorReportTime.Length];
                    for (int i = 0; i < strSensorReportTime.Length; i++)
                    {
                        dblSensorReportTime[i] = SensorFormula.ToUnixTimestamp(Convert.ToDateTime(strSensorReportTime[i]));
                    }
                    double[] dblSensor = strSensor.Select(double.Parse).ToArray();
                    //int[] intSensor = Array.ConvertAll(SensorColumn,p=>Convert.ToInt32(p));
                    obj_SensorReadings.SensorReportTime = dblSensorReportTime;
                    double[] dblSensorReadings = new double[dblSensor.Length];

                    for (int i = 0; i < dblSensor.Length; i++)
                    {
                        //doubleSensorReadings[i] = SensorFormula.ADCToSensorValueByDeviceType(intSensor[i], Convert.ToInt32(obj_SensorReadings.DeviceType), sensorport);
                        dblSensorReadings[i] = dblSensor[i];
                    }
                    obj_SensorReadings.SensorReadings = dblSensorReadings;

                    return obj_SensorReadings;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MHDevices GetAllDevices(string clientid)
        {
            try
            {
                MHDevices obj_Devices = new MHDevices();
                SensorDAL obj_AllDevices = new SensorDAL();
                DataSet ds_AllDevices = obj_AllDevices.GetAllDevices(clientid);
                if (ds_AllDevices.Tables[0].Rows.Count > 0)
                {
                    obj_Devices.AllDevices = (from _GetAllDevices in ds_AllDevices.Tables[0].AsEnumerable()
                                              select new AllDevices()
                                              {
                                                  DeviceID = Convert.ToInt32(_GetAllDevices["DeviceID"]),
                                                  DeviceType = Convert.ToInt32(_GetAllDevices["DeviceType"]),
                                                  Esn = Convert.ToString(_GetAllDevices["Esn"]),
                                              }).ToList<AllDevices>();
                    return obj_Devices;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FormulaConfig GetFormulaConfiguration(string clientid)
        {
            try
            {
                FormulaConfig obj_FormulaConfig = new FormulaConfig();
                SensorDAL obj_AllFormula = new SensorDAL();
                DataSet ds_FormulaConfig = obj_AllFormula.GetAllFormula(clientid);
                if (ds_FormulaConfig.Tables[0].Rows.Count > 0)
                {
                    obj_FormulaConfig.FormulaConfiguration = (from _GetAllFormula in ds_FormulaConfig.Tables[0].AsEnumerable()
                                                              select new FormulaConfiguration()
                                                              {
                                                                  //FormulaID = Convert.ToString(_GetAllFormula["FormulaID"]),

                                                                  /*To fix dev datatype mismatch issue - Change string to int*/
                                                                  FormulaID = Convert.ToInt32(_GetAllFormula["FormulaID"]),
                                                                  SensorType = Convert.ToString(_GetAllFormula["SensorType"]),
                                                                  Formula = Convert.ToString(_GetAllFormula["Formula"]),
                                                                  WCDefaultFormula = Convert.ToString(_GetAllFormula["WCDefaultFormula"]),
                                                                  Unit = Convert.ToString(_GetAllFormula["Unit"]),
                                                                  ReverseFormula = Convert.ToString(_GetAllFormula["ReverseFormula"]),
                                                                  Name = Convert.ToString(_GetAllFormula["Name"]),
                                                                  FileName = Convert.ToString(_GetAllFormula["FileName"])
                                                              }).ToList<FormulaConfiguration>();
                    return obj_FormulaConfig;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Alert GetAlertConfiguration(string clientid)
        {
            try
            {
                Alert obj_AlertConfig = new Alert();
                SensorDAL obj_AllAlert = new SensorDAL();
                DataSet ds_AlertConfig = obj_AllAlert.GetAllAlert(clientid);
                if (ds_AlertConfig.Tables[0].Rows.Count > 0)
                {
                    obj_AlertConfig.AlertConfiguration = (from _GetAllAlert in ds_AlertConfig.Tables[0].AsEnumerable()
                                                          select new AlertConfiguration()
                                                            {
                                                                AlertID = Convert.ToInt32(_GetAllAlert["AlertID"]),
                                                                CreatedDate = SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetAllAlert["CreatedDate"])),
                                                                AlertName = Convert.ToString(_GetAllAlert["AlertName"]),
                                                                AlertMessageForLowThreshold = Convert.ToString(_GetAllAlert["AlertMessageForLowThreshold"]),
                                                                AlertMessageForHighThreshold = Convert.ToString(_GetAllAlert["AlertMessageForHighThreshold"]),
                                                                DeviceID = Convert.ToInt32(_GetAllAlert["DeviceID"]),
                                                                ESN = Convert.ToString(_GetAllAlert["ESN"]),
                                                                SensorLabel = Convert.ToString(_GetAllAlert["SensorLabel"]),
                                                                PortNumber = Convert.ToString(_GetAllAlert["PortNumber"]),
                                                                LowThreshold = Convert.ToDouble(_GetAllAlert["LowThreshold"]),
                                                                HighThreshold = Convert.ToDouble(_GetAllAlert["HighThreshold"]),
                                                                EmailIDsForLowThreshold = Convert.ToString(_GetAllAlert["EmailIDsForLowThreshold"]),
                                                                PhoneNumbersForLowThreshold = Convert.ToString(_GetAllAlert["PhoneNumbersForLowThreshold"]),
                                                                EmailIDsForHighThreshold = Convert.ToString(_GetAllAlert["EmailIDsForHighThreshold"]),
                                                                PhoneNumbersForHighThreshold = Convert.ToString(_GetAllAlert["PhoneNumbersForHighThreshold"]),
                                                                SensorType = Convert.ToString(_GetAllAlert["SensorType"]),
                                                                CarrierIDsForLowThreshold = Convert.ToString(_GetAllAlert["CarrierIDsForLowThreshold"]),
                                                                CarrierIDsForHighThreshold = Convert.ToString(_GetAllAlert["CarrierIDsForHighThreshold"]),
                                                                Custom = Convert.ToInt32(_GetAllAlert["Custom"])
                                                            }).ToList<AlertConfiguration>();
                    return obj_AlertConfig;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AlertLog GetAlertLogs(string clientid)
        {
            try
            {
                AlertLog obj_AlertGeneration = new AlertLog();
                SensorDAL obj_AllAlertLogs = new SensorDAL();
                DataSet ds_AlertLogs = obj_AllAlertLogs.GetAllAlertLogs(clientid);
                if (ds_AlertLogs.Tables[0].Rows.Count > 0)
                {
                    obj_AlertGeneration.AlertLogs = (from _GetAlertLogs in ds_AlertLogs.Tables[0].AsEnumerable()
                                                     select new AlertLogs()
                                                          {
                                                              ReportDate = SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetAlertLogs["ReportDate"])),
                                                              ESN = Convert.ToString(_GetAlertLogs["ESN"]),
                                                              SensorType = Convert.ToString(_GetAlertLogs["SensorType"]),
                                                              AlertMessage = Convert.ToString(_GetAlertLogs["AlertMessage"]),
                                                              EmailIDs = Convert.ToString(_GetAlertLogs["EmailIDs"]),
                                                              PhoneNumbers = Convert.ToString(_GetAlertLogs["PhoneNumbers"])
                                                          }).ToList<AlertLogs>();
                    return obj_AlertGeneration;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public WirelessProvider GetWirelessProviders()
        {
            try
            {
                WirelessProvider obj_Carrier = new WirelessProvider();
                SensorDAL obj_AllProviders = new SensorDAL();
                DataSet ds_CarrierInfo = obj_AllProviders.GetAllWirelessProvider();
                if (ds_CarrierInfo.Tables[0].Rows.Count > 0)
                {
                    obj_Carrier.CarrierInformation = (from _GetWirelessProviders in ds_CarrierInfo.Tables[0].AsEnumerable()
                                                      select new CarrierInformations()
                                                    {
                                                        CarrierID = Convert.ToInt32(_GetWirelessProviders["CarrierID"]),
                                                        Carrier = Convert.ToString(_GetWirelessProviders["Carrier"])
                                                    }).ToList<CarrierInformations>();
                    return obj_Carrier;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public ReadingsReportData GetSensorReadingsReport(ReportContract reportfilter)
        {
            try
            {
                NHUtils.WriteLog(NHSiteGlobal.LogFile, "GetSensorReadingsReport in business invoked");

                List<string> snsrColumns = new List<string>();
                snsrColumns.Add("Port1");
                snsrColumns.Add("Port2");
                snsrColumns.Add("Port3");
                snsrColumns.Add("Port4");
                snsrColumns.Add("Port5");
                snsrColumns.Add("Port6");//Sensor6ProcessedValue

                string[] strSensor = new string[snsrColumns.Count];

                ReadingsReportData obj_SensorReadings = new ReadingsReportData();

                SensorDAL obj_SensorReadingsBetweenDates = new SensorDAL();

                NHUtils.WriteLog(NHSiteGlobal.LogFile, "StartDate :" + reportfilter.StartDate + " End Date : " + reportfilter.EndDate);

                NHUtils.WriteLog(NHSiteGlobal.LogFile, "GetSensorReadingsReport in business: Startdate and enddate conversion completed.");


                DataSet dsSnsrReadingsReport = obj_SensorReadingsBetweenDates.GetSensorReadingsReport(reportfilter, reportfilter.StartDate, reportfilter.EndDate);

                NHUtils.WriteLog(NHSiteGlobal.LogFile, "GetSensorReadingsReport in business:" + "Executed DAL method");

                if (dsSnsrReadingsReport != null && dsSnsrReadingsReport.Tables.Count > 0)
                {
                    for (int i = 0; i < dsSnsrReadingsReport.Tables.Count; i++)
                    {
                        SensorReadingsData snr = new SensorReadingsData();

                        for (int j = 0; j < dsSnsrReadingsReport.Tables[i].Rows.Count; j++)
                        {
                            DataColumnCollection columns = dsSnsrReadingsReport.Tables[i].Columns;

                            snr.ESN = dsSnsrReadingsReport.Tables[i].Rows[j]["ESN"].ToString();

                            snr.DeviceID = Convert.ToInt32(dsSnsrReadingsReport.Tables[i].Rows[j]["DeviceID"].ToString());


                            string[] strSensorReportTime = dsSnsrReadingsReport.Tables[i].Rows.OfType<DataRow>().Select(k => k["SensorReportTime"].ToString()).ToArray();

                            foreach (var strColumnName in snsrColumns)
                            {
                                if (columns.Contains(strColumnName))
                                {
                                    strSensor = dsSnsrReadingsReport.Tables[i].Rows.OfType<DataRow>().Select(k => k[strColumnName].ToString()).ToArray();

                                    //double[] dblSensor = strSensor.Select(double.Parse).ToArray();                                                         
                                    //double[] dblSensorReadings = new double[dblSensor.Length];
                                    //for (int l = 0; l < dblSensor.Length; l++)
                                    //{
                                    //    dblSensorReadings[l] = dblSensor[l];
                                    //}

                                    double[] dblSensorReadings = new double[strSensor.Length];

                                    for (int l = 0; l < dblSensorReadings.Length; l++)
                                    {
                                        if (string.IsNullOrEmpty(strSensor[l]))
                                        {
                                            dblSensorReadings[l] = 0;
                                        }
                                        else
                                        {
                                            dblSensorReadings[l] = Double.Parse(strSensor[l]);
                                        }
                                    }

                                    if (strColumnName == "Port1")
                                        snr.Port1 = dblSensorReadings;
                                    if (strColumnName == "Port2")
                                        snr.Port2 = dblSensorReadings;
                                    if (strColumnName == "Port3")
                                        snr.Port3 = dblSensorReadings;
                                    if (strColumnName == "Port4")
                                        snr.Port4 = dblSensorReadings;
                                    if (strColumnName == "Port5")
                                        snr.Port5 = dblSensorReadings;
                                    if (strColumnName == "Port6")
                                        snr.Port6 = dblSensorReadings;
                                }
                            }


                            double[] dblSensorReportTime = new double[strSensorReportTime.Length];

                            for (int k = 0; k < strSensorReportTime.Length; k++)
                            {
                                dblSensorReportTime[k] = SensorFormula.ToUnixTimestamp(Convert.ToDateTime(strSensorReportTime[k]));
                            }

                            snr.SensorReportTime = dblSensorReportTime;

                            //int length = strSensor.Length;                            
                            //snr.SensorReadings = dblSensorReadings;
                            //snr.snsrReadings.Add(snrReads);                            
                        }

                        obj_SensorReadings.SensorReadingsReport.Add(snr);
                    }


                    return obj_SensorReadings;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                NHUtils.WriteLog(NHSiteGlobal.LogFile, "GetSensorReadingsReport in business exception block, Error Code[" + ex.StackTrace + "] , Error Message[" + ex.Message + "]");
                throw ex;
            }
        }

        public AlertLog GetAlertLogsByDeviceID(string clientid, int deviceid)
        {
            try
            {
                AlertLog obj_AlertGeneration = new AlertLog();
                SensorDAL obj_AllAlertLogs = new SensorDAL();
                DataSet ds_AlertLogs = obj_AllAlertLogs.GetAllAlertLogsByDeviceID(clientid, deviceid);
                if (ds_AlertLogs.Tables[0].Rows.Count > 0)
                {
                    obj_AlertGeneration.AlertLogs = (from _GetAlertLogs in ds_AlertLogs.Tables[0].AsEnumerable()
                                                     select new AlertLogs()
                                                     {
                                                         ReportDate = SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetAlertLogs["ReportDate"])),
                                                         ESN = Convert.ToString(_GetAlertLogs["ESN"]),
                                                         DeviceID = Convert.ToInt32(_GetAlertLogs["DeviceID"]),
                                                         SensorType = Convert.ToString(_GetAlertLogs["SensorType"]),
                                                         AlertMessage = Convert.ToString(_GetAlertLogs["AlertMessage"]),
                                                         EmailIDs = Convert.ToString(_GetAlertLogs["EmailIDs"]),
                                                         PhoneNumbers = Convert.ToString(_GetAlertLogs["PhoneNumbers"])
                                                     }).ToList<AlertLogs>();
                    return obj_AlertGeneration;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AlertLog GetLatestAlertLogsByDeviceID(string clientid, int deviceid, int count)
        {
            try
            {
                AlertLog obj_AlertGeneration = new AlertLog();
                SensorDAL obj_AllAlertLogs = new SensorDAL();
                DataSet ds_AlertLogs = obj_AllAlertLogs.GetLatestAlertLogsByDeviceID(clientid, deviceid, count);
                if (ds_AlertLogs.Tables[0].Rows.Count > 0)
                {
                    obj_AlertGeneration.AlertLogs = (from _GetAlertLogs in ds_AlertLogs.Tables[0].AsEnumerable()
                                                     select new AlertLogs()
                                                     {
                                                         ReportDate = SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetAlertLogs["ReportDate"])),
                                                         ESN = Convert.ToString(_GetAlertLogs["ESN"]),
                                                         DeviceID = Convert.ToInt32(_GetAlertLogs["DeviceID"]),
                                                         SensorType = Convert.ToString(_GetAlertLogs["SensorType"]),
                                                         AlertMessage = Convert.ToString(_GetAlertLogs["AlertMessage"]),

                                                         EmailIDs = Convert.ToString(_GetAlertLogs["EmailIDs"]),
                                                         PhoneNumbers = Convert.ToString(_GetAlertLogs["PhoneNumbers"])
                                                     }).ToList<AlertLogs>();
                    return obj_AlertGeneration;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetEventTriggerStatusByDeviceID(int deviceid)
        {
            string strStatus = "TIMEOUT";
            try
            {
                SensorDAL obj_GetDeviceStatus = new SensorDAL();
                strStatus = obj_GetDeviceStatus.GetEventTriggerStatusByDeviceID(deviceid);
                return strStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAlertInDevice(int Alertid)
        {
            try
            {
                int DeviceID;
                int PortNo;
                double LowThreshold;
                double HighThreshold;
                string opCodeSuffix = "2,";//number of sensors is 2 - To be modified later to support more than 2 sensors.

                SensorDAL obj_GetDeviceID = new SensorDAL();
                DataSet ds = obj_GetDeviceID.GetDeviceIdByAlertID(Alertid);/*GetDeviceIDByAlertID*/

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DeviceID = Int32.Parse(ds.Tables[0].Rows[0]["DeviceID"].ToString());
                    PortNo = Int32.Parse(ds.Tables[0].Rows[0]["PortNumber"].ToString());
                    LowThreshold = Double.Parse(ds.Tables[0].Rows[0]["LowThreshold"].ToString());
                    HighThreshold = Double.Parse(ds.Tables[0].Rows[0]["HighThreshold"].ToString());



                    NHSensor sensor = new NHSensor(DeviceID);
                    sensor.Command = new NHDeviceControl("ET").ControlName;
                    sensor.CommandSent = DateTime.Now;
                    sensor.CommandStatus = "SENDING";

                    /*1  or 9999 - To disable the events in device*/
                    /*As per Kelly to disable threshold, sending 0000 is enough*/
                    if (PortNo == 1)
                    {

                        opCodeSuffix = opCodeSuffix + LowThreshold + "," + HighThreshold;
                        //opCodeSuffix = opCodeSuffix + "," + "9999,0001";  /*ToDo concatenate with port4 value*/
                        opCodeSuffix = opCodeSuffix + "," + "0000,0000";  /*ToDo concatenate with port4 value*/

                        sensor.Sensor1LowSent = Int32.Parse(LowThreshold.ToString());
                        sensor.Sensor1HighSent = Int32.Parse(HighThreshold.ToString());
                        sensor.Sensor4LowSent = 0000;
                        sensor.Sensor4HighSent = 0000;

                    }                 
                    if (PortNo == 4)
                    {
                        opCodeSuffix = opCodeSuffix + "0000,0000" + ",";
                        opCodeSuffix = opCodeSuffix + LowThreshold + "," + HighThreshold;

                        sensor.Sensor1LowSent = 0000;
                        sensor.Sensor1HighSent = 0000;
                        sensor.Sensor4LowSent = Int32.Parse(LowThreshold.ToString());
                        sensor.Sensor4HighSent = Int32.Parse(HighThreshold.ToString());
                    }

                    NHDeviceManagement.SendUpdateReportCommand(sensor, "WDS User", "ET", opCodeSuffix);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public string CheckStatus(string DeviceID)
        {
            try
            {
                string CurrentSession = "WDS User";
                //NHSensor sensor = new NHSensor();
                //sensor.DeviceID = int.Parse(DeviceID);
                //sensor.Command = new NHDeviceControl("SP").ControlName;
                //sensor.CommandSent = DateTime.UtcNow;
                //sensor.CommandStatus = "SENDING";

                NHDevice objDevice = new NHDevice(int.Parse(DeviceID));
                NHSensor sensor = new NHSensor(int.Parse(DeviceID));
                NHSynapse Synapse = new NHSynapse(objDevice);
                sensor.Command = new NHDeviceControl("SP").ControlName;
                sensor.CommandSent = DateTime.UtcNow;
                sensor.CommandStatus = "SENDING";

                if (objDevice.DeviceType == 81)
                {
                    NHDeviceManagement.SendUpdateReportCommand(sensor, CurrentSession, Synapse.MD + "SP", Synapse.LeafAddress);
                }
                else
                {
                    NHDeviceManagement.SendUpdateReportCommand(sensor, CurrentSession, "SP", null);
                }
                return "SUCCESS";

            }
            catch (Exception ex)
            {
                throw ex;               
            }
        }



        public DeviceStatus CheckStatuspoll(int deviceid)
        {

            try
            {
                DeviceStatus obj_devisensor = new DeviceStatus();
                SensorDAL obj_sendal = new SensorDAL();
                DataSet ds_SensorNoOfMeasurements = obj_sendal.CheckStatuspoll(deviceid);
                NHDevice objDevice = new NHDevice(deviceid);

                if (ds_SensorNoOfMeasurements.Tables[0].Rows.Count > 0)
                {
                    DataTable dtSensorConfiguration = dtSensorConfiguration = obj_SensorDAL.GetPortConfiguration(deviceid);

                    obj_devisensor.CommandStatus = Convert.ToString(ds_SensorNoOfMeasurements.Tables[0].Rows[0]["CommandStatus"].ToString());


                    

                    obj_devisensor.Measurement = (from _GetMHDeviceSensors in ds_SensorNoOfMeasurements.Tables[0].AsEnumerable()
                                                  select new Devicesensorstatus()
                                                     {
                                                         StatusReportTime = (_GetMHDeviceSensors["StatusReportTime"] == DBNull.Value || _GetMHDeviceSensors["StatusReportTime"] == string.Empty) ? 0 : SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetMHDeviceSensors["StatusReportTime"])),
                                                         
                                                          SensorReadings = new double[] {  

                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor1ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor1ProcessedValue"]) ,2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor2ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor2ProcessedValue"]),2),
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor3ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor3ProcessedValue"]),2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor4ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor4ProcessedValue"]),2),
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor5ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor5ProcessedValue"]),2),                                                                 
                                Math.Round(Convert.ToDouble(_GetMHDeviceSensors["Sensor6ProcessedValue"] == DBNull.Value ? 0 : _GetMHDeviceSensors["Sensor6ProcessedValue"]),2),
                                                         

                            }
                                                     }).SingleOrDefault();

             
                    
                        double[] SelectedColumns = new double[dtSensorConfiguration.Rows.Count];
                        int j = 0;
                        List<int> lstPorts = new List<int>();
                        foreach (DataRow dr in dtSensorConfiguration.Rows)
                        {
                            lstPorts.Add(int.Parse(dr["PortNumber"].ToString()));
                            string strProcessedColumnName = "Sensor" + int.Parse(dr["PortNumber"].ToString()) + "ProcessedValue";
                            SelectedColumns[j] = obj_devisensor.Measurement.SensorReadings[int.Parse(dr["PortNumber"].ToString()) - 1];
                            j++;
                        }

                        //
                        if (objDevice.CommandStatus.ToLower() != "Completed".ToLower())
                        {

                            SelectedColumns = null;

                           
                        }




                        obj_devisensor.Measurement.SensorReadings = SelectedColumns;

             
                    return obj_devisensor;



                   
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.OK);
                }
            }
        
 
         catch (Exception ex)
            {
                throw ex;
            }
        

        }

        public Datalog Getdatalogs(string clientid)
        {
            try
            {
                string strNotAvailable = "n/a";
                Datalog obj_data = new Datalog();
                SensorDAL obj_AllDevices = new SensorDAL();
                DataSet ds_AllDevices = obj_AllDevices.Getdatalogs(clientid);
                if (ds_AllDevices.Tables[0].Rows.Count > 0)
                {
                    obj_data.devicesdatalog = (from _GetAllDevices in ds_AllDevices.Tables[0].AsEnumerable()
                                               select new Devicesdatalog()
                                               {
                                                   
                                                   DeviceID = Convert.ToInt32(_GetAllDevices["DeviceID"]),
                                                   LogEnabled = (_GetAllDevices["LogEnabled"] == DBNull.Value) ? strNotAvailable : Convert.ToString(_GetAllDevices["LogEnabled"]),
                                                   CommandStatus = Convert.ToString(_GetAllDevices["CommandStatus"]),
                                                   LogInterval = Convert.ToString(_GetAllDevices["LogInterval"]),
                                                   ESN = Convert.ToString(_GetAllDevices["ESN"]),
                                                   StatusReportTime = (_GetAllDevices["StatusReportTime"] == DBNull.Value || _GetAllDevices["StatusReportTime"] == string.Empty) ? 0 : SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetAllDevices["StatusReportTime"]))
                                               }).ToList<Devicesdatalog>();
                    return obj_data;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public string DeviceLogRecording(Devicestate data)
        {
            try
            {
                string UserID = ConfigurationManager.AppSettings["UserID"]; 
                NHDevice objDevice = new NHDevice(data.DeviceID);
                NHSensor sensor = new NHSensor(data.DeviceID);
                NHSynapse Synapse = new NHSynapse(objDevice);

                if (data.Command == "activate")
                {
                    string opcodesuffix = data.Interval.PadLeft(4, '0');
                    sensor.Command = new NHDeviceControl("LA").ControlName;
                    sensor.LogIntervalSent = Convert.ToInt32(data.Interval);

                    if (objDevice.DeviceType == 81)
                    {
                        NHDeviceManagement.SendUpdateReportCommand(sensor, UserID, Synapse.MD + "LA", opcodesuffix + ",0" + Synapse.LeafAddress);
                    }
                    else
                    {
                        NHDeviceManagement.SendUpdateReportCommand(sensor, UserID, "LA", opcodesuffix + ",0");

                    }
                }
                else if (data.Command == "deactivate")
                {
                    sensor.Command = new NHDeviceControl("LD").ControlName;
                    sensor.LogIntervalSent = 0;

                    if (objDevice.DeviceType == 81)
                    {
                        NHDeviceManagement.SendUpdateReportCommand(sensor, UserID, Synapse.MD + "LD", Synapse.LeafAddress);
                    }
                    else
                    {
                        NHDeviceManagement.SendUpdateReportCommand(sensor, UserID, "LD", null);

                    }

                }

                else if ((data.Command == "upload") && (String.IsNullOrEmpty(data.Interval)))
                {
                    sensor.Command = new NHDeviceControl("LU").ControlName;

                    NHNetwork network = new NHNetwork();
                    network.NetworkID = objDevice.Network;
                    network.Command = "SELECT";
                    NHUtils.ManageNetworks(network);

                    string IPServer = ConfigurationManager.AppSettings["IPServer"];
                    string PortNumber = ConfigurationManager.AppSettings["PortNumber"];

                    string opCodeSuffix = "000," + "000," + network.APNServer + ",";
                    opCodeSuffix += IPServer + ",";
                    opCodeSuffix += PortNumber + ",";

                    if (objDevice.DeviceType == 81)
                    {    
                        NHDeviceManagement.SendUpdateReportCommand(sensor, UserID, Synapse.MD + "LU", opCodeSuffix + Synapse.LeafAddress);
                    }
                    else
                    {
                        NHDeviceManagement.SendUpdateReportCommand(sensor, UserID, "LU", opCodeSuffix);
                    }
                }

                else if ((data.Command == "upload") && (!String.IsNullOrEmpty(data.Interval)))
                {
                    int result = obj_SensorDAL.uploadlogschedule(data);
                
                }

                return data.DeviceID + ":success";
            }
            catch (Exception ex)
            {
                return data.DeviceID + ":failure";
                throw ex;
            }
        }

        public Datalog GetDataLogSchedule(string clientid)
          {
              try
              {
                Datalog obj_data = new Datalog();
                SensorDAL obj_AllDevices = new SensorDAL();
                DataSet ds_AllDevices = obj_AllDevices.GetDataLogSchedule(clientid);
                if (ds_AllDevices.Tables[0].Rows.Count > 0)
                {
                    obj_data.devicesdatalog = (from _GetAllDevices in ds_AllDevices.Tables[0].AsEnumerable()
                                               select new Devicesdatalog()
                                               {
                                                   
                                                   DeviceID = Convert.ToInt32(_GetAllDevices["DeviceID"]),
                                                   CommandStatus = Convert.ToString(_GetAllDevices["CommandStatus"]),
                                                   LogInterval = Convert.ToString((_GetAllDevices["ScheduleDateTime"]== DBNull.Value) || (_GetAllDevices["ScheduleDateTime"]== string.Empty) ? 0 :Convert.ToInt32(SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetAllDevices["ScheduleDateTime"])))),              
                                                   ESN = Convert.ToString(_GetAllDevices["ESN"]),
                                                   StatusReportTime = (_GetAllDevices["StatusReportTime"] == DBNull.Value || _GetAllDevices["StatusReportTime"] == string.Empty) ? 0 : SensorFormula.ToUnixTimestamp(Convert.ToDateTime(_GetAllDevices["StatusReportTime"]))
                                               }).ToList<Devicesdatalog>();
                    return obj_data;
                }
                else
                {
                    throw new WebFaultException<string>("No Records", System.Net.HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

          }

    }
        }































