﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Services;
using System.Web.Security;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Web;
using System.Reflection;

using McWane.Industrial.Contracts;
using McWane.Industrial.Business;
using McWane.Industrial.Utility;
using McWane.Industrial.Security;
using NHUtilsClass;


namespace WebconnectDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RestWDService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RestWDService.svc or RestWDService.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(Namespace = "http://test.example.net", InstanceContextMode = InstanceContextMode.Single,
     ConcurrencyMode = ConcurrencyMode.Multiple, AddressFilterMode = AddressFilterMode.Any)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class RestWDService : IRestWDService
    {
        SensorBusiness obj_SensorBiz = new SensorBusiness();
        SensorFormula obj_SensorFormula = new SensorFormula();
        Authentication obj_Authentication = new Authentication();

        public Authentication GetToken()
        {
            //Authenticatin & Authorization            
            obj_Authentication = obj_Authentication.AutheticateUser();
            return obj_Authentication;
        }

        /// <summary>
        /// GetMHDevicePortConfig
        /// Get Device Sensor value by DeviceID               
        /// Created: 7/10/2015
        /// Author: Thinesh
        /// Updated: 10/13/2015 Thinesh       
        /// </summary>
        /// <param name="DeviceID"></param>
        [OperationBehavior]
        public MHDevice GetMHDeviceConfig(string Deviceid)
        {
            try
            {
                MHDevice obj_DeviceConfig = new MHDevice();
                int numeric;
                bool CheckDeviceIDHasOnlyInt = Int32.TryParse(Deviceid, out numeric);
                if (CheckDeviceIDHasOnlyInt) //To make sure deviceid contains only numerical value
                {
                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        int deviceid = Convert.ToInt32(Deviceid);
                        obj_DeviceConfig = obj_SensorBiz.GetMHDeviceConfig(deviceid);
                        return obj_DeviceConfig;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetMHDeviceLatestValue
        /// Get latest Sensor value by DeviceID            
        /// Created: 7/14/2015
        /// Author: Thinesh
        /// Updated: 7/28/2015 Thinesh       
        /// </summary>
        /// <param name="DeviceID"></param>
        [OperationBehavior]
        public LatestMeasurements GetMHDeviceLatestValue(string Deviceid)
        {
            try
            {
                int numeric;
                bool CheckDeviceIDHasOnlyInt = Int32.TryParse(Deviceid, out numeric);
                if (CheckDeviceIDHasOnlyInt) //To make sure deviceid contains only numerical value
                {
                    int deviceid = Convert.ToInt32(Deviceid); //Converting string deviceid from GET request to int type

                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();

                    if (SessionStatus)
                    {
                        LatestMeasurements obj_SensorMeasurement = new LatestMeasurements();
                        obj_SensorMeasurement = obj_SensorBiz.GetMHDeviceLatestValue(deviceid);
                        return obj_SensorMeasurement;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetMHDeviceSensors
        /// Get no of Sensor readings by DeviceID          
        /// Created: 7/10/2015
        /// Author: Thinesh
        /// Updated: 7/28/2015 Thinesh       
        /// </summary>
        /// <param name="DeviceID"></param>
        /// <param name="Count"></param>
        [OperationBehavior]
        public NoOfMeasurements GetMHDeviceNoOfReading(string Deviceid, string Count)
        {
            try
            {
                int numeric;
                bool CheckDeviceIDHasOnlyInt = Int32.TryParse(Deviceid, out numeric);
                bool CheckCountHasOnlyInt = Int32.TryParse(Count, out numeric);
                if (CheckDeviceIDHasOnlyInt && CheckCountHasOnlyInt) //To make sure deviceid and count contains only numerical value
                {
                    int deviceid = Convert.ToInt32(Deviceid);
                    int count = Convert.ToInt32(Count);

                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();

                    if (SessionStatus)
                    {
                        NoOfMeasurements obj_SensorMeasurements = new NoOfMeasurements();
                        obj_SensorMeasurements = obj_SensorBiz.GetMHDeviceNoOfReadings(deviceid, count);
                        return obj_SensorMeasurements;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetMHDeviceSensorReading
        /// Get no of Sensor readings by DeviceID, Sensor,and Date          
        /// Created: 7/29/2015
        /// Author: Thinesh
        /// Updated: 7/29/2015 Thinesh       
        /// </summary>
        /// <param name="DeviceID"></param>        
        [OperationBehavior]
        public HistoricMeasurements GetMHDeviceSensorReading(string Deviceid, string PortNumber, string epochstartdatetime, string epochenddatetime)
        {
            try
            {
                int numeric;
                bool CheckDeviceIDHasOnlyInt = Int32.TryParse(Deviceid, out numeric);
                bool CheckPortNumberHasOnlyInt = Int32.TryParse(PortNumber, out numeric);
                //bool CheckSensorHasOnlyString = System.Text.RegularExpressions.Regex.IsMatch(Sensor, @"^[a-zA-Z]+$");

                if (CheckDeviceIDHasOnlyInt && CheckPortNumberHasOnlyInt) //To make sure deviceid and port# contains only numerical value
                {
                    if (Convert.ToInt32(epochenddatetime) > Convert.ToInt32(epochstartdatetime))
                    {
                        int deviceid = Convert.ToInt32(Deviceid);
                        string Sensor = string.Empty;
                        //Validate User Token and return the status
                        bool SessionStatus = obj_Authentication.ValidateSession();
                        if (SessionStatus)
                        {
                            Sensor = "Sensor" + PortNumber + "ProcessedValue";
                            String sDateTime = SensorFormula.EpochToString(Convert.ToInt32(epochstartdatetime));
                            String eDateTime = SensorFormula.EpochToString(Convert.ToInt32(epochenddatetime));
                            HistoricMeasurements obj_SensorReadings = new HistoricMeasurements();
                            obj_SensorReadings = obj_SensorBiz.GetMHDeviceSensorReadings(deviceid, Sensor, sDateTime, eDateTime);
                            return obj_SensorReadings;
                        }
                        else
                        {
                            throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                        }
                    }
                    else
                    {
                        throw new WebFaultException<string>("End time must greater than start time", System.Net.HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetMHDevices
        /// Get all devices for a client            
        /// Created: 9/29/2015
        /// Author: Thinesh
        /// Updated: 9/29/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public MHDevices GetMHDevices(string Clientid)
        {
            try
            {
                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();
                if (SessionStatus)
                {
                    MHDevices obj_Device = new MHDevices();
                    obj_Device = obj_SensorBiz.GetAllDevices(Clientid);
                    return obj_Device;
                }
                else
                {
                    throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// GetFormulaConfiguration
        /// Get all formula configuration for a client            
        /// Created: 10/13/2015
        /// Author: Thinesh
        /// Updated: 10/13/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public FormulaConfig GetFormulaConfiguration(string Clientid)
        {

            try
            {
                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();
                if (SessionStatus)
                {
                    FormulaConfig obj_FormulaConfig = new FormulaConfig();
                    obj_FormulaConfig = obj_SensorBiz.GetFormulaConfiguration(Clientid);
                    return obj_FormulaConfig;
                }
                else
                {
                    throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// CreateForumulaConfiguration
        /// Create formula configuration            
        /// Created: 10/14/2015
        /// Author: Thinesh
        /// Updated: 10/14/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public string CreateForumulaConfiguration(string Clientid, FormulaConfiguration data)
        {
            try
            {
                string msg = string.Empty;
                if (data != null)
                {
                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        int result = obj_SensorBiz.CreateFormula(Clientid, data);
                        if (result > 0)
                        {
                            msg = result.ToString();
                        }
                        else
                        {
                            msg = "Added";
                        }
                        return msg;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("No Request body", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// DeleteForumulaConfiguration
        /// Delete formula configuration            
        /// Created: 10/15/2015
        /// Author: Thinesh
        /// Updated: 10/15/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="FormulaID"></param>
        [OperationBehavior]
        public string DeleteForumulaConfiguration(string Clientid, string Formulaid)
        {
            try
            {
                int numeric;
                bool CheckFormulaIDHasOnlyInt = Int32.TryParse(Formulaid, out numeric);
                if (CheckFormulaIDHasOnlyInt) //To make sure formulaid contains only numerical value
                {
                    int formulaid = Convert.ToInt32(Formulaid);

                    string msg = string.Empty;
                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();

                    if (SessionStatus)
                    {
                        int result = obj_SensorBiz.DeleteForumula(Clientid, formulaid);
                        if (result == 1)
                        {
                            msg = "Deleted";
                        }
                        else
                        {
                            msg = "Not Deleted";
                        }
                        return msg;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// UpdatePortConfiguration
        /// Update port configuration            
        /// Created: 10/16/2015
        /// Author: Thinesh
        /// Updated: 10/16/2015 Thinesh       
        /// </summary>
        /// <param name="DeviceID"></param>
        [OperationBehavior]
        public string UpdatePortConfiguration(string Deviceid, Port data)
        {
            try
            {
                int numeric;
                string msg = string.Empty;
                if (data.PortConfiguration.Count > 0)
                {
                    bool CheckDeviceIDHasOnlyInt = Int32.TryParse(Deviceid, out numeric);
                    if (CheckDeviceIDHasOnlyInt)
                    {
                        int deviceid = Convert.ToInt32(Deviceid);

                        //Validate User Token and return the status
                        bool SessionStatus = obj_Authentication.ValidateSession();
                        if (SessionStatus)
                        {
                            obj_SensorBiz.BulkUpdatePortConfiguration(deviceid, data);
                            return msg = "Updated";
                        }
                        else
                        {
                            throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                        }
                    }
                    else
                    {
                        throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("No Request body", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetAlertConfiguration
        /// Get all Alert configuration for a client            
        /// Created: 11/18/2015
        /// Author: Thinesh
        /// Updated: 11/18/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public Alert GetAlertConfiguration(string Clientid)
        {
            try
            {
                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();
                if (SessionStatus)
                {
                    Alert obj_AlertConfig = new Alert();
                    obj_AlertConfig = obj_SensorBiz.GetAlertConfiguration(Clientid);
                    return obj_AlertConfig;
                }
                else
                {
                    throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetAlertLogs
        /// Get all Alert Logs for a client            
        /// Created: 12/10/2015
        /// Author: Thinesh
        /// Updated: 12/10/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public AlertLog GetAlertLogs(string Clientid)
        {
            try
            {
                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();
                if (SessionStatus)
                {
                    AlertLog obj_AlertLog = new AlertLog();
                    obj_AlertLog = obj_SensorBiz.GetAlertLogs(Clientid);
                    return obj_AlertLog;
                }
                else
                {
                    throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetWirelessProviders
        /// Get all Wireless Provider          
        /// Created: 12/16/2015
        /// Author: Thinesh
        /// Updated: 12/16/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public WirelessProvider GetWirelessProviders()
        {
            try
            {
                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();
                if (SessionStatus)
                {
                    WirelessProvider obj_Carrier = new WirelessProvider();
                    obj_Carrier = obj_SensorBiz.GetWirelessProviders();
                    return obj_Carrier;
                }
                else
                {
                    throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// DeleteAlertConfiguration
        /// Delete Alert configuration            
        /// Created: 11/23/2015
        /// Author: Thinesh
        /// Updated: 11/23/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="AlertID"></param>
        [OperationBehavior]
        public string DeleteAlertConfiguration(string Clientid, string Alertid)
        {
            try
            {
                int numeric;
                bool CheckAlertIDHasOnlyInt = Int32.TryParse(Alertid, out numeric);
                if (CheckAlertIDHasOnlyInt) //To make sure alertid contains only numerical value
                {
                    int alertid = Convert.ToInt32(Alertid);
                    string msg = string.Empty;

                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        int result = obj_SensorBiz.DeleteAlert(Clientid, alertid);
                        if (result == 1)
                        {
                            msg = "Deleted";
                        }
                        else
                        {
                            msg = "Not Deleted";
                        }
                        return msg;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// CreateAlertConfiguration
        /// Create formula configuration            
        /// Created: 11/23/2015
        /// Author: Thinesh
        /// Updated: 11/23/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public string CreateAlertConfiguration(string Clientid, AlertConfiguration data)
        {
            try
            {
                string msg = string.Empty;
                if (data != null)
                {
                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        int result = obj_SensorBiz.CreateAlert(Clientid, data);
                        string reverseFormula = obj_SensorFormula.GetReverseFormula(data.DeviceID, Int32.Parse(data.PortNumber));
                        if (reverseFormula != string.Empty)
                        {
                            //Send the newly created alert configuration with includes threshold values to MHDevice on its saved
                            obj_SensorBiz.SendReportCommand(reverseFormula, data);
                        }
                        if (result > 0)
                        {

                            msg = result.ToString();
                        }
                        else
                        {
                            msg = "Not Added";
                        }
                        return msg;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("No Request body", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// UpdateAlertConfiguration
        /// Update formula configuration            
        /// Created: 11/24/2015
        /// Author: Thinesh
        /// Updated: 11/24/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public string UpdateAlertConfiguration(string Clientid, AlertConfiguration data)
        {
            try
            {
                string msg = string.Empty;
                if (data != null)
                {
                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        int result = obj_SensorBiz.UpdateAlert(Clientid, data);
                        string reverseFormula = obj_SensorFormula.GetReverseFormula(data.DeviceID, Int32.Parse(data.PortNumber));
                        if (reverseFormula != string.Empty)
                        {
                            //Send the newly updated alert configuration with includes threshold values to MHDevice on its saved
                            obj_SensorBiz.SendReportCommand(reverseFormula, data);
                        }
                        return result.ToString();
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("No Request body", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         [OperationBehavior]
        public ReadingsReportData GetSensorReadingsReport(ReportContract reportFilter)
        {
            try
            {

                if (reportFilter != null)
                {
                    if (Convert.ToInt32(reportFilter.EndDate) > Convert.ToInt32(reportFilter.StartDate))
                    {
                        //Validate User Token and return the status
                        bool SessionStatus = obj_Authentication.ValidateSession();
                        if (SessionStatus)
                        {
                            ReadingsReportData rdReport = new ReadingsReportData();

                            reportFilter.StartDate = SensorFormula.EpochToString(Convert.ToInt32(reportFilter.StartDate));
                            reportFilter.EndDate = SensorFormula.EpochToString(Convert.ToInt32(reportFilter.EndDate));

                            rdReport = obj_SensorBiz.GetSensorReadingsReport(reportFilter);

                            return rdReport;
                        }
                        else
                        {
                            throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                        }
                    }
                    else
                    {
                        throw new WebFaultException<string>("End time must greater than start time", System.Net.HttpStatusCode.BadRequest);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("No Request body", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //GetAlertLogsByDeviceID
        /// <summary>
        /// GetAlertLogs
        /// Get all Alert Logs for a client            
        /// Created: 12/10/2015
        /// Author: Thinesh
        /// Updated: 12/10/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public AlertLog GetAlertLogsByDeviceID(string Clientid, string deviceid)
        {
            try
            {
                int numeric;
                bool CheckdeviceidHasOnlyInt = Int32.TryParse(deviceid, out numeric);
                if (CheckdeviceidHasOnlyInt) //To make sure alertid contains only numerical value
                {
                    int DeviceID = Convert.ToInt32(deviceid);

                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        AlertLog obj_AlertLog = new AlertLog();
                        obj_AlertLog = obj_SensorBiz.GetAlertLogsByDeviceID(Clientid, DeviceID);
                        return obj_AlertLog;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetAlertConfiguration
        /// Get all Alert configuration for a client            
        /// Created: 11/18/2015
        /// Author: Thinesh
        /// Updated: 11/18/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public AlertLog GetLatestAlertLogsByDeviceID(string Clientid, string deviceid, string count)
        {
            try
            {
                int numeric;
                bool CheckdeviceidHasOnlyInt = Int32.TryParse(deviceid, out numeric);
                bool CheckcountHasOnlyInt = Int32.TryParse(count, out numeric);
                if (CheckdeviceidHasOnlyInt && CheckcountHasOnlyInt) //To make sure alertid contains only numerical value
                {
                    int DeviceID = Convert.ToInt32(deviceid);
                    int Count = Convert.ToInt32(count);

                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        AlertLog obj_AlertLog = new AlertLog();
                        obj_AlertLog = obj_SensorBiz.GetLatestAlertLogsByDeviceID(Clientid, DeviceID, Count);
                        return obj_AlertLog;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// GetAlertConfiguration
        /// Get all Alert configuration for a client            
        /// Created: 11/18/2015
        /// Author: Thinesh
        /// Updated: 11/18/2015 Thinesh       
        /// </summary>
        /// <param name="ClientID"></param>
        [OperationBehavior]
        public string GetEventTriggerStatusByDeviceID(string deviceid)
        {
            try
            {
                int numeric;
                string strStatus = "TIMEOUT";
                bool CheckdeviceidHasOnlyInt = Int32.TryParse(deviceid, out numeric);
                if (CheckdeviceidHasOnlyInt) //To make sure alertid contains only numerical value
                {
                    int DeviceID = Convert.ToInt32(deviceid);

                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {

                        strStatus = obj_SensorBiz.GetEventTriggerStatusByDeviceID(DeviceID);
                        return strStatus;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }



        [OperationBehavior]
        public string DeleteAlertInDevice(string Alertid)
        {
            try
            {
                int numeric;
                bool CheckAlertIDHasOnlyInt = Int32.TryParse(Alertid, out numeric);
                if (CheckAlertIDHasOnlyInt) //To make sure alertid contains only numerical value
                {
                    int alertid = Convert.ToInt32(Alertid);
                    string msg = string.Empty;

                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        /*Get the deviceidbyalertid*/
                        /*Call sendupdatereportcommand method in NHDevice.dll and disable both */
                        obj_SensorBiz.DeleteAlertInDevice(alertid);
                        return "SUCCESS";
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }


        [OperationBehavior]
        public string Checkstatus(string DeviceID)
        {
            try
            {
                string ChkStatus = string.Empty;

                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();

                if (SessionStatus)
                {

                    ChkStatus = obj_SensorBiz.CheckStatus(DeviceID);

                }
                return ChkStatus;
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }


        [OperationBehavior]
        public DeviceStatus CheckStatuspoll(string DeviceID)
        {
            try
            {
                int numeric;
                bool CheckdeviceidHasOnlyInt = Int32.TryParse(DeviceID, out numeric);

                if (CheckdeviceidHasOnlyInt) //To make sure alertid contains only numerical value
                {
                    int deviceid = Convert.ToInt32(DeviceID);

                    //Validate User Token and return the status
                    bool SessionStatus = obj_Authentication.ValidateSession();
                    if (SessionStatus)
                    {
                        DeviceStatus obj_devsensor = new DeviceStatus();
                        obj_devsensor = obj_SensorBiz.CheckStatuspoll(deviceid);
                        return obj_devsensor;
                    }
                    else
                    {
                        throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid input/datatype mismatch", System.Net.HttpStatusCode.BadRequest);
                }
            }


            catch (Exception ex)
            {
                throw ex;
            }
        }

         [OperationBehavior]
        public Datalog Getdatalogs(string clientid)
        {
            try
            {
                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();
                if (SessionStatus)
                {
                    Datalog obj_datalog = new Datalog();
                    obj_datalog = obj_SensorBiz.Getdatalogs(clientid);
                    return obj_datalog;
                }
                else
                {
                    throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


         [OperationBehavior]
        public string DeviceLogRecording(Devicestate data)
        {
            try
            {
                string  msg=string.Empty;
                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();
                if (SessionStatus)
                {
                    msg = obj_SensorBiz.DeviceLogRecording(data);
                }

                return msg;
             }
           
            catch (Exception ex)
            {
                throw ex;
            }

        }

         [OperationBehavior]
        public Datalog GetDataLogSchedule(string clientid)
        {
            try
            {
                //Validate User Token and return the status
                bool SessionStatus = obj_Authentication.ValidateSession();
                if (SessionStatus)
                {
                    Datalog obj_datalog = new Datalog();
                    obj_datalog = obj_SensorBiz.GetDataLogSchedule(clientid);
                    return obj_datalog;
                }
                else
                {
                    throw new WebFaultException<string>("Session Expired", System.Net.HttpStatusCode.Unauthorized);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}










                  
              







 