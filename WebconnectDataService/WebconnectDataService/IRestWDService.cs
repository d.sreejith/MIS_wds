﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using McWane.Industrial.Contracts;
using McWane.Industrial.Security;
using McWane.Industrial.Business;


namespace WebconnectDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRestWDService" in both code and config file together.
    [ServiceContract]
    public interface IRestWDService
    {
        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/auth_token", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        Authentication GetToken();                

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/measurement/recent/{deviceid}", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        LatestMeasurements GetMHDeviceLatestValue(string deviceid);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/measurements/{deviceid}/{count}", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        NoOfMeasurements GetMHDeviceNoOfReading(string deviceid, string count);    

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/2.0/sensor/measurements/{deviceid}/{portnumber}/{epochstartdatetime}/{epochenddatetime}", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        HistoricMeasurements GetMHDeviceSensorReading(string deviceid, string portnumber, string epochstartdatetime, string epochenddatetime);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/devices/{clientid}", ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        MHDevices GetMHDevices(string clientid);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/2.0/device/config/{deviceid}", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        MHDevice GetMHDeviceConfig(string deviceid);        

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/formula/config/{clientid}", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        FormulaConfig GetFormulaConfiguration(string clientid);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "PUT", UriTemplate = "/wds/1.0/formula/{clientid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Bare)]
        string CreateForumulaConfiguration(string clientid, FormulaConfiguration data);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "/wds/1.0/formula/{clientid}/{formulaid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Bare)]
        string DeleteForumulaConfiguration(string clientid, string formulaid);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/wds/1.0/port/config/{deviceid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Bare)]
        string UpdatePortConfiguration(string deviceid, Port data);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/alerts/{clientid}", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        Alert GetAlertConfiguration(string clientid);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/wds/1.0/alert/{clientid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Bare)]
        string CreateAlertConfiguration(string clientid, AlertConfiguration data);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/wds/1.0/alert/{clientid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Bare)]
        string UpdateAlertConfiguration(string clientid, AlertConfiguration data);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "/wds/1.0/alert/{clientid}/{alertid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Bare)]
        string DeleteAlertConfiguration(string clientid, string alertid);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/alert/logs/{clientid}", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        AlertLog GetAlertLogs(string clientid);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/carrier", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        WirelessProvider GetWirelessProviders();

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "POST", UriTemplate = "/wds/1.0/report", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        ReadingsReportData GetSensorReadingsReport(ReportContract reportFilter);


        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/alert/devicelogs/{clientid}/{deviceid}", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        AlertLog GetAlertLogsByDeviceID(string clientid, string deviceid);

       

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/alert/latestdevicelogs/{clientid}/{deviceid}/{count}", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        AlertLog GetLatestAlertLogsByDeviceID(string clientid, string deviceid, string count);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/alert/status/{deviceid}", ResponseFormat = WebMessageFormat.Json,
          BodyStyle = WebMessageBodyStyle.Bare)]
        string GetEventTriggerStatusByDeviceID(string deviceid);


        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "DELETE", UriTemplate = "/wds/1.0/alert/device/{alertid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Bare)]
        string DeleteAlertInDevice(string alertid);


        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/latest/init/{deviceid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare)]
        string Checkstatus(string DeviceID);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/measurement/latest/{deviceid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare)]
        DeviceStatus CheckStatuspoll(string DeviceID);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/dataloginterval/{clientid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare)]
        Datalog Getdatalogs(string clientid);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "POST", UriTemplate = "/wds/1.0/datalog", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare)]
        string DeviceLogRecording(Devicestate data);

        [OperationContract]
        [FaultContract(typeof(FaultException))]
        [WebInvoke(Method = "GET", UriTemplate = "/wds/1.0/datalogschedule/{clientid}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare)]
        Datalog GetDataLogSchedule(string clientid);

    }
}


