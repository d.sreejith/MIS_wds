﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Messaging;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Data.SqlTypes;
using System.Text;

using McWane.Industrial.Contracts;
using NHDeviceClass;
using McWane.Industrial.Utility;


namespace McWane.Industrial.DAL
{
    public class SensorDAL
    {
        public DataSet GetMHDeviceLatestValue(int deviceid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Devices_Sensor_Readings_GetRecentValue";
                    cmd.Parameters.AddWithValue("@DeviceID", deviceid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetMHDeviceNoOfReadings(int deviceid, int count)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Devices_Sensor_Readings_GetNoOfReadings";
                    cmd.Parameters.AddWithValue("@DeviceID", deviceid);
                    cmd.Parameters.AddWithValue("@Count", count);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetMHDeviceSensorReadings(int deviceid, string sensorcolumn, string fromdate, string todate)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Devices_Sensor_Readings_GetBetWeenDateTime";
                    cmd.Parameters.AddWithValue("@DeviceID", deviceid);
                    cmd.Parameters.AddWithValue("@SensorColumn", sensorcolumn);
                    cmd.Parameters.AddWithValue("@FromDate", fromdate);
                    cmd.Parameters.AddWithValue("@ToDate", todate);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(dt);
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAllDevices(string clientid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetAllDevices";
                    cmd.Parameters.AddWithValue("@ClientID", clientid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetPortConfiguration(int deviceid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetPortConfiguration";
                    cmd.Parameters.AddWithValue("@DeviceID", deviceid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(dt);
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAllFormula(string clientid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetFormulaConfiguration";
                    cmd.Parameters.AddWithValue("@ClientID", clientid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeletePortConfiguration(int Deviceid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_MIS_DeletePortConfiguration", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DeviceID", Deviceid);
                    cmd.ExecuteNonQuery();
                    //return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteForumula(string Clientid, int Formulaid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_MIS_DeleteFormulaConfiguration", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormulaID", Formulaid);
                    cmd.Parameters.AddWithValue("@ClientID", Clientid);
                    int result = cmd.ExecuteNonQuery();
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CreateForumula(string Clientid, FormulaConfiguration data)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_MIS_InsertOrUpdateFormulaConfiguration", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (data.FormulaID == null)
                    {
                        cmd.Parameters.AddWithValue("@FormulaID", DBNull.Value);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@FormulaID", data.FormulaID);
                    }
                    cmd.Parameters.AddWithValue("@Formula", data.Formula);
                    cmd.Parameters.AddWithValue("@WCDefaultFormula", DBNull.Value);
                    cmd.Parameters.AddWithValue("@SensorType", data.SensorType);
                    cmd.Parameters.AddWithValue("@ClientID", Clientid);
                    cmd.Parameters.AddWithValue("@Unit", data.Unit);
                    cmd.Parameters.AddWithValue("@ReverseFormula", data.ReverseFormula);
                    cmd.Parameters.AddWithValue("@Name", data.Name);
                    cmd.Parameters.AddWithValue("@FileName", data.FileName);
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BulkUpdatePortConfiguration(int Deviceid, Port data)
        {
            try
            {
                int count = data.PortConfiguration.Count;
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("DeviceID");
                dt.Columns.Add("PortNumber");
                dt.Columns.Add("FormulaID");
                dt.Columns.Add("SensorLabel");
                dt.Columns.Add("LowRange");
                dt.Columns.Add("HighRange");
                DataRow dr;
                for (int i = 0; count > i; i++)
                {
                    dr = dt.NewRow();
                    dr["DeviceID"] = Deviceid;
                    dr["PortNumber"] = data.PortConfiguration[i].Port;
                    dr["FormulaID"] = data.PortConfiguration[i].FormulaID;
                    dr["SensorLabel"] = data.PortConfiguration[i].SensorLabel;
                    dr["LowRange"] = data.PortConfiguration[i].LowRange;
                    dr["HighRange"] = data.PortConfiguration[i].HighRange;
                    dt.Rows.Add(dr);
                }
                if (dt.Rows.Count > 0)
                {
                    DeletePortConfiguration(Deviceid);
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                    {
                        SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn);
                        sqlBulkCopy.DestinationTableName = "dbo.tbl_NH_Port_Configuration";
                        conn.Open();
                        sqlBulkCopy.WriteToServer(dt);
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAllAlert(string clientid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetAlertConfiguration";
                    cmd.Parameters.AddWithValue("@ClientID", clientid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CreateAlert(string Clientid, AlertConfiguration data)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_MIS_InsertAlertConfiguration", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@AlertName", data.AlertName);
                    cmd.Parameters.AddWithValue("@AlertMessageForLowThreshold", data.AlertMessageForLowThreshold);
                    cmd.Parameters.AddWithValue("@AlertMessageForHighThreshold", data.AlertMessageForHighThreshold);
                    cmd.Parameters.AddWithValue("@DeviceID", data.DeviceID);
                    cmd.Parameters.AddWithValue("@ESN", data.ESN);
                    cmd.Parameters.AddWithValue("@SensorLabel", data.SensorLabel);
                    cmd.Parameters.AddWithValue("@PortNumber", data.PortNumber);
                    cmd.Parameters.AddWithValue("@LowThreshold", data.LowThreshold);
                    cmd.Parameters.AddWithValue("@HighThreshold", data.HighThreshold);
                    cmd.Parameters.AddWithValue("@EmailIDsForLowThreshold", data.EmailIDsForLowThreshold);
                    cmd.Parameters.AddWithValue("@PhoneNumbersForLowThreshold", data.PhoneNumbersForLowThreshold);
                    cmd.Parameters.AddWithValue("@EmailIDsForHighThreshold", data.EmailIDsForHighThreshold);
                    cmd.Parameters.AddWithValue("@PhoneNumbersForHighThreshold", data.PhoneNumbersForHighThreshold);
                    cmd.Parameters.AddWithValue("@SensorType", data.SensorType);
                    cmd.Parameters.AddWithValue("@ClientID", Clientid);
                    cmd.Parameters.AddWithValue("@CarrierIDsForLowThreshold", data.CarrierIDsForLowThreshold);
                    cmd.Parameters.AddWithValue("@CarrierIDsForHighThreshold", data.CarrierIDsForHighThreshold);
                    cmd.Parameters.AddWithValue("@Custom", data.Custom);
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateAlert(string Clientid, AlertConfiguration data)
        {
            //int result = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_MIS_UpdateAlertConfiguration", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AlertID", data.AlertID);
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@AlertName", data.AlertName);
                    cmd.Parameters.AddWithValue("@AlertMessageForLowThreshold", data.AlertMessageForLowThreshold);
                    cmd.Parameters.AddWithValue("@AlertMessageForHighThreshold", data.AlertMessageForHighThreshold);
                    cmd.Parameters.AddWithValue("@DeviceID", data.DeviceID);
                    cmd.Parameters.AddWithValue("@ESN", data.ESN);
                    cmd.Parameters.AddWithValue("@SensorLabel", data.SensorLabel);
                    cmd.Parameters.AddWithValue("@PortNumber", data.PortNumber);
                    cmd.Parameters.AddWithValue("@LowThreshold", data.LowThreshold);
                    cmd.Parameters.AddWithValue("@HighThreshold", data.HighThreshold);
                    cmd.Parameters.AddWithValue("@EmailIDsForLowThreshold", data.EmailIDsForLowThreshold);
                    cmd.Parameters.AddWithValue("@PhoneNumbersForLowThreshold", data.PhoneNumbersForLowThreshold);
                    cmd.Parameters.AddWithValue("@EmailIDsForHighThreshold", data.EmailIDsForHighThreshold);
                    cmd.Parameters.AddWithValue("@PhoneNumbersForHighThreshold", data.PhoneNumbersForHighThreshold);
                    cmd.Parameters.AddWithValue("@SensorType", data.SensorType);
                    cmd.Parameters.AddWithValue("@ClientID", Clientid);
                    cmd.Parameters.AddWithValue("@CarrierIDsForLowThreshold", data.CarrierIDsForLowThreshold);
                    cmd.Parameters.AddWithValue("@CarrierIDsForHighThreshold", data.CarrierIDsForHighThreshold);
                    cmd.Parameters.AddWithValue("@Custom", data.Custom);
                    int result = cmd.ExecuteNonQuery();
                    //result =  Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                //return result = 0;
                throw ex;
            }
        }


        public int DeleteAlert(string Clientid, int Alertid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_MIS_DeleteAlertConfiguration", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AlertID", Alertid);
                    cmd.Parameters.AddWithValue("@ClientID", Clientid);
                    int result = cmd.ExecuteNonQuery();
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAllAlertLogs(string clientid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetAlertLogs";
                    cmd.Parameters.AddWithValue("@ClientID", clientid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetAllWirelessProvider()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetAllWirelessProvider";
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetFormula(int Deviceid, int Portnumber)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_MIS_GetReverseFormula", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DeviceID", Deviceid);
                    cmd.Parameters.AddWithValue("@PortNumber", Portnumber);
                    object result = cmd.ExecuteScalar();
                    conn.Close();
                    if (result == null) //is System.DBNull
                    {
                        return string.Empty;
                    }
                    else
                    {
                        return result.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetSensorReadingsReport(ReportContract reportfilter, string StartDate, string EndDate)
        {
            try
            {
                string[] strSplitPortNo;
                string strSnsr = "Sensor";
                string strProcessedValue = "ProcessedValue";
                string strSnsrFieldName = "";
                string strAliasColumName = "Port";

                /*ToDo:: IMPORTANT Change the table name*/
                //string strSelectQry = " FROM tbl_NH_Devices_Sensor_Readings_backup WHERE DeviceID = ";
                //string strSelectQry = " FROM tbl_NH_Devices_Sensor_Readings WHERE DeviceID = ";

                string strSelectQry = " FROM tbl_NH_Devices_Sensor_Readings INNER JOIN tbl_NH_Devices ON tbl_NH_Devices_Sensor_Readings.DeviceID = tbl_NH_Devices.DeviceID WHERE tbl_NH_Devices_Sensor_Readings.DeviceID = ";

                StringBuilder strInLineQry = new StringBuilder();

                foreach (var devicefilter in reportfilter.Devices)
                {
                    strSplitPortNo = devicefilter.PortNos.Split(',');
                    if (strSplitPortNo.Length > 0)
                    {
                        for (int i = 0; i < strSplitPortNo.Length; i++)
                        {
                            strSnsrFieldName = strSnsrFieldName + strSnsr + strSplitPortNo[i].ToString() + strProcessedValue + " AS " + strAliasColumName + strSplitPortNo[i].ToString() + ",";
                        }
                    }

                    strSnsrFieldName = strSnsrFieldName.TrimEnd(',');
                    strInLineQry.Append("SELECT tbl_NH_Devices.ESN, tbl_NH_Devices_Sensor_Readings.DeviceID, SensorReportTime, " + strSnsrFieldName + strSelectQry + devicefilter.DeviceID + " AND SensorReportTime >= '" + StartDate + "' AND SensorReportTime <= '" + EndDate + "';");
                    strInLineQry.AppendLine();
                    strSnsrFieldName = "";
                }

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = conn;
                    command.CommandText = strInLineQry.ToString();
                    command.CommandType = CommandType.Text;
                    SqlDataAdapter adp = new SqlDataAdapter(command);
                    DataSet ds = new DataSet();
                    command.Connection = conn;
                    adp.Fill(ds);
                    conn.Close();
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet GetAllAlertLogsByDeviceID(string clientid, int deviceid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetAlertLogsByDeviceID";
                    cmd.Parameters.AddWithValue("@ClientID", clientid);
                    cmd.Parameters.AddWithValue("@DeviceID", deviceid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetLatestAlertLogsByDeviceID(string clientid, int deviceid, int count)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetLatestAlertLogsByDeviceID";
                    cmd.Parameters.AddWithValue("@ClientID", clientid);
                    cmd.Parameters.AddWithValue("@DeviceID", deviceid);
                    cmd.Parameters.AddWithValue("@Count", count);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetEventTriggerStatusByDeviceID(int deviceid)
        {
            string strReturnStatus = "TIMEOUT";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_GetEventTriggerStatusByDeviceID";
                    cmd.Parameters.AddWithValue("@DeviceID", deviceid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    conn.Close();

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                            strReturnStatus = ds.Tables[0].Rows[0][0].ToString();
                    }
                }
                return strReturnStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetDeviceIdByAlertID(int Alertid)
        {
            try
            {
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "sp_MIS_GetDeviceIdByAlertID";
                    command.Parameters.AddWithValue("@AlertID", Alertid);
                    SqlDataAdapter adpt = new SqlDataAdapter(command);
                    command.Connection = conn;
                    conn.Open();
                    adpt.Fill(ds);
                    conn.Close();
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet CheckStatuspoll(int deviceid)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_checkstatuspoll";
                    cmd.Parameters.AddWithValue("@DeviceID", deviceid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    conn.Close();
                    return ds;


                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet Getdatalogs(string clientid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Getdatalogs";
                    cmd.Parameters.AddWithValue("@ClientID", clientid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetDataLogSchedule(string clientid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Getuploadlogs";
                    cmd.Parameters.AddWithValue("@ClientID", clientid);
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    cmd.Connection = conn;
                    conn.Open();
                    adp.Fill(ds);
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int uploadlogschedule(Devicestate data)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_NH_Client_Manage_BatchProcessSchedule", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    DateTime scheduledatetime = SensorFormula.UnixTimeStampToUTCDateTime(Convert.ToDouble(data.Interval));
                    cmd.Parameters.AddWithValue("@ScheduleDateTime", scheduledatetime);
                    cmd.Parameters.AddWithValue("@ClientID",data.ClientID); 
                    cmd.Parameters.AddWithValue("@NextRunDateTime", scheduledatetime);
                    cmd.Parameters.AddWithValue("@ConfiguredDeviceID",Convert.ToString(data.DeviceID));
                    cmd.Parameters.AddWithValue("@Command","Insert");
                    cmd.Parameters.AddWithValue("@BatchID",51);
                    cmd.Parameters.AddWithValue("@BatchMode","Daily");
                    cmd.Parameters.AddWithValue("@ProcessStatus","Activated");
                    cmd.Parameters.AddWithValue("@Active","1");
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        }
    }
