﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Messaging;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Data.SqlTypes;
using System.Text;
using System.Runtime.Serialization;

using McWane.Industrial.Contracts;

namespace McWane.Industrial.DAL
{
    public class SecurityDAL
    {
        [DataMember]
        public string SessionID { get; set; }
        public static DateTime SessionTimeOut;

        //Check the SessionID exist in database
        public bool ValidateSecretKey(string secretKey)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Session_CheckSessionIDExist";
                    cmd.Parameters.AddWithValue("@SessionID", secretKey);
                    cmd.Connection = conn;
                    conn.Open();
                    int count = (int)cmd.ExecuteScalar();
                    if (count > 0)
                    {
                        bool IsTimeOut = CheckClientTimeOut(secretKey);
                        conn.Close();
                        return IsTimeOut;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        //Check the timeout for given sessionID
        private static bool CheckClientTimeOut(string secretKey)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Session_SelectSessionID";
                    cmd.Parameters.AddWithValue("@SessionID", secretKey);
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adp.Fill(dt);
                    //SessionTimeOut = (from DataRow dr in dt.Rows
                    //                  where (string)dr["SessionID"] == secretKey
                    //                  select (DateTime)dr["SessionTimeOut"]).FirstOrDefault();                    

                    SessionTimeOut = dt.Rows[0].Field<DateTime>("SessionTimeOut");

                    DateTime Sessiontime = SessionTimeOut;
                    DateTime Currenttime= DateTime.UtcNow;
                    int result = DateTime.Compare(Currenttime, Sessiontime);
                    if (result < 0)
                    {
                        //"Session time out is less than curren time"
                        bool IsRenewal = RenewalSessionTimeOut(secretKey);
                        return true;
                    }
                    else
                    {
                        //"Session time out is greater than current time"
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Renewal the timeout for given SessionID
        private static bool RenewalSessionTimeOut(string secretKey)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Session_UpdateSessionTimeOut";
                    cmd.Parameters.AddWithValue("@SessionID", secretKey);
                    cmd.Connection = conn;
                    conn.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Create SessionID for Client
        public string CreateSecretKey(string ClientID)
        {
            string secretKey;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_MIS_Session_InsertSessionReturnSessionID";
                    cmd.Parameters.AddWithValue("@ClientID", ClientID);
                    cmd.Parameters.AddWithValue("@RequestIP", "192.168.2.68"); //*****ToDO Get the source of request and find the IP
                    //cmd.Parameters.AddWithValue("@WhenCreated", DateTime.Now);
                    //cmd.Parameters.AddWithValue("@SessionTimeOut", DateTime.Now);

                    cmd.Parameters.AddWithValue("@WhenCreated", DateTime.UtcNow);
                    cmd.Parameters.AddWithValue("@SessionTimeOut", DateTime.UtcNow);
                    cmd.Connection = conn;
                    conn.Open();
                    secretKey = cmd.ExecuteScalar().ToString();
                    return secretKey;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
