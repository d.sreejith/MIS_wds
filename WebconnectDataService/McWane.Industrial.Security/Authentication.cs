﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;

using NHClientClass;
using NHMembershipClass;
using NHUtilsClass;
using McWane.Industrial.Business;

namespace McWane.Industrial.Security
{
    public class Authentication
    {
        SensorBusiness obj_SensorBiz = new SensorBusiness();
        [DataMember]
        public string SessionID { get; set; }           

        //Authenticate the provided header information
        public Authentication AutheticateUser()
        {
            Authentication obj_Auth = new Authentication();
            string Password = "";
            string ClientID = "";
            string Username = "";
            
            HttpContext httpContext = HttpContext.Current;            
            NameValueCollection headerList = httpContext.Request.Headers;
            var authorizationField = headerList.Get("Authorization");
            if (!string.IsNullOrEmpty(authorizationField))
            {
                string[] split = authorizationField.ToString().Split(' ');                
                if (split.Length == 2)
                {
                    string credentArray = split[1];

                    string[] credentials = credentArray.Split(':');
                    if (credentials.Length > 1 && credentials.Length == 3)
                    {
                        ClientID = credentials[0];
                        Username = credentials[1];
                        Password = credentials[2];

                        //Check the given credentials against database
                        obj_Auth = obj_Auth.DBAuthetication(ClientID, Username, Password);                        
                        return obj_Auth;
                    }
                    else
                    {                        
                        throw new WebFaultException<string>("Invalid credentials", System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Provided Header information is incorrect.", System.Net.HttpStatusCode.BadRequest);
                }
            }
            else
            {
                throw new WebFaultException<string>("No Authorization in provided header", System.Net.HttpStatusCode.BadRequest);               
            }            
        }

        //Validate the SessionID
        public bool ValidateSession()
        {
            string sessionID = "";            
            HttpContext httpContext = HttpContext.Current;
            NameValueCollection headerList = httpContext.Request.Headers;
            var authorizationField = headerList.Get("Authorization");
            if (!string.IsNullOrEmpty(authorizationField))
            {
                string[] split = authorizationField.ToString().Split(' ');
                if (split.Length == 2)
                {
                    sessionID = split[1];
                    bool IsSecretKeyValid = obj_SensorBiz.ValidateSecretKey(sessionID);
                    if (IsSecretKeyValid)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new WebFaultException<string>("Invalid credentials", System.Net.HttpStatusCode.Unauthorized);
                }
            }
            else
            {
                throw new WebFaultException<string>("No Authorization in provided header: " + authorizationField, System.Net.HttpStatusCode.BadRequest);
            }          
        }

        //Convert string into encoded(Base64) value
        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }       

        //Check the provided credentials are valid in database
        public Authentication DBAuthetication(string ClientID, string UserName, string Password)
        {

            NHMember user = NHMembership.GetUser(ClientID, UserName);
            Authentication obj_Auth = new Authentication();

            if (user.ErrorCode != 0)
            {
                if (user.ErrorCode == 03021401)
                {
                    //user doesn't exist in the system                       
                    NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[" + user.ErrorCode + "] , Error Message[" + user.ErrorMsg + "]");

                }
                else if (user.ErrorCode == 03021499)
                {
                    NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[" + user.ErrorCode + "] , Error Message[" + user.ErrorMsg + "]");
                }               
                //return "Unable to Login User, Error Code[" + user.ErrorCode + "] , Error Message[" + user.ErrorMsg + "]";

                throw new WebFaultException<string>("User does not exist in the system.", System.Net.HttpStatusCode.Unauthorized);
            }


            //if user exist in the system
            NHClient currentClient = new NHClient(ClientID);
            if (currentClient.ErrorCode != 0)
            {
                if (currentClient.ErrorCode == 01010201)
                {
                    //no client found
                    //modify the error message so we don't expose our system information to the public
                    //Error.Text = "Unable to Login User, Error Code[" + currentClient.ErrorCode + "] , Error Message[User doesn't exist in the system, Check ClientID, Username and try again]";
                }
                else if (currentClient.ErrorCode == 01010299)
                {
                    //Error.Text = "Unable to Login User, Error Code[" + currentClient.ErrorCode + "] , Error Message[Unknown], " + NHSiteGlobal.ContactMsg;
                }

                NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[" + currentClient.ErrorCode + "] , Error Message[" + currentClient.ErrorMsg + "]");                
                //return "Unable to Login User, Error Code[" + currentClient.ErrorCode + "] , Error Message[" + currentClient.ErrorMsg + "]";

                throw new WebFaultException<string>("No client found.", System.Net.HttpStatusCode.Unauthorized);
            }


            //check if the client is active
            if (!currentClient.Active)
            {
                //user can not log in b/c the client is not active
                //Error.Text = "Unable to Login User, Error Code[51070101], Error Message[ClientID is not Active]";
                NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[51070101], Error Message[ClientID is not Active]");               
                //return "Unable to Login User, Error Code[51070101], Error Message[ClientID is not Active]";

                throw new WebFaultException<string>("Client is not active. Please contact your administrator for further assistance.", System.Net.HttpStatusCode.Unauthorized);
            }

            //check if the user is a WC User                
            if (user.ApplicationID != 1)
            {
                //Error.Text = "Unable to Login User, Error Code[51070102], Error Message[User does not have permission to access WebConnect]";
                NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[51070102], Error Message[User does not have permission to access WebConnect]");                
                //return "Unable to Login User, Error Code[51070102], Error Message[User does not have permission to access WebConnect]";
            }

            //user is not active
            if (!user.IsActive)
            {
                //Error.Text = "Unable to Login User, Error Code[51070103], Error Message[User Not Active], Please Contact Your Administrator for Further Assistance]";
                NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[51070103], Error Message[User Not Active], Please Contact Your Administrator for Further Assistance]");                
                //return "Unable to Login User, Error Code[51070103], Error Message[User Not Active], Please Contact Your Administrator for Further Assistance]";

                throw new WebFaultException<string>("User is not active. Please contact your administrator for further assistance.", System.Net.HttpStatusCode.Unauthorized);
            }

            //user is lockedOut
            if (user.IsLockedOut)
            {
                //Error.Text = "Unable to Login User, Error Code[51070104], Error Message[Your account has been locked out because of maximum number of incorrect login attempts. "
                //+ "You will NOT be able to login until you contact your Administrator and have your account unlocked.]";
                NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[51070104], Error Message[User Locked Out]");               
                //return "Unable to Login User, Error Code[51070104], Error Message[User Locked Out]";

                throw new WebFaultException<string>("User locked out. Please contact your administrator for further assistance.", System.Net.HttpStatusCode.Unauthorized);
            }

            //userID exists but the user needs to register with the system. this will be the prompt user will get when the user logs in for the first time
            if (!user.IsRegistered)
            {
                //Error.Text = "Unable to Login User, Error Code[51070105], Error Message[User Not Registered], You will be redirected to the Registration page to register with the system.";
                NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[51070105], Error Message[User Not Registered]");               
                //return "Unable to Login User, Error Code[51070105], Error Message[User Not Registered]";

                throw new WebFaultException<string>("User not registered. Please contact your administrator for further assistance.", System.Net.HttpStatusCode.Unauthorized);

            }

            NHError error = new NHError();
            if (error.ErrorCode != 0)
            {
                //Error.Text = "Unable to Login User, Error Code[" + error.ErrorCode + "] , Error Message[Unknown], " + NHSiteGlobal.ContactMsg;
                NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[" + error.ErrorCode + "] , Error Message[" + error.ErrorMsg + "]");                
                //return "Unable to Login User, Error Code[" + error.ErrorCode + "] , Error Message[" + error.ErrorMsg + "]";

                throw new WebFaultException<string>("Invalid credentials. Please contact your administrator for further assistance.", System.Net.HttpStatusCode.Unauthorized);
            }

            NHMembership.AuthenticateUser(user.UserID, Password, error);

            if (error.ErrorCode != 0)
            {
                if (error.ErrorCode == 03021601)
                {
                    //Error.Text = "Unable to Login User, Error Code[" + error.ErrorCode + "] , Error Message[ClientID/UserName/Password is not valid]";

                    NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[" + error.ErrorCode + "] , Error Message[" + error.ErrorMsg + "]");

                }

                else if (error.ErrorCode == 03021699)
                {
                    //Error.Text = "Unable to Login User, Error Code[" + error.ErrorCode + "] , Error Message[Unknown], " + NHSiteGlobal.ContactMsg;
                    NHUtils.WriteLog(NHSiteGlobal.LogFile, "Unable to Login User, Error Code[" + error.ErrorCode + "] , Error Message[" + error.ErrorMsg + "]");

                }               
                throw new WebFaultException<string>("ClientID/Username/Password is not valid.", System.Net.HttpStatusCode.Unauthorized);
            }

            //If all the above are valid then create a sessionID for this client
            string SecretKey = obj_SensorBiz.CreateSecretKey(ClientID);           
            obj_Auth.SessionID = SecretKey;
            return obj_Auth;
        }       
    }
}
