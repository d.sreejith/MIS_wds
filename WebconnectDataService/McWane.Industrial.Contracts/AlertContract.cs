﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace McWane.Industrial.Contracts
{
    [DataContract]
    public class Alert
    {
        [DataMember]
        public List<AlertConfiguration> AlertConfiguration { get; set; }
    }

    [DataContract]
    public class AlertConfiguration
    {
        [DataMember]
        public int AlertID { get; set; }

        [DataMember]
        public double CreatedDate { get; set; }

        [DataMember]
        public string AlertName { get; set; }

        [DataMember]
        public string AlertMessageForLowThreshold { get; set; }

        [DataMember]
        public string AlertMessageForHighThreshold { get; set; }
        
        [DataMember]
         public int DeviceID { get; set; }

        [DataMember]
        public string ESN { get; set; }

        [DataMember]
        public string SensorLabel { get; set; }

        [DataMember]
        public string PortNumber { get; set; }

        [DataMember]
        public double LowThreshold { get; set; }

        [DataMember]
        public double HighThreshold { get; set; }

        [DataMember]
        public string EmailIDsForLowThreshold { get; set; }

        [DataMember]
        public string PhoneNumbersForLowThreshold { get; set; }

        [DataMember]
        public string EmailIDsForHighThreshold { get; set; }

        [DataMember]
        public string PhoneNumbersForHighThreshold { get; set; }

        [DataMember]
        public string SensorType { get; set; }

        [DataMember]
        public string CarrierIDsForLowThreshold { get; set; }

        [DataMember]
        public string CarrierIDsForHighThreshold { get; set; }

        [DataMember]
        public int Custom { get; set; }

    }


    [DataContract]
    public class AlertLog
    {
        [DataMember]
        public List<AlertLogs> AlertLogs { get; set; }
    }

    [DataContract]
    public class AlertLogs
    {
        [DataMember]
        public double ReportDate { get; set; }

        [DataMember]
        public string ESN { get; set; }

        [DataMember]
        public double DeviceID { get; set; }

        [DataMember]
        public string SensorType { get; set; }

        [DataMember]
        public string AlertMessage { get; set; }


        [DataMember]
        public string EmailIDs { get; set; }

        [DataMember]
        public string PhoneNumbers { get; set; }
    }

    [DataContract]
    public class WirelessProvider
    {
        [DataMember]
        public List<CarrierInformations> CarrierInformation { get; set; }
    }

    public class CarrierInformations
    {
        [DataMember]
        public int CarrierID { get; set; }

        [DataMember]
        public string Carrier { get; set; }        
    }
}
