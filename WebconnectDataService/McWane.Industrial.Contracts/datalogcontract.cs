﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace McWane.Industrial.Contracts
{

    [DataContract]
    public class Datalog
    {
        [DataMember]
        public List<Devicesdatalog> devicesdatalog { get; set; }
    }
    [DataContract]
    public class Devicesdatalog
    {
        [DataMember]
        public int DeviceID { get; set; }

        [DataMember]
        public string LogEnabled { get; set; }

        [DataMember]
        public string CommandStatus { get; set; }

        [DataMember]
        public string LogInterval { get; set; }

        [DataMember]
        public double StatusReportTime { get; set; }

        [DataMember]
        public string ESN { get; set; }
    }

    [DataContract]
    public class Devicestate
    {
        [DataMember]
        public int DeviceID { get; set; }

        [DataMember]
        public string Command{ get; set; }

        [DataMember]
        public string Interval { get; set; }

        [DataMember]
        public string ClientID { get; set; }
     }
}

