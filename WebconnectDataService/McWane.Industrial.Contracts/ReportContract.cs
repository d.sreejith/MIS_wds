﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace McWane.Industrial.Contracts
{
    [DataContract]
    public class ReportContract
    {
        [DataMember]
        public string StartDate { get; set; }

        [DataMember]
        public string EndDate { get; set; }

        [DataMember]
        public List<DeviceDetails> Devices { get; set; }
    }

    [DataContract]
    public class DeviceDetails
    {
        [DataMember]
        public int DeviceID { get; set; }

        [DataMember]
        public string ESN { get; set; }

        [DataMember]
        public string PortNos { get; set; }
    }

    [DataContract]
    public class ReadingsReportData
    {
        public ReadingsReportData()
        {
            SensorReadingsReport = new List<SensorReadingsData>();
        }

        [DataMember]
        public List<SensorReadingsData> SensorReadingsReport { get; set; }
    }


    [DataContract]
    public class SensorReadingsData
    {
        [DataMember]
        public int DeviceID { get; set; }
        [DataMember]
        public string ESN { get; set; }

        [DataMember]
        public double[] SensorReportTime { get; set; }

        [DataMember]        
        public double[] Port1 { get; set; }
        [DataMember]
        public double[] Port2 { get; set; }
        [DataMember]
        public double[] Port3 { get; set; }
        [DataMember]
        public double[] Port4 { get; set; }
        [DataMember]
        public double[] Port5 { get; set; }
        [DataMember]
        public double[] Port6 { get; set; }

    }

    

}
