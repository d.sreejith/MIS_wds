﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace McWane.Industrial.Contracts
{
    [DataContract]
    public class MHDevices
    {
        [DataMember]
        public List<AllDevices> AllDevices { get; set; }
    }

    [DataContract]
    public class AllDevices
    {
        [DataMember]
        public int DeviceID { get; set; }

        [DataMember]
        public int DeviceType { get; set; }

        [DataMember]
        public string Esn { get; set; }

    }

    [DataContract]
    public class MHDevice
    {
        public MHDevice()
        {
            SensorPort = new List<SensorPort>();
        }

        [DataMember]
        public int DeviceID { get; set; }

        [DataMember]
        public int DeviceType { get; set; }

        [DataMember]
        public List<SensorPort> SensorPort { get; set; }

    }
    [DataContract]
    public class SensorPort
    {
        public SensorPort()
        {

        }

        [DataMember]
        public string Port { get; set; }

        [DataMember]
        public string LTE { get; set; }

        [DataMember]
        public double LTV { get; set; }

        [DataMember]
        public string HTE { get; set; }

        [DataMember]
        public double HTV { get; set; }

        [DataMember]
        public string SensorLabel { get; set; }

        [DataMember]
        public string LowRange { get; set; }

        [DataMember]
        public string HighRange { get; set; }

        [DataMember]
        public string SensorType { get; set; }

        [DataMember]
        public string FormulaID { get; set; }

        [DataMember]
        public string Formula { get; set; }

        [DataMember]
        public string WCDefaultFormula { get; set; }

        [DataMember]
        public string Unit { get; set; }

        [DataMember]
        public string Name { get; set; }

    }

    [DataContract]
    public class FormulaConfig
    {
        [DataMember]
        public List<FormulaConfiguration> FormulaConfiguration { get; set; }
    }

    [DataContract]
    public class FormulaConfiguration
    {
        //[DataMember]
        //public string FormulaID { get; set; }

        /*To fix dev datatype mismatch issue - Change string to int*/
        [DataMember]
        public int FormulaID { get; set; }

        [DataMember]
        public string SensorType { get; set; }

        [DataMember]
        public string Formula { get; set; }

        [DataMember]
        public string WCDefaultFormula { get; set; }

        [DataMember]
        public string Unit { get; set; }

        [DataMember]
        public string ReverseFormula { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FileName { get; set; }

    }

    [DataContract]
    public class Port
    {
        [DataMember]
        public List<PortConfiguration> PortConfiguration { get; set; }
    }

    [DataContract]
    public class PortConfiguration
    {
        [DataMember]
        public int FormulaID { get; set; }

        [DataMember]
        public string Port { get; set; }

        [DataMember]
        public string SensorLabel { get; set; }

        [DataMember]
        public string LowRange { get; set; }

        [DataMember]
        public string HighRange { get; set; }

    }

    [DataContract]
    public class DeviceStatus
    {
        [DataMember]
        public string CommandStatus { get; set; }    

        [DataMember]
        public List<DeviceReadings> Measurement { get; set; }
    }

    [DataContract]
    public class DeviceReadings
    {


        [DataMember]
        public double StatusReportTime { get; set; }
       
        [DataMember]
        public double[] SensorReadings { get; set; }

      

    }

}
