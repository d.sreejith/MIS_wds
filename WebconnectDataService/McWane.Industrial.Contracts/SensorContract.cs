﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace McWane.Industrial.Contracts

    // NOTE: sensor contracts.
{
    [DataContract]
    public class NoOfMeasurements 
    {        
        [DataMember]
        public int DeviceID { get; set; }
        [DataMember]
        public int DeviceType { get; set; }
        [DataMember]
        public int Count { get;set; }
        [DataMember]
        public List<Records> Records { get; set; }
    }

    [DataContract]
    public class Records
    {
        [DataMember]
        public double SensorReportTime { get; set; }
        [DataMember]
        public double[] SensorReadings { get; set; }        
    }

    [DataContract]
    public class LatestMeasurements 
    {        
        [DataMember]
        public int DeviceID { get; set; }
        [DataMember]
        public int DeviceType { get; set; }
        [DataMember]
        public List<Records> LatestRecord { get; set; }       
    }

    [DataContract]
    public class HistoricMeasurements
    {
        [DataMember]
        public int DeviceID { get; set; }
        [DataMember]
        public int DeviceType { get; set; }
        [DataMember]
        public double[] SensorReportTime { get; set; }
        [DataMember]
        public double[] SensorReadings { get; set; }       
    }












    
}
