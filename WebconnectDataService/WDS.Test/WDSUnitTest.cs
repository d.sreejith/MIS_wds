﻿//unit test code
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using McWane.Industrial.Utility;
using WebconnectDataService;

namespace WDS.Test
{
    [TestClass]
    public class WDSUnitTest
    {
        [TestMethod]
        public void TestGetParameter()
        {
            SensorFormula target = new SensorFormula();
            string formula = "((((0.0011 * Pow((((330.0 / (((adc * 2.048) / 1023.0))) - 100.0) - 6) , 2)) + (2.3382 * (((330.0 / (((adc * 2.048) / 1023.0))) - 100.0) - 6)) - 244.69) * 9.0) / 5.0) + 32.0";
            string expected = "adc";
            Assert.AreEqual(expected, target.GETParameter(formula));
        }

        [TestMethod]
        public void TestToUnixTimestamp()
        {
            DateTime value = new DateTime(2014, 1, 18);
            long expected = 1390003200;
            Assert.AreEqual(expected, SensorFormula.ToUnixTimestamp(value));
        }

        //[TestMethod]
        //public void TestGetMHDeviceConfig()
        //{
        //    RestWDService target = new RestWDService();
        //    //bool SessionStatus = true;
        //    string deviceid = "19442";
        //    string expected = "fgt";
        //    Assert.AreEqual(expected, target.GetMHDeviceLatestValue(deviceid));
        //}
    }
}
