USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_MIS_Getdatalogs]    Script Date: 5/6/2016 9:59:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_MIS_Getdatalogs] 

      @ClientID nvarchar(50)

AS

BEGIN

      SET NOCOUNT ON;
SELECT ds.DeviceID,ds.CommandStatus, ds.StatusReportTime ,dss.LogEnabled,dss.LogInterval,dev.Esn
FROM [dbo].[tbl_NH_DeviceStatus] as ds

INNER JOIN [dbo].[tbl_NH_DeviceStatusSensor] as dss 
ON ds.DeviceID = dss.DeviceID 

INNER JOIN [dbo].[tbl_NH_Devices] as dev
ON ds.DeviceID = dev.DeviceID

WHERE		
		 ClientID = @ClientID

END




/**insert new column to batchprocess schedule table**/
ALTER TABLE [dbo].[tbl_NH_BatchProcessSchedule]
ADD ConfiguredDeviceID varchar




USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_MIS_Getuploadlogs]    Script Date: 5/6/2016 10:04:12 AM ******/
/**add configured DeviceID in batchprocess table **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_MIS_Getuploadlogs] 

      @ClientID nvarchar(50)

AS

BEGIN

      SET NOCOUNT ON;

select Deviceid,CommandStatus,statusreporttime,esn,scheduledatetime from
 (SELECT tbl_NH_Devices.deviceID,CommandStatus,StatusReportTime,row_number() over(partition by tbl_NH_UserActivity.deviceID order by statusreporttime desc) as rownum,
		tbl_NH_Devices.esn,tbl_NH_BatchProcessSchedule.scheduledatetime
			FROM tbl_NH_UserActivity
				INNER JOIN tbl_NH_Devices
					ON tbl_NH_Devices.DeviceID = tbl_NH_UserActivity.DeviceID 
				LEFT outer JOIN tbl_NH_BatchProcessSchedule
					ON tbl_NH_Devices.DeviceID = tbl_NH_BatchProcessSchedule.ConfiguredDeviceID 
				where Command='Sensor IP' and tbl_NH_Devices.clientid = @ClientID
) temp where temp.rownum = 1
		
END