USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_NH_MIS_GetDeviceStatusData]    Script Date: 4/26/2016 11:12:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_NH_MIS_GetDeviceStatusData]
@DeviceID int
AS	

SELECT 
	dvStatus.DeviceID, 
	ISNULL(dvSnsrStatus.Sensor1, 0) AS [Sensor1],
	ISNULL(dvSnsrStatus.Sensor2, 0) AS [Sensor2], 
	ISNULL(dvSnsrStatus.Sensor3, 0) AS [Sensor3], 
	ISNULL(dvSnsrStatus.Sensor4, 0) AS [Sensor4], 
	ISNULL(dvSnsrStatus.Sensor5, 0) AS [Sensor5], 
	ISNULL(dvSnsrStatus.Sensor6, 0) AS [Sensor6],
	dvSnsrStatus.TranslatedTime,dvStatus.statusreporttime
	 FROM 
	 tbl_NH_DeviceStatusSensor as dvSnsrStatus
	 INNER JOIN 
	 tbl_NH_DeviceStatus as dvStatus ON 
	 dvSnsrStatus.DeviceID = dvStatus.DeviceID
	 WHERE 
	
	 dvSnsrStatus.DeviceID  = @DeviceID AND
dvStatus.StatusReportTime > dvSnsrStatus.TranslatedTime










USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_NH_Device_UpdateSensorStatus]    Script Date: 4/26/2016 11:13:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[sp_NH_Device_UpdateSensorStatus] 	
	@xmlString xml
AS

BEGIN
    /* Initialize a handle for the XmlDocument */ 
    DECLARE @xmlHandle INT 
	    /* 
    Create the XmlDocument using the handle above and the Xml 
	string as parameters. If your stored procedure has an varchar input 
    parameter named @xmlString, it would look like this instead: 
    EXEC sp_xml_preparedocument @xmlHandle output,@xmlString 
    */ 
	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @xmlString 
	 /* 
    Use the OPENXML method to query the XmlDocument starting at 
    /NewDataSet/SampleDataTable node.
    */ 
	/*To update the values into tbl_NH_Devices_Sensor_Readings table*/
	MERGE tbl_NH_DeviceStatusSensor as T
	USing (SELECT *
    FROM OPENXML (@xmlHandle, '/NewDataSet/ReadingsTable',1) 
        WITH (DeviceID int '@DeviceID',
		[Sensor1ProcessedValue] float '@Sensor1ProcessedValue',
		[TranslatedTime] DateTime '@TranslatedTime'		
             )) as S
		ON T.DeviceID = S.DeviceID
	WHEN MATCHED AND T.DeviceID = S.DeviceID
		THEN
				UPDATE  SET T.Sensor1ProcessedValue = S.Sensor1ProcessedValue,
				T.TranslatedTime = S.TranslatedTime ;
    EXEC sp_xml_removedocument @xmlHandle
	--END
END





USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_NH_Device_UpdateSensorStatus2]    Script Date: 4/26/2016 11:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_NH_Device_UpdateSensorStatus2] 	
	@xmlString xml
AS
BEGIN
    /* Initialize a handle for the XmlDocument */ 
    DECLARE @xmlHandle INT 
	    /* 
    Create the XmlDocument using the handle above and the Xml 
    string as parameters. If your stored procedure has an varchar input 
    parameter named @xmlString, it would look like this instead: 
    EXEC sp_xml_preparedocument @xmlHandle output,@xmlString 
    */ 
	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @xmlString 
	 /* 
    Use the OPENXML method to query the XmlDocument starting at 
    /NewDataSet/SampleDataTable node.
    */ 
	/*To update the values into tbl_NH_Devices_Sensor_Readings table*/
	MERGE tbl_NH_DeviceStatusSensor as T
	USing (SELECT *
    FROM OPENXML (@xmlHandle, '/NewDataSet/ReadingsTable',1) 
        WITH (DeviceID int '@DeviceID',
		--[Sensor1ProcessedValue] float '@Sensor1ProcessedValue'
		[Sensor2ProcessedValue] float '@Sensor2ProcessedValue',
		[TranslatedTime] DateTime '@TranslatedTime'
		--[Sensor3ProcessedValue] float '@Sensor3ProcessedValue', 
		--[Sensor4ProcessedValue] float '@Sensor4ProcessedValue', 
		--[Sensor5ProcessedValue] float '@Sensor5ProcessedValue', 
		--[Sensor6ProcessedValue] float '@Sensor6ProcessedValue'
             )) as S
		ON T.DeviceID = S.DeviceID
	WHEN MATCHED AND T.DeviceID = S.DeviceID
		THEN
				UPDATE  SET T.Sensor2ProcessedValue = S.Sensor2ProcessedValue,
				T.TranslatedTime = S.TranslatedTime ;
    EXEC sp_xml_removedocument @xmlHandle
	--END
END









USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_NH_Device_UpdateSensorStatus3]    Script Date: 4/26/2016 11:15:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_NH_Device_UpdateSensorStatus3] 	

	

	@xmlString xml



AS

BEGIN

    /* Initialize a handle for the XmlDocument */ 

    DECLARE @xmlHandle INT 

	    /* 

    Create the XmlDocument using the handle above and the Xml 

    string as parameters. If your stored procedure has an varchar input 

    parameter named @xmlString, it would look like this instead: 

    EXEC sp_xml_preparedocument @xmlHandle output,@xmlString 

    */ 

	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @xmlString 

	 /* 

    Use the OPENXML method to query the XmlDocument starting at 

    /NewDataSet/SampleDataTable node.

    */ 

	/*To update the values into tbl_NH_Devices_Sensor_Readings table*/

	MERGE tbl_NH_DeviceStatusSensor as T

	USing (SELECT *

    FROM OPENXML (@xmlHandle, '/NewDataSet/ReadingsTable',1) 

        WITH (DeviceID int '@DeviceID',

		--[Sensor1ProcessedValue] float '@Sensor1ProcessedValue'

		--[Sensor2ProcessedValue] float '@Sensor2ProcessedValue'

		[Sensor3ProcessedValue] float '@Sensor3ProcessedValue',
		
		[TranslatedTime] DateTime '@TranslatedTime'	

		--[Sensor4ProcessedValue] float '@Sensor4ProcessedValue'

		--[Sensor5ProcessedValue] float '@Sensor5ProcessedValue'

		--[Sensor6ProcessedValue] float '@Sensor6ProcessedValue'

             )) as S

		ON T.DeviceID = S.DeviceID

	WHEN MATCHED AND T.DeviceID = S.DeviceID

		THEN

				UPDATE  SET T.Sensor3ProcessedValue = S.Sensor3ProcessedValue,
				T.TranslatedTime = S.TranslatedTime ;

    EXEC sp_xml_removedocument @xmlHandle

	--END

END







USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_NH_Device_UpdateSensorStatus4]    Script Date: 4/26/2016 11:16:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_NH_Device_UpdateSensorStatus4] 	

	

	@xmlString xml



AS

BEGIN

    /* Initialize a handle for the XmlDocument */ 

    DECLARE @xmlHandle INT 

	    /* 

    Create the XmlDocument using the handle above and the Xml 

    string as parameters. If your stored procedure has an varchar input 

    parameter named @xmlString, it would look like this instead: 

    EXEC sp_xml_preparedocument @xmlHandle output,@xmlString 

    */ 

	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @xmlString 

	 /* 

    Use the OPENXML method to query the XmlDocument starting at 

    /NewDataSet/SampleDataTable node.

    */ 

	/*To update the values into tbl_NH_Devices_Sensor_Readings table*/

	MERGE tbl_NH_DeviceStatusSensor as T

	USing (SELECT *

    FROM OPENXML (@xmlHandle, '/NewDataSet/ReadingsTable',1) 

       WITH (DeviceID int '@DeviceID',

		--[Sensor1ProcessedValue] float '@Sensor1ProcessedValue'

		--[Sensor2ProcessedValue] float '@Sensor2ProcessedValue'

		--[Sensor3ProcessedValue] float '@Sensor3ProcessedValue'

		[Sensor4ProcessedValue] float '@Sensor4ProcessedValue',
		[TranslatedTime] DateTime '@TranslatedTime'	

		--[Sensor5ProcessedValue] float '@Sensor5ProcessedValue'

		--[Sensor6ProcessedValue] float '@Sensor6ProcessedValue'

             )) as S

		ON T.DeviceID = S.DeviceID

	WHEN MATCHED AND T.DeviceID = S.DeviceID

		THEN

				UPDATE  SET T.Sensor4ProcessedValue = S.Sensor4ProcessedValue,
				T.TranslatedTime = S.TranslatedTime ;

    EXEC sp_xml_removedocument @xmlHandle

	--END

END





USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_NH_Device_UpdateSensorStatus5]    Script Date: 4/26/2016 11:17:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_NH_Device_UpdateSensorStatus5] 	

	

	@xmlString xml



AS

BEGIN

    /* Initialize a handle for the XmlDocument */ 

    DECLARE @xmlHandle INT 

	    /* 

    Create the XmlDocument using the handle above and the Xml 

    string as parameters. If your stored procedure has an varchar input 

    parameter named @xmlString, it would look like this instead: 

    EXEC sp_xml_preparedocument @xmlHandle output,@xmlString 

    */ 

	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @xmlString 

	 /* 

    Use the OPENXML method to query the XmlDocument starting at 

    /NewDataSet/SampleDataTable node.

    */ 

	/*To update the values into tbl_NH_Devices_Sensor_Readings table*/

	MERGE tbl_NH_DeviceStatusSensor as T

	USing (SELECT *

    FROM OPENXML (@xmlHandle, '/NewDataSet/ReadingsTable',1) 

       WITH (DeviceID int '@DeviceID',

		--[Sensor1ProcessedValue] float '@Sensor1ProcessedValue'

		--[Sensor2ProcessedValue] float '@Sensor2ProcessedValue'

		--[Sensor3ProcessedValue] float '@Sensor3ProcessedValue'

		--[Sensor4ProcessedValue] float '@Sensor4ProcessedValue',
		

		[Sensor5ProcessedValue] float '@Sensor5ProcessedValue',
		[TranslatedTime] DateTime '@TranslatedTime'	

		--[Sensor6ProcessedValue] float '@Sensor6ProcessedValue'

             )) as S

		ON T.DeviceID = S.DeviceID

	WHEN MATCHED AND T.DeviceID = S.DeviceID

		THEN

				UPDATE  SET T.Sensor5ProcessedValue = S.Sensor5ProcessedValue,
				T.TranslatedTime = S.TranslatedTime ;

    EXEC sp_xml_removedocument @xmlHandle

	--END

END








USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_NH_Device_UpdateSensorStatus6]    Script Date: 4/26/2016 11:18:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_NH_Device_UpdateSensorStatus6] 	

	

	@xmlString xml



AS

BEGIN

    /* Initialize a handle for the XmlDocument */ 

    DECLARE @xmlHandle INT 

	    /* 

    Create the XmlDocument using the handle above and the Xml 

    string as parameters. If your stored procedure has an varchar input 

    parameter named @xmlString, it would look like this instead: 

    EXEC sp_xml_preparedocument @xmlHandle output,@xmlString 

    */ 

	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @xmlString 

	 /* 

    Use the OPENXML method to query the XmlDocument starting at 

    /NewDataSet/SampleDataTable node.

    */ 

	/*To update the values into tbl_NH_Devices_Sensor_Readings table*/

	MERGE tbl_NH_DeviceStatusSensor as T

	USing (SELECT *

    FROM OPENXML (@xmlHandle, '/NewDataSet/ReadingsTable',1) 

       WITH (DeviceID int '@DeviceID',

		--[Sensor1ProcessedValue] float '@Sensor1ProcessedValue'

		--[Sensor2ProcessedValue] float '@Sensor2ProcessedValue'

		--[Sensor3ProcessedValue] float '@Sensor3ProcessedValue'

		--[Sensor4ProcessedValue] float '@Sensor4ProcessedValue',
		

		--[Sensor5ProcessedValue] float '@Sensor5ProcessedValue',
	

		[Sensor6ProcessedValue] float '@Sensor6ProcessedValue',
	    [TranslatedTime] DateTime '@TranslatedTime'	

             )) as S

		ON T.DeviceID = S.DeviceID

	WHEN MATCHED AND T.DeviceID = S.DeviceID

		THEN

				UPDATE  SET T.Sensor6ProcessedValue = S.Sensor6ProcessedValue,
				T.TranslatedTime = S.TranslatedTime ;

    EXEC sp_xml_removedocument @xmlHandle

	--END

END








USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_MIS_checkstatuspoll]    Script Date: 4/26/2016 11:19:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_MIS_checkstatuspoll] 
      @DeviceID int
	
AS
BEGIN
SET NOCOUNT ON;
SELECT top  1 ds.CommandStatus, ds.StatusReportTime ,dss.Sensor1ProcessedValue,dss.Sensor2ProcessedValue,dss.Sensor3ProcessedValue,dss.Sensor4ProcessedValue,dss.Sensor5ProcessedValue,dss.Sensor6ProcessedValue
FROM [dbo].[tbl_NH_DeviceStatus] as ds
INNER JOIN [dbo].[tbl_NH_DeviceStatusSensor] as dss
ON ds.DeviceID = dss.DeviceID
WHERE		
			dss. DeviceID = @DeviceID 
			 order by dss.StatusReportTime desc


			 
END








