

/**add  batchid to the batchprocess table **/
INSERT INTO tbl_NH_BatchProcess
VALUES (51,'UPLoadDataLog','','LU','Client');

/** add extra columns to the table **/
ALTER TABLE [dbo].[tbl_NH_BatchProcessSchedule]
ADD [FCSDailyReadReportFile] varchar(max) null,[ImportErrorCode] int null,[ImportErrorMessage] varchar(max) null,[NotificationOnly] bit null,[UserID] varchar(max) null






USE [Test]
GO
/****** Object:  StoredProcedure [dbo].[sp_NH_Client_Manage_BatchProcessSchedule]    Script Date: 5/9/2016 6:59:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================
-- Author:        Moshiul Arefin
-- Create date: 11/05/2011
-- Description:   Select/add/edit/delete BatchProcessSchedule
-- Added CME and NISCCME file formats - Girija - 08/01/2014
-- =============================================
ALTER PROCEDURE [dbo].[sp_NH_Client_Manage_BatchProcessSchedule]
@ClientID VARCHAR(50) = NULL,
@BatchID INT = NULL,
@BatchMode VARCHAR(50) = NULL,
@BatchDevices VARCHAR(50) = NULL,
@ScheduleDateTime DATETIME = NULL,
@ProcessStatus VARCHAR(MAX) = NULL,      
@OriginalFileName VARCHAR(MAX) = NULL,
@FileName VARCHAR(255) = NULL OUTPUT,
@BatchReportFile VARCHAR(255) = NULL OUTPUT,
@NISCIVUEReportFile VARCHAR(255) = NULL OUTPUT,
@HHFReportFile VARCHAR(255) = NULL OUTPUT,
@CMEReportFile VARCHAR(255) = NULL OUTPUT,
@MVRSReportFile VARCHAR(255) = NULL OUTPUT,
@NISCCMEReportFile VARCHAR(255) = NULL OUTPUT,
@FCSDailyReadReportFile VARCHAR(255) = NULL OUTPUT,
@Active BIT = NULL,
@LastRunDateTime DATETIME = NULL,
@LastRunFinished DATETIME = NULL,
@NextRunDateTime DATETIME = NULL,
@BatchAlertEmail VARCHAR(256) = NULL,
@BatchReportSent DATETIME = NULL,        
@TzOffset FLOAT = NULL,
@Command VARCHAR(10),
@BatchScheduleID INT = 0 OUTPUT,
@BatchOpCodeSuffix VARCHAR(255) = NULL,
@CurrentVersions NCHAR(30) = NULL,
@UpdatedVersion NCHAR(30) = NULL,
@UpdatedDeviceType INT = NULL,
@BatchType VARCHAR(50) = NULL,
@ImportErrorCode INT = NULL,
@ImportErrorMessage VARCHAR(1000) = NULL,
@UserID VARCHAR(50) = NULL,
@NotificationOnly BIT = NULL,
@ConfiguredDeviceID VARCHAR(255) = NULL
AS   
BEGIN
      IF (@Command = 'SELECT')
      BEGIN
            SELECT tbl_NH_BatchProcessSchedule.BatchScheduleID, tbl_NH_BatchProcessSchedule.ClientID, tbl_NH_BatchProcessSchedule.BatchID,
        tbl_NH_BatchProcessSchedule.BatchMode, tbl_NH_BatchProcessSchedule.BatchDevices, tbl_NH_BatchProcessSchedule.ProcessStatus,
            tbl_NH_BatchProcessSchedule.OriginalFileName, tbl_NH_BatchProcessSchedule.FileName, tbl_NH_BatchProcessSchedule.Active,
            tbl_NH_BatchProcessSchedule.BatchReportFile, tbl_NH_BatchProcessSchedule.NISCIVUEReportFile, tbl_NH_BatchProcessSchedule.HHFReportFile, tbl_NH_BatchProcessSchedule.CMEReportFile,
            tbl_NH_BatchProcessSchedule.MVRSReportFile, tbl_NH_BatchProcessSchedule.NISCCMEReportFile,tbl_NH_BatchProcessSchedule.FCSDailyReadReportFile,
            DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.BatchReportSent) AS [BatchReportSent],
            DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.ScheduleDateTime) AS [ScheduleDateTime],
        DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.DateCreated) AS [DateCreated],
        DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.LastRunDateTime) AS [LastRunTime], tbl_NH_BatchProcessSchedule.LastRunDateTime, --- utc time for the service usage
        DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.LastRunFinished) AS [LastRunFinished], tbl_NH_BatchProcessSchedule.LastRunFinished, --- utc time for the service usage
            DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.NextRunDateTime) AS [NextRunTime], tbl_NH_BatchProcessSchedule.NextRunDateTime, --- utc time for the service usage
            tbl_NH_BatchProcessSchedule.BatchAlertEmail, tbl_NH_BatchProcessSchedule.BatchOPCodeSuffix, tbl_NH_BatchProcessSchedule.CurrentVersions,
            tbl_NH_BatchProcessSchedule.UpdatedVersion, tbl_NH_BatchProcessSchedule.UpdatedDeviceType, tbl_NH_BatchProcess.BatchName, ISNULL(tbl_NH_BatchProcess.BatchType, '') AS [BatchType],
            ISNULL(tbl_NH_BatchProcessSchedule.ImportErrorCode, 0) AS [ImportErrorCode], ISNULL(tbl_NH_BatchProcessSchedule.ImportErrorMessage, '') AS [ImportErrorMessage], tbl_NH_BatchProcessSchedule.UserID AS [UserID],
            ISNULL(tbl_NH_BatchProcessSchedule.NotificationOnly, 0) AS [NotificationOnly],
            tbl_NH_BatchProcessSchedule.ConfiguredDeviceID
            FROM tbl_NH_BatchProcessSchedule INNER JOIN
        tbl_NH_BatchProcess ON tbl_NH_BatchProcessSchedule.BatchID = tbl_NH_BatchProcess.BatchID
            WHERE (tbl_NH_BatchProcessSchedule.ClientID = @ClientID OR @ClientID IS NULL)
            AND (tbl_NH_BatchProcessSchedule.BatchScheduleID = @BatchScheduleID OR @BatchScheduleID = 0)
            AND (tbl_NH_BatchProcess.BatchType = @BatchType OR @BatchType IS NULL)
      END        
      ELSE IF (@Command = 'INSERT')
      BEGIN
            INSERT INTO tbl_NH_BatchProcessSchedule
            (ClientID, BatchID, BatchMode, BatchDevices, ScheduleDateTime, ProcessStatus, DateCreated, OriginalFileName, Active, BatchAlertEmail, BatchOpCodeSuffix, CurrentVersions, UpdatedVersion, UpdatedDeviceType, UserID, NotificationOnly, ConfiguredDeviceID)
            VALUES(@ClientID, @BatchID, @BatchMode, @BatchDevices, @ScheduleDateTime, @ProcessStatus, GETUTCDATE(), @OriginalFileName, 'True', @BatchAlertEmail, @BatchOpCodeSuffix, @CurrentVersions, @UpdatedVersion,
            @UpdatedDeviceType, @UserID, @NotificationOnly,@ConfiguredDeviceID)
 
            SET @BatchScheduleID = SCOPE_IDENTITY()  
                 
            IF (@BatchDevices = 'Upload' )
            BEGIN
                  SET  @FileName = DB_NAME() + '-' + @ClientID + '-' + CAST(@BatchScheduleID AS VARCHAR) + '.txt'
 
                  UPDATE tbl_NH_BatchProcessSchedule
                  SET FileName = @FileName
                  WHERE BatchScheduleID = @BatchScheduleID
 
            END  
      END
      ELSE IF (@Command = 'UPDATE')      
      BEGIN
            UPDATE tbl_NH_BatchProcessSchedule
            SET LastRunDateTime = CASE WHEN @LastRunDateTime IS NULL THEN LastRunDateTime ELSE @LastRunDateTime END,
            ScheduleDateTime = CASE WHEN @ScheduleDateTime IS NULL THEN ScheduleDateTime ELSE @ScheduleDateTime END,
            BatchMode = CASE WHEN @BatchMode IS NULL THEN BatchMode ELSE @BatchMode END,
            OriginalFileName = CASE WHEN @OriginalFileName IS NULL THEN OriginalFileName ELSE @OriginalFileName END,
            LastRunFinished = CASE WHEN @LastRunFinished IS NULL THEN LastRunFinished ELSE @LastRunFinished END,
            NextRunDateTime = CASE WHEN @NextRunDateTime IS NULL THEN NextRunDateTime ELSE @NextRunDateTime END,              
            ProcessStatus = CASE WHEN @ProcessStatus IS NULL THEN ProcessStatus ELSE @ProcessStatus END,
            Active = CASE WHEN @Active IS NULL THEN Active ELSE @Active END,
            BatchReportFile = CASE WHEN @BatchReportFile IS NULL THEN BatchReportFile ELSE @BatchReportFile END,
            NISCIVUEReportFile = CASE WHEN @NISCIVUEReportFile IS NULL THEN NISCIVUEReportFile ELSE @NISCIVUEReportFile END,
            HHFReportFile = CASE WHEN @HHFReportFile IS NULL THEN HHFReportFile ELSE @HHFReportFile END,
            CMEReportFile = CASE WHEN @CMEReportFile IS NULL THEN HHFReportFile ELSE @CMEReportFile END,
            MVRSReportFile = CASE WHEN @MVRSReportFile IS NULL THEN MVRSReportFile ELSE @MVRSReportFile END,
            NISCCMEReportFile = CASE WHEN @NISCCMEReportFile IS NULL THEN NISCCMEReportFile ELSE @NISCCMEReportFile END,
            FCSDailyReadReportFile = CASE WHEN @FCSDailyReadReportFile IS NULL THEN FCSDailyReadReportFile ELSE @FCSDailyReadReportFile END,
            BatchReportSent = CASE WHEN @BatchReportSent IS NULL THEN BatchReportSent ELSE @BatchReportSent END,
            ImportErrorCode = CASE WHEN @ImportErrorCode IS NULL THEN ImportErrorCode ELSE @ImportErrorCode END,
            ImportErrorMessage = CASE WHEN @ImportErrorMessage IS NULL THEN ImportErrorMessage ELSE @ImportErrorMessage END                              
            WHERE BatchScheduleID = @BatchScheduleID AND ClientID = @ClientID
      END
      ELSE IF (@Command = 'DELETE')
      BEGIN
            DELETE FROM tbl_NH_BatchProcessSchedule
            WHERE BatchScheduleID = @BatchScheduleID AND ClientID = @ClientID
      END  
      ELSE IF (@Command = 'ACTIVE')
      BEGIN
            SELECT tbl_NH_BatchProcessSchedule.BatchScheduleID, tbl_NH_BatchProcessSchedule.ClientID, tbl_NH_BatchProcessSchedule.BatchID,
        tbl_NH_BatchProcessSchedule.BatchMode, tbl_NH_BatchProcessSchedule.BatchDevices, tbl_NH_BatchProcessSchedule.ProcessStatus,
            tbl_NH_BatchProcessSchedule.OriginalFileName, tbl_NH_BatchProcessSchedule.FileName, tbl_NH_BatchProcessSchedule.Active,
            tbl_NH_BatchProcessSchedule.BatchReportFile, tbl_NH_BatchProcessSchedule.NISCIVUEReportFile, tbl_NH_BatchProcessSchedule.HHFReportFile, tbl_NH_BatchProcessSchedule.CMEReportFile,
            tbl_NH_BatchProcessSchedule.MVRSReportFile, tbl_NH_BatchProcessSchedule.NISCCMEReportFile,tbl_NH_BatchProcessSchedule.FCSDailyReadReportFile,
            DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.BatchReportSent) AS [BatchReportSent],
            DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.ScheduleDateTime) AS [ScheduleDateTime],
        DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.DateCreated) AS [DateCreated],
        DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.LastRunDateTime) AS [LastRunTime], tbl_NH_BatchProcessSchedule.LastRunDateTime, --- utc time for the service usage
        DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.LastRunFinished) AS [LastRunFinished], tbl_NH_BatchProcessSchedule.LastRunFinished, --- utc time for the service usage
            DATEADD(HOUR, @TzOffset, tbl_NH_BatchProcessSchedule.NextRunDateTime) AS [NextRunTime], tbl_NH_BatchProcessSchedule.NextRunDateTime, --- utc time for the service usage
            tbl_NH_BatchProcessSchedule.BatchAlertEmail, tbl_NH_BatchProcessSchedule.BatchOPCodeSuffix, tbl_NH_BatchProcessSchedule.CurrentVersions,
            tbl_NH_BatchProcessSchedule.UpdatedVersion, tbl_NH_BatchProcessSchedule.UpdatedDeviceType, tbl_NH_BatchProcess.BatchName, ISNULL(tbl_NH_BatchProcess.BatchType, '') AS [BatchType],
            ISNULL(tbl_NH_BatchProcessSchedule.ImportErrorCode, 0) AS [ImportErrorCode], ISNULL(tbl_NH_BatchProcessSchedule.ImportErrorMessage, '') AS [ImportErrorMessage], tbl_NH_BatchProcessSchedule.UserID AS [UserID],
            ISNULL(tbl_NH_BatchProcessSchedule.NotificationOnly, 0) AS [NotificationOnly],tbl_NH_BatchProcessSchedule.ConfiguredDeviceID
            FROM tbl_NH_BatchProcessSchedule INNER JOIN
        tbl_NH_BatchProcess ON tbl_NH_BatchProcessSchedule.BatchID = tbl_NH_BatchProcess.BatchID
            WHERE (tbl_NH_BatchProcessSchedule.ClientID = @ClientID OR @ClientID IS NULL)
            AND (tbl_NH_BatchProcessSchedule.BatchScheduleID = @BatchScheduleID OR @BatchScheduleID = 0)
            AND (tbl_NH_BatchProcess.BatchType = @BatchType OR @BatchType IS NULL)
            AND (tbl_NH_BatchProcessSchedule.Active = 1)
      END  
END
 






