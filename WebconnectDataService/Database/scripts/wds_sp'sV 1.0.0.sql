Use[WebSense]
GO
Create PROCEDURE [dbo].[sp_MIS_DeleteAlertConfiguration] 

      @AlertID int,	  

	  @ClientID nvarchar(50)	  



AS     

	  Delete from tbl_NH_Alert_Configuration where AlertID = @AlertID and ClientID = @ClientID

RETURN

GO

Use[WebSense]
GO
Create PROCEDURE [dbo].[sp_MIS_DeleteFormulaConfiguration] 
      @FormulaID int,	  
	  @ClientID nvarchar(50)	  

AS     
	  Delete from tbl_NH_SensorClientFormula where FormulaID = @FormulaID and ClientID = @ClientID

RETURN

GO

CREATE PROCEDURE [dbo].[sp_MIS_DeletePortConfiguration]
      @DeviceID int
AS     
	  Delete from tbl_NH_Port_Configuration where DeviceID = @DeviceID
RETURN

GO

Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_Devices_Sensor_Readings_GetBetWeenDateTime] 
      @DeviceID int,
	  @SensorColumn nvarchar(50),
	  @FromDate nvarchar(40),
	  @ToDate nvarchar(40)
AS
BEGIN
      SET NOCOUNT ON;
	  DECLARE @columnName nvarchar(50)
	  DECLARE @sql nvarchar(4000)
	  DECLARE @ID nvarchar(10)
	  DECLARE @fDate nvarchar(40)
	  DECLARE @tDate nvarchar(40)	 
	  set @columnName = @SensorColumn
	  set @ID = @DeviceID
	  set @fDate = @FromDate
	  set @tDate = @ToDate	  
	  	  	 
      SET @sql = 'SELECT DSR.DeviceID, D.DeviceType, DSR.SensorReportTime, ' + @columnName + ' as Sensor from tbl_NH_Devices_Sensor_Readings DSR inner join tbl_NH_Devices D on D.DeviceID = DSR.DeviceID where DSR.DeviceID =' + @ID + ' AND ' + @columnName +

	  ' IS NOT NULL AND DSR.SensorReportTime between ''' + convert(varchar(24), @fDate, 121) + ''' and ''' + convert(varchar(24), @tDate, 121) + ''' ORDER BY DSR.SensorReportTime ASC'
      EXEC sp_executesql @sql, N'@columnName varchar(600)', @columnName = @columnName
	  print @sql
END
GO


Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_Devices_Sensor_Readings_GetNoOfReadings] 
      @DeviceID int,
	  @Count int
AS
BEGIN
      SET NOCOUNT ON;

	  DECLARE @items int = @Count

	  select top (@items) DSR.DeviceID, d.DeviceType, DSR.SensorReportTime, DSR.Sensor1ProcessedValue, DSR.Sensor2ProcessedValue, DSR.Sensor3ProcessedValue, DSR.Sensor4ProcessedValue, DSR.Sensor5ProcessedValue, DSR.Sensor6ProcessedValue from  tbl_NH_Devices_
Sensor_Readings DSR 

	  inner join tbl_NH_Devices d on d.DeviceID = dsr.DeviceID where dsr.DeviceID = @DeviceID order by DSR.SensorReportTime desc

END

GO

Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_Devices_Sensor_Readings_GetRecentValue] 
      @DeviceID int

AS
BEGIN
      SET NOCOUNT ON;

	  select top 1 DSR.DeviceID, DeviceType, DSR.SensorReportTime, DSR.Sensor1ProcessedValue, DSR.Sensor2ProcessedValue, DSR.Sensor3ProcessedValue, DSR.Sensor4ProcessedValue, DSR.Sensor5ProcessedValue, DSR.Sensor6ProcessedValue from  tbl_NH_Devices_Sensor_Re
adings DSR 

	  inner join tbl_NH_Devices d on d.DeviceID = dsr.DeviceID where dsr.DeviceID = @DeviceID order by DSR.SensorReportTime desc

END

GO





Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_GetAlertConfiguration] 
      @ClientID nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
	  select AlertID, CreatedDate, AlertName, AlertMessage, DeviceID, ESN, SensorLabel, PortNumber, LowThreshold, HighThreshold, 

EmailIDsForLowThreshold, PhoneNumbersForLowThreshold,
	  EmailIDsForHighThreshold, PhoneNumbersForHighThreshold, SensorType, CarrierIDsForLowThreshold, CarrierIDsForHighThreshold,Custom from 

tbl_NH_Alert_Configuration
		where ClientID = @ClientID order by CreatedDate desc
END

GO




Use[WebSense]
GO

CREATE PROCEDURE [dbo].[sp_MIS_GetAlertConfigurationByPortNumber] 

      @DeviceID int,	  

	  @PortNumber int	  

AS     

	  select DeviceID, ESN, PortNumber, SensorType, ClientID, AlertName, AlertMessage, EmailIDsForLowThreshold, EmailIDsForHighThreshold, 

	  PhoneNumbersForLowThreshold, PhoneNumbersForHighThreshold, CarrierIDsForLowThreshold, CarrierIDsForHighThreshold from tbl_NH_Alert_Configuration

      where DeviceID = @DeviceID and PortNumber = @PortNumber

RETURN
GO



Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_GetAlertLogs] 
      @ClientID nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
	  select ReportDate, ESN, SensorType, AlertMessage, EmailIDs, PhoneNumbers from tbl_NH_Alert_Logs
		where ClientID = @ClientID order by ReportDate desc
END

GO


Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_GetAlertLogsByDeviceID] 
      @ClientID nvarchar(50),
	   @DeviceID int
AS
BEGIN
      SET NOCOUNT ON;
	  select ReportDate, ESN, DeviceID, SensorType, AlertMessage, EmailIDs, PhoneNumbers from tbl_NH_Alert_Logs
		where ClientID= @ClientID AND DeviceID = @DeviceID order by ReportDate desc
END
GO


Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_GetAllDevices] 

      @ClientID nvarchar(50)

AS

BEGIN

      SET NOCOUNT ON;
Select DeviceID, DeviceType, Esn from tbl_NH_Devices where ClientID=@ClientID	  

END

GO


Use[WebSense]
GO

CREATE PROCEDURE [dbo].[sp_MIS_GetAllWirelessProvider] 
AS
BEGIN
      SET NOCOUNT ON;
	  select CarrierID, Carrier from tbl_NH_WirelessProvider		
END

GO


Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_GetDeviceIdByAlertID]  	
	  @AlertID int	
AS     
	  BEGIN
	 DECLARE @DeviceID INT
	 DECLARE @PortNo INT

	 SELECT @DeviceID = DeviceID  FROM tbl_NH_Alert_Configuration WHERE AlertID = @AlertID
	 SELECT @PortNo = PortNumber   FROM tbl_NH_Alert_Configuration WHERE AlertID = @DeviceID

SELECT DeviceID, PortNumber, LowThreshold, HighThreshold 
FROM 
tbl_NH_Alert_Configuration
WHERE 
DeviceID = @DeviceID AND
PortNumber <> @PortNo	  
	  END

GO


Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_GetEventTriggerStatusByDeviceID]       
	  @DeviceID int
AS
BEGIN
      SET NOCOUNT ON;

SELECT CommandStatus
FROM  tbl_NH_Devicestatus 
WHERE		
			 DeviceID = @DeviceID 
END

GO


Use[WebSense]
GO

Create PROCEDURE [dbo].[sp_MIS_GetFormula] 
      @DeviceID int,	  
	  @PortNumber int	  
AS     
	  select SCF.Formula from tbl_NH_SensorClientFormula SCF where 
	  SCF.FormulaID = (select PC.FormulaID from tbl_NH_Port_Configuration PC where PC.DeviceID = @DeviceID and PC.PortNumber = @PortNumber)
RETURN


CREATE PROCEDURE [dbo].[sp_MIS_GetFormulaConfiguration] 
      @ClientID nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
	  select FormulaID, SensorType, Formula, WCDefaultFormula, Unit, ReverseFormula, Name from tbl_NH_SensorClientFormula
		where ClientID = @ClientID
END
GO



Use[WebSense]
GO

CREATE PROCEDURE [dbo].[sp_MIS_GetLatestAlertLogsByDeviceID] 
      @ClientID nvarchar(50),
	   @DeviceID int,
	   @Count int
AS
BEGIN
      SET NOCOUNT ON;
	  	  exec('select Top '+@Count+' ReportDate, ESN, DeviceID, SensorType, AlertMessage, EmailIDs, PhoneNumbers from tbl_NH_Alert_Logs
		where ClientID= '''+@ClientID+''' AND DeviceID = '+@DeviceID+' order by ReportDate desc')
END
GO

Use[WebSense]
GO

CREATE PROCEDURE [dbo].[sp_MIS_GetPortConfiguration] 
      @DeviceID int
AS
BEGIN
      SET NOCOUNT ON;
	  select PC.PortNumber, PC.FormulaID, PC.SensorLabel, PC.LowRange, PC.HighRange, SCF.SensorType, SCF.Formula, SCF.WCDefaultFormula, SCF.Unit, SCF.Name from tbl_NH_Port_Configuration PC
		inner join tbl_NH_SensorClientFormula SCF on PC.FormulaID = SCF.FormulaID where PC.DeviceID = @DeviceID
END
GO

Use[WebSense]
GO
Create PROCEDURE [dbo].[sp_MIS_GetReverseFormula] 
      @DeviceID int,	  
	  @PortNumber int	  
AS     
	  select SCF.ReverseFormula from tbl_NH_SensorClientFormula SCF where 
	  SCF.FormulaID = (select PC.FormulaID from tbl_NH_Port_Configuration PC where PC.DeviceID = @DeviceID and PC.PortNumber = @PortNumber)
RETURN
GO


Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_InsertAlertConfiguration]  
	  @CreatedDate datetime,
	  @AlertName nvarchar(200),
	  @AlertMessage nvarchar(max),
	  @DeviceID int,
	  @ESN nvarchar(50),
	  @SensorLabel nvarchar(50),
	  @PortNumber nvarchar(50),
	  @LowThreshold float,
	  @HighThreshold float,
	  @EmailIDsForLowThreshold nvarchar(max),
	  @PhoneNumbersForLowThreshold nvarchar(max),
	  @EmailIDsForHighThreshold nvarchar(max),
	  @PhoneNumbersForHighThreshold nvarchar(max),
	  @SensorType nvarchar(50),
	  @ClientID nvarchar(50),
	  @CarrierIDsForLowThreshold nvarchar(50),
	  @CarrierIDsForHighThreshold nvarchar(50),
	  @Custom int
	  
AS     
	  BEGIN
	  Insert into tbl_NH_Alert_Configuration (CreatedDate, AlertName, AlertMessage, DeviceID, ESN, SensorLabel, PortNumber, 

LowThreshold, HighThreshold, EmailIDsForLowThreshold, PhoneNumbersForLowThreshold, 
	  EmailIDsForHighThreshold, PhoneNumbersForHighThreshold, SensorType, ClientID, CarrierIDsForLowThreshold, CarrierIDsForHighThreshold, Custom) values 

	  (@CreatedDate, @AlertName, @AlertMessage, @DeviceID, @ESN, @SensorLabel, @PortNumber, @LowThreshold, @HighThreshold, 
@EmailIDsForLowThreshold, @PhoneNumbersForLowThreshold, @EmailIDsForHighThreshold, @PhoneNumbersForHighThreshold, 
	  @SensorType, @ClientID, @CarrierIDsForLowThreshold, @CarrierIDsForHighThreshold,@Custom)
	  select SCOPE_IDENTITY()
	  END
RETURN
GO





Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_InsertAlertLogs]  
	  @ReportDate datetime,
	  @AlertName nvarchar(200),
	  @AlertMessage nvarchar(max),
	  @DeviceID int,
	  @ESN nvarchar(50),	  
	  @PortNumber nvarchar(50),	  
	  @SensorType nvarchar(50),
	  @ClientID nvarchar(50),
	  @EmailIDs nvarchar(max),
	  @PhoneNumbers nvarchar(max),
	  @UserID nvarchar(50)
AS     
	  BEGIN
	  Insert into tbl_NH_Alert_Logs (ReportDate, AlertName, AlertMessage, DeviceID, ESN, PortNumber, SensorType, ClientID, EmailIDs, PhoneNumbers, UserID) values 
	  (@ReportDate, @AlertName, @AlertMessage, @DeviceID, @ESN, @PortNumber, @SensorType, @ClientID, @EmailIDs, @PhoneNumbers, @UserID)		  
	  END
RETURN

GO



Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_InsertOrUpdateFormulaConfiguration]  
      @FormulaID int,
	  @Formula nvarchar(max),
	  @WCDefaultFormula nvarchar(max),
	  @SensorType nvarchar(50),
	  @ClientID nvarchar(50),
	  @Unit nvarchar(50),
	  @ReverseFormula nvarchar(max),
	  @Name nvarchar(max)
AS     
	  IF NULLIF(@FormulaID, '') IS NULL
	  BEGIN
	  Insert into tbl_NH_SensorClientFormula (Formula, WCDefaultFormula, SensorType, ClientID, Unit, ReverseFormula, Name) values (@Formula, @WCDefaultFormula, @SensorType, @ClientID, @Unit, @ReverseFormula, @Name)
	  select SCOPE_IDENTITY()
	  END
	  Else
	  BEGIN
	  Update tbl_NH_SensorClientFormula set Formula = @Formula, WCDefaultFormula = @WCDefaultFormula, SensorType = @SensorType, ClientID= @ClientID, Unit = @Unit, ReverseFormula = @ReverseFormula, Name=@Name where FormulaID = @FormulaID
	  END
RETURN
GO


Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_Session_CheckSessionIDExist] 
      @SessionID nvarchar(max)
AS
BEGIN

      SET NOCOUNT ON;

	  select count(*) from tbl_NH_Session where SessionID = @SessionID
END

GO

Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_Session_InsertSessionReturnSessionID] 

      @ClientID nvarchar(50),
      @RequestIP nvarchar(50),
      @WhenCreated datetime,
      @SessionTimeOut datetime
AS
BEGIN
      SET NOCOUNT ON;

	  DECLARE @SecretKey uniqueidentifier;
	  SET @SecretKey = NewID();
	  SET @SessionTimeOut = DATEADD(MINUTE, 40, @SessionTimeOut)

      INSERT INTO  tbl_NH_Session (SessionID,ClientID, RequestIP, WhenCreated, SessionTimeOut)
      VALUES (@SecretKey, @ClientID, @RequestIP, @WhenCreated, @SessionTimeOut)

      SELECT @SecretKey
END

GO

Use[WebSense]
GO

CREATE PROCEDURE [dbo].[sp_MIS_Session_SelectSessionID] 
      @SessionID nvarchar(max)
AS
BEGIN

      SET NOCOUNT ON;

	  select * from tbl_NH_Session where SessionID = @SessionID

END

GO



CREATE PROCEDURE [dbo].[sp_MIS_Session_UpdateSessionTimeOut] 
      @SessionID nvarchar(max)
AS
BEGIN
      SET NOCOUNT ON;

	  update tbl_NH_Session set SessionTimeOut = DATEADD(MINUTE, 40, GETDATE()) where SessionID = @SessionID

END

GO



Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_MIS_UpdateAlertConfiguration]  
      @AlertID int,
	  @CreatedDate datetime,
	  @AlertName nvarchar(200),
	  @AlertMessage nvarchar(max),
	  @DeviceID int,
	  @ESN nvarchar(50),
	  @SensorLabel nvarchar(50),
	  @PortNumber nvarchar(50),
	  @LowThreshold float,
	  @HighThreshold float,
	  @EmailIDsForLowThreshold nvarchar(max),
	  @PhoneNumbersForLowThreshold nvarchar(max),
	  @EmailIDsForHighThreshold nvarchar(max),
	  @PhoneNumbersForHighThreshold nvarchar(max),
	  @SensorType nvarchar(50),
	  @ClientID nvarchar(50),
	  @CarrierIDsForLowThreshold nvarchar(50),
	  @CarrierIDsForHighThreshold nvarchar(50),
	  @Custom int
AS     	  
	  BEGIN	
	  Update tbl_NH_Alert_Configuration set CreatedDate = @CreatedDate, AlertName = @AlertName, AlertMessage = @AlertMessage, 
	  DeviceID= @DeviceID, ESN = @ESN , SensorLabel = @SensorLabel, PortNumber = @PortNumber, LowThreshold = @LowThreshold, 

HighThreshold = @HighThreshold, 
	  EmailIDsForLowThreshold = @EmailIDsForLowThreshold, PhoneNumbersForLowThreshold = @PhoneNumbersForLowThreshold, 
	  EmailIDsForHighThreshold = @EmailIDsForHighThreshold, PhoneNumbersForHighThreshold = @PhoneNumbersForHighThreshold, SensorType 

= @SensorType,
	  ClientID = @ClientID, CarrierIDsForLowThreshold = @CarrierIDsForLowThreshold, CarrierIDsForHighThreshold = 

@CarrierIDsForHighThreshold, Custom = @Custom where AlertID = @AlertID

--SET NOCOUNT ON
--go
	  END
RETURN

GO




Use[WebSense]
GO
-- =============================================
-- Author:		Sivakumar
-- Create date: 10/23/2015
-- Description:	Getting Device ID's and Formula ID's
-- =============================================

CREATE PROCEDURE [dbo].[sp_NH_CalcService_GetDeviceAndFormula]
AS	

	SELECT tbl_NH_Port_Configuration.DeviceID AS [DeviceID], FormulaID AS [FormulaID], DeviceType AS [DeviceType], PortNumber AS [PortNumber] 
	FROM tbl_NH_Port_Configuration INNER JOIN tbl_NH_Devices ON tbl_NH_Port_Configuration.DeviceID = tbl_NH_Devices.DeviceID AND 
	Active = 1 ORDER BY tbl_NH_Port_Configuration.DeviceID

GO



Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_NH_CalcService_GetFormulaByFormulaID]

@FormulaID varchar(max)

AS	



 declare @var1 varchar(max)

 declare @var2 varchar(max)

 Set @var1 = @FormulaID

Set @var2 = ',' + @var1 + ','

--select * from tbl_NH_SensorClientFormula Where Charindex(','+cast(FormulaID as varchar)+',', @var2) > 0



	SELECT ISNULL(Formula, '') AS [Formula], ISNULL(WCDefaultFormula, '') AS [WCDefaultFormula], ISNULL(SensorType, '') AS [SensorType] 

	FROM tbl_NH_SensorClientFormula Where Charindex(','+cast(FormulaID as varchar)+',', @var2) > 0

GO




Use[WebSense]
GO
CREATE PROCEDURE [dbo].[sp_NH_CalcService_GetReadingByDeviceID]



@DeviceID int



AS	




--Declare @DeviceID int
--set @DeviceID = 17


	SELECT Guid AS [Guid], ISNULL(Sensor1, 0) AS [Sensor1], ISNULL(Sensor2, 0) AS [Sensor2], ISNULL(Sensor3, 0) AS [Sensor3], ISNULL(Sensor4, 0) AS [Sensor4], ISNULL(Sensor5, 0) AS [Sensor5], ISNULL(Sensor6, 0) AS [Sensor6] FROM tbl_NH_Devices_Sensor_Reading
s 



	WHERE DeviceID =  @DeviceID AND (Sensor1ProcessedValue IS NULL or Sensor4ProcessedValue IS NULL) 
	--AND Sensor3ProcessedValue IS NULL AND Sensor4ProcessedValue IS NULL AND Sensor5ProcessedValue IS NULL AND Sensor6ProcessedValue IS NULL ORDER BY SensorReportTime DESC

GO







Use[WebSense]
GO
-- =============================================
-- Author:		Sivakumar
-- Create date: 10/23/2015
-- Description:	Updating Calculated Sensor Values
-- =============================================

CREATE PROCEDURE [dbo].[sp_NH_CalcService_UpdatedCalculatedSensorValue]
@Sensor varchar(500),
@SensorValue varchar(500),
@Guid varchar(max)
AS	
	DECLARE    @SQLquery  AS NVARCHAR(MAX)
--set @SQLquery = N'update tbl_NH_Devices_Sensor_Readings SET' + @Sensor +N'='+ @SensorValue +N'where Guid = '+ @Guid +''

--N'update tbl_NH_Devices_Sensor_Readings SET ' + @MyColumns + N' FROM GdRateFixedPosts 
--            where MonitorDateTime  BETWEEN ' + @fromDateTime + ' and  ' + @ToDateTime + ''

SELECT @SQLquery = 'UPDATE [tbl_NH_Devices_Sensor_Readings] SET [' + @Sensor + '] =  ' + @SensorValue + ' WHERE Guid = '''+ @Guid + ''''
--print @SQLquery


exec sp_executesql @SQLquery;

GO




Use[WebSense]
GO

 CREATE PROCEDURE [dbo].[sp_NH_CalcService_GetReadingByDeviceID_Test]
@DeviceID int
AS	
	SELECT distinct Guid AS [Guid], ISNULL(Sensor1, 0) AS [Sensor1],ISNULL(Sensor2, 0) AS [Sensor2], ISNULL(Sensor3, 0) AS [Sensor3], ISNULL(Sensor4, 0) AS [Sensor4], ISNULL(Sensor5, 0) AS [Sensor5], ISNULL(Sensor6, 0) AS [Sensor6]
	 FROM tbl_NH_Devices_Sensor_Readings
	 WHERE ((Sensor1ProcessedValue IS NULL and sensor1 > 0) or (Sensor4ProcessedValue IS NULL and sensor4 > 0) or (Sensor2ProcessedValue IS NULL  and sensor2 > 0) or (Sensor3ProcessedValue IS NULL and sensor3 > 0)  or (Sensor5ProcessedValue IS NULL and senso
r5 > 0)  or (Sensor6ProcessedValue IS NULL and sensor6 > 0)) 
	--WHERE ((Sensor1ProcessedValue IS NULL and sensor1 > 0) or (Sensor4ProcessedValue IS NULL and sensor4 > 0) or (Sensor2ProcessedValue IS NULL  and sensor2 > 0) or (Sensor3ProcessedValue IS NULL and sensor3 > 0)) 
	and tbl_NH_Devices_Sensor_Readings.DeviceID  = @DeviceID

GO


















