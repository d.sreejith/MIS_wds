﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
//using System.Web.UI.WebControls;
using System.Web.UI;
using System.IO;
using System.Net.Mail;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO.Compression;

namespace NHUtilsClass
{
    public class NHWCNavigation
    {
        public NHWCNavigation()
        {

        }

        public NHWCNavigation(Menu NavigationMenu, NHLoginSession CurrentSession)
        {

            //Create Client Menu
            MenuItem Client;
            Client = CreateMenuItem("CLIENTS", "/Admin/Clients.aspx", "Clients");

            // Create the Client submenu.
            MenuItem addClient;
            addClient = CreateMenuItem("Add Client", "/Admin/AddClient.aspx", "Add Client");

            MenuItem searchClient;
            searchClient = CreateMenuItem("Search Client", "/Admin/SearchClient.aspx", "Search Client");

            MenuItem importDevices;
            importDevices = CreateMenuItem("Import Devices", "/Admin/ImportDevices.aspx", "Import Devices");

            MenuItem searchDevices;
            searchDevices = CreateMenuItem("Search Devices", "/Admin/SearchDevices.aspx", "Search Devices");

            MenuItem moveUnits;  //will be used by the nighthawk admin to move units from one client to the other in the same db
            moveUnits = CreateMenuItem("Move Devices", "/Admin/MoveResellerInventory.aspx", "Move units to a different client");

            MenuItem batchProcessClient;
            batchProcessClient = CreateMenuItem("Batch Process", "/Admin/BatchProcess.aspx", "Batch Process");

            //Create nighthawk Report Menu
            MenuItem AdminReports;
            string adminReportText = "REPORTS";
            //string mapDevicesText = "GRID POWER FAILURE MAP";
            string reportText = "REPORTS";
            string systemSetupText = "SYSTEM SETUP";
            string meterShopText = "METER SHOP";
            string addDeviceText = "ADD DEVICE"; //FOR METERHSOP
            if (CurrentSession.UserRoleID == 1)
            {
                adminReportText = "NIGHTHAWK REPORTS";
                //mapDevicesText = "Grid Power Failure Map";
                reportText = "Customer Reports";
                systemSetupText = "System Setup";
                meterShopText = "Meter Shop";
                addDeviceText = "Add Device"; //FOR METERSHOP
            }
            AdminReports = CreateMenuItem(adminReportText, "/Admin/AdminReports.aspx", "Admin Reports");


            string customText = "Customer";

            if (CurrentSession.ClientID == "40497")
            {
                customText = "Client";

            }



            //Create Customer Menu
            MenuItem Customer;
            Customer = CreateMenuItem(customText.ToUpper() + "S", "/Client/Customers.aspx", customText + "s");

            // Create the Customer submenu.
            MenuItem addCustomer;
            addCustomer = CreateMenuItem("Add " + customText, "/Client/AddCustomer.aspx", "Add " + customText);

            //for metershop
            MenuItem addDevice;
            addDevice = CreateMenuItem(addDeviceText, "/Client/AddCustomer.aspx", "Add Device");
            //////

            // Create ImportERTDevice Submenu
            MenuItem importERTDevices;
            importERTDevices = CreateMenuItem("Import ERT Devices", "/Client/ImportERTDevices.aspx", "Import ERTDevices");


            MenuItem searchCustomer;
            searchCustomer = CreateMenuItem("Search " + customText, "/Client/SearchCustomer.aspx", "Search " + customText);

            MenuItem inventory;
            inventory = CreateMenuItem("Device Inventory", "/Client/DeviceInventory.aspx", "Device Inventory");

            MenuItem ertInventory;
            ertInventory = CreateMenuItem("ERT Inventory", "/Client/ERTInventory.aspx", "ERT Inventory");

            MenuItem searchInventory;
            searchInventory = CreateMenuItem("Search Device Inventory", "/Client/SearchInventory.aspx", "Search Device Inventory");

            MenuItem searchERTInventory;
            searchERTInventory = CreateMenuItem("Search ERT Inventory", "/Client/SearchERTInventory.aspx", "Search ERT Inventory");

            MenuItem recentDevices;
            recentDevices = CreateMenuItem("Recent Devices", "/Client/RecentDevices.aspx", "Recently Activated Devices");

            MenuItem batchProcess;

            batchProcess = CreateMenuItem("Batch Process", "/Client/BatchProcess.aspx", "Batch Process");

            //Adding this restriction so that the ERT devices doesn't show up in the production

            //Create ErtDevices Menu
            MenuItem ErtDevices;
            ErtDevices = CreateMenuItem("ErtDevices", "/Client/ErtDevices.aspx", "ErtDevices");

            // Create the ErtDevices submenu.
            MenuItem addErtDevice;
            addErtDevice = CreateMenuItem("Add ErtDevice", "/Client/AddErtDevice.aspx", "Add ErtDevice");

            //Create Synapse Network Menu
            MenuItem MapErtNetwork;
            MapErtNetwork = CreateMenuItem("Ert Network Map", "/Client/MapErtNetwork.aspx", "Ert Network Map");

            ////ErtDevices
            //ErtDevices.ChildItems.Add(MapErtNetwork);
            //ErtDevices.ChildItems.Add(addErtDevice);
            //NavigationMenu.Items.Add(ErtDevices);




            MenuItem MapDevices;
            MapDevices = CreateMenuItem("MAP DEVICES", null, "MAP DEVICES");

            MenuItem MapGridFailed;
            MapGridFailed = CreateMenuItem("Grid Power Failure Map", "/Client/MapGridFailed.aspx", "Grid Power Failure Map");

            MenuItem MapConnected;
            MapConnected = CreateMenuItem("Connected Devices Map", "/Client/MapConnected.aspx", "Connected Devices Map");

            //Create Synapse Network Menu
            MenuItem MapNetwork;
            MapNetwork = CreateMenuItem("Node/Network Map", "/Client/MapNetwork.aspx", "Network Map");

            //Create Report Menu
            MenuItem Reports;
            Reports = CreateMenuItem(reportText, "/Client/Reports.aspx", "Reports");

            //Create System Setup Menu
            MenuItem SystemSetup;
            SystemSetup = CreateMenuItem(systemSetupText, "/Client/GlobalOptions.aspx", "System Setup");

            //Create UserManagement Menu
            MenuItem Users;
            Users = CreateMenuItem("USERS", "/Account/Users.aspx", "Users");

            MenuItem addUser;
            addUser = CreateMenuItem("Add User", "/Account/AddUser.aspx", "Add User");

            MenuItem editProfile;

            editProfile = CreateMenuItem("Edit Profile", "/Account/EditProfile.aspx", "Edit Profile");

            MenuItem ChangePassword;
            ChangePassword = CreateMenuItem("Change Password", "/Account/ChangePassword.aspx", "Change Password");

            MenuItem MeterShop;
            MeterShop = CreateMenuItem(meterShopText, "/Client/CustomerDetail.aspx", "Meter Shop");

            //MenuItem AddDevice;
            // AddDevice = CreateMenuItem(addDeviceText, "/Client/AddCustomer.aspx", "Add Device");

            /* Added by Venkatesh 09/03/2015 */
            MenuItem MVRS;
            MVRS = CreateMenuItem("NH-RS", "/Client/ImportHDLFile.aspx", "NH-RS");

            MenuItem ImportHDLFile;
            ImportHDLFile = CreateMenuItem("Import Host Download file", "/Client/ImportHDLFile.aspx", "Import Host Download file");

            MenuItem ImportHULFile;
            ImportHULFile = CreateMenuItem("Import Host Upload file", "/Client/ImportHULFile.aspx", "Import Host Upload file");

            MVRS.ChildItems.Add(ImportHDLFile);

            MVRS.ChildItems.Add(ImportHULFile);
            /* Added by Venkatesh 09/03/2015 */

            /* Added by Venkatesh 11/09/2015 */
            /*MenuItem FCSMVRS;
            FCSMVRS = CreateMenuItem("FCS-MVRS", "/Client/ImportDailyReads.aspx", "FCS-MVRS");

            MenuItem ImportDailyReads;
            ImportDailyReads = CreateMenuItem("Import Daily Reads file", "/Client/ImportDailyReads.aspx", "Import Daily Reads file");

            FCSMVRS.ChildItems.Add(ImportDailyReads);*/
            /* Added by Venkatesh 11/09/2015 */

            //check permission and populate navigation menu   

            //navigation for all nighthawk users
            if (CurrentSession.UserRoleID <= 50)
            {
                if (CurrentSession.AddAccount)
                {
                    Client.ChildItems.Add(addClient);
                }


                Client.ChildItems.Add(searchClient);
                Client.ChildItems.Add(importDevices);

                //Client.ChildItems.Add(importERTDevices);
                Client.ChildItems.Add(searchDevices);

                if (ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") == "Demo" || CurrentSession.UserRoleID == 2)
                {
                    Client.ChildItems.Add(moveUnits);
                }

                if (CurrentSession.UserRoleID == 1 || CurrentSession.UserRoleID == 2)
                {
                    Client.ChildItems.Add(batchProcessClient);

                }

                NavigationMenu.Items.Add(Client);

                NavigationMenu.Items.Add(AdminReports);

            }

            //navigation for nighthawk developers
            if (CurrentSession.UserRoleID == 1)
            {

                Customer.ChildItems.Add(addCustomer);

                /*if (ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString() != "NHKWCConnectionStringProduction")
                {
                    Customer.ChildItems.Add(addErtDevice);
                }*/
                Customer.ChildItems.Add(addErtDevice);
                Customer.ChildItems.Add(searchCustomer);
                Customer.ChildItems.Add(inventory);
                Customer.ChildItems.Add(ertInventory);
                Customer.ChildItems.Add(searchInventory);
                Customer.ChildItems.Add(searchERTInventory);
                Customer.ChildItems.Add(importERTDevices);
                Customer.ChildItems.Add(recentDevices);
                Customer.ChildItems.Add(batchProcess);
                Customer.ChildItems.Add(MapGridFailed);
                Customer.ChildItems.Add(MapConnected);
                if (ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString() != "NHKWCConnectionStringProduction")
                {
                    //Synapse Network
                    Customer.ChildItems.Add(MapNetwork);
                }
                Customer.ChildItems.Add(Reports);
                Customer.ChildItems.Add(SystemSetup);
                Customer.ChildItems.Add(MeterShop);
                Customer.ChildItems.Add(addDevice);
                NavigationMenu.Items.Add(Customer);
                NavigationMenu.Items.Add(MVRS);
                //NavigationMenu.Items.Add(FCSMVRS);
            }

            //navigation for nighthawk client
            // also make sure it's not a metershop user
            if (CurrentSession.UserRoleID >= 51 && CurrentSession.UserRoleID != 53)
            {
                if (CurrentSession.AddAccount)
                {
                    Customer.ChildItems.Add(addCustomer);
                }

                /*if (ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString() != "NHKWCConnectionStringProduction")
                {
                    Customer.ChildItems.Add(addErtDevice);
                }*/
                Customer.ChildItems.Add(addErtDevice);
                Customer.ChildItems.Add(searchCustomer);
                Customer.ChildItems.Add(inventory);
                Customer.ChildItems.Add(ertInventory);
                Customer.ChildItems.Add(searchInventory);
                Customer.ChildItems.Add(searchERTInventory);
                Customer.ChildItems.Add(importERTDevices);
                Customer.ChildItems.Add(recentDevices);
                if (CurrentSession.BatchProcess)
                {
                    Customer.ChildItems.Add(batchProcess);
                }
                NavigationMenu.Items.Add(Customer);
                NavigationMenu.Items.Add(MVRS);
                //NavigationMenu.Items.Add(FCSMVRS);

                MapDevices.ChildItems.Add(MapGridFailed);
                MapDevices.ChildItems.Add(MapConnected);

                //if (ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString() != "NHKWCConnectionStringProduction")
                //{
                MapDevices.ChildItems.Add(MapNetwork);
                //}

                //if (ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString() != "NHKWCConnectionStringProduction")
                //{
                MapDevices.ChildItems.Add(MapErtNetwork);
                //}

                NavigationMenu.Items.Add(MapDevices);

                NavigationMenu.Items.Add(Reports);

                if (CurrentSession.UserRoleID == 51)
                {
                    NavigationMenu.Items.Add(SystemSetup);
                }
            }



            //metershop user
            if (CurrentSession.UserRoleID == 53)
            {
                NavigationMenu.Items.Add(MeterShop);
                NavigationMenu.Items.Add(addDevice);
            }


            if (ConfigurationManager.AppSettings["NHKWCConnectionString"].ToString() != "NHKWCConnectionStringProduction")
            {

                /*Commented it as part of ERB hub #6 requirement*/
                ////ErtDevices
                //ErtDevices.ChildItems.Add(MapErtNetwork);
                //ErtDevices.ChildItems.Add(addErtDevice);
                //NavigationMenu.Items.Add(ErtDevices);


                //Customer.ChildItems.Add(MapErtNetwork);
                //Customer.ChildItems.Add(addErtDevice);
            }

            // navigation for client and nighthawk user management
            //nighthawk super admin/tech support or client admin can add user
            if (CurrentSession.UserRoleID == 1 || CurrentSession.UserRoleID == 2 || CurrentSession.UserRoleID == 51)
            {
                Users.ChildItems.Add(addUser);
            }


            //check if the user has access to this page or not
            //only nighthawk developer, technical support, admin and client admin can view this page
            if (CurrentSession.UserRoleID == 1 || CurrentSession.UserRoleID == 2 || CurrentSession.UserRoleID == 3 || CurrentSession.UserRoleID == 51)
            {
                Users.ChildItems.Add(editProfile);
                Users.ChildItems.Add(ChangePassword);
                NavigationMenu.Items.Add(Users);

            }
            else
            {
                editProfile.Text = "EDIT PROFILE";
                ChangePassword.Text = "CHANGE PASSWORD";
                NavigationMenu.Items.Add(editProfile);
                NavigationMenu.Items.Add(ChangePassword);

            }





            //user manuals for all users

            //3/9/2012 MArefin Added user manuals

            MenuItem UserGuide;
            UserGuide = CreateMenuItem("USER GUIDE", "/Client/UserGuides.aspx", "User Guide");
            /*
                        MenuItem AppendixSignOn;
                        AppendixSignOn = CreateMenuItem("Appendix A", "/UserGuides/Appendix-CreatingAdditionalSignOn.pdf", "Appendix Creating Additional Sign On's");

                        MenuItem AppendixBatch;
                        AppendixBatch = CreateMenuItem("Appendix B", "/UserGuides/Appendix-SchedulingBatchCommands.pdf", "Scheduling and Batch Commands");

                        MenuItem AppendixAlerts;
                        AppendixAlerts = CreateMenuItem("Appendix C", "/UserGuides/Appendix-SettingUp Alerts-Outages-HighLowVoltages-Tampers.pdf", "Appendix Setting Up Alerts-Outages-High Low Voltages-Tampers");

                        MenuItem AppendixZeroMeter;
                        AppendixZeroMeter = CreateMenuItem("Appendix D", "/UserGuides/Appendix-ZeroingMeters.pdf", "Appendix Zeroing Meters");

                        MenuItem AppendixUnusualMsg;
                        AppendixUnusualMsg = CreateMenuItem("Appendix D", "/UserGuides/Appendix-UnusualMessages.pdf", "Appendix Unusual Messages");

                        UserGuide.ChildItems.Add(AppendixSignOn);
                        UserGuide.ChildItems.Add(AppendixBatch);
                        UserGuide.ChildItems.Add(AppendixAlerts);
                        UserGuide.ChildItems.Add(AppendixZeroMeter);
                        UserGuide.ChildItems.Add(AppendixUnusualMsg);
                        */
            NavigationMenu.Items.Add(UserGuide);
            /*
              

           // * CLIENTS (<= 50)
                //* Add Client (addAccount)
             //   * Search Client (<=50)
               // * Import Devices (<=50)
            //* NIGHTHAWK REPORTS (<=50)
            //* CUSTOMERS (>=50, 1)
               // * Add Customer (with add perm)
               // * Search Customer
              //  * Device Inventory
               // * Search Device Inventory
               // * Recent Devices
               // * GRID POWER FAILURE MAP                
               // * REPORTS
               // * SYSTEM SETUP
                * METER SHOP (53, 1)
                * ADD DEVICE (53,1)	
            //* USERS (<=50, 51)
                //* Add User (AddAccount)
               // * Edit Profile
               // * CHANGE PASSWORD
                     
            */

            //Create Menu for R&D or engineering

            MenuItem Engineering;
            Engineering = CreateMenuItem("ENGINEERING MENU", null, "Engineering Menu");

            MenuItem batchTest;
            batchTest = CreateMenuItem("Batch Test", "/Engineering/BatchTest.aspx", "Batch Test");

            if (CurrentSession.UserRoleID == 5 || CurrentSession.UserRoleID == 1)
            {
                Engineering.ChildItems.Add(batchTest);
                NavigationMenu.Items.Add(Engineering);
            }


            //create super Admin/Developers menu
            if (CurrentSession.UserRoleID == 1)
            {
                MenuItem Developers;
                //Developers = CreateMenuItem("DEVELOPERS MENU", "/Developer/Developers.aspx", "Developers Menu");
                Developers = CreateMenuItem("DEVELOPERS MENU", null, "Developers Menu");

                MenuItem manageNetworks;
                manageNetworks = CreateMenuItem("Manage Networks", "/Developer/ManageNetworks.aspx", "Manage Networks");

                MenuItem manageReports;
                manageReports = CreateMenuItem("Manage Reports", "/Developer/ManageReports.aspx", "Manage Reports");

                MenuItem manageWirelessProvider;
                manageWirelessProvider = CreateMenuItem("Manage Wireless Providers", "/Developer/ManageWirelessProviders.aspx", "Manage Wireless Providers");

                MenuItem manageBatchProcess;
                manageBatchProcess = CreateMenuItem("Manage Batch Process", "/Developer/ManageBatchProcess.aspx", "Manage Batch Process");

                MenuItem manageDeviceControls;
                manageDeviceControls = CreateMenuItem("Manage Device Controls", "/Developer/ManageDeviceControls.aspx", "Manage Device Controls");

                MenuItem addDeviceControl;
                addDeviceControl = CreateMenuItem("Add Control", "/Developer/AddControl.aspx", "Add Device Control");


                MenuItem manageDeviceTypes;
                manageDeviceTypes = CreateMenuItem("Manage Device Types", "/Developer/ManageDeviceTypes.aspx", "Manage Device Types");

                MenuItem addDeviceType;
                addDeviceType = CreateMenuItem("Add Device Types", "/Developer/AddDeviceType.aspx", "Add Device Type");

                MenuItem managePrePay;
                managePrePay = CreateMenuItem("Manage PrePay Providers", "/Developer/ManagePrePay.aspx", "Manage PrePay Providers");

                MenuItem manageApplications;
                manageApplications = CreateMenuItem("Manage Applications", "/Developer/ManageApplications.aspx", "Manage Applications");

                MenuItem manageUserRoles;
                manageUserRoles = CreateMenuItem("Manage User Roles", "/Developer/ManageUserRoles.aspx", "Manage User Roles");

                MenuItem manageAlerts;
                manageAlerts = CreateMenuItem("Manage Alert Notification", "/Developer/ManageAlerts.aspx", "Manage Alert Notification");

                MenuItem moveResellerInventory;
                moveResellerInventory = CreateMenuItem("Move Reseller Inventory", "/Admin/MoveResellerInventory.aspx", "Move Reseller Inventory");

                MenuItem transferInventory;
                transferInventory = CreateMenuItem("Transfer Inventory", "/Admin/TransferInventory.aspx", "Transfer Inventory");

                MenuItem moveBetweendb;
                moveBetweendb = CreateMenuItem("Move Devices Between Databases", "/Admin/MoveDevicesBetweenDatabases.aspx", "Move Devices Between Databases");

                MenuItem firmwareUpgrade;
                firmwareUpgrade = CreateMenuItem("Firmware Upgrade", "/Developer/FirmwareUpgrade.aspx", "Firmware Upgrade");

                manageDeviceControls.ChildItems.Add(addDeviceControl);
                manageDeviceTypes.ChildItems.Add(addDeviceType);
                Developers.ChildItems.Add(manageDeviceControls);
                Developers.ChildItems.Add(manageDeviceTypes);
                Developers.ChildItems.Add(manageNetworks);
                Developers.ChildItems.Add(manageReports);
                Developers.ChildItems.Add(manageWirelessProvider);
                Developers.ChildItems.Add(manageBatchProcess);
                Developers.ChildItems.Add(managePrePay);
                Developers.ChildItems.Add(manageAlerts);
                Developers.ChildItems.Add(manageApplications);
                Developers.ChildItems.Add(manageUserRoles);
                Developers.ChildItems.Add(moveResellerInventory);
                Developers.ChildItems.Add(transferInventory);
                Developers.ChildItems.Add(moveBetweendb);
                Developers.ChildItems.Add(firmwareUpgrade);
                NavigationMenu.Items.Add(Developers);
            }
        }



        MenuItem CreateMenuItem(String itemName, String url, String toolTip)
        {

            // Create a new MenuItem object.
            MenuItem menuItem = new MenuItem();

            // Set the properties of the MenuItem object using
            // the specified parameters.
            menuItem.Text = itemName;
            menuItem.NavigateUrl = url;
            menuItem.ToolTip = toolTip;

            return menuItem;

        }
    }

    public class NHLoginSession
    {

        public string UserID;
        public string UserName;
        public int UserRoleID;
        public string ClientID;
        public string ClientName;
        //public int TzOffset;
        public double TzOffset; /*** Timezone offset vaule could be double not integer. FOr example IST timezone offset is +5.30 which will be 5.5 ***/
        public bool AddAccount;
        public bool EditAccount;
        public bool DeleteAccount;
        public bool BatchProcess;
        //public string LogFile;
        //public string BatchFolder;
        public int ErrorCode;
        public string ErrorMsg;


        public NHLoginSession()
        {
            try
            {
                this.UserID = HttpContext.Current.Session["UserID"].ToString();
                this.UserName = HttpContext.Current.Session["UserName"].ToString();
                this.UserRoleID = Convert.ToInt32(HttpContext.Current.Session["UserRoleID"]);
                this.ClientID = HttpContext.Current.Session["ClientID"].ToString();
                this.ClientName = HttpContext.Current.Session["ClientName"].ToString();

                /*** Timezone offset vaule could be double not integer. FOr example IST timezone offset is +5.30 which will be 5.5 ***/
                this.TzOffset = Convert.ToDouble(HttpContext.Current.Session["TzOffset"].ToString());
               
                this.AddAccount = (bool)HttpContext.Current.Session["AddAccount"];
                this.EditAccount = (bool)HttpContext.Current.Session["EditAccount"];
                this.DeleteAccount = (bool)HttpContext.Current.Session["DeleteAccount"];
                this.BatchProcess = (bool)HttpContext.Current.Session["BatchProcess"];
                //this.LogFile = "C:/Logs/WebConnect2-0" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
                //this.BatchFolder = "C:/BatchFiles" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "");
                this.ErrorCode = 0;
                this.ErrorMsg = "";

            }
            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;

            }


        }

    }


    public class NHAlert
    {
        /// <summary>
        /// NHAlert
        /// Created: 2/27/2012
        /// Author : Moshiul Arefin 
        /// </summary>
        public int AlertID;
        public string AlertName;
        public string AlertDescription;
        public string Command; //select, insert, update, delete record from tbl_NH_Alerts
        public int ErrorCode;
        public string ErrorMsg;

        /// <summary>
        /// Initializes a new instance of NHAlert
        /// Created: 2/27/2012
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        public NHAlert()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 06100199;
                this.ErrorMsg = err.Message;
            }
        }



        public NHAlert(int AlertID)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_Alerts", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = AlertID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = "SELECT";

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {

                            this.AlertID = Convert.ToInt32(myReader["AlertID"]);
                            this.AlertName = myReader["AlertName"].ToString();
                            this.AlertDescription = myReader["AlertDescription"].ToString();

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        this.ErrorCode = 06100201;
                        this.ErrorMsg = "Unable to find Alert information for  AlertID[" + AlertID + "]";

                    }
                    conn.Close();




                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 06100299;
                this.ErrorMsg = err.Message;
            }
        }
    }


    public class NHWirelessProvider
    {
        /// <summary>
        /// NHWirelessProvider 
        /// Created: 8/4/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        public int CarrierID;
        public string Carrier;
        public string MailToSMS;
        public string Command; //select, insert, update, delete smtp information form tbl_NH_WirelessProvider
        public int ErrorCode;
        public string ErrorMsg;

        /// <summary>
        /// Initializes a new instance of NHWirelessProvider
        /// Created: 8/4/2011
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        public NHWirelessProvider()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 06050199;
                this.ErrorMsg = err.Message;
            }
        }



        public NHWirelessProvider(int CarrierID)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_WirelessProviders", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = CarrierID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = "SELECT";

                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {

                            this.CarrierID = Convert.ToInt32(myReader["CarrierID"]);
                            this.Carrier = myReader["Carrier"].ToString();
                            this.MailToSMS = myReader["MailToSMS"].ToString();

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        this.ErrorCode = 01;
                        this.ErrorMsg = "Unable to find wireless carrier with CarrierID[" + CarrierID + "]";

                    }
                    conn.Close();




                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 99;
                this.ErrorMsg = err.Message;
            }
        }
    }


    public class NHPrePayProvider
    {
        public int ProviderID;
        public string ProviderName;
        public string Login;
        public string Password;
        public string WebReferenceURL;
        public string WebReferenceMethod;
        public string Command; //select, insert, update, delete smtp information form tbl_NH_PrePayProvider
        public int ErrorCode;
        public string ErrorMsg;

        public NHPrePayProvider()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 06090199;
                this.ErrorMsg = err.Message;
            }
        }
    }

    public class NHNetwork
    {
        /// <summary>
        /// NHNetwork 
        /// Created: 9/23/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        public int NetworkID;
        public string Description;
        public string URL;
        public string Sender;
        public string SecurityCode;
        public string Protocol;
        public string APNServer;
        public string IPAddress;
        public string PortNumber;
        public string Command; //select, insert, update, delete smtp information form tbl_NH_WirelessProvider
        public int ErrorCode;
        public string ErrorMsg;

        /// <summary>
        /// Initializes a new instance of NHNetwork
        /// Created: 8/4/2011
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        public NHNetwork()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 06060199;
                this.ErrorMsg = err.Message;
            }
        }


        /// <summary>
        /// Initializes a new instance of NHNetwork with the NetworkID
        /// Created: 5/24/2012
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        /// <param name="networkID"></param>
        public NHNetwork(int networkID)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_Networks", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@NetworkID", SqlDbType.Int).Value = networkID;
                    /*
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = Network.Description;
                    command.Parameters.Add("@URL", SqlDbType.VarChar).Value = Network.URL;
                    command.Parameters.Add("@Sender", SqlDbType.VarChar).Value = Network.Sender;
                    command.Parameters.Add("@SecurityCode", SqlDbType.VarChar).Value = Network.SecurityCode;
                    command.Parameters.Add("@Protocol", SqlDbType.VarChar).Value = Network.Protocol;
                    command.Parameters.Add("@APNServer", SqlDbType.VarChar).Value = Network.APNServer;
                    command.Parameters.Add("@IPAddress", SqlDbType.VarChar).Value = Network.IPAddr6ess;
                    command.Parameters.Add("@PortNumber", SqlDbType.VarChar).Value = Network.PortNumber;
                    */
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = "SELECT";



                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {
                            this.Description = myReader["Description"].ToString();
                            this.URL = myReader["URL"].ToString();
                            this.Sender = myReader["Sender"].ToString();
                            this.SecurityCode = myReader["SecurityCode"].ToString();
                            this.Protocol = myReader["Protocol"].ToString();
                            this.APNServer = myReader["APNServer"].ToString();
                            this.IPAddress = myReader["IPAddress"].ToString();
                            this.PortNumber = myReader["PortNumber"].ToString();
                        }

                        this.ErrorCode = 0;
                        this.ErrorMsg = "";
                    }
                    else
                    {
                        this.ErrorCode = 06060291;
                        this.ErrorMsg = "Unable to find Network with NetworkID[" + networkID + "]";
                    }





                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 06060299;
                this.ErrorMsg = err.Message;
            }



        }

    }

    public class NHReport
    {
        /// <summary>
        /// NHReports 
        /// Created: 9/23/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        public int ReportID;
        public string ReportName;
        public string ReportType; //Admin report is for nighthawk internal users, Client reports is for the clients, in future may be we'll have reports for customers (home owners)
        public string ReportDescription;
        public string ReportPath;
        public string ReportServerUrl;
        public string Command; //select, insert, update, delete smtp information form tbl_NH_WirelessProvider
        public int ErrorCode;
        public string ErrorMsg;

        /// <summary>
        /// Initializes a new instance of NHReports
        /// Created: 8/4/2011
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        /// 
        public NHReport()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 06070199;
                this.ErrorMsg = err.Message;
            }
        }
    }



    public class NHBatchProcess
    {
        /// <summary>
        /// NHBatchProcess 
        /// Created: 9/23/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        public int BatchID;
        public string BatchName;
        public string OPCode;
        public string Description;
        public string Command; //select, insert, update, delete available batch commands from tbl_NH_BatchProcess
        public int ErrorCode;
        public string ErrorMsg;

        //MArefin 6/12/2012
        public string BatchType;

        /// <summary>
        /// Initializes a new instance of NHBatchProcess
        /// Created: 9/23/2011
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        /// 
        public NHBatchProcess()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 06080199;
                this.ErrorMsg = err.Message;
            }
        }




        /// <summary>
        ///  Initializes a new instance of NHBatchProcess
        /// Created: 11/16/2011
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        /// <param name="BatchID"></param>
        public NHBatchProcess(int BatchID)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_BatchProcess", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@BatchID", SqlDbType.Int).Value = BatchID;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = "SELECT";
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {

                            this.BatchID = Convert.ToInt32(myReader["BatchID"]);
                            this.BatchName = myReader["BatchName"].ToString();
                            this.OPCode = myReader["OPCode"].ToString();
                            this.Description = myReader["Description"].ToString();
                            //Marefin 6/12/2012
                            this.BatchType = myReader["BatchType"].ToString();

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        this.ErrorCode = 06080201;
                        this.ErrorMsg = "Unable to find any batch process with BatchID[" + BatchID + "]";

                    }
                    conn.Close();



                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 06080299;
                this.ErrorMsg = err.Message;
            }



        }



        /// <summary>
        ///  Initializes a new instance of NHBatchProcess
        /// Created: 5/15/2012
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        /// <param name="parName"></param>
        /// <param name="parVal"></param>
        public NHBatchProcess(string parName, string parVal)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_BatchProcess", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@" + parName, SqlDbType.VarChar).Value = parVal;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = "SELECT";
                    SqlDataReader myReader = null;
                    conn.Open();
                    myReader = command.ExecuteReader();

                    if (myReader.HasRows)
                    {
                        while (myReader.Read())
                        {

                            this.BatchID = Convert.ToInt32(myReader["BatchID"]);
                            this.BatchName = myReader["BatchName"].ToString();
                            this.OPCode = myReader["OPCode"].ToString();
                            this.Description = myReader["Description"].ToString();
                            //Marefin 6/12/2012
                            this.BatchType = myReader["BatchType"].ToString();

                            this.ErrorCode = 0;
                            this.ErrorMsg = "";
                        }
                    }
                    else
                    {
                        this.ErrorCode = 06080301;
                        this.ErrorMsg = "Unable to find any batch process with " + parName + "[" + parVal + "]";

                    }
                    conn.Close();



                }
            }
            catch (Exception err)
            {
                this.ErrorCode = 06080399;
                this.ErrorMsg = err.Message;
            }



        }


    }


    public static class NHSiteGlobal
    {
        /// <summary>
        /// Globalthat is constant.
        /// </summary>        

        public const string pageMsg = "You will need to enter following required (<span style='color: #ff0000'>*</span>) information ";
        public const string PasswordPolicy = "Password must be at least 8 characters. Password must have uppercase, lowercase letter, number and special character.";
        public const int TimeOut = 15;
        public const string ConnectionString = "NHKWCConnectionString";

        public static readonly string LogFile = "C:/Logs/WebConnect2-0" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        //public const string LogFile = "C:/Logs/WebConnect2-0-Dev.log";
        //public const string LogFileTest = "C:/Logs/WebConnect2-0-Test.log";
        //public const string LogFileDemo = "C:/Logs/WebConnect2-0-Demo.log";
        // public const string LogFileProduction = "C:/Logs/WebConnect2-0-Prod.log";
        public static readonly string ConsumerLogFile = "C:/Logs/ConsumerPortal1-0" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        public const string ContactMsg = "Please contact Nighthawk Customer Service at 972.717.5555 between 8am-5pm CST, Monday through Friday.";
        public const int SessionExpiredCode = 3024; //this code can generate from any missing/expired session


        public static readonly string BatchFilesFolder = "C:/BatchFiles" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "");
        //public const string BatchFilesFolder = "C:/BatchFiles";
        //public const string BatchFilesFolderDemo = "C:/BatchFilesDemo";
        // public const string BatchFilesFolderProduction = "C:/BatchFilesProd";

        public static readonly string BatchFileArchiveFolder = "C:/BatchFilesArchive" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "");

        public static readonly string APILogFile = "C:/Logs/ServicesApi" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";

        public static readonly string BatchLogFile = "C:/Logs/BatchProcess" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        public static readonly string BatchLogFileInternal = "C:/Logs/BatchProcessInternal.log";
        //public const string BatchLogFileDemo = "C:/Logs/BatchProcessDemo.log";
        //public const string BatchLogFileProduction = "C:/Logs/BatchProcessProd.log";

        public static readonly string AlertLogFile = "C:/Logs/Alert" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        //public const string AlertLogFile = "C:/Logs/Alert.log";
        //public const string AlertLogFileDemo = "C:/Logs/AlertDemo.log";
        //public const string AlertLogFileProduction = "C:/Logs/AlertProd.log";

        public static readonly string TrilliantLogFile = "C:/Logs/Trilliant" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        public static readonly string RTVerifyLogFile = "C:/Logs/RTVerify" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        //public static readonly string ExportReadingsLogFile = "C:/Logs/ExportReadings" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        public static readonly string ExportReadingsLogFile = "C:/Logs/ExportReadings.log";

        public const string EvntSrvLogFile = "C:/Logs/EventService.log";

        public static readonly string MultiSpeakLogFile = "C:/Logs/MultiSpeak" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        //public const string MultiSpeakLogFileDemo = "C:/Logs/DemoMultiSpeak.log";
        //public const string MultiSpeakLogFileProduction = "C:/Logs/ProdMultiSpeak.log";


        public const string NumerexDevLogFile = "C:/Logs/NServiceDev.log";
        public const string NumerexProdLogFile = "C:/Logs/NumerexProdService.log";

        public const string TestBatchLogFile = "C:/Logs/BatchProcessTest.log";
        public const string TestBatchFilesFolder = "C:/BatchFilesTest";

        public const string ServAlertLogFile = "C:/Logs/ServiceAlert.log";

        public const string TestDevicesLogFile = "C:/Logs/TestDevices.log";


        public const int AlertSleepTimer = 5000; //in miliseconds
        public const int PagingNetworkDelay = 2; //in minutes
        public const int BusyTimer = 10000; //in miliseconds
        public const int DeviceLockTime = 2; //in minutes // we'll have to make this a dynamic options for the customer to decide their lockout option

        public static readonly string ArchivesFolder = "C:/Logs/Archives";
        public static readonly string VerizonLogFile = "C:/Logs/Verizon" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";



        public static readonly string IPServerLogDir = "C:/Logs/IPServer" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "/");


        public static readonly string DBConsistencyLog = "C:/Logs/DBConsistency" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";



        public static readonly string TrilliantMSMQ = "txTrilliantmsmq" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "");

        //Added By Venkatesh
        public static readonly string CPCostCalculationLogFile = "C:/Logs/CPCostcalculation" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        public static readonly string CPThresholdAlertLogFile = "C:/Logs/CPThresholdAlert" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        public static readonly string CPUtilityAlertLogFile = "C:/Logs/CPUtilityAlert" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        public static readonly string CPAlertsLogFile = "C:/Logs/CPAlerts" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
        public static readonly string MVRSFilesFolder = "C:/MVRSFiles" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "");

        //MArefin 7/9/15
        public static readonly string SmtpPagingLog = "C:/Logs/SmtpPaging" + ConfigurationManager.AppSettings["NHKWCConnectionString"].Replace("NHKWCConnectionString", "") + ".log";
    }



    public class NHEmail
    {
        /// <summary>
        /// NHEmail 
        /// Created: 8/4/2011
        /// Author : Moshiul Arefin 
        /// </summary>
        public string EmailAddress;
        public string Subject;
        public string MsgBody;
        public int ErrorCode;
        public string ErrorMsg;
        public string AttachmentFileName;
        public string AttachmentSentName;


        /// <summary>
        /// Initializes a new instance of NHEmail
        /// Created: 8/4/2011
        /// Author: Moshiul Arefin 
        /// Version: 1.0.0.0
        /// </summary>
        public NHEmail()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 06020199;
                this.ErrorMsg = err.Message;
            }
        }

    }


    public class NHError
    {
        /// <summary>
        /// NHError is used to return errorcode, error msg to the calling function
        /// </summary>

        public int ErrorCode;
        public string ErrorMsg;

        public NHError()
        {
            try
            {
                this.ErrorCode = 0;
                this.ErrorMsg = "";
            }
            catch (Exception err)
            {
                this.ErrorCode = 06030199;
                this.ErrorMsg = err.Message;
            }
        }

    }



    public static class NHUtils
    {
        public static StreamWriter LogWriter;

        public static StreamWriter FileWriter;

        //Marefin 6/20/2013
        // used to synchronize access to the file
        private static readonly object _fileLock = new object();

        /// <summary>

        /// Finds a Control recursively. Note finds the first match and exists

        /// </summary>

        /// <param name="ContainerCtl"></param>

        /// <param name="IdToFind"></param>

        /// <returns></returns>

        public static Control FindControlRecursive(Control Root, string Id)
        {

            if (Root.ID == Id)

                return Root;



            foreach (Control Ctl in Root.Controls)
            {

                Control FoundCtl = FindControlRecursive(Ctl, Id);

                if (FoundCtl != null)

                    return FoundCtl;

            }



            return null;

        }

        //this function has been defined in the NHDeviceClass since NHdeviceClass functions are the only ones using this
        //not used
        public static string UTCString()
        {
            DateTime d = DateTime.UtcNow;
            string utcString = (d.Month) + "/";
            utcString += d.Day + "/";
            utcString += d.Year + " ";

            utcString += d.Hour + ":";
            utcString += d.Minute + ":";
            utcString += d.Second;

            return utcString;

        }

        /*

        /// <summary>
        /// WriteLog will be used to write log entries by the webconnect or anyother calling application
        /// Version: 1.0.0.0
        /// Created: 7/20/2011
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="logFile">fully qualified file name, webconnect uses the global const LogFile</param>
        /// <param name="logMessage">message that needs to be written the log file</param>

        public static void WriteLog(string logFile, string logMessage)
        {
            LogWriter = File.AppendText(logFile);

            LogWriter.Write("\r\nLog Entry : ");
            LogWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                DateTime.Now.ToLongDateString());
            LogWriter.WriteLine("  :");
            LogWriter.WriteLine("  :{0}", logMessage);
            LogWriter.WriteLine("-------------------------------");
            // Update the underlying file.
            LogWriter.Flush();

            LogWriter.Close();
        }
        */

        /// <summary>
        /// WriteLog will be used to write log entries by the webconnect or anyother calling application
        /// Version: 1.0.0.0
        /// Created: 7/20/2011
        /// Modified: 6/20/2013
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="logFile">fully qualified file name, webconnect uses the global const LogFile</param>
        /// <param name="logMessage">message that needs to be written the log file</param>

        public static void WriteLog(string logFile, string logMessage)
        {
            //MArefin 6/20/2013
            //For thread safety we need check file lock before starting the write

            lock (_fileLock)
            {
                LogWriter = File.AppendText(logFile);

                LogWriter.Write("\r\nLog Entry : ");
                LogWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                LogWriter.WriteLine("  :");
                LogWriter.WriteLine("  :{0}", logMessage);
                LogWriter.WriteLine("-------------------------------");
                // Update the underlying file.
                LogWriter.Flush();

                LogWriter.Close();

                ArchiveFile(logFile);


            }
        }

        /// <summary>
        /// ArchiveFile will check for file size and archive if necessery
        /// </summary>
        public static void ArchiveFile(string logFile)
        {
            long fileSize = (new FileInfo(logFile).Length) / (1024 * 1024);

            //filesize will be 100 to 150 mb max, using 1mb for testing
            if (fileSize <= 150)
            //if (fileSize <= 1) //for debugging
            {
                return;
            }

            // Get the stream of the source file.
            using (FileStream inFile = File.OpenRead(logFile))
            {
                if (!Directory.Exists(NHSiteGlobal.ArchivesFolder))
                {
                    Directory.CreateDirectory(NHSiteGlobal.ArchivesFolder);
                }
                // Create the compressed file.                
                using (FileStream outFile = File.Create(NHSiteGlobal.ArchivesFolder + "\\" + Path.GetFileNameWithoutExtension(logFile) + "-" + DateTime.Today.ToString("ddMMMyyyy") + ".log.gz"))
                {
                    using (GZipStream Compress =
                        new GZipStream(outFile,
                        CompressionMode.Compress))
                    {
                        // Copy the source file into 
                        // the compression stream.
                        inFile.CopyTo(Compress);

                    }
                }

            }

            File.Delete(logFile);


        }


        /*
        /// <summary>
        /// IsFileLocked will check if the file is already open by another process
        /// Version: 1.0.0.0
        /// Created: 06/20/2013 
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="file">fully qualified file name</param>
        /// <returns></returns>
        protected static bool IsFileLocked(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        */
        public static void WriteToFile(string FilePath, string writeLine)
        {


            FileWriter = File.AppendText(FilePath);

            FileWriter.WriteLine(writeLine);

            FileWriter.Flush();
            FileWriter.Close();


        }

        /// <summary>
        /// SendEmail is used to send email from nighthawk smtp server
        /// Version: 1.0.0.0
        /// Created: 8/4/2011
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="email"></param>
        public static void SendEmail(NHEmail email)
        {
            try
            {
                MailMessage Message = new MailMessage();

                Message.To.Add(email.EmailAddress);
                Message.Subject = email.Subject;
                Message.IsBodyHtml = true;
                Message.Body = email.MsgBody;

                if (email.AttachmentFileName != null)
                {
                    Attachment File = new Attachment(email.AttachmentFileName);
                    File.Name = email.AttachmentSentName;
                    Message.Attachments.Add(File);
                }
                SmtpClient SmtpMail = new SmtpClient("secure.emailsrvr.com");
                SmtpMail.Send(Message);

                email.ErrorCode = 0;

            }

            catch (Exception err)
            {
                email.ErrorCode = 06040499;
                email.ErrorMsg = err.Message;
            }

        }



        /// <summary>
        /// CreateListItem is used to conveniently add list items to any controls
        /// </summary>
        /// <param name="text"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static ListItem CreateListItem(string text, string value)
        {
            ListItem listItem = new ListItem();
            listItem.Text = text;
            listItem.Value = value;

            return listItem;
        }




        public static void PopulateDatabase(DropDownList dropDownListDatabase, NHError error)
        {
            try
            {
                dropDownListDatabase.Items.Add(NHUtils.CreateListItem("Development", "Development"));
                dropDownListDatabase.Items.Add(NHUtils.CreateListItem("Test", "Test"));
                dropDownListDatabase.Items.Add(NHUtils.CreateListItem("Staging", "Staging"));
                dropDownListDatabase.Items.Add(NHUtils.CreateListItem("Demo", "Demo"));
                dropDownListDatabase.Items.Add(NHUtils.CreateListItem("Production", "Production"));
                dropDownListDatabase.Items.Add(NHUtils.CreateListItem("WebSense", "WebSense"));

                //dropDownListDatabase.Items.Add(CreateListItem("Demo", "Demo"));
                //dropDownListDatabase.Items.Add(CreateListItem("Demo", "Demo"));

                error.ErrorCode = 0;
                error.ErrorMsg = "";
            }
            catch (Exception err)
            {
                error.ErrorCode = 06040699;
                error.ErrorMsg = err.Message;
            }
        }



        /// <summary>
        /// ManageWirelessProvider is used to select, insert, update and delete smtp information for wireless carrier that will be used for text notification
        /// Version: 1.0.0.0
        /// Created: 8/25/2011
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="Carrier"></param>
        /// <returns></returns>
        public static DataTable ManageWirelessProviders(NHWirelessProvider Carrier)
        {
            DataTable dt = new DataTable("Carrier");
            Carrier.ErrorCode = 0;
            Carrier.ErrorMsg = "";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_WirelessProviders", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@CarrierID", SqlDbType.Int).Value = Carrier.CarrierID;
                    command.Parameters.Add("@Carrier", SqlDbType.VarChar).Value = Carrier.Carrier;
                    command.Parameters.Add("@MailToSMS", SqlDbType.VarChar).Value = Carrier.MailToSMS;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = Carrier.Command;

                    if (Carrier.Command == "SELECT")
                    {
                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        adapter.Fill(dt);

                        if (dt.Rows.Count <= 0)
                        {
                            Carrier.ErrorCode = 06040791;
                            Carrier.ErrorMsg = "Stored Procedure [sp_NH_Update_WirelessProviders] returned zero record";
                        }

                    }

                    else //insert, update, delete
                    {
                        conn.Open();

                        int rows = command.ExecuteNonQuery();

                        conn.Close();

                        if (rows == 0)
                        {
                            Carrier.ErrorCode = 06040792;
                            Carrier.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_WirelessProviders] failed to update database tables for the CarrierID[" + Carrier.CarrierID + "]";
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception err)
            {
                Carrier.ErrorCode = 06040799;
                Carrier.ErrorMsg = err.Message;
            }

            return dt;

        }


        /// <summary>
        /// ManageNetwork is used to select, insert, update and delete network information for sms or paging or IP services
        /// Version: 1.0.0.0
        /// Created: 9/23/2011
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="Network"></param>
        /// <returns></returns>       
        public static DataTable ManageNetworks(NHNetwork Network)
        {
            DataTable dt = new DataTable("Network");
            Network.ErrorCode = 0;
            Network.ErrorMsg = "";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_Networks", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@NetworkID", SqlDbType.Int).Value = Network.NetworkID;
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = Network.Description;
                    command.Parameters.Add("@URL", SqlDbType.VarChar).Value = Network.URL;
                    command.Parameters.Add("@Sender", SqlDbType.VarChar).Value = Network.Sender;
                    command.Parameters.Add("@SecurityCode", SqlDbType.VarChar).Value = Network.SecurityCode;
                    command.Parameters.Add("@Protocol", SqlDbType.VarChar).Value = Network.Protocol;
                    command.Parameters.Add("@APNServer", SqlDbType.VarChar).Value = Network.APNServer;
                    command.Parameters.Add("@IPAddress", SqlDbType.VarChar).Value = Network.IPAddress;
                    command.Parameters.Add("@PortNumber", SqlDbType.VarChar).Value = Network.PortNumber;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = Network.Command;

                    if (Network.Command == "SELECT")
                    {
                        //to select one network
                        if (Network.NetworkID >= 1)
                        {
                            SqlDataReader myReader = null;
                            conn.Open();
                            myReader = command.ExecuteReader();

                            if (myReader.HasRows)
                            {
                                while (myReader.Read())
                                {
                                    Network.Description = myReader["Description"].ToString();
                                    Network.URL = myReader["URL"].ToString();
                                    Network.Sender = myReader["Sender"].ToString();
                                    Network.SecurityCode = myReader["SecurityCode"].ToString();
                                    Network.Protocol = myReader["Protocol"].ToString();
                                    Network.APNServer = myReader["APNServer"].ToString();
                                    Network.IPAddress = myReader["IPAddress"].ToString();
                                    Network.PortNumber = myReader["PortNumber"].ToString();
                                }
                            }
                        }
                        else
                        {

                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(dt);

                            if (dt.Rows.Count <= 0)
                            {
                                Network.ErrorCode = 06040891;
                                Network.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_Networks] returned zero record";
                            }
                        }

                    }

                    else //insert, update, delete
                    {
                        conn.Open();

                        int rows = command.ExecuteNonQuery();

                        conn.Close();

                        if (rows == 0)
                        {
                            Network.ErrorCode = 06040892;
                            Network.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_Networks] failed to update database tables for the CarrierID[" + Network.NetworkID + "]";
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception err)
            {
                Network.ErrorCode = 06040899;
                Network.ErrorMsg = err.Message;
            }

            return dt;

        }




        /// <summary>
        /// ManageReports is used to select, insert, update and delete Reports
        /// Version: 1.0.0.0
        /// Created: 9/23/2011
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="Reports"></param>
        /// <returns></returns>       
        public static DataTable ManageReports(NHReport Report)
        {
            DataTable dt = new DataTable("Reports");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_Reports", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@ReportID", SqlDbType.Int).Value = Report.ReportID;
                    command.Parameters.Add("@ReportName", SqlDbType.VarChar).Value = Report.ReportName;
                    command.Parameters.Add("@ReportType", SqlDbType.VarChar).Value = Report.ReportType;
                    command.Parameters.Add("@ReportDescription", SqlDbType.VarChar).Value = Report.ReportDescription;
                    command.Parameters.Add("@ReportPath", SqlDbType.VarChar).Value = Report.ReportPath;
                    command.Parameters.Add("@ReportServerUrl", SqlDbType.VarChar).Value = Report.ReportServerUrl;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = Report.Command;

                    if (Report.Command == "SELECT")
                    {
                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        adapter.Fill(dt);

                        dt.PrimaryKey = new DataColumn[] { dt.Columns["ReportID"] };
                        if (dt.Rows.Count <= 0)
                        {
                            Report.ErrorCode = 06040991;
                            Report.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_Reports] returned zero record";
                        }

                    }

                    else //insert, update, delete
                    {
                        conn.Open();

                        int rows = command.ExecuteNonQuery();

                        conn.Close();

                        if (rows == 0)
                        {
                            Report.ErrorCode = 06040992;
                            Report.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_Reports] failed to update database tables for the ReportID[" + Report.ReportID + "]";
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception err)
            {
                Report.ErrorCode = 06040999;
                Report.ErrorMsg = err.Message;
            }

            return dt;

        }


        /// <summary>
        /// ManageBatchProcess is used to select, insert, update and delete Batch Process
        /// Version: 1.0.0.0
        /// Created: 9/23/2011
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="BatchProcess"></param>
        /// <returns></returns>       
        public static DataTable ManageBatchProcess(NHBatchProcess BatchProcess)
        {
            DataTable dt = new DataTable("BatchProcess");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_BatchProcess", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@BatchID", SqlDbType.Int).Value = BatchProcess.BatchID;
                    command.Parameters.Add("@BatchName", SqlDbType.VarChar).Value = BatchProcess.BatchName;
                    command.Parameters.Add("@OPCode", SqlDbType.VarChar).Value = BatchProcess.OPCode;
                    command.Parameters.Add("@Description", SqlDbType.VarChar).Value = BatchProcess.Description;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = BatchProcess.Command;

                    //MArefin 6/12/2012
                    command.Parameters.Add("@BatchType", SqlDbType.VarChar).Value = BatchProcess.BatchType;

                    if (BatchProcess.Command == "SELECT")
                    {
                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        adapter.Fill(dt);

                        if (dt.Rows.Count <= 0)
                        {
                            BatchProcess.ErrorCode = 06041091;
                            BatchProcess.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_BatchProcess] returned zero record";
                        }

                    }

                    else //insert, update, delete
                    {
                        conn.Open();

                        int rows = command.ExecuteNonQuery();

                        conn.Close();

                        if (rows == 0)
                        {
                            BatchProcess.ErrorCode = 06041092;
                            BatchProcess.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_BatchProcess] failed to update database tables for the BatchID[" + BatchProcess.BatchID + "]";
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception err)
            {
                BatchProcess.ErrorCode = 06041099;
                BatchProcess.ErrorMsg = err.Message;
            }

            return dt;

        }







        /// <summary>
        /// ManagePrePayProviders is used to select, insert, update and delete PrePayProviders
        /// Version: 1.0.0.0
        /// Created: 10/06/2011
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="PrePayProvider"></param>
        /// <returns></returns>
        public static DataTable ManagePrePayProviders(NHPrePayProvider PrePayProvider)
        {
            DataTable dt = new DataTable("PrePayProviders");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_PrePayProviders", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@ProviderID", SqlDbType.Int).Value = PrePayProvider.ProviderID;
                    command.Parameters.Add("@ProviderName", SqlDbType.VarChar).Value = PrePayProvider.ProviderName;
                    command.Parameters.Add("@Login", SqlDbType.VarChar).Value = PrePayProvider.Login;
                    command.Parameters.Add("@Password", SqlDbType.VarChar).Value = PrePayProvider.Password;
                    command.Parameters.Add("@WebReferenceURL", SqlDbType.VarChar).Value = PrePayProvider.WebReferenceURL;
                    command.Parameters.Add("@WebReferenceMethod", SqlDbType.VarChar).Value = PrePayProvider.WebReferenceMethod;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = PrePayProvider.Command;

                    if (PrePayProvider.Command == "SELECT")
                    {
                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        adapter.Fill(dt);

                        if (dt.Rows.Count <= 0)
                        {
                            PrePayProvider.ErrorCode = 06041191;
                            PrePayProvider.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_PrePayProviders] returned zero record";
                        }

                    }

                    else //insert, update, delete
                    {
                        conn.Open();

                        int rows = command.ExecuteNonQuery();

                        conn.Close();

                        if (rows == 0)
                        {
                            PrePayProvider.ErrorCode = 06041192;
                            PrePayProvider.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_PrePayProviders] failed to update database tables for the ProviderID[" + PrePayProvider.ProviderID + "]";
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception err)
            {
                PrePayProvider.ErrorCode = 06041199;
                PrePayProvider.ErrorMsg = err.Message;
            }

            return dt;

        }



        /// <summary>
        /// ManageAlert is used to select, insert, update and delete alerts
        /// Version: 1.0.0.0
        /// Created: 2/27/2012
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="Alert"></param>
        /// <returns></returns>
        public static DataTable ManageAlert(NHAlert Alert)
        {
            DataTable dt = new DataTable("Alerts");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_Manage_Alerts", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@AlertID", SqlDbType.Int).Value = Alert.AlertID;
                    command.Parameters.Add("@AlertName", SqlDbType.VarChar).Value = Alert.AlertName;
                    command.Parameters.Add("@AlertDescription", SqlDbType.VarChar).Value = Alert.AlertDescription;
                    command.Parameters.Add("@Command", SqlDbType.VarChar).Value = Alert.Command;

                    if (Alert.Command == "SELECT")
                    {
                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        adapter.Fill(dt);

                        if (dt.Rows.Count <= 0)
                        {
                            Alert.ErrorCode = 06041291;
                            Alert.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_Alerts] returned zero record";
                        }

                    }

                    else //insert, update, delete
                    {
                        conn.Open();

                        int rows = command.ExecuteNonQuery();

                        conn.Close();

                        if (rows == 0)
                        {
                            Alert.ErrorCode = 06041292;
                            Alert.ErrorMsg = "Stored Procedure [sp_NH_Utils_Manage_Alerts] failed to update database tables for the AlertID[" + Alert.AlertID + "]";
                        }

                        conn.Close();
                    }
                }
            }
            catch (Exception err)
            {
                Alert.ErrorCode = 06041299;
                Alert.ErrorMsg = err.Message;
            }

            return dt;

        }







        /// <summary>
        /// ConvertSortDirectionToSql is used by gridview sort functions
        /// Version: 1.0.0.0
        /// Created: 10/06/2011
        /// Author: Moshiul Arefin
        /// </summary>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public static string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }



        public static DataTable GetStates(NHError Error)
        {
            DataTable dt = new DataTable("States");

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["NHKWCConnectionString"]].ToString()))
                {
                    SqlCommand command = new SqlCommand("sp_NH_Utils_GetStates", conn);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);

                    if (dt.Rows.Count <= 0)
                    {
                        Error.ErrorCode = 91;
                        Error.ErrorMsg = "Stored Procedure [sp_NH_Utils_GetStates] returned zero record";
                    }
                }
            }
            catch (Exception err)
            {
                Error.ErrorCode = 06041199;
                Error.ErrorMsg = err.Message;
            }

            return dt;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="test"></param>
        /// <returns></returns>
        public static bool IsHexString(string hexString)
        {
            // For C-style hex notation (0xFF) you can use @"\A\b(0[xX])?[0-9a-fA-F]+\b\Z"
            return System.Text.RegularExpressions.Regex.IsMatch(hexString, @"\A\b[0-9a-fA-F]+\b\Z");
        }

        public static byte[] ToByteArray(String HexString)
        {
            int NumberChars = HexString.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(HexString.Substring(i, 2), 16);
            }
            return bytes;
        }


        public static byte ToByte(String HexString)
        {
            byte singleByte = new byte();
            singleByte = Convert.ToByte(HexString.Substring(0, 2), 16);
            return singleByte;
        }
    }


}
