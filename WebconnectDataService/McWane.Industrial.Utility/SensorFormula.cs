﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Data;
using System.Data.OleDb;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using McWane.Industrial.DAL;

namespace McWane.Industrial.Utility
{
    public class SensorFormula
    {

        SensorDAL dal = new SensorDAL();

        /// <summary>
        /// GetReverseFormula
        /// Get Reverse Formula by DeviceID and PortNumber             
        /// Created: 12/1/2015
        /// Author: Thinesh
        /// Updated: 12/1/2015 Thinesh       
        /// </summary>
        /// <param name="DeviceID"></param>
        /// /// <param name="PortNumber"></param>
        public string GetReverseFormula(int Deviceid, int Portnumber)
        {           
           string formula = dal.GetFormula(Deviceid, Portnumber);
           return formula;
        }

        //Method is used to get parameter from given formula
        public string GETParameter(string formula)
        {
            char[] replaceChars = { '+', '-', '%', '/', '.', '(', ')', '&', '<', '>', '{', '}', '[', ']', ':', ';', '$', '*', ',', '^' };
            string[] replacestring = { "Abs", "Acos", "Asin", "Atan", "Pow", "Ceiling", "Cos", "Exp", "Floor", "IEEERemainder", "Log", "Log10", "Round", "Sign", "Sin", "Sqrt", "Tan", "Truncate", "Max", "Min", "if", "in" };
            string parameter = ParameterReplace(formula, replaceChars);
            parameter = ParameterReplace(parameter, replacestring);

            string[] splitParameter = parameter.Split(' ');
            string FirstParamValue = string.Empty;
            bool blnEqual = false;

            if (splitParameter.Length > 1)
            {
                FirstParamValue = splitParameter[0].ToString().Trim();

                for (int paramRow = 0; paramRow < splitParameter.Length; paramRow++)
                {
                    if (paramRow != 0)
                    {
                        if (FirstParamValue == splitParameter[paramRow].ToString().Trim())
                            blnEqual = true;
                        else
                        {
                            blnEqual = false;
                            break;
                        }
                    }
                }

                //if (blnEqual == false)
                //{
                //    NHUtils.WriteLog(strLogfilePath, "Formula contains more than one parameter. Formula ID: " + FormulaIDs.ToString());
                //}
                //else
                //    parameter = FirstParamValue;
            }
            return parameter;
        }

        private static string ParameterReplace(string strFormula, char[] replaceChars)
        {
            //Replacing digits and math operators to get the parameter name
            var parameter = Regex.Replace(strFormula, @"\d", "");
            foreach (char split in replaceChars)
            {
                parameter = parameter.Replace(split, ' ');
            }

            //parameter = parameter.Substring(0, parameter.Length / 2).Trim();//Thinesh
            parameter = parameter.Trim();
            return parameter;
        }

        private static string ParameterReplace(string strFormula, string[] replaceChars)
        {
            //Replacing digits and math operators to get the parameter name
            var parameter = Regex.Replace(strFormula, @"\d", "");
            foreach (string split in replaceChars)
            {
                parameter = parameter.Replace(split, " ");
            }

            if (parameter.Trim().Contains(' '))
                parameter = parameter.Substring(0, parameter.Trim().Length / 2).Trim();
            return parameter;
        }
        
        //Analog to Digital conversion 
        public static double ADCToSensorValueByDeviceType(int adcvalue, int devicetype, int sensorport)
        {
            double Value = 0.0;
            if(adcvalue != 0)
            {

            if (sensorport == 1)
            {
                if (devicetype == 66)
                {
                    Value = ADCHumidity(adcvalue);
                }
                else
                {
                    Value = ADCVibration(adcvalue);
                }
            }

            if (sensorport == 2)
            {
                Value = ADCTemperature(adcvalue);
            }

            }

            return Math.Round(Value, 2);
        }


        //Convert ADC to ips for Vibration sensor
        public static double ADCVibration(int adc)
        {
            try
            {
                double ips = (adc - 200) / 799.0;

                return ips;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Convert ADC to Farenhit for Temperature sensor
        public static double ADCTemperature(int adc)
        {
            try
            {
                double v = (adc * 2.048) / 1023.0;

                double r = (330.0 / v) - 100.0;
                r -= 6;

                double tempC = (0.0011 * Math.Pow(r, 2)) + (2.3382 * r) - 244.69;

                double tempF = (tempC * 9.0 / 5.0) + 32.0;

                return tempF;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Convert ADC to percent for Humidity sensor
        public static double ADCHumidity(int adc)
        {
            try
            {
                double percent = (0.1251 * adc) - 24.99;

                return percent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Convert Epoch time format to human readable
        public static string EpochToString(long epoch)
        {
            DateTime datetime = new DateTime(1970, 1, 1, 0, 0, 0);
            datetime = datetime.AddSeconds(epoch);
            string strDate = datetime.ToString("yyyy-MM-dd") + " " + datetime.ToString("HH:mm:ss");
            return strDate;
        }

        //Convert unix time format to datetime
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        //Convert datetime to unix time format
        public static long ToUnixTimestamp(DateTime target)
        {
            var date = new DateTime(1970, 1, 1, 0, 0, 0, target.Kind);
            var unixTimestamp = System.Convert.ToInt64((target - date).TotalSeconds);
            return unixTimestamp;
        }

        //Convert unix time format to UTCdatetime
        public static DateTime UnixTimeStampToUTCDateTime(double unixTimeStamp)
        {
            System.DateTime dtUTCDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtUTCDateTime = dtUTCDateTime.AddSeconds(unixTimeStamp).ToUniversalTime();
            return dtUTCDateTime;
        }
        //protected string ADCTemperature(int? adc, int offset)
        //{
        //    if (adc == 1 || adc == 9999)
        //    {
        //        return "disabled";
        //    }
        //    /*         
        //    ADC Voltage   V = (ADC Reading)*2.048/1023

        //    Sensor Resistance  R = (330 / V) - 100
        //                 * * offset -6 ohms from R (from Kelly Hawkings 12/8/2014

        //    Temperature (in deg C)  Temp = (0.0011*R^2) + (2.3382*R) – 244.69
        //       */
        //    double v = ((int)adc * 2.048) / 1023.0;
        //    double r = (330.0 / v) - 100.0;
        //    r -= offset;
        //    double tempC = (0.0011 * Math.Pow(r, 2)) + (2.3382 * r) - 244.69;
        //    double tempF = (tempC * 9.0 / 5.0) + 32.0;
        //    return String.Format("{0:0}", tempF) + "&deg; F";
        //}

        //Convert sensor Temperature format to ADC value
        public static int TemperatureADC(string tempF, int offset)
        {
            //C = (F-32) * 5/9
            double tempC = (((Convert.ToDouble(tempF)) - 32) * 5) / 9.0;
            /*         
            ADC Voltage   V = (ADC Reading)*2.048/1023

            Sensor Resistance  R = (330 / V) - 100

                         * offset -6 ohms from R (from Kelly Hawkings 12/8/2014
            Temperature (in deg C)  Temp = (0.0011*R^2) + (2.3382*R) – 244.69                        
                */
            double a = 0.0011;
            double b = 2.3382;
            double c = -244.69 - tempC;
            double r = SolveQuadratic(a, b, c);
            r += offset;
            double v = 330 / (r + 100.0);
            double adc = v * 1023 / 2.048;
            return ((int)adc);
        }

        //Convert humidity percent value into ADC value
        public static int HumidityADC(string percent)
        {
            //%Rh = (0.1251*ADC Reading) - 24.99
            //double adc = Convert.ToDouble(percent) * 0.1251 + 24.99;
            //MArefin 6/18/15 fixing the formula
            double adc = (Convert.ToDouble(percent) + 24.99) / 0.1251;
            return ((int)adc);
        }
        
        public static double SolveQuadratic(double a, double b, double c)
        {
            double sqrtpart = b * b - 4 * a * c;
            double x, x1, x2, img;
            if (sqrtpart > 0)
            {
                x1 = (-b + System.Math.Sqrt(sqrtpart)) / (2 * a);
                if (x1 > 0) return x1;

                x2 = (-b - System.Math.Sqrt(sqrtpart)) / (2 * a);
                if (x2 > 0) return x2;

                //Console.WriteLine("Two Real Solutions: {0,8:f4} or  {1,8:f4}", x1, x2);
            }
            else if (sqrtpart < 0)
            {
                //do nothing for imaginary soltuions
                /*
                sqrtpart = -sqrtpart;
                x = -b / (2 * a);
                img = System.Math.Sqrt(sqrtpart) / (2 * a);
                */
                //Console.WriteLine("Two Imaginary Solutions: {0,8:f4} + {1,8:f4} i or {2,8:f4} + {3,8:f4} i", x, img, x, img);
            }
            else
            {
                x = (-b + System.Math.Sqrt(sqrtpart)) / (2 * a);
                if (x > 0) return x;
                //Console.WriteLine("One Real Solution: {0,8:f4}", x);
            }
            return -1; //for error value

            //return only positive soltuion
        }
    }
}
